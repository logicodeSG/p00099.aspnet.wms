﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="viewPurchaseOrderModal.ascx.cs" Inherits="wmsGoldbell.subModule.purchaseOrder.viewPurchaseOrderModal" %>
<style>
    #divViewPOModalId .form-group {
        margin-bottom: 0px;
    }


</style>

<div class="modal fade" id="divViewPOModalId" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90vw;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Purchase Order</h4>
            </div>
            <div class="modal-footer" id="appPOX" style="text-align:left; padding-top:5px;">

                <div class="row" style="padding-bottom: 4px;">
                    <div id="divViewPONumber" class="form-group col-md-3 " >
                        <asp:HiddenField ID="hfViewPOId" runat="server" ClientIDMode="Static" />
                        <label id="lblViewPONumber" class="control-label" for="inputError">PO#:</label>
                        <input type="text" maxlength="50" class="form-control input-sm" id="tbViewPONumber" disabled="disabled"/>
                    </div>
                    <div id="divViewPODate" class="form-group col-md-3">
                        <label id="lblViewPODate" class="control-label lb-md" for="inputError">Date:</label>
                        <input type="text" maxlength="50" class="form-control input-sm" id="tbViewPODate" disabled="disabled"/>
                    </div>
                </div>


                <div class="row" id="appViewPO">
                    <div class="col-md-12" >
                        <table class="dynamicTable" style="width: 100%;">
                            <tr>
                                <td style="padding-bottom:0px;">
                                    <table class="table" style="margin-bottom: 0px;">
                                        <tr>
                                            <th style="width: 5%;"><strong>#</strong></th>
                                            <td style="width: 20%;"><strong>Item</strong></td>
                                            <td style="width: 40%;"><strong>Description</strong></td>
                                            <td style="width: 8%;"><strong>UOM</strong></td>
                                            <%--<td style="width: 8%;"><strong>Price</strong></td>--%>
                                            <td style="width: 8%;"><strong>Qty</strong></td>
                                            <td style="width: 8%;"><strong>Received</strong></td>
                                            <td style="width: 8%;"></td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="height:70vh; overflow-y:auto;">
                                        <table class="table table-hover">
                                            <tbody >
                                                <tr v-for="row in rows">
                                                    <td style="vertical-align:middle; width: 5%;">{{$index+1}}</td>
                                                    <td  style="width:20%;">
                                                        <div id="divViewPOItemCode{{$index+1}}">
                                                            <input type="text" id="tbViewPOItemCode{{$index+1}}" class="form-control input-sm" v-model="row.item_code" disabled="disabled"/>
                                                        </div>
                                                        <input type="hidden" id="hfViewItemId{{$index+1}}" v-model="row.item_Id"/>
                                                    </td>
                                                    <td  style="width: 40%;">
                                                        <input type="text" id="tbViewPOItemDescription{{$index+1}}" class="form-control input-sm" v-model="row.description" disabled="disabled"/>
                                                    </td>
                                                    <td  style="width: 8%;">
                                                        <input type="text" id="tbViewPOItemUOM{{$index+1}}" class="form-control input-sm" v-model="row.uom" disabled="disabled"/>
                                                    </td>
                                                    <%--<td  style="width: 8%;">
                                                        <div id="divViewPOItemPrice{{$index+1}}">
                                                            <input type="text" id="tbViewPOItemPrice{{$index+1}}" class="form-control input-sm" v-model="row.price | currency" 
                                                                onclick="onCurrencySelect(this.id)"  disabled="disabled"/>
                                                        </div>
                                                    </td>--%>
                                                    <td  style="width: 8%;">
                                                        <div id="divViewPOQuantity{{$index+1}}">
                                                            <input id="tbViewPOQuantity{{$index+1}}" type="text" maxlength="4" class="form-control input-sm" v-model="row.quantity" 
                                                                   onclick="this.select()"  disabled="disabled"
                                                                   onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57"  />
                                                        </div>
                                                    </td>
                                                    <td  style="width: 8%;">
                                                        <input type="text" id="tbViewPOReceivedQuantity{{$index+1}}" class="form-control input-sm" v-model="row.received_quantity" disabled="disabled"/>
                                                    </td>
                                                    <td style="vertical-align:middle; padding-bottom:0; padding-top:0;  width: 8%;">
                                                        <%--<button id="btnPODelete{{$index+1}}" class="btn btn-danger btn-xs" @click="removeRow(row)" disabled="disabled"><i class="fa fa-close"></i> </button>
                                                        <button type="button" style="display:none;" class="btn btn-warning btn-xs" id="btnPOScanItemSN{{$index+1}}" onclick="setViewScanItemSNWLocationCodeModalInfo({{$index+1}})"  
                                                            data-toggle="modal" data-target="#divViewScanItemSNWLocationModalId">
                                                            <i class="fa fa-list-ul "></i></button>
                                                        <span id="poScannViewemBadge{{$index+1}}" style="display:none;" class="badge badge-error">0</span>
                                                        <input type="hidden" id="hfPOScanItemSNList{{$index+1}}" v-model="row.snList"/> --%>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>

<%--                            <tr>
                                <td>
                                    <table class="table tableTotalDisplay">
                                        <tr>
                                            <td style="width: 83vw;" class="text-right">TOTAL:</td>
                                            <td style="width: 15vw;" class="text-right">{{ total | currency }}</td>
                                            <td class="hidden">{{currencySymbol}}</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 83vw;" class="text-right">GST ({{gst == 0 ? '-' : gst}}%):</td>
                                            <td style="display:none;">{{gst}}</td>
                                            <td style="width: 15vw;"  class="text-right">{{  (total * (gst /100)) | currency }}</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 83vw;" class="text-right"><strong>GRAND TOTAL:</strong></td>
                                            <td style="width: 15vw;" class="text-right"><strong>{{ grandTotal = (total * (gst /100)) + total | currency }}</strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>--%>

                            <%--<tr>
                                <td>
                                    <div class="row" style="padding-top:10px;">
                                        <div class="col-md-offset-3 col-md-9">
                                            <div class="col-md-4">
                                                <button id="btnViewPOSave" type="button" class="btn btn-primary btn-block" disabled="disabled">Save</button>
                                            </div>
                                            <div class="col-md-4">
                                                <button id="btnViewPOPrint" type="button" class="btn btn-primary btn-block" disabled="disabled">Print</button>
                                            </div>
                                            <div class="col-md-4">
                                            <button id="btnViewPOCancelReopen" type="button" class="btn btn-warning btn-block" disabled="disabled">Cancel Order</button>
                                        </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>--%>

                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<%--<rsweb:ReportViewer ID="ViewPOReportViewer" runat="server" Height="80%" Width="100%" Style="display:none"></rsweb:ReportViewer>--%>


<script src="js/purchaseOrder/viewPurchaseOrderModalJS.js"></script>