﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="viewSalesOrderModal.ascx.cs" Inherits="wmsGoldbell.subModule.salesOrder.viewSalesOrderModal" %>
<style>
    #divViewSOModalId .form-group {
        margin-bottom: 0px;
    }

</style>

<div class="modal fade" id="divViewSOModalId" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90vw;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Sales Order</h4>
            </div>
            <div class="modal-footer" id="appViewSOX" style="text-align:left; padding-top:5px;">
                <div id="appViewSO">
                    <div class="row">
                        <div id="divViewSOSalesId" class="form-group col-md-3 " >
                            <input type="hidden" id="hfViewAXSOId" v-model="soId" />
                            <label id="lblViewSOSalesID" class="control-label" for="inputError">Sales ID:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewSOSalesId" disabled="disabled"/>
                        </div>
                        <div id="divViewSORouteID" class="form-group col-md-3 " >
                            <label id="lblViewSORouteID" class="control-label" for="inputError">Route ID:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewSORouteId" disabled="disabled"/>
                        </div>
                        <div id="divViewSOCreatedDate" class="form-group col-md-3">
                            <label id="lblViewSOCreatedDate" class="control-label lb-md" for="inputError">Created Date:</label>
                            <div class="input-group date" data-provide="datepicker" id="divViewSOCreatedDatePicker">
                                <input type="text" class="form-control input-sm" id="tbViewSOCreatedDate" readonly="readonly" disabled="disabled">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="row" >
                    </div>

                    <div class="row">
                        <div class="col-md-12" >
                            <table class="dynamicTable" style="width: 100%;">
                                <tr>
                                    <td>
                                        <table class="table" style="margin-bottom: 0px;">
                                            <tr>
                                                <th style="width: 4%;"><strong>#</strong></th>
                                                <th style="width: 2%; display:none;"><input type="checkbox" id="cbAll" onclick="cbAllOnClick()"></th>
                                                <td style="width: 5%;"><strong>Line#</strong></td>
                                                <td style="width: 18%;"><strong>Item Id</strong></td>
                                                <td style="width: 18%;"><strong>Part Code</strong></td>
                                                <td style="width: 8%;"><strong>UOM</strong></td>
                                                <td style="width: 8%;"><strong>Qty</strong></td>
                                                <td style="width: 8%;"><strong>Scan Qty</strong></td>
                                                <td style="width: 15%;"><strong>Scan DT</strong></td>
                                                <td style="width: 8%;"><strong>User</strong></td>
                                                <td style="width: 3%;"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="height:50vh; overflow-y:auto;">
                                            <table class="table table-hover">
                                                <tbody >
                                                    <tr v-for="row in rows" >
                                                        <td style="vertical-align:middle; width: 4%;">{{$index+1}}</td>
                                                        <td style="vertical-align:middle; width: 2%; display:none;">
                                                            <input type="checkbox" id="cbPrint{{$index+1}}" value="{{$index+1}}" <%--v-model="cb_print"--%> @click="cbPrintCheck($event,row)">
                                                        </td>
                                                        <td  style="width:5%;">
                                                            <div id="divViewSOLineNum{{$index+1}}">
                                                                <input type="text" id="tbViewSOLineNum{{$index+1}}" class="form-control input-sm" v-model="row.line_num" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td  style="width:18%;">
                                                            <div id="divViewSOItemId{{$index+1}}">
                                                                <input type="text" id="tbViewSOItemId{{$index+1}}" class="form-control input-sm" v-model="row.item_id" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td  style="width: 18%;">
                                                            <input type="text" id="tbViewSOPartCode{{$index+1}}" class="form-control input-sm" v-model="row.part_code" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewSOUOM{{$index+1}}" class="form-control input-sm" v-model="row.uom" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewSOQty{{$index+1}}" class="form-control input-sm" v-model="row.qty" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewSOScanQty{{$index+1}}" class="form-control input-sm" v-model="row.scan_qty" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 15%;">
                                                            <input type="text" id="tbViewSOScanDatetime{{$index+1}}" class="form-control input-sm" v-model="row.scan_datetime" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewSOUser{{$index+1}}" class="form-control input-sm" v-model="row.user" disabled="disabled"/>
                                                        </td>
                                                        <td style="vertical-align:middle; padding-bottom:0; padding-top:0;  width: 3%;">
                                                            <%--<button type="button" style="display:none;" class="btn btn-warning btn-xs" id="btnViewSOScanItemSN{{$index+1}}" onclick="setViewScanItemSNModalInfo({{$index+1}})"  
                                                                data-toggle="modal" data-target="#divViewScanItemSNModalId" >
                                                                <i class="fa fa-list-ul "></i>
                                                            </button>
                                                            <span id="viewSOScannedItemBadge{{$index+1}}" style="display:none;" class="badge badge-error">0</span>
                                                            <input type="hidden" id="hfViewSOScanItemSNList{{$index+1}}" v-model="row.snList"/> --%>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="table tableTotalDisplay hidden">
                                            <tr>
                                                <td style="width: 83vw;" class="text-right"><strong>ITEM TO PRINT:</strong></td>
                                                <td style="width: 15vw;" class="text-right"><strong>{{ total2Print }}</strong></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row hidden" style="padding-top:10px;">
                                            <div class="col-md-offset-9 col-md-3">
                                                <div class="col-md-12">
                                                    <button id="btnSOPrint" type="button" class="btn btn-warning btn-block">Print</button>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="js/salesOrder/viewSalesOrderModalJs.js"></script>