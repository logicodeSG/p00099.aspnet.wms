﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="viewAJGoodsReceiveModal.ascx.cs" Inherits="wmsGoldbell.subModule.ajGoodsReceive.viewAJGoodsReceiveModal" %>
<style>
    #divViewAJGRModalId .form-group {
        margin-bottom: 0px;
    }

</style>

<div class="modal fade" id="divViewAJGRModalId" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90vw;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Goods Receive (AJ)</h4>
            </div>
            <div class="modal-footer" id="appViewAJX" style="text-align:left; padding-top:5px;">
                <div id="appViewAJGR">
                    <div class="row">
                        <div id="divViewAJGRJournalId" class="form-group col-md-3 " >
                            <input type="hidden" id="hfViewAXAJId" v-model="ajId" />
                            <label id="lblViewAJGRJournalId" class="control-label" for="inputError">Journal ID:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewAJGRJournalId" disabled="disabled"/>
                        </div>
                        <div id="divViewAJGRCreatedDate" class="form-group col-md-3">
                            <label id="lblViewAJGRCreatedDate" class="control-label lb-md" for="inputError">Created Date:</label>
                            <div class="input-group date" data-provide="datepicker" id="divViewAJGRCreatedDatePicker">
                                <input type="text" class="form-control input-sm" id="tbViewAJGRCreatedDate" readonly="readonly" disabled="disabled">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                        <div id="divViewAJGRPackingSlip" class="form-group col-md-3 " >
                            <label id="lblViewAJGRPackingSlip" class="control-label" for="inputError">Packing Slip:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewAJGRPackingSlip" disabled="disabled"/>
                        </div>
                    </div>
                
                    <div class="row" >
                    </div>

                    <div class="row">
                        <div class="col-md-12" >
                            <table class="dynamicTable" style="width: 100%;">
                                <tr>
                                    <td>
                                        <table class="table" style="margin-bottom: 0px;">
                                            <tr>
                                                <th style="width: 4%;"><strong>#</strong></th>
                                                <th style="width: 2%; display:none;"><input type="checkbox" id="cbAll" onclick="cbAllOnClick()"></th>
                                                <td style="width: 5%;"><strong>Line#</strong></td>
                                                <td style="width: 10%;"><strong>PURCH ID</strong></td>
                                                <td style="width: 17%;"><strong>Item Id</strong></td>
                                                <td style="width: 17%;"><strong>Part Code</strong></td>
                                                <%--<td style="width: 8%;"><strong>UOM</strong></td>--%>
                                                <td style="width: 8%;"><strong>Qty</strong></td>
                                                <td style="width: 8%;"><strong>Scan Qty</strong></td>
                                                <td style="width: 15%;"><strong>Scan DT</strong></td>
                                                <td style="width: 8%;"><strong>User</strong></td>
                                                <td style="width: 3%;"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="height:50vh; overflow-y:auto;">
                                            <table class="table table-hover">
                                                <tbody >
                                                    <tr v-for="row in rows" >
                                                        <td style="vertical-align:middle; width: 4%;">{{$index+1}}</td>
                                                        <td style="vertical-align:middle; width: 2%; display:none;">
                                                            <input type="checkbox" id="cbPrint{{$index+1}}" value="{{$index+1}}" <%--v-model="cb_print"--%> @click="cbPrintCheck($event,row)">
                                                        </td>
                                                        <td  style="width:5%;">
                                                            <div id="divViewAJGRLineNum{{$index+1}}">
                                                                <input type="text" id="tbViewAJGRLineNum{{$index+1}}" class="form-control input-sm" v-model="row.line_num" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td  style="width: 10%;">
                                                            <input type="text" id="tbViewAJGRPurchId{{$index+1}}" class="form-control input-sm" v-model="row.purch_id" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width:17%;">
                                                            <div id="divViewAJGRItemId{{$index+1}}">
                                                                <input type="text" id="tbViewAJGRItemId{{$index+1}}" class="form-control input-sm" v-model="row.item_id" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td  style="width: 17%;">
                                                            <input type="text" id="tbViewAJGRPartCode{{$index+1}}" class="form-control input-sm" v-model="row.part_code" disabled="disabled"/>
                                                        </td>
                                                        <%--<td  style="width: 8%;">
                                                            <input type="text" id="tbViewAJGRUOM{{$index+1}}" class="form-control input-sm" v-model="row.uom" disabled="disabled"/>
                                                        </td>--%>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewAJGRQty{{$index+1}}" class="form-control input-sm" v-model="row.qty" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewAJGRScanQty{{$index+1}}" class="form-control input-sm" v-model="row.scan_qty" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 15%;">
                                                            <input type="text" id="tbViewAJGRScanDatetime{{$index+1}}" class="form-control input-sm" v-model="row.scan_datetime" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewAJGRUser{{$index+1}}" class="form-control input-sm" v-model="row.user" disabled="disabled"/>
                                                        </td>
                                                        <td style="vertical-align:middle; padding-bottom:0; padding-top:0;  width: 3%;">
                                                            <%--<button type="button" style="display:none;" class="btn btn-warning btn-xs" id="btnViewAJGRScanItemSN{{$index+1}}" onclick="setViewScanItemSNModalInfo({{$index+1}})"  
                                                                data-toggle="modal" data-target="#divViewScanItemSNModalId" >
                                                                <i class="fa fa-list-ul "></i>
                                                            </button>
                                                            <span id="viewAJGRScannedItemBadge{{$index+1}}" style="display:none;" class="badge badge-error">0</span>
                                                            <input type="hidden" id="hfViewAJGRScanItemSNList{{$index+1}}" v-model="row.snList"/> --%>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="table tableTotalDisplay hidden">
                                            <tr>
                                                <td style="width: 83vw;" class="text-right"><strong>ITEM TO PRINT:</strong></td>
                                                <td style="width: 15vw;" class="text-right"><strong>{{ total2Print }}</strong></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row hidden" style="padding-top:10px;">
                                            <div class="col-md-offset-9 col-md-3">
                                                <div class="col-md-12">
                                                    <button id="btnAJGRPrint" type="button" class="btn btn-warning btn-block">Print</button>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="js/ajGoodsReceive/viewAJGoodsReceiveModalJs.js"></script>