﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="viewPOGoodsReceiveModal.ascx.cs" Inherits="wmsGoldbell.subModule.poGoodsReceive.viewPOGoodsReceiveModal" %>
<style>
    #divViewPOGRModalId .form-group {
        margin-bottom: 0px;
    }

</style>

<div class="modal fade" id="divViewPOGRModalId" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90vw;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Goods Receive (PO)</h4>
            </div>
            <div class="modal-footer" id="appViewPOX" style="text-align:left; padding-top:5px;">
                <div id="appViewPOGR">
                    <div class="row">
                        <div id="divViewPOGRPurchId" class="form-group col-md-3 " >
                            <input type="hidden" id="hfViewAXPOId" v-model="poId" />
                            <label id="lblViewPOGRPurchId" class="control-label" for="inputError">Purch ID:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewPOGRPurchId" disabled="disabled"/>
                        </div>
                        <div id="divViewPOGRCompanyCode" class="form-group col-md-3 " >
                            <label id="lblViewPOGRCompanyCode" class="control-label" for="inputError">Company Code:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewPOGRCompanyCode" disabled="disabled"/>
                        </div>
                        <div id="divViewPOGRVendorCode" class="form-group col-md-3 " >
                            <label id="lblViewPOGRVendorCode" class="control-label" for="inputError">Vendor Code:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewPOGRVendorCode" disabled="disabled"/>
                        </div>
                        <div id="divViewPOGRCreatedDate" class="form-group col-md-3">
                            <label id="lblViewPOGRCreatedDate" class="control-label lb-md" for="inputError">Created Date:</label>
                            <div class="input-group date" data-provide="datepicker" id="divViewPOGRCreatedDatePicker">
                                <input type="text" class="form-control input-sm" id="tbViewPOGRCreatedDate" readonly="readonly" disabled="disabled">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="row" >
                        <div id="divViewPOGRNumber" class="form-group col-md-3 " >
                            <label id="lblViewPOGRNumber" class="control-label" for="inputError">GR#:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewPOGRNumber" disabled="disabled"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" >
                            <table class="dynamicTable" style="width: 100%;">
                                <tr>
                                    <td>
                                        <table class="table" style="margin-bottom: 0px;">
                                            <tr>
                                                <th style="width: 4%;"><strong>#</strong></th>
                                                <th style="width: 2%; display:none;"><input type="checkbox" id="cbAll" onclick="cbAllOnClick()"></th>
                                                <td style="width: 5%;"><strong>Line#</strong></td>
                                                <td style="width: 5%;"><strong>Company Code</strong></td>
                                                <td style="width: 18%;"><strong>Item Id</strong></td>
                                                <td style="width: 18%;"><strong>Part Code</strong></td>
                                                <td style="width: 8%;"><strong>UOM</strong></td>
                                                <td style="width: 8%;"><strong>Qty</strong></td>
                                                <td style="width: 8%;"><strong>Scan Qty</strong></td>
                                                <td style="width: 10%;"><strong>Scan DT</strong></td>
                                                <td style="width: 8%;"><strong>User</strong></td>
                                                <td style="width: 3%;"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="height:50vh; overflow-y:auto;">
                                            <table class="table table-hover">
                                                <tbody >
                                                    <tr v-for="row in rows" >
                                                        <td style="vertical-align:middle; width: 4%;">{{$index+1}}</td>
                                                        <td style="vertical-align:middle; width: 2%; display:none;">
                                                            <input type="checkbox" id="cbPrint{{$index+1}}" value="{{$index+1}}" <%--v-model="cb_print"--%> @click="cbPrintCheck($event,row)">
                                                        </td>
                                                        <td  style="width:5%;">
                                                            <div id="divViewPOGRLineNum{{$index+1}}">
                                                                <input type="text" id="tbViewPOGRLineNum{{$index+1}}" class="form-control input-sm" v-model="row.line_num" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td  style="width:5%;">
                                                            <div id="divViewPOGRCompanyCode{{$index+1}}">
                                                                <input type="text" id="tbViewPOGRCompanyCode{{$index+1}}" class="form-control input-sm" v-model="row.company_code" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td  style="width:18%;">
                                                            <div id="divViewPOGRItemId{{$index+1}}">
                                                                <input type="text" id="tbViewPOGRItemId{{$index+1}}" class="form-control input-sm" v-model="row.item_id" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td  style="width: 18%;">
                                                            <input type="text" id="tbViewPOGRPartCode{{$index+1}}" class="form-control input-sm" v-model="row.part_code" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewPOGRUOM{{$index+1}}" class="form-control input-sm" v-model="row.uom" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewPOGRQty{{$index+1}}" class="form-control input-sm" v-model="row.qty" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewPOGRScanQty{{$index+1}}" class="form-control input-sm" v-model="row.scan_qty" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 10%;">
                                                            <input type="text" id="tbViewPOGRScanDatetime{{$index+1}}" class="form-control input-sm" v-model="row.scan_datetime" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewPOGRUser{{$index+1}}" class="form-control input-sm" v-model="row.user" disabled="disabled"/>
                                                        </td>
                                                        <td style="vertical-align:middle; padding-bottom:0; padding-top:0;  width: 3%;">
                                                            <%--<button type="button" style="display:none;" class="btn btn-warning btn-xs" id="btnViewPOGRScanItemSN{{$index+1}}" onclick="setViewScanItemSNModalInfo({{$index+1}})"  
                                                                data-toggle="modal" data-target="#divViewScanItemSNModalId" >
                                                                <i class="fa fa-list-ul "></i>
                                                            </button>
                                                            <span id="viewPOGRScannedItemBadge{{$index+1}}" style="display:none;" class="badge badge-error">0</span>
                                                            <input type="hidden" id="hfViewPOGRScanItemSNList{{$index+1}}" v-model="row.snList"/> --%>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="table tableTotalDisplay hidden">
                                            <tr>
                                                <td style="width: 83vw;" class="text-right"><strong>ITEM TO PRINT:</strong></td>
                                                <td style="width: 15vw;" class="text-right"><strong>{{ total2Print }}</strong></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row hidden" style="padding-top:10px;">
                                            <div class="col-md-offset-9 col-md-3">
                                                <div class="col-md-12">
                                                    <button id="btnPOGRPrint" type="button" class="btn btn-warning btn-block">Print</button>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="js/poGoodsReceive/viewPOGoodsReceiveModalJs.js"></script>