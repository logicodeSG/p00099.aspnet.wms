﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="updateUserPasswordModal.ascx.cs" Inherits="wmsGoldbell.subModule.masterSite.updateUserPasswordModal" %>

<div class="modal fade" id="divUpdateUserPasswordId" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update User Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="hfChangePasswordUserId"/>
                    <div id="divChangePasswordUserName" class="form-group col-xs-12 ">
                        <label for="inputError" class="control-label lb-md">Username:</label>
                        <input type="text" maxlength="45" class="form-control" id="tbChangePasswordUsername" disabled="disabled">
                    </div>
                    <div id="divChangePasswordUserOldPass" class="form-group col-xs-12">
                        <label for="inputError" class="control-label lb-md">Old Password:</label>
                        <input type="password" maxlength="20" class="form-control" id="tbChangePasswordUserOldPass">
                    </div>
                    <div id="divChangePasswordUserNewPass1" class="form-group col-xs-12">
                        <label for="inputError" class="control-label lb-md">New Password:</label>
                        <input type="password" maxlength="20" class="form-control" id="tbChangePasswordUserNewPass1">
                    </div>
                    <div id="divChangePasswordUserNewPass2" class="form-group col-xs-12">
                        <label for="inputError" class="control-label lb-md">Confirmed New Password:</label>
                        <input type="password" maxlength="20" class="form-control" id="tbChangePasswordUserNewPass2">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnChangePasswordUserUpdate" type="button" class="btn btn-default" data-dismiss="modal">Update</button>   
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


