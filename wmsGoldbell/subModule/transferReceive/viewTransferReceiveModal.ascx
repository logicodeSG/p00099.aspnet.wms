﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="viewTransferReceiveModal.ascx.cs" Inherits="wmsGoldbell.subModule.transferReceive.viewTransferReceiveModal" %>
<style>
    #divViewTRModalId .form-group {
        margin-bottom: 0px;
    }

</style>

<div class="modal fade" id="divViewTRModalId" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90vw;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Transfer Receive</h4>
            </div>
            <div class="modal-footer" id="appViewTRX" style="text-align:left; padding-top:5px;">
                <div id="appViewTR">
                    <div class="row">
                        <div id="divViewTRTransferId" class="form-group col-md-3 " >
                            <input type="hidden" id="hfViewAXTRId" v-model="soId" />
                            <label id="lblViewTRTransferID" class="control-label" for="inputError">Transfer ID:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewTRTransferId" disabled="disabled"/>
                        </div>
                        <div id="divViewTRVoucherID" class="form-group col-md-3 " >
                            <label id="lblViewTRVoucherID" class="control-label" for="inputError">Voucher ID:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewTRVoucherId" disabled="disabled"/>
                        </div>
                        <div id="divViewTRModifiedDate" class="form-group col-md-3">
                            <label id="lblViewTRModifiedDate" class="control-label lb-md" for="inputError">Modified Date:</label>
                            <div class="input-group date" data-provide="datepicker" id="divViewTRModifiedDatePicker">
                                <input type="text" class="form-control input-sm" id="tbViewTRModifiedDate" readonly="readonly" disabled="disabled">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="row" >
                        
                        <div id="divViewTRLocationFrom" class="form-group col-md-3 " >
                            <label id="lblViewTRLocationFrom" class="control-label" for="inputError">Location From:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewTRLocationFrom" disabled="disabled"/>
                        </div>
                        
                        <div id="divViewTRLocationTo" class="form-group col-md-3 " >
                            <label id="lblViewTRLocationTo" class="control-label" for="inputError">Location To:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewTRLocationTo" disabled="disabled"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" >
                            <table class="dynamicTable" style="width: 100%;">
                                <tr>
                                    <td>
                                        <table class="table" style="margin-bottom: 0px;">
                                            <tr>
                                                <th style="width: 4%;"><strong>#</strong></th>
                                                <th style="width: 2%; display:none;"><input type="checkbox" id="cbAll" onclick="cbAllOnClick()"></th>
                                                <td style="width: 5%; display:none;"><strong>Line#</strong></td>
                                                <td style="width: 18%;"><strong>Item Id</strong></td>
                                                <td style="width: 18%;"><strong>Part Code</strong></td>
                                                <td style="width: 8%;"><strong>UOM</strong></td>
                                                <td style="width: 8%;"><strong>Qty</strong></td>
                                                <td style="width: 8%;"><strong>Scan Qty</strong></td>
                                                <td style="width: 15%;"><strong>Scan DT</strong></td>
                                                <td style="width: 8%;"><strong>User</strong></td>
                                                <td style="width: 3%;"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="height:50vh; overflow-y:auto;">
                                            <table class="table table-hover">
                                                <tbody >
                                                    <tr v-for="row in rows" >
                                                        <td style="vertical-align:middle; width: 4%;">{{$index+1}}</td>
                                                        <td style="vertical-align:middle; width: 2%; display:none;">
                                                            <input type="checkbox" id="cbPrint{{$index+1}}" value="{{$index+1}}" <%--v-model="cb_print"--%> @click="cbPrintCheck($event,row)">
                                                        </td>
                                                        <td  style="width:5%; display:none;">
                                                            <div id="divViewTRLineNum{{$index+1}}">
                                                                <input type="text" id="tbViewTRLineNum{{$index+1}}" class="form-control input-sm" v-model="row.line_num" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td  style="width:18%;">
                                                            <div id="divViewTRItemId{{$index+1}}">
                                                                <input type="text" id="tbViewTRItemId{{$index+1}}" class="form-control input-sm" v-model="row.item_id" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td  style="width: 18%;">
                                                            <input type="text" id="tbViewTRPartCode{{$index+1}}" class="form-control input-sm" v-model="row.part_code" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewTRUOM{{$index+1}}" class="form-control input-sm" v-model="row.uom" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewTRQty{{$index+1}}" class="form-control input-sm" v-model="row.qty" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewTRScanQty{{$index+1}}" class="form-control input-sm" v-model="row.scan_qty" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 15%;">
                                                            <input type="text" id="tbViewTRScanDatetime{{$index+1}}" class="form-control input-sm" v-model="row.scan_datetime" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewTRUser{{$index+1}}" class="form-control input-sm" v-model="row.user" disabled="disabled"/>
                                                        </td>
                                                        <td style="vertical-align:middle; padding-bottom:0; padding-top:0;  width: 3%;">
                                                            <%--<button type="button" style="display:none;" class="btn btn-warning btn-xs" id="btnViewTRScanItemSN{{$index+1}}" onclick="setViewScanItemSNModalInfo({{$index+1}})"  
                                                                data-toggle="modal" data-target="#divViewScanItemSNModalId" >
                                                                <i class="fa fa-list-ul "></i>
                                                            </button>
                                                            <span id="viewTRScannedItemBadge{{$index+1}}" style="display:none;" class="badge badge-error">0</span>
                                                            <input type="hidden" id="hfViewTRScanItemSNList{{$index+1}}" v-model="row.snList"/> --%>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="table tableTotalDisplay hidden">
                                            <tr>
                                                <td style="width: 83vw;" class="text-right"><strong>ITEM TO PRINT:</strong></td>
                                                <td style="width: 15vw;" class="text-right"><strong>{{ total2Print }}</strong></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row hidden" style="padding-top:10px;">
                                            <div class="col-md-offset-9 col-md-3">
                                                <div class="col-md-12">
                                                    <button id="btnTRPrint" type="button" class="btn btn-warning btn-block">Print</button>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="js/transferReceive/viewTransferReceiveModalJs.js"></script>