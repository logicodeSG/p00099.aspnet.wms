﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="viewGoodsReceiveModal.ascx.cs" Inherits="wmsGoldbell.subModule.goodsReceive.viewGoodsReceiveModal" %>
<style>
    #divViewGRModalId .form-group {
        margin-bottom: 0px;
    }

</style>

<div class="modal fade" id="divViewGRModalId" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90vw;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Goods Receive</h4>
            </div>
            <div class="modal-footer" id="appViewPOX" style="text-align:left; padding-top:5px;">
                <div id="appViewGR">
                    <div class="row">
                        <div id="divViewGRNumber" class="form-group col-md-3 " >
                            <input type="hidden" id="hfViewGRId" v-model="poId" />
                            <label id="lblViewGRNumber" class="control-label" for="inputError">GR#:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewGRNumber" disabled="disabled"/>
                        </div>
                        <div id="divViewGRPONumber" class="form-group col-md-3 " >
                            <label id="lblViewGRPONumber" class="control-label" for="inputError">PO#:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewGRPONumber" disabled="disabled"/>
                        </div>
                        <div id="divViewGRReceiveDate" class="form-group col-md-3">
                            <label id="lblViewGRReceiveDate" class="control-label lb-md" for="inputError">Receive Date:</label>
                            <div class="input-group date" data-provide="datepicker" id="divViewGRReceiveDatePicker">
                                <input type="text" class="form-control input-sm" id="tbViewGRReceiveDate" readonly="readonly" disabled="disabled">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                        <div id="divViewGRDONumber" class="form-group col-md-3" style="padding-bottom:10px">                                      
                            <label id="lblViewGRDONumber" class="control-label" for="inputError">DO#</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbViewGRDONumber" disabled="disabled"/>
                        </div>
                    </div>
                
                    <div class="row" >
                        <div id="divEditGRCreatedBy" class="form-group col-md-offset-9 col-md-3 " >
                            <label id="lblEditGRCreatedBy" class="control-label" for="inputError">Created By:</label>
                            <input type="text" maxlength="50" class="form-control input-sm" id="tbEditGRCreatedBy" disabled="disabled"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" >
                            <table class="dynamicTable" style="width: 100%;">
                                <tr>
                                    <td>
                                        <table class="table" style="margin-bottom: 0px;">
                                            <tr>
                                                <th style="width: 5%;"><strong>#</strong></th>
                                                <td style="width: 18%;"><strong>Item</strong></td>
                                                <td style="width: 33%;"><strong>Description</strong></td>
                                                <td style="width: 8%;"><strong>UOM</strong></td>
                                                <td style="width: 8%;"><strong>Receive</strong></td>
                                                <td style="width: 17%;"><strong>Location</strong></td>
                                                <td style="width: 8%;"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="height:50vh; overflow-y:auto;">
                                            <table class="table table-hover">
                                                <tbody >
                                                    <tr v-for="row in rows">
                                                        <td style="vertical-align:middle; width: 5%;">{{$index+1}}</td>
                                                        <td  style="width:18%;">
                                                            <div id="divViewGRItemCode{{$index+1}}">
                                                                <input type="text" id="tbViewGRItemCode{{$index+1}}" class="form-control input-sm" v-model="row.item_code" disabled="disabled"/>
                                                            </div>
                                                            <input type="hidden" id="hfViewItemId{{$index+1}}" v-model="row.item_Id"/>
                                                        </td>
                                                        <td  style="width: 33%;">
                                                            <input type="text" id="tbViewGRItemDescription{{$index+1}}" class="form-control input-sm" v-model="row.description" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <input type="text" id="tbViewGRItemUOM{{$index+1}}" class="form-control input-sm" v-model="row.uom" disabled="disabled"/>
                                                        </td>
                                                        <td  style="width: 8%;">
                                                            <div id="divViewGRReceiveQuantity{{$index+1}}">
                                                                <input id="tbViewGRReceiveQuantity{{$index+1}}" type="text" maxlength="4" class="form-control input-sm" v-model="row.receive_quantity" 
                                                                       onclick="this.select()"  disabled="disabled"
                                                                       onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57"  />
                                                            </div>
                                                        </td>
                                                        <td style="width:17%">
                                                            <div id="divViewGRItemLocation{{$index+1}}" style="width:100%;">
                                                                <input type="text" id="tbViewGRItemLocation{{$index+1}}" class="form-control input-sm" v-model="row.location" disabled="disabled"/>
                                                            </div>
                                                        </td>
                                                        <td style="vertical-align:middle; padding-bottom:0; padding-top:0;  width: 8%;">
                                                            <%--<button type="button" style="display:none;" class="btn btn-warning btn-xs" id="btnViewGRScanItemSN{{$index+1}}" onclick="setViewScanItemSNModalInfo({{$index+1}})"  
                                                                data-toggle="modal" data-target="#divViewScanItemSNModalId" >
                                                                <i class="fa fa-list-ul "></i>
                                                            </button>
                                                            <span id="viewGRScannedItemBadge{{$index+1}}" style="display:none;" class="badge badge-error">0</span>
                                                            <input type="hidden" id="hfViewGRScanItemSNList{{$index+1}}" v-model="row.snList"/> --%>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>

                                <%--<tr>
                                    <td>
                                        <div class="row" style="padding-top:10px;">
                                            <div class="col-md-offset-9 col-md-3">
                                                <div class="col-md-12">
                                                    <button id="btnGRUndo" type="button" class="btn btn-warning btn-block" disabled="disabled">Undo</button>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>--%>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="js/goodsReceive/viewGoodsReceiveModalJs.js"></script>