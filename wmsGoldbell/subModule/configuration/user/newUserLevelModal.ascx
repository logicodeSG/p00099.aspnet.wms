﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="newUserLevelModal.ascx.cs" Inherits="wmsGoldbell.subModule.configuration.user.newUserLevelModal" %>
<div class="modal fade" id="divNewUserLevelModalId" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create New User Group</h4>
            </div>
            <div class="modal-body" style="height: 70vh;">
                <div class="row">

                    <div id="divError" class="alert alert-danger hidden">
                    </div>

                    <div id="divUserLevelDescription" class="form-group col-md-6">
                        <input type="hidden" id="hfUserLevelId" v-model="row.id" />
                        <label for="inputError" class="control-label">Description:</label>
                        <input type="text" maxlength="50" class="form-control" id="tbUserLevelDescription">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Active</label>
                        <input id="chkUserLevelActive" type="checkbox" value="" checked="checked" disabled="disabled">
                    </div>

                </div>
                <div class="col-md-12" id="appUserLevelACL">
                    <table class="dynamicTable" style="width: 100%;">
                        <tr>
                            <td>
                                <table class="table" style="margin-bottom: 0px;">
                                    <tr>
                                        <th style="width: 5%;"><strong>#</strong></th>
                                        <td style="width: 30%;"><strong>ACTION</strong></td>
                                        <td style="width: 7%;"><strong>Read</strong></td>
                                        <td style="width: 7%;"><strong>Add</strong></td>
                                        <td style="width: 7%;"><strong>Edit</strong></td>
                                        <td style="width: 7%;"><strong>Delete</strong></td>
                                        <td style="width: 7%;"><strong>Print</strong></td>
                                        <td style="width: 20%;"><strong></strong></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="height: 50vh; overflow-y: auto;">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align: middle; width: 5%;"></td>
                                                <td style="width: 30%;">
                                                    
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanRead0">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanRead0" type="checkbox" onchange="chkHeaderOnChanged('canRead')" />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanAdd0">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanAdd0" type="checkbox" onchange="chkHeaderOnChanged('canAdd')" />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanEdit0">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanEdit0" type="checkbox" onchange="chkHeaderOnChanged('canEdit')" />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanDelete0">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanDelete0" type="checkbox" onchange="chkHeaderOnChanged('canDelete')" />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanPrint0">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanPrint0" type="checkbox" onchange="chkHeaderOnChanged('canPrint')" />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 20%;"></td>
                                            </tr>
                                            <tr v-for="row in rows">
                                                <td style="vertical-align: middle; width: 5%;">{{$index+1}}</td>
                                                <td style="width: 30%;">
                                                    <div>
                                                        <input type="hidden" id="hfActionId{{$index+1}}" v-model="row.id" />
                                                        <input type="text" id="tbAction{{$index+1}}" class="form-control input-sm" v-model="row.action" disabled="disabled" />
                                                    </div>
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanRead{{$index+1}}">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanRead{{$index+1}}" type="checkbox" />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanAdd{{$index+1}}">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanAdd{{$index+1}}" type="checkbox"  />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanEdit{{$index+1}}">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanEdit{{$index+1}}" type="checkbox"  />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanDelete{{$index+1}}">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanDelete{{$index+1}}" type="checkbox"  />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 7%;">
                                                    <div id="divCanPrint{{$index+1}}">
                                                        <div class="checkbox" style="margin-top: 0px;">
                                                            <label>
                                                                <input id="chkCanPrint{{$index+1}}" type="checkbox" " />
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 20%;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnAddUserLevel" type="button" class="btn btn-default" data-dismiss="modal">Create</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
 <script src="js/configuration/user/userLevelACLJs.js"></script>