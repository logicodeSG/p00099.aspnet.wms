﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="editUserModal.ascx.cs" Inherits="wmsGoldbell.DynamicData.editUserModal" %>
<div class="modal fade" id="divEditUserModalId" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit user</h4>
    </div>
        <div class="modal-body">
            <div class="row">

                <div id="divEditUserError" class="alert alert-danger hidden">
                </div>
                <div class="form-group" style="display:none;">
                    <label>User Id:</label>
                    <input type="text" maxlength="10" class="form-control" id="tbEditUserId" readonly="readonly">
                </div>
                <div id="divEditUserCode" class="form-group col-md-6">
                    <label>User Code:</label>
                    <input type="text" maxlength="10" class="form-control" id="tbEditUsercode" readonly="readonly">
                </div>
                <div id="divEditUserName" class="form-group col-md-6">
                    <label>Username:</label>
                    <input type="text" maxlength="10" class="form-control" id="tbEditUsername">
                </div>
                <div id="divEditUserPass1" class="form-group col-md-6">
                    <label for="inputError" class="control-label"> Password:</label>
                    <input type="password" maxlength="10" class="form-control" id="tbEditUserPass1">
                </div>
                <div id="divEditUserPass2" class="form-group col-md-6">
                    <label for="inputError" class="control-label"> Confirmed Password:</label>
                    <input type="password" maxlength="10" class="form-control" id="tbEditUserPass2">
                </div>
                <div id="divEditUserLevel" class="form-group col-md-6">
                    <label>User Level:</label>
                    <select class="form-control" id="selectEditUserLevel">
                    </select>
                </div>
                <div class="form-group ">
                    <label >Active</label>
                       <input id="chkEditUserActive" type="checkbox" value="" checked="checked" disabled="disabled">
                </div>

            </div>
            <div class="row">
                    <div class="form-group col-md-6">
                        <div class="row">
                            <div class="col-md-10">
                                <div id="divEditUserLocation">
                                    <label for="inputError" class="control-label">Location:</label>
                                    <select class="form-control selectpicker" id="selectEditUserLocation" data-style="btn-default" data-live-search="true" data-size="5">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="padding-top: 20px;">
                                <a class="btn btn-social-icon btn-linkedin" id="btnEditUserAddLocation" onclick="editAddLocation()"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" id="appEditUserLocation">
                        <table class="dynamicTable" style="width: 100%;">
                            <tr>
                                <td>
                                    <table class="table" style="margin-bottom: 0px;">
                                        <tr>
                                            <th style="width: 5%;"><strong>#</strong></th>
                                            <td style="width: 75%;"><strong>Location</strong></td>
                                            <td style="width: 20%;"><strong></strong></td>

                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="height: 30vh; overflow-y: auto;">
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr v-for="row in rows">
                                                    <td style="vertical-align: middle; width: 5%;">{{$index+1}}</td>
                                                    <td style="width: 75%;">
                                                        <div>
                                                            <input type="hidden" id="hfEditUserLocationId{{$index+1}}" v-model="row.locationId" />
                                                            <input type="text" id="tbEditUserLocationName{{$index+1}}" class="form-control input-sm" v-model="row.locationName" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                    <td style="width: 20%;">
                                                        <button class="btn btn-danger" @click="removeRow(row)" ><i class="fa fa-close"></i> </button>
                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button id="btnEditUser" type="button" class="btn btn-default">Edit</button>   
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
