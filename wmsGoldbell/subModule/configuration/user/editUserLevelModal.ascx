﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="editUserLevelModal.ascx.cs" Inherits="wmsGoldbell.subModule.configuration.user.editUserLevelModal" %>
<div class="modal fade" id="divEditUserLevelModalId" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit User Group</h4>
    </div>
        <div class="modal-body" style="height: 70vh;">
            <div class="row">

                <div class="form-group" style="display:none;">
                    <label>Id:</label>
                    <input type="text" maxlength="10" class="form-control" id="tbEditUserLevelId" readonly="readonly">
                </div>
                <div id="divEditUserLevelDescription" class="form-group col-md-6">
                    <label>Description:</label>
                    <input type="text" maxlength="50" class="form-control" id="tbEditUserLevelDescription">
                </div>
                <div class="form-group ">
                    <label >Active</label>
                       <input id="chkEditUserLevelActive" type="checkbox" value="" checked="checked" disabled="disabled">
                </div>
            </div>
            <div class="col-md-12" id="appEditUserLevelACL">
                        <table class="dynamicTable" style="width: 100%;">
                            <tr>
                                <td>
                                    <table class="table" style="margin-bottom: 0px;">
                                        <tr>
                                            <th style="width: 5%;"><strong>#</strong></th>
                                            <td style="width: 30%;"><strong>ACTION</strong></td>
                                            <td style="width: 7%;"><strong>Read</strong></td>
                                            <td style="width: 7%;"><strong>Add</strong></td>
                                            <td style="width: 7%;"><strong>Edit</strong></td>
                                            <td style="width: 7%;"><strong>Delete</strong></td>
                                            <td style="width: 7%;"><strong>Print</strong></td>
                                            <td style="width: 20%;"><strong></strong></td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="height: 50vh; overflow-y: auto;">
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <td style="vertical-align: middle; width: 5%;"></td>
                                                    <td style="width: 30%;">
                                                    
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanRead0">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanRead0" type="checkbox" onchange="chkEditHeaderOnChanged('canRead')" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanAdd0">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanAdd0" type="checkbox" onchange="chkEditHeaderOnChanged('canAdd')" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanEdit0">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanEdit0" type="checkbox" onchange="chkEditHeaderOnChanged('canEdit')" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanDelete0">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanDelete0" type="checkbox" onchange="chkEditHeaderOnChanged('canDelete')" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanPrint0">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanPrint0" type="checkbox" onchange="chkEditHeaderOnChanged('canPrint')" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 20%;"></td>
                                                </tr>
                                                <tr v-for="row in rows">
                                                    <td style="vertical-align: middle; width: 5%;">{{$index+1}}</td>
                                                    <td style="width: 30%;">
                                                        <div>
                                                            <input type="hidden" id="hfEditActionId{{$index+1}}" v-model="row.id" />
                                                            <input type="text" id="tbEditAction{{$index+1}}" class="form-control input-sm" v-model="row.action" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanRead{{$index+1}}">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanRead{{$index+1}}" type="checkbox" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanAdd{{$index+1}}">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanAdd{{$index+1}}" type="checkbox"  />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanEdit{{$index+1}}">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanEdit{{$index+1}}" type="checkbox"  />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanDelete{{$index+1}}">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanDelete{{$index+1}}" type="checkbox"  />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 7%;">
                                                        <div id="divEditCanPrint{{$index+1}}">
                                                            <div class="checkbox" style="margin-top: 0px;">
                                                                <label>
                                                                    <input id="chkEditCanPrint{{$index+1}}" type="checkbox" " />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 20%;"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
        <div class="modal-footer">
            <button id="btnEditUserLevel" type="button" class="btn btn-default" data-dismiss="modal">Edit</button>   
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
 <script src="js/configuration/user/editUserLevelACLJs.js"></script>