﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="lvThreeLocModal.ascx.cs" Inherits="wmsGoldbell.subModule.configuration.location.lvThreeLocModal" %>

<link href="ajnRes/ajnVuetableCss.css" rel="stylesheet" type="text/css" media="screen" />
<div class="modal fade" id="divLvThreeLocModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:60% !important;">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="modal-title"><label id="lblLvThreeLocTitleId"></label></h3>
            <input type="hidden" ID="hfLocation2Code" />
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-lg-5">
                    <a class="btn btn-block btn-social btn-linkedin" id="btnOpenDivLvThreeLoc">
                                    <i class="fa fa-plus"></i> Create New LV-3 Location
                    </a>   
                </div>
                <div class="form-group col-lg-2 col-lg-offset-5">
                    <div style="margin: 10px 0 0 0;">
                        <label >Show All</label>
                        <input id="chkLocation3ShowAll" type="checkbox" onchange="activateLvThreeTable()" value="" />
                    </div>
                </div>
            </div>

            <div id="appLvThreeLoc">
                <div class="table-responsive">
                    <h4 class="ui header">List of LV-3 Level Location</h4>
                    <vuetable
                        api-url="/ws/ims-ws.asmx/getAllLocationByLocLvlAndParentLocCode"
                        table-class="table table-bordered table-striped table-hover"
                        :fields="columnsLvThreeLoc"
                        :item-actions="lvThreeLocActions"
                        :append-params="moreParamsLvThreeLoc"
                            pagination-path=""
                            :sort-order="sortOrder"
                            :multi-sort="multiSort"
                            ascending-icon="glyphicon glyphicon-chevron-up"
                            descending-icon="glyphicon glyphicon-chevron-down"
                            pagination-class=""
                            pagination-info-class=""
                            pagination-component-class=""
                            :pagination-component="paginationComponent"
                            :per-page="perPage"
                            wrapper-class="vuetable-wrapper"
                            table-wrapper=".vuetable-wrapper"
                            loading-class="loading"
                    ></vuetable>
                </div>
            </div>
        </div>

  <%--      <div class="modal-footer">
            <label>hint</label>
        </div>--%>
        </div>
    </div>
</div>

<script>
    var lvThreeLocVuetable = new Vue({
        el: '#appLvThreeLoc',
        data: {
            //apiUrl: '/ws/ims-ws.asmx/getItemEntryByItemCode?argItemCode=' + curItemCode,
            columnsLvThreeLoc: [
                {
                    name: 'id',
                    title: 'Location ID',
                    sortField: 'id',
                    visible: false
                },
                {
                    name: 'code',
                    title: 'Location Code',
                    sortField: 'code'
                },
                {
                    name: 'description',
                    title: 'Location',
                    sortField: 'description'
                },
                {
                    name: 'active',
                    title: 'Active',
                    sortField: 'active',
                    callback: 'isActive'
                },
                {
                    name: '__actions',
                    dataClass: 'text-center',
                }
            ], sortOrder: [{
                field: 'id',
                direction: 'desc'
            }],
            multiSort: true,
            perPage: 8,
            paginationComponent: 'vuetable-pagination',
            paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
            lvThreeLocActions: [
               {
                   name: 'edit-lvthree-location', label: '', icon: 'glyphicon glyphicon-edit', class: 'label label-info edit-location' + setActionVisible(!!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)))
                , extra: { 'title': 'Edit', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
               },
            ],
            moreParamsLvThreeLoc: [
                'argLocLevel=' + '3',
                'argParentLocCode=' + 'tempLocCode',
                'showAll=false'
            ]
        },
        watch: {
            'perPage': function (val, oldVal) {
                this.$broadcast('vuetable:refresh')
            },
            'paginationComponent': function (val, oldVal) {
                this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
                this.paginationConfig(this.paginationComponent)
            }
        },
        methods: {
            isActive: function (value) {
                return value == 'True'
                  ? '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> TRUE</span>'
                  : '<span class="label label-danger"><i class="glyphicon glyphicon-minus"></i> FALSE</span>'
            },
            paginationConfig: function (componentName) {
                //console.log('paginationConfig: ', componentName)
                if (componentName == 'vuetable-pagination') {
                    this.$broadcast('vuetable-pagination:set-options', {
                        wrapperClass: 'pagination',
                        icons: { first: '', prev: '', next: '', last: '' },
                        activeClass: 'active',
                        linkClass: 'btn btn-default',
                        pageClass: 'btn btn-default'
                    })
                }
                if (componentName == 'vuetable-pagination-dropdown') {
                    this.$broadcast('vuetable-pagination:set-options', {
                        wrapperClass: 'form-inline',
                        icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                        dropdownClass: 'form-control'
                    })
                }
            },
        },
        events: {
            'vuetable:action': function (action, data) {
                if (action == 'edit-lvthree-location') {
                    $("#divEditLvThreeLocId").modal()

                    $.ajax({
                        url: '/ws/ims-ws.asmx/getLocationByLocationId',
                        contentType: "application/json",
                        data: JSON.stringify({ argLocationId: data.id }),
                        dataType: 'JSON',
                        cache: false,
                        context: document.body,
                        type: 'POST',
                        success: function (returnData) {
                            $("#tbEditLocation3ID").val(returnData.d[0].locationId)
                            $("#tbEditLocation3Code").val(returnData.d[0].locationCode)
                            $("#tbEditLocation3Desc").val(returnData.d[0].locationDescription)
                            document.getElementById("chkEditLocation3Active").checked = returnData.d[0].locationActive;
                        },
                        error: function (err) {
                            showSystemErrorMsg(err.responseText);
                        }
                    });
                }
            }
        }
    })

    function activateLvThreeTable(curLocCode) {
        lvThreeLocVuetable.moreParamsLvThreeLoc = [
            'argLocLevel=' + '3',
            'argParentLocCode=' + document.getElementById('hfLocation2Code').value,
                'showAll=' + document.getElementById('chkLocation3ShowAll').checked
        ]

        lvThreeLocVuetable.$nextTick(function () {
            lvThreeLocVuetable.$broadcast('vuetable:refresh')
        })
    }

</script>