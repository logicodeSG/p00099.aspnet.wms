﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="editLvTwoLocModal.ascx.cs" Inherits="wmsGoldbell.subModule.configuration.location.editLvTwoLocModal" %>
<div class="modal fade" id="divEditLvTwoLocId" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit LV-2 Location</h4>
            </div>
            <div class="modal-body">

                <div class="form-group hidden">
                    <label>Location ID:</label>
                    <input type="text" maxlength="10" class="form-control" id="tbEditLocation2ID" readonly="readonly">
                </div>
                <div id="divEditLocation2Code" class="form-group">
                    <label for="inputError">Location Code:</label>
                    <input type="text" maxlength="17" class="form-control" id="tbEditLocation2Code">
                </div>
                <div id="divEditLocation2Desc" class="form-group">
                    <label for="inputError">Location Description:</label>
                    <input type="text" maxlength="20" class="form-control" id="tbEditLocation2Desc">
                </div>
                <div class="form-group">
                    <label>Active</label>
                    <input id="chkEditLocation2Active" type="checkbox" value="" checked="checked" disabled="disabled">
                </div>

            </div>
            <div class="modal-footer">
                <button id="btnEditLvTwoLoc" type="button" class="btn btn-default">Edit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>