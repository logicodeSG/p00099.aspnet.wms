﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="newLvTwoLocModal.ascx.cs" Inherits="wmsGoldbell.subModule.configuration.location.newLvTwoLocModal" %>
<div class="modal fade" id="divLvTwoLocId" role="dialog">
    <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create a new LV-2 Location</h4>
    </div>
        <div class="modal-body">
            
                <div id="divL2LocationCode" class="form-group">
                    <label for="inputError" >Location Code:</label>
                    <input type="text" maxlength="17" class="form-control" id="tbL2LocationCode">
                </div>
                <div id="divL2LocationDesc" class="form-group">
                    <label for="inputError" >Location Description:</label>
                    <input type="text" maxlength="20" class="form-control" id="tbL2LocationDesc">
                </div>
                <div class="form-group">
                    <label>Active</label>
                    <input id="chkL2LocationActive" type="checkbox" value="" checked="checked" disabled="disabled">
                </div>
            
        </div>
        <div class="modal-footer">
            <button id="btnAddLvTwoLoc" type="button" class="btn btn-default">Create</button>   
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>