﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="lvTwoLocModal.ascx.cs" Inherits="wmsGoldbell.subModule.configuration.location.lvTwoLocModal" %>
<link href="ajnRes/ajnVuetableCss.css" rel="stylesheet" type="text/css" media="screen" />
<div class="modal fade" id="divLvTwoLocModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:90% !important;">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="modal-title"><label id="lblLvTwoLocTitleId"></label></h3>
            <input type="hidden" ID="hfLocationCode" />
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-lg-3">
                    <a class="btn btn-block btn-social btn-linkedin" id="btnOpenDivLvTwoLoc">
                                    <i class="fa fa-plus"></i> Create New LV-2 Location
                    </a>   
                </div>
                <div class="form-group col-lg-2 col-lg-offset-7">
                    <div style="margin: 10px 0 0 0;">
                        <label >Show All</label>
                        <input id="chkLocation2ShowAll" type="checkbox" onchange="activateLvTwoTable()" value="" />
                    </div>
                </div>
            </div>

            <div id="appLvTwoLoc">
                <div class="table-responsive">
                    <h4 class="ui header">List of LV-2 Level Location</h4>
                    <vuetable
                        api-url="/ws/ims-ws.asmx/getAllLocationByLocLvlAndParentLocCode"
                        table-class="table table-bordered table-striped table-hover"
                        :fields="columnsLvTwoLoc"
                        :item-actions="lvTwoLocActions"
                        :append-params="moreParamsLvTwoLoc"
                        pagination-path=""
                            :sort-order="sortOrder"
                            :multi-sort="multiSort"
                            ascending-icon="glyphicon glyphicon-chevron-up"
                            descending-icon="glyphicon glyphicon-chevron-down"
                            pagination-class=""
                            pagination-info-class=""
                            pagination-component-class=""
                            :pagination-component="paginationComponent"
                            :per-page="perPage"
                            wrapper-class="vuetable-wrapper"
                            table-wrapper=".vuetable-wrapper"
                            loading-class="loading"
                    ></vuetable>
                </div>
            </div>
        </div>

<%--        <div class="modal-footer">
            <label>hint</label>
        </div>--%>
        </div>
    </div>
</div>

<script>
    var curSelectLvlTwoLocation;

    var lvTwoLocVuetable = new Vue({
        el: '#appLvTwoLoc',
        data: {
            //apiUrl: '/ws/ims-ws.asmx/getItemEntryByItemCode?argItemCode=' + curItemCode,
            columnsLvTwoLoc: [
                {
                    name: 'id',
                    title: 'Location ID',
                    sortField: 'id',
                    visible: false
                },
                {
                    name: 'code',
                    title: 'Location Code',
                    sortField: 'code',
                },
                {
                    name: 'description',
                    title: 'Location',
                    sortField: 'description',
                },
                {
                    name: 'active',
                    title: 'Active',
                    sortField: 'active',
                    callback: 'isActive'
                },
                {
                    name: '__actions',
                    dataClass: 'text-center',
                }
            ],
            sortOrder: [{
                field: 'id',
                direction: 'desc'
            }],
            multiSort: true,
            perPage: 8,
            paginationComponent: 'vuetable-pagination',
            paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
            lvTwoLocActions: [
                 {
                     name: 'edit-lvtwo-location', label: '', icon: 'glyphicon glyphicon-edit', class: 'label label-info edit-location' + setActionVisible(!!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)))
                , extra: { 'title': 'Edit', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
                 },
                //{
                //    name: 'view-lvthree-location', label: '', icon: 'glyphicon glyphicon-info-sign', class: 'label label-info'
                //    , extra: { 'title': 'View Location LV3', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
                //},
            ],
            moreParamsLvTwoLoc: [
                'argLocLevel=' + '2',
                'argParentLocCode=' + 'tempLocCode',
                'showAll=false',
            ]
        },
        watch: {
            'perPage': function (val, oldVal) {
                this.$broadcast('vuetable:refresh')
            },
            'paginationComponent': function (val, oldVal) {
                this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
                this.paginationConfig(this.paginationComponent)
            }
        },
        methods: {
            isActive: function (value) {
                return value == 'True'
                  ? '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> TRUE</span>'
                  : '<span class="label label-danger"><i class="glyphicon glyphicon-minus"></i> FALSE</span>'
            },
            paginationConfig: function (componentName) {
                //console.log('paginationConfig: ', componentName)
                if (componentName == 'vuetable-pagination') {
                    this.$broadcast('vuetable-pagination:set-options', {
                        wrapperClass: 'pagination',
                        icons: { first: '', prev: '', next: '', last: '' },
                        activeClass: 'active',
                        linkClass: 'btn btn-default',
                        pageClass: 'btn btn-default'
                    })
                }
                if (componentName == 'vuetable-pagination-dropdown') {
                    this.$broadcast('vuetable-pagination:set-options', {
                        wrapperClass: 'form-inline',
                        icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                        dropdownClass: 'form-control'
                    })
                }
            },
        },
        events: {
            'vuetable:action': function (action, data) {
                if (action == 'edit-lvtwo-location') {
                    $("#divEditLvTwoLocId").modal()

                    $.ajax({
                        url: '/ws/ims-ws.asmx/getLocationByLocationId',
                        contentType: "application/json",
                        data: JSON.stringify({ argLocationId: data.id }),
                        dataType: 'JSON',
                        cache: false,
                        context: document.body,
                        type: 'POST',
                        success: function (returnData) {
                            $("#tbEditLocation2ID").val(returnData.d[0].locationId)
                            $("#tbEditLocation2Code").val(returnData.d[0].locationCode)
                            $("#tbEditLocation2Desc").val(returnData.d[0].locationDescription)
                            document.getElementById("chkEditLocation2Active").checked = returnData.d[0].locationActive;
                        },
                        error: function (err) {
                            showSystemErrorMsg(err.responseText);
                        }
                    });
                }
                else if (action == 'view-lvthree-location') {
                    curSelectLvlTwoLocation = data.id

                    $('#lblLvThreeLocTitleId').text("LV-2 Location: " + data.code)
                    $("#hfLocation2Code").val(data.code)
                    activateLvThreeTable(data.code)
                    $("#divLvThreeLocModal").modal()
                }
            }
        }
    })

    function activateLvTwoTable(curLocCode) {
        lvTwoLocVuetable.moreParamsLvTwoLoc = [
            'argLocLevel=' + '2',
            'argParentLocCode=' + document.getElementById('hfLocationCode').value,
                'showAll=' + document.getElementById('chkLocation2ShowAll').checked
        ]

        lvTwoLocVuetable.$nextTick(function () {
            lvTwoLocVuetable.$broadcast('vuetable:refresh')
        })
    }


</script>
