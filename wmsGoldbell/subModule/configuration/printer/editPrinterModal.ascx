﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="editPrinterModal.ascx.cs" Inherits="wmsGoldbell.js.configuration.printer.editPrinterModal" %>
<div class="modal fade" id="divEditPrinterModalId" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Printer</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="divEditPrinterId" class="form-group col-md-6" style="display: none;">
                        <label for="inputError" class="control-label">Printer ID:</label>
                        <input type="text" maxlength="30" class="form-control" id="tbEditPrinterId">
                    </div>
                    <div id="divEditPrinterName" class="form-group col-md-12">
                        <label for="inputError" class="control-label">Name:</label>
                        <input type="text" maxlength="50" class="form-control" id="tbEditPrinterName">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Active</label>
                        <input id="chkEditPrinterActive" type="checkbox" value="" checked="checked" disabled="disabled">
                    </div>
                    <div class="form-group col-md-5 hidden">
                        <label>Is Default</label>
                        <input id="chkEditPrinterIsDefault" type="checkbox" value="" disabled="disabled">
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button id="btnEditPrinterUpdate" type="button" class="btn btn-default">Edit</button>
                <%--<button id="btnEditPrinterReset" type="button" class="btn btn-default">Reset</button>--%>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
