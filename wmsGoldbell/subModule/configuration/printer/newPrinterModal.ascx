﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="newPrinterModal.ascx.cs" Inherits="wmsGoldbell.js.configuration.printer.newPrinterModal" %>
<div class="modal fade" id="divNewPrinterModalId" role="dialog">
    <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Printer</h4>
    </div>
        <div class="modal-body">
            <div class="row">
                <div id="divPrinterName" class="form-group col-md-12">
                    <label for="inputError" class="control-label">Name:</label>
                    <input type="text" maxlength="50" class="form-control" id="tbPrinterName">
                </div>
                <div class="form-group col-md-4">
                    <label >Active</label>
                       <input id="chkPrinterActive" type="checkbox" value="" checked="checked" disabled="disabled">
                </div>
                
             </div>
        </div>
        <div class="modal-footer">
            <button id="btnPrinterAdd" type="button" class="btn btn-default" >Create</button> 
            <button id="btnPrinterReset" type="button" class="btn btn-default" >Reset</button>  
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>