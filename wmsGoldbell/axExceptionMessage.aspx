﻿<%@ Page Title="" Language="C#" MasterPageFile="~/customSite.Master" AutoEventWireup="true" CodeBehind="axExceptionMessage.aspx.cs" Inherits="wmsGoldbell.axExceptionMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="page-wrapper" style="padding:0px;">
        <div class="row">   
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="appAXExceptionMessageDivId">
                    <div class="table-responsive" style="height:85vh">
                        <vuetable
                            api-url="/ws/ims-ws.asmx/getAllAXExceptionMsgInfo"
                            table-class="table table-bordered table-striped table-hover"
                            :fields="columns"
                            :item-actions="actions"
                            :append-params="moreParams"      
                            pagination-path=""
                            :sort-order="sortOrder"
                            :multi-sort="multiSort"
                            ascending-icon="glyphicon glyphicon-chevron-up"
                            descending-icon="glyphicon glyphicon-chevron-down"
                            pagination-class=""
                            pagination-info-class=""
                            pagination-component-class=""
                            :pagination-component="paginationComponent"
                            :per-page="perPage"
                            wrapper-class="vuetable-wrapper"
                            table-wrapper="vuetable-wrapper"
                            loading-class="loading"
                            ></vuetable>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <script src="js/axExceptionMessage/axExceptionMessageJs.js"></script>
</asp:Content>
