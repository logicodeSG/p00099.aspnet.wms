﻿<%@ Page Title="" Language="C#" MasterPageFile="~/customSite.Master" AutoEventWireup="true" CodeBehind="settingDocNumber.aspx.cs" Inherits="wmsGoldbell.settingDocNumber" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="page-wrapper">
        <div class="col-md-12" id="appDocNumber">
            <table class="dynamicTable" style="width: 100%;">
                <tr>
                    <td>
                        <table class="table" style="margin-bottom: 0px;">
                            <tr>
                                <th style="width: 5%;"><strong>#</strong></th>
                                <td style="width: 23%;"><strong>Type</strong></td>
                                <td style="width: 16%;"><strong>Prefix</strong></td>
                                <td style="width: 14%;"><strong>Length</strong></td>
                                <td style="width: 15%;"><strong>Next Number</strong></td>
                                <td style="width: 17%;"><strong>Preview</strong></td>

                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="height: 60vh; overflow-y: auto;">
                            <table class="table table-hover">
                                <tbody>
                                    <tr v-for="row in rows">
                                        <td style="vertical-align: middle; width: 5%;">{{$index+1}}</td>
                                        <td style="width: 23%;">
                                            <div>
                                                <input type="hidden" id="hfDocNumberId{{$index+1}}" v-model="row.id"  />
                                                <input type="text" id="tbDocNumberType{{$index+1}}" class="form-control input-sm" v-model="row.type" disabled="disabled" />
                                            </div>
                                        </td>
                                        <td style="width: 16%;">
                                            <div id="divDocNumberPrefix{{$index+1}}">
                                                <input type="text" id="tbDocNumberPrefix{{$index+1}}" class="form-control input-sm" v-model="row.prefix" 
                                                    onkeyup="columnInputOnChanged({{$index+1}})"  disabled="disabled"/>
                                            </div>
                                        </td>
                                        <td style="width: 14%;">
                                            <div id="divDocNumberLength{{$index+1}}">
                                            <input type="text" id="tbDocNumberLength{{$index+1}}" class="form-control input-sm" v-model="row.numberLength" onkeyup="columnInputOnChanged({{$index+1}})"
                                                onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" 
                                                 disabled="disabled" />
                                                </div>
                                        </td>
                                        <td style="width: 15%;">
                                            <div id="divDocNumberNextNumber{{$index+1}}">
                                            <input type="text" id="tbDocNumberNextNumber{{$index+1}}" class="form-control input-sm" v-model="row.nextNumber" onkeyup="columnInputOnChanged({{$index+1}})" 
                                                    onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57"
                                                 disabled="disabled" />
                                                </div>
                                        </td>
                                        <td style="width: 17%;">
                                            <div id="divDocNumberPreview{{$index+1}}">
                                                <input id="tbDocNumberPreview{{$index+1}}" type="text" maxlength="4" class="form-control input-sm" v-model="row.preview"
                                                    disabled="disabled"  />
                                            </div>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="row" style="padding-top: 10px">
                            <div class="col-md-offset-9 col-md-3">
                                <div class="col-md-12">
                                    <button id="btnSettingDocNumberSave" type="button" class="btn btn-primary btn-block" disabled="disabled" >Save</button>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>

            </table>
        </div>
    </div>
    <script src="js/configuration/docNumber/docNumberJs.js"></script>
</asp:Content>
