﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using wmsGoldbell.ws;
using wmsGoldbell.Class;

namespace wmsGoldbell
{
    public partial class Login : System.Web.UI.Page
    {
        //public string adminLoginName = WebConfigurationManager.AppSettings["adminName"];
        //public string adminLoginPass = WebConfigurationManager.AppSettings["adminPass"];

        protected void Page_Load(object sender, EventArgs e)
        {
            tbUsername.Focus();
            this.tbUsername.Attributes.Add("onkeypress", "tbUsername_click()");
            this.tbUserPassword.Attributes.Add("onkeypress", "tbUserPassword_click()");
        }

        protected void btnUserLogin_Click(object sender, EventArgs e)
        {

            ims_ws myWebService = new ims_ws();
            sysUserS[] userList = myWebService.getUserByUsernamePassword(tbUsername.Text.ToString(), tbUserPassword.Text.ToString(), "");
            if (userList.Length < 1)
            {

                //Response.Write("<script language='javascript'>swal('Invalid Username/Password','','error');</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "swal({title: 'Error', text: 'Invalid Username/Password', type: 'error',timer: 1000})", true);
            }
            else
            {
                if (userList[0].userActive)
                {
                    xUserLevelXAccessObject[] xUxAObject = myWebService.getUserLevelXAccessByUserLevelId(userList[0].userLevelId);
                    if (xUxAObject.Length < 1)
                    {

                        //Response.Write("<script language='javascript'>swal('Invalid Username/Password','','error');</script>");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "swal({title: 'Error', text: 'Invalid Userlevel', type: 'error',timer: 1000})", true);
                    }
                    else
                    {
                        Session["curSessionUsername"] = userList[0].username;
                        Session["curSessionUserId"] = userList[0].userId;

                        String strACL = "";
                        for (int i = 0; i < xUxAObject.Length; i++)
                        {
                            if (i > 0) strACL += "&";
                            strACL += xUxAObject[i].userAccessId + "|" +
                                 ((xUxAObject[i].canRead == "True") ? '1' : '0') + "|" +
                                ((xUxAObject[i].canAdd == "True") ? '1' : '0') + "|" +
                                ((xUxAObject[i].canEdit == "True") ? '1' : '0') + "|" +
                                ((xUxAObject[i].canDelete == "True") ? '1' : '0') + "|" +
                                ((xUxAObject[i].canPrint == "True") ? '1' : '0');
                        }
                        Session["curSessionUserLevelACL"] = strACL;

                        userLocationObject[] uLocationObject = myWebService.getUserXLocationByUserId(userList[0].userId);
                        if (uLocationObject.Length < 1)
                        {

                            //Response.Write("<script language='javascript'>swal('Invalid Username/Password','','error');</script>");
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "swal({title: 'Error', text: 'Invalid User location', type: 'error',timer: 1000})", true);
                        }
                        else
                        {
                            Session["curSessionWarehouseCode"] = "-1";
                            Session["curSessionWarehouseId"] = -1;

                            sysUserS[] tempUserObject = myWebService.getUserByUserId(userList[0].userId);
                            if (Convert.ToInt64(tempUserObject[0].userLastSelectedLocationId) > -1)
                            {
                                foreach (userLocationObject tempLocation in uLocationObject)
                                {
                                    if (tempLocation.locationId == tempUserObject[0].userLastSelectedLocationId)
                                    {
                                        Session["curSessionWarehouseCode"] = tempLocation.locationCode;
                                        Session["curSessionWarehouseId"] = tempLocation.locationId;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                myWebService.setUserSelectedLocationByUserId(uLocationObject[0].locationId, userList[0].userId);
                                Session["curSessionWarehouseCode"] = uLocationObject[0].locationCode;
                                Session["curSessionWarehouseId"] = uLocationObject[0].locationId;
                            }
                        }


                        Response.Redirect("~/axExceptionMessage.aspx");

                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Invalid Username/Password')", true);
                }
            }
        }


    }
}