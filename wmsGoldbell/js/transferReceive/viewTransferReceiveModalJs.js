﻿Vue.config.async = false;
var viewTRVue = new Vue({
    el: "#appViewTR",
    data: {
        //cb_print: [],
        total2Print: 0,
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        cbPrintCheck: function (e, row) {
            calculateTotal2Print();
            //if (e.target.checked) {
            //    this.rows.color = 'lightblue';
            //} else {
            //    this.rows.color = '';
            //}
        },
        addRow: function (line_num, item_id, part_code, uom, qty, scan_qty, scan_datetime, user) {
            this.rows.push({
                line_num: line_num, item_id: item_id, part_code: part_code, uom: uom,
                qty: qty, scan_qty: scan_qty, scan_datetime: scan_datetime, user: user
            });
        },
        removeRow: function (row) {
            this.rows.$remove(row);
        }
    }
});


$(document).ready(function () {

    $("#btnTRPrint").click(function () {
        printTR();
    });

    $('#divViewTRModalId').on('hidden.bs.modal', function () {
        clearAllViewTR();
    })
});

function cbAllOnClick() {
    var isChecked = document.getElementById('cbAll').checked;
    for (i = 0; i < viewTRVue.rows.length; i++) {
        document.getElementById('cbPrint' + (i + 1)).checked = isChecked;
        //document.getElementById('cbPrint' + (i + 1)).dispatchEvent(new Event('change'));
    }
    calculateTotal2Print();
}


function calculateTotal2Print() {
    var tempTotal = 0;
    for (i = 0; i < viewTRVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            tempTotal += 1;
        }
    }
    viewTRVue.total2Print = tempTotal;
}

function printTR() {
    for (i = 0; i < viewTRVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            console.log("Printing item Id =" + document.getElementById('tbViewTRItemId' + (i + 1)).value +
               " Part Code = " + document.getElementById('tbViewTRPartCode' + (i + 1)).value)
        }
    }
}

function clearAllViewTR() {
    clearViewTRInput();
    for (i = viewTRVue.rows.length - 1; i >= 0; i--) {
        viewTRVue.rows.$remove(viewTRVue.rows[i]);
    }
}

function clearViewTRInput() {
    document.getElementById('tbViewTRTransferId').value = "";
    document.getElementById('tbViewTRVoucherId').value = "";
    document.getElementById('hfViewAXTRId').value = "";
    document.getElementById('tbViewTRModifiedDate').value = "";
    document.getElementById('tbViewTRLocationFrom').value = "";
    document.getElementById('tbViewTRLocationtTo').value = "";
    document.getElementById('cbAll').checked = false;
}


