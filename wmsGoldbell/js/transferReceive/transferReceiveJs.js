﻿
// fields definition
var tableColumns = [
    {
        name: 'id',
        title: 'ID',
        sortField: 'tbl_ax_tr.id',
        dataClass: 'text-center',
        visible: false
        //callback: 'showDetailRow'
    },
    {
        name: 'axTRANSFERID',
        title: 'TRANSFER ID',
        sortField: 'tbl_ax_tr.axTRANSFERID',
    },
    {
        name: 'axVOUCHERID',
        title: 'VOUCHER ID',
        sortField: 'tbl_ax_tr.axVOUCHERID',
    },
    {
        name: 'axMODIFIEDDATETIME',
        title: 'Modified Date',
        sortField: 'tbl_ax_tr.axMODIFIEDDATETIME',
        callback: 'formatDate'
    },
    {
        name: 'tbl_status_Id',
        title: 'Status',
        sortField: 'tbl_ax_tr.tbl_status_Id',
        callback: 'statusCallback'
    },
    {
        name: '__actions',
        title: 'ACTION',
        dataClass: 'text-center',
    }
]

var slSearchOptions = [
    {
        optValue: "tbl_ax_tr.axTRANSFERID",
        optText: tableColumns[1].title,
        optType: ''
    },
    {
        optValue: "tbl_ax_tr.axVOUCHERID",
        optText: tableColumns[2].title,
        optType: ''
    },
    {
        optValue: "tbl_ax_tr.axMODIFIEDDATETIME",
        optText: tableColumns[3].title,
        optType: 'DATE'
    },
    {
        optValue: 'tbl_ax_tr.tbl_status_Id',
        optText: tableColumns[4].title,
        optType: 'DROPDOWN',
        optDDData: ['PENDING', 'COMPLETED']
    }
]

var trVuetable = new Vue({
    el: '#appTRDivId',
    data: {
        columns: tableColumns,
        sortOrder: [{
            field: 'tbl_ax_tr.tbl_status_Id asc, tbl_ax_tr.id ',
            direction: 'desc'
        }],
        multiSort: true,
        perPage: 10,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        actions: [
            //{ name: 'assign-item', label: '', icon: 'glyphicon glyphicon-paperclip', class: 'btn btn-primary', extra: { 'title': 'Assign', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            //{ name: 'print-item', label: '', icon: 'glyphicon glyphicon-print', class: 'btn btn-info', extra: { 'title': 'Print', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            {
                name: 'view-transferReceive', label: '', icon: 'glyphicon glyphicon-info-sign', class: 'label label-info'
                , extra: { 'title': 'View', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },
        ],
        moreParams: [
            'filter=tbl_ax_tr.id|',
            'filterType=', 'warehouseCode=-1',
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        /**
         * Callback functions
         */
        allCap: function (value) {
            return value.toUpperCase()
        },
        gender: function (value) {
            return value == 'True'
              ? '<span class="label label-info"><i class="glyphicon glyphicon-star"></i> TRUE</span>'
              : '<span class="label label-success"><i class="glyphicon glyphicon-heart"></i> FALSE</span>'
        },
        formatDate: function (value, fmt) {
            if (value == null) return ''
            fmt = (typeof fmt == 'undefined') ? 'DD/MM/YYYY' : fmt
            return moment(value, 'DD/MM/YYYY hh:mm:ss A').format(fmt)
        },
        statusCallback: function (value) {
            switch (value) {
                case "1":
                    return '<span class="label label-info"><i class="glyphicon glyphicon-refresh"></i> PENDING</span>'
                    break;
                case "9":
                    return '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> COMPLETED</span>'
                    break;

            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        // -------------------------------------------------------------------------------------------
        // You can change how sort params string is constructed by overriding getSortParam() like this
        // -------------------------------------------------------------------------------------------
        // getSortParam: function(sortOrder) {
        //     console.log('parent getSortParam:', JSON.stringify(sortOrder))
        //     return sortOrder.map(function(sort) {
        //         return (sort.direction === 'desc' ? '+' : '') + sort.field
        //     }).join(',')
        // }
    },
    events: {
        'vuetable:action': function (action, data) {
            //console.log('vuetable:action', action, data)

            if (action == 'view-transferReceive') {
                loadViewTR(data.axTRANSFERID);
            }
        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            var searchValue = document.getElementById('inputSearchKeyId').value;
            var searchHeader = document.getElementById('slSearch').value;

            if (searchValue !== '') {
                for (n in data) {
                    if (searchHeader == "tbl_ax_tr.axTRANSFERID" || searchHeader == "tbl_ax_tr.axVOUCHERID") {
                        //data[n]["axPICKINGROUTEID"] = this.highlight(searchValue, data[n]["axPICKINGROUTEID"]);
                    } else {
                        data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
                    }
                }
            }
        },
    }
})

$(document).ready(function () {
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > STOCK CONTROL > TRANSFER RECEIVE");

    reloadSlSearch(slSearchOptions);
    setFilterVuetable(trVuetable);

    $('#inputSearchKeyId').keypress(function (event) {
        if (event.keyCode == 13) {
            filterTable();
        }
    });

    $(function () {
        $("#selectUserLocation").on('change', function () {
            setWarehouseIdSession();
            setWarehouseCodeSession();
            filterTable();
        });

    });


    //$('#divNewGRModalId').on('hidden.bs.modal', function () {
    //    trVuetable.$broadcast('vuetable:refresh');
    //})


});

function loadViewTR(transferId) {
    $.ajax({
        url: '/ws/ims-ws.asmx/getAXTRByAXTransferId',
        contentType: "application/json",
        data: JSON.stringify({ argAXTRANSFERID: transferId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                $('#hfViewAXTRId').val(returnData.d[0].trId);
                $('#tbViewTRTransferId').val(returnData.d[0].trAX_TransferId);
                $('#tbViewTRVoucherId').val(returnData.d[0].trAX_VoucherId);
                $('#tbViewTRModifiedDate').val(returnData.d[0].trAX_ModifiedDatetime);
                $('#tbViewTRLocationFrom').val(returnData.d[0].trAX_InventLocationIdFrom);
                $('#tbViewTRLocationTo').val(returnData.d[0].trAX_InventLocationIdTo);

                loadViewTRDetail(transferId);
            } else {
                swal(
                        'Load Transfer Receive - Fail',
                        'Failed in loading selected transfer receive',
                        'error'
                        );
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function loadViewTRDetail(transferId) {
    waitingDialog.show();
    $.ajax({
        url: '/ws/ims-ws.asmx/getAXTRDetailByAXTransferId',
        contentType: "application/json",
        data: JSON.stringify({ argAXTRANSFERID: transferId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                for (var i = 0; i < returnData.d.length; i++) {
                    viewTRVue.addRow(
                        "",
                        returnData.d[i].trdAX_ItemId,
                        returnData.d[i].trdAX_Dot_PartCode,
                        returnData.d[i].trdAX_UnitId,
                        returnData.d[i].trdAX_QtyShipped,
                        returnData.d[i].trdScanQuantity,
                        returnData.d[i].trdScanDatetime,
                        returnData.d[i].trdUsername);
                }
                $("#divViewTRModalId").modal({ backdrop: 'static', keyboard: false });
            } else {
                swal(
                        'Load Transfer Receive - Fail',
                        'Failed in loading selected transfer receive',
                        'error'
                        );
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        },
        complete: function (data) {
            waitingDialog.hide();
        }
    });
}

function filterTable() {
    var e = document.getElementById("slSearch");
    var strSearchColumnName = e.options[e.selectedIndex].value;

    var tempSelectUserLocation = document.getElementById('selectUserLocation');
    var selectedUserLocationText = tempSelectUserLocation.options[tempSelectUserLocation.selectedIndex].text;

    if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        trVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('tbSearchDateRangeFrom').value + '|' + document.getElementById('tbSearchDateRangeTo').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType,
            'warehouseCode=' + selectedUserLocationText
        ];
    } else if ($('#divSearchDateRange').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        trVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType,
            'warehouseCode=' + selectedUserLocationText
        ];
    } else if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDateRange').hasClass('hidden')) {
        trVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('slSearchDropdown').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType,
            'warehouseCode=' + selectedUserLocationText
        ];
    }

    trVuetable.$nextTick(function () {
        trVuetable.$broadcast('vuetable:refresh');
    });
}


function chkTRShowAll_onChanged() {
    filterTable();
}

