﻿var waitingDialog2 = waitingDialog || (function ($) {
    'use strict';

    // Creating modal dialog's DOM
    var $dialog = $(
        '<div id="divLoader" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"' +
        'aria-hidden="true" style="padding-top:50%; width: 150px; margin: 0 auto; ">' +
        '<div class="modal-dialog modal-xs" style="width: 100%;!important height: 100%;!important;>' +
        '<div class="modal-content">' +
            '' +
            '<div class="modal-body">' +
                 '<div class="loader"></div>' +
            '</div>' +
        '</div></div></div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Loading';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };

})(jQuery);


$(document).ready(function () {
    setSearchTabVisibility();
    setUserLocationVisibility();

    $(document).on("keypress", "form", function (event) {
        return event.keyCode != 13;
    });

    //$(function () {
    //    $("#selectUserLocation").on('change', function () {
    //        setWarehouseIdSession();
    //        setUserSelectedLocationByUserId();
    //    });

    //});

    $(".input-daterange").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true,
    });

    setMenuAccessControl();
    if (!$("#selectUserLocation").hasClass('hidden')) {
        loadAllUserLocation();
    }
});

function setSearchTabVisibility() {
    switch (window.location.pathname) {
        case "/itemList":
        case "/currentStock":
        case "/poGoodsReceive":
        case "/ajGoodsReceive":
        case "/salesOrder":
        case "/transferPicking":
        case "/transferReceive":
        case "/countingJournal":
        case "/axExceptionMessage":
        case "/location":
        case "/settingPrinter":
        case "/settingUser":
        case "/settingUserLevel":
            $("#searchTabId").removeClass('hidden');
            break;
        case "/movementHistory":
        default:
            $("#searchTabId").addClass('hidden');
            break;
    }
}

function setUserLocationVisibility() {
    switch (window.location.pathname) {
        case "/dashboard":
        case "/itemList":
        case "/currentStock":
        case "/poGoodsReceive":
        case "/ajGoodsReceive":
        case "/salesOrder":
        case "/transferPicking":
        case "/transferReceive":
        case "/countingJournal":
        case "/movementHistory":
            $("#selectUserLocation").removeClass('hidden');
            break;
        case "/location":
        case "/settingUser":
        case "/settingUserLevel":
        case "/axExceptionMessage":
        default:
            $("#selectUserLocation").addClass('hidden');
            break;
    }
}

function loadAllUserLocation() {
    $.ajax({
        url: '/ws/ims-ws.asmx/getUserXLocationByUserId',
        contentType: "application/json",
        data: JSON.stringify({ "argUserId": document.getElementById('hfUserId').value }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            var tempSelect = document.getElementById('selectUserLocation');
            for (var i = 0; i < returnData.d.length; i++) {
                var opt = document.createElement('option');
                opt.value = returnData.d[i].locationId;
                opt.innerHTML = returnData.d[i].locationCode;
                tempSelect.appendChild(opt);
            }
            var warehouseId = document.getElementById('hfWarehouseId').value;
            if (!(selectHasValue("selectUserLocation", warehouseId))) {
                if (returnData.d.length > 0) {
                    $("#selectUserLocation").val(returnData.d[0].locationId);
                    $("#selectUserLocation").selectpicker('refresh');
                    setWarehouseIdSession();
                    setWarehouseCodeSession();
                    filterTable();
                }
                //swal({
                //    title: "Failed to load item",
                //    text: "Location does not found in the selection list",
                //    type: "error"
                //}).then(function () {
                   
                //    return;
                //});
            } else {
                $("#selectUserLocation").val(warehouseId);
                $("#selectUserLocation").selectpicker('refresh');
                setWarehouseIdSession();
                setWarehouseCodeSession();
                filterTable();
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function getTodayDate() {
    var currentTime = new Date();
    var month = ("0" + (currentTime.getMonth() + 1)).slice(-2);
    var day = ("0" + currentTime.getDate()).slice(-2);
    var year = currentTime.getFullYear();
    var todayDate = day + "/" + month + "/" + year;
    return todayDate;
}


function getPreviousYearDate() {
    var currentTime = new Date();
    var month = ("0" + (currentTime.getMonth() + 1)).slice(-2);
    var day = ("0" + currentTime.getDate()).slice(-2);
    var year = currentTime.getFullYear() - 1;
    var previousYearDate = day + "/" + month + "/" + year;
    return previousYearDate;
}

function masterFormatDate(date) {
    var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [day, month, year].join('/');
}

function formatDate(value, fmt) {
    if (value == null) return ''
    fmt = (typeof fmt == 'undefined') ? 'DD/MM/YYYY' : fmt
    return moment(value, 'DD/MM/YYYY hh:mm:ss A').format(fmt)
}

function reloadSlSearch(slSearchOptions) {
    $('#slSearch').children().remove();

    var typeStr = "";

    for (var i = 0; i <= slSearchOptions.length - 1 ; i++) {
        var opt = document.createElement('option');
        opt.value = slSearchOptions[i].optValue;
        opt.innerHTML = slSearchOptions[i].optText;
        document.getElementById('slSearch').appendChild(opt);
        typeStr += slSearchOptions[i].optType + "|";
    }

    $('#slSearch').attr('onChange', '').unbind('change');
    $("#slSearch").attr("onChange", "slSearchOnChange('" + typeStr + "')");
    slSearchOnChange(typeStr);
}

function slSearchOnChange(typeStr) {
    typeStr = typeStr.substring(0, typeStr.length - 1);
    var typeArr = [];

    //if (typeStr.length > 0) {
    var typeArr = typeStr.split('|');
    //}

    var e = document.getElementById("slSearch");
    if (typeArr[e.selectedIndex] == "") {
        showInputSearch(true);
        showDateSearch(false);
        showDropdownSearch(false);
        //document.getElementById('tbSearchDateRangeFrom').value = "";
        //document.getElementById('tbSearchDateRangeTo').value = "";
    } else if (typeArr[e.selectedIndex] == "DATE") {
        showInputSearch(false);
        showDateSearch(true);
        showDropdownSearch(false);
    } else if (typeArr[e.selectedIndex] == "DROPDOWN") {
        showInputSearch(false);
        showDateSearch(false);
        showDropdownSearch(true);
    }
}

function showInputSearch(b) {
    document.getElementById('inputSearchKeyId').value = "";
    if (b) {
        $("#divSearchInput").removeClass('hidden');
    } else {
        $("#divSearchInput").addClass('hidden');
    }
}

function showDateSearch(b) {
    $('#divSearchDateRange input').each(function () {
        $(this).datepicker('clearDates');
    });
    if (b) {
        $("#divSearchDateRange").removeClass('hidden');
    } else {
        $("#divSearchDateRange").addClass('hidden');
    }
}

function showDropdownSearch(b) {
    clearSelectOption("slSearchDropdown");
    if (b) {
        var e = document.getElementById("slSearch");
        for (var i = 0; i <= slSearchOptions[e.selectedIndex].optDDData.length - 1 ; i++) {
            var opt = document.createElement('option');
            opt.value = slSearchOptions[e.selectedIndex].optDDData[i];
            opt.innerHTML = slSearchOptions[e.selectedIndex].optDDData[i];
            document.getElementById('slSearchDropdown').appendChild(opt);
        }
        $("#divSearchDropdown").removeClass('hidden');
    } else {
        $("#divSearchDropdown").addClass('hidden');
    }
}

function setFilterVuetable(vt) {
    filterVueTable = vt;
}

function showSwalErrorMsg(msg, div) {
    swal('', msg, 'error');
    if (div !== undefined) {
        $(div).addClass('has-error');
    }
}

function clearDivErrorMsg(div) {
    $(div).removeClass('has-error');
}

function setSystemTitle(argTitle) {
    document.getElementById("lblSystemTitle").innerHTML = argTitle;
}

function onCurrencySelect(tbId) {
    $('#' + tbId).val(parseFloat(document.getElementById(tbId).value.replace(/[^0-9-.]/g, '')));
    $('#' + tbId).select();
}

function getGST(vueTB) {
    var ce = {
        "argName": "GST"
    }

    $.ajax({
        url: 'ws/ims-ws.asmx/getSystemData',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                vueTB.gst = parseInt(returnData.d[0].systemDataValue);
            }

            //stockVuetable.reloadStockTable();
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function getAllowNegativeInventory(vueTB) {
    var ce = {
        "argName": "NEG_INV"
    }

    $.ajax({
        url: 'ws/ims-ws.asmx/getSystemData',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                vueTB.neg_inv = parseInt(returnData.d[0].systemDataValue);
            }

            //stockVuetable.reloadStockTable();
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function clearSelectOption(tempSelectId) {
    var selectTemp = document.getElementById(tempSelectId);
    for (var i = selectTemp.options.length - 1; i >= 0; i--) {
        selectTemp.options[i] = null;
    }
}

function makeTableResizable() {
    $(".vuetable").colResizable({
        liveDrag: true,
        gripInnerHtml: "<div class='grip'></div>",
        draggingClass: "dragging"
    });

    //var thElm;
    //var startOffset;

    //Array.prototype.forEach.call(
    //  document.querySelectorAll("table th"),
    //  function (th) {
    //      th.style.position = 'relative';

    //      var grip = document.createElement('div');
    //      grip.innerHTML = "&nbsp;";
    //      grip.style.top = 0;
    //      grip.style.right = 0;
    //      grip.style.bottom = 0;
    //      grip.style.width = '5px';
    //      grip.style.position = 'absolute';
    //      grip.style.cursor = 'col-resize';
    //      grip.addEventListener('mousedown', function (e) {
    //          thElm = th;
    //          startOffset = th.offsetWidth - e.pageX;
    //      });

    //      th.appendChild(grip);
    //  });

    //document.addEventListener('mousemove', function (e) {
    //    if (thElm) {
    //        thElm.style.width = startOffset + e.pageX + 'px';
    //    }
    //});

    //document.addEventListener('mouseup', function () {
    //    thElm = undefined;
    //});
}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    var today = new Date();
    var dArray = masterFormatDate(today).split("/");
    fileName += dArray[2] + '' + dArray[1] + '' + dArray[0];

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

function selectHasValue(select, value) {
    obj = document.getElementById(select);

    if (obj !== null) {
        return (obj.innerHTML.indexOf('value="' + value + '"') > -1);
    } else {
        return false;
    }
}

function selectHasDisplayText(select, displayText) {
    obj = document.getElementById(select);

    if (obj !== null) {
        for (var i = 0; i < obj.options.length; i++) {
            if (obj.options[i].text == displayText) {
                return i;
            }
        }
        return -1;
    } else {
        return -1;
    }
}

function showSystemErrorMsg(errMsg) {
    if (document.getElementById("hfSystemErrorToggle").value == 1) {
        alert(errMsg);
    }
}

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var INVENTORY_AC_ID = 1; var PO_GOODS_RECEIVE_AC_ID = 2; var AJ_GOODS_RECEIVE_AC_ID = 3;
var SALES_PICKING_AC_ID = 5; 
var STOCK_TRANSFER_PICKING_AC_ID = 7; var STOCK_TRANSFER_RECEIVE_AC_ID = 8;
var SETTING_AC_ID = 13; var REPORTING_AC_ID = 14; var MOVEMENT_HISTORY_AC_ID = 15;
var COUNTING_JOURNAL_AC_ID = 16; var AX_EXCEPTION_MESSAGE_AC_ID = 17;

var CAN_READ = 1; var CAN_ADD = 2; var CAN_EDIT = 3; var CAN_DELETE = 4; var CAN_PRINT = 5;
function getAccessControl(acId, acType) {
    var tempVal = document.getElementById("hfUserLevelACL").value;
    var acArr = tempVal.split("&");
    var AC_ID = 0;
    for (var i = 0; i < acArr.length; i++) {
        var acDetailArr = acArr[i].split("|");
        if (acDetailArr[0] == acId) {
            return acDetailArr[acType];
            break;
        }
    }
}

function setMenuAccessControl() {
    //setElementIdVisible("#menuInventory", !!parseInt(getAccessControl(INVENTORY_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuItemList", !!parseInt(getAccessControl(INVENTORY_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuCurrentStock", !!parseInt(getAccessControl(INVENTORY_AC_ID, CAN_READ)));

    setElementIdVisible("#menuAXExceptionMessage", !!parseInt(getAccessControl(AX_EXCEPTION_MESSAGE_AC_ID, CAN_READ)));
    setElementIdVisible("#menuPOGoodsReceive", !!parseInt(getAccessControl(PO_GOODS_RECEIVE_AC_ID, CAN_READ)));
    setElementIdVisible("#menuAJGoodsReceive", !!parseInt(getAccessControl(AJ_GOODS_RECEIVE_AC_ID, CAN_READ)));
    setElementIdVisible("#menuSalesPicking", !!parseInt(getAccessControl(SALES_PICKING_AC_ID, CAN_READ)));
    setElementIdVisible("#menuTransferPicking", !!parseInt(getAccessControl(STOCK_TRANSFER_PICKING_AC_ID, CAN_READ)));
    setElementIdVisible("#menuTransferReceive", !!parseInt(getAccessControl(STOCK_TRANSFER_RECEIVE_AC_ID, CAN_READ)));
    setElementIdVisible("#menuCountingJournal", !!parseInt(getAccessControl(COUNTING_JOURNAL_AC_ID, CAN_READ)));
    setElementIdVisible("#menuMovementHistory", !!parseInt(getAccessControl(MOVEMENT_HISTORY_AC_ID, CAN_READ)));
    if (
        !!parseInt(getAccessControl(PO_GOODS_RECEIVE_AC_ID, CAN_READ)) ||
        !!parseInt(getAccessControl(AJ_GOODS_RECEIVE_AC_ID, CAN_READ)) ||
        !!parseInt(getAccessControl(SALES_ORDER_PICKING_AC_ID, CAN_READ)) ||
        !!parseInt(getAccessControl(STOCK_TRANSFER_PICKING_AC_ID, CAN_READ)) ||
        !!parseInt(getAccessControl(STOCK_TRANSFER_RECEIVE_AC_ID, CAN_READ)) ||
        !!parseInt(getAccessControl(COUNTING_JOURNAL_AC_ID, CAN_READ)) ||
        !!parseInt(getAccessControl(MOVEMENT_HISTORY_AC_ID, CAN_READ))) {
        //||
        //!!parseInt(getAccessControl(MOVEMENT_HISTORY_AC_ID, CAN_READ))) {
        setElementIdVisible("#menuStockControl", true);
    }

    //if (!!parseInt(getAccessControl(REPORTING_AC_ID, CAN_READ)) &&
    //    !!parseInt(getAccessControl(REPORTING_AC_ID, CAN_PRINT))) {
    //    setElementIdVisible("#menuReporting", true);
    //}

    setElementIdVisible("#menuSetting", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    setElementIdVisible("#menuSettingUser", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    setElementIdVisible("#menuSettingUserList", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    setElementIdVisible("#menuSettingUserLevel", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuSettingSupplier", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuSettingCustomer", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    setElementIdVisible("#menuSettingLocation", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuSettingCompany", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuSettingUOM", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuSettingDocNumber", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuSettingPrinter", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuSettingPriceList", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
    //setElementIdVisible("#menuSettingSystem", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_READ)));
}

function setElementIdVisible(eId, v) {
    if (v) {
        $(eId).removeClass('hidden');
    } else {
        $(eId).addClass('hidden');
    }
}

function enableElementById(eId, e) {
    if (e) {
        $(eId).attr("disabled", false);
    } else {
        $(eId).attr("disabled", true);
    }
}

function setActionVisible(v) {
    if (v) {
        return '';
    }
    else {
        return ' hidden';
    }
}

var addressTTTemplate = '<div class="popover" style="background-color: #536878; border: none; width:500px;"><div class="arrow"></div>' +
                                  '<h3 class="popover-title" style="background-color: #36454f; color:white; border: none; font-size: 16px;"></h3>' +
                                  '<div class="popover-content" style="background-color: #536878; color:white;">' +
                                  '</div>' +
                                  '</div></div>';

function createAddressDataContent(c) {
    return '<div class="row">' +
                '<div class="col-xs-3">' +
                ' <label style="font-size: 14px; font-weight:normal; color:#dddddd;">Address </label>' +
                '</div>' +
                '<div class="col-xs-9">' + c +
                '</div>' +
            '</div>';
}

function setPopover(elementId, title, dataContent, dataTemplate, dataPlacement) {

    var tempElement = $('#' + elementId);
    tempElement.attr('data-toggle', 'popover');
    tempElement.attr("title", title);
    tempElement.attr("data-original-title", title);
    tempElement.attr('data-html', true);
    tempElement.attr("data-content", dataContent);
    if (dataPlacement !== undefined) {
        tempElement.attr('data-placement', dataPlacement);
    }

    $('[data-toggle="popover"]').popover({
        trigger: "hover",
        container: "body",
        template: dataTemplate
    });
}

function loadGSTBySupplierId(supplierId, tempVue) {
    tempVue.gst = 0;
    $.ajax({
        url: '/ws/ims-ws.asmx/getSupplierBySupplierId',
        contentType: "application/json",
        data: JSON.stringify({ argSupplierId: supplierId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                if (returnData.d[0].supplierGST) {
                    getGST(tempVue);
                }
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function loadGSTByCustomerId(customerId, tempVue) {
    tempVue.gst = 0;
    $.ajax({
        url: '/ws/ims-ws.asmx/getCustomerByCustomerId',
        contentType: "application/json",
        data: JSON.stringify({ argCustomerId: customerId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                if (returnData.d[0].customerGST) {
                    getGST(tempVue);
                }
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function setWarehouseIdSession() {
    $.ajax({
        url: '/ws/ims-ws.asmx/setSession',
        contentType: "application/json",
        data: JSON.stringify({ argSessionName: "curSessionWarehouseId", argSessionValue: document.getElementById('selectUserLocation').value }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == "true") {
            } else {
                swal(
                    'Session Updated - Fail',
                    data.d,
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function setWarehouseCodeSession() {
    var tempSelect = document.getElementById('selectUserLocation');
    var selectedText = tempSelect.options[tempSelect.selectedIndex].text;
    $.ajax({
        url: '/ws/ims-ws.asmx/setSession',
        contentType: "application/json",
        data: JSON.stringify({ argSessionName: "curSessionWarehouseCode", argSessionValue: selectedText }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == "true") {
            } else {
                swal(
                    'Session Updated - Fail',
                    data.d,
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function setUserSelectedLocationByUserId() {
    $.ajax({
        url: '/ws/ims-ws.asmx/setUserSelectedLocationByUserId',
        contentType: "application/json",
        data: JSON.stringify({ argSelectedLocationId: document.getElementById('selectUserLocation').value ,
            argUserdId: document.getElementById('hfUserId').value }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == "true") {
            } else {
                swal(
                    'User Selected Location Updated - Fail',
                    data.d,
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}