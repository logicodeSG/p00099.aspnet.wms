
$(document).ready(function () {
    $("#hfChangePasswordUserId").val(document.getElementById('hfUserId').value);
    $("#tbChangePasswordUsername").val(document.getElementById('hfUsername').value);

    $('#btnChangePasswordUserUpdate').click(function () {
        if (!changePasswordInputVerification()) {
            return false
        }
        ;
        var ce = {
            "argUserId": document.getElementById('hfUserId').value
            , "argUsername": document.getElementById('hfUsername').value
            , "argUserOldPass": document.getElementById('tbChangePasswordUserOldPass').value
            , "argUserNewPass": document.getElementById('tbChangePasswordUserNewPass1').value
        }


        $.ajax({
            url: 'ws/ims-ws.asmx/updateUserPassword',
            contentType: "application/json",
            data: JSON.stringify(ce),
            dataType: 'JSON',
            cache: false,
            context: document.body,
            type: 'POST',
            success: function (data) {
                if (data.d == "true") {
                    swal('', 'Update User Password - Success', 'success')
                            .then(function () {
                                window.location.href = 'login.aspx';
                            });
                    
                } else {
                    swal(
                            'Update User Password - Fail',
                            data.d,
                            'error'
                            )
                }
            },
            error: function (err) {
                showSystemErrorMsg(err.responseText);
            }
        });

    });

    $('#divUpdateUserPasswordId').on('hidden.bs.modal', function () {
        changePasswordClearAll();
    })
});

function changePasswordInputVerification() {
    changePasswordClearAllError();

    if (document.getElementById('tbChangePasswordUsername').value.length == 0) {
        showSwalErrorMsg('Invalid User Name', "#divChangePasswordUserName");
        return false;
    }
    if (document.getElementById('tbChangePasswordUserOldPass').value.length == 0) {
        showSwalErrorMsg('Invalid Old Password', "#divChangePasswordUserOldPass");
        return false;
    }
    if (document.getElementById('tbChangePasswordUserNewPass1').value.length == 0) {
        showSwalErrorMsg('Invalid New Password', "#divChangePasswordUserNewPass1");
        return false;
    }
    if (document.getElementById('tbChangePasswordUserNewPass1').value != document.getElementById('tbChangePasswordUserNewPass2').value) {
        showSwalErrorMsg('Confirmed New Password must be the same as New Password', "#divChangePasswordUserNewPass2");
        return false;
    }
    return true;
}

function changePasswordClearAllError() {

    $("#divChangePasswordUserName").removeClass('has-error');
    $("#divChangePasswordUserOldPass").removeClass('has-error');
    $("#divChangePasswordUserNewPass1").removeClass('has-error');
    $("#divChangePasswordUserNewPass2").removeClass('has-error');
}

function changePasswordClearAll() {
    $("#tbChangePasswordUserOldPass").val("");
    $("#tbChangePasswordUserNewPass1").val("");
    $("#tbChangePasswordUserNewPass2").val("");
}