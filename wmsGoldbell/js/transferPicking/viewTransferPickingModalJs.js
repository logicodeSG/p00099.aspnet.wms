﻿Vue.config.async = false;
var viewTPVue = new Vue({
    el: "#appViewTP",
    data: {
        //cb_print: [],
        total2Print: 0,
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        cbPrintCheck: function (e, row) {
            calculateTotal2Print();
            //if (e.target.checked) {
            //    this.rows.color = 'lightblue';
            //} else {
            //    this.rows.color = '';
            //}
        },
        addRow: function (line_num, item_id, part_code, uom, qty, scan_qty, scan_datetime, user) {
            this.rows.push({
                line_num: line_num, item_id: item_id, part_code: part_code, uom: uom,
                qty: qty, scan_qty: scan_qty, scan_datetime: scan_datetime, user: user
            });
        },
        removeRow: function (row) {
            this.rows.$remove(row);
        }
    }
});


$(document).ready(function () {

    $("#btnTPPrint").click(function () {
        printTP();
    });

    $('#divViewTPModalId').on('hidden.bs.modal', function () {
        clearAllViewTP();
    })
});

function cbAllOnClick() {
    var isChecked = document.getElementById('cbAll').checked;
    for (i = 0; i < viewTPVue.rows.length; i++) {
        document.getElementById('cbPrint' + (i + 1)).checked = isChecked;
        //document.getElementById('cbPrint' + (i + 1)).dispatchEvent(new Event('change'));
    }
    calculateTotal2Print();
}


function calculateTotal2Print() {
    var tempTotal = 0;
    for (i = 0; i < viewTPVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            tempTotal += 1;
        }
    }
    viewTPVue.total2Print = tempTotal;
}

function printTP() {
    for (i = 0; i < viewTPVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            console.log("Printing item Id =" + document.getElementById('tbViewTPItemId' + (i + 1)).value +
               " Part Code = " + document.getElementById('tbViewTPPartCode' + (i + 1)).value)
        }
    }
}

function clearAllViewTP() {
    clearViewTPInput();
    for (i = viewTPVue.rows.length - 1; i >= 0; i--) {
        viewTPVue.rows.$remove(viewTPVue.rows[i]);
    }
}

function clearViewTPInput() {
    document.getElementById('tbViewTPTransferId').value = "";
    document.getElementById('tbViewTPRouteId').value = "";
    document.getElementById('hfViewAXTPId').value = "";
    document.getElementById('tbViewTPModifiedDate').value = "";
    document.getElementById('tbViewTPLocationFrom').value = "";
    document.getElementById('tbViewTPLocationtTo').value = "";
    document.getElementById('cbAll').checked = false;
}


