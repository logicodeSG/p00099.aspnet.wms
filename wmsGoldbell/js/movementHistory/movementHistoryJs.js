﻿
// fields definition
var tableColumns = [
    {
        name: 'id',
        title: 'ID',
        sortField: 'id',
        dataClass: 'text-center',
        visible: false
    },
    {
        name: 'axDOCUMENTID',
        title: 'Document ID',
        sortField: 'axDOCUMENTID',
    },
    {
        name: 'axSubDOCUMENTID',
        title: 'Sub Document ID',
        sortField: 'axSubDOCUMENTID',
    },
    {
        name: 'axLINENUM',
        title: 'Line#',
        sortField: 'axLINENUM',
        callback: 'intCallback'
    },
    {
        name: 'axITEMID',
        title: 'Item ID',
        sortField: 'axITEMID',
    },
    {
        name: 'axPART_CODE',
        title: 'Part Code',
        sortField: 'axPART_CODE',
    },
    {
        name: 'axWMSLOCATION_FROMID',
        title: 'Loc. From',
        sortField: 'axWMSLOCATION_FROMID',
    },
    {
        name: 'axWMSLOCATION_TOID',
        title: 'LOC. To',
        sortField: 'axWMSLOCATION_TOID',
    },
    {
        name: 'qty',
        title: 'QTY',
        sortField: 'qty',
        callback: 'intCallback'
    },
    {
        name: 'movementDatetime',
        title: 'Date',
        sortField: 'movementDatetime',
        callback: 'formatDate'
    },
    {
        name: 'tbl_username',
        title: 'User',
        sortField: 'tbl_username'
    },
    {
        name: 'movementType',
        title: 'Type',
        sortField: 'movementType'
    }
]

var slSearchOptions = [
    {
        optValue: tableColumns[1].name,
        optText: tableColumns[1].title,
        optType: ''
    },
    {
        optValue: tableColumns[2].name,
        optText: tableColumns[2].title,
        optType: ''
    },
    {
        optValue: tableColumns[3].name,
        optText: tableColumns[3].title,
        optType: ''
    },
    {
        optValue: tableColumns[4].name,
        optText: tableColumns[4].title,
        optType: ''
    },
    {
        optValue: tableColumns[5].name,
        optText: tableColumns[5].title,
        optType: ''
    },
    {
        optValue: tableColumns[6].name,
        optText: tableColumns[6].title,
        optType: ''
    },
    {
        optValue: tableColumns[7].name,
        optText: tableColumns[7].title,
        optType: ''
    },
    {
        optValue: tableColumns[8].name,
        optText: tableColumns[8].title,
        optType: ''
    },
    {
        optValue: tableColumns[9].name,
        optText: tableColumns[9].title,
        optType: 'DATE'
    },
    {
        optValue: tableColumns[10].name,
        optText: tableColumns[10].title,
        optType: ''
    },
    {
        optValue: tableColumns[11].name,
        optText: tableColumns[11].title,
        optType: ''
    }
]

var mhVuetable = new Vue({
    el: '#appMHDivId',
    data: {
        columns: tableColumns,
        sortOrder: [{
            field: 'id',
            direction: 'desc'
        }],
        multiSort: true,
        perPage: 10,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        actions: [

        ],
        moreParams: [
            'filter=id||', 'warehouseCode=', 'showEntries=' + document.getElementById('selectMHShowEntries').value
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        /**
         * Callback functions
         */
        allCap: function (value) {
            return value.toUpperCase();
        },
        formatDate: function (value, fmt) {
            if (value == null) return ''
            fmt = (typeof fmt == 'undefined') ? 'DD/MM/YYYY hh:mmA' : fmt
            return moment(value, 'DD/MM/YYYY hh:mm:ss A').format(fmt);
        },
        intCallback: function (value) {
            return (value == '') ? '' : parseInt(value);
        },
        entryTypeCallback: function (value) {
            switch (value) {
                case "IN":
                    return '<span class="label label-success"><i class="glyphicon glyphicon-arrow-left"></i> IN</span> '
                    break;
                case "OUT":
                    return '<span class="label label-danger"><i class="glyphicon glyphicon-arrow-right"></i> OUT</span> '
                    break;
            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        // -------------------------------------------------------------------------------------------
        // You can change how sort params string is constructed by overriding getSortParam() like this
        // -------------------------------------------------------------------------------------------
        // getSortParam: function(sortOrder) {
        //     console.log('parent getSortParam:', JSON.stringify(sortOrder))
        //     return sortOrder.map(function(sort) {
        //         return (sort.direction === 'desc' ? '+' : '') + sort.field
        //     }).join(',')
        // }
    },
    events: {
        'vuetable:action': function (action, data) {

        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            //var searchValue = document.getElementById('inputSearchKeyId').value;
            //var searchHeader = document.getElementById('slSearch').value;

            //if (searchValue !== '') {
            //    for (n in data) {
            //        if (searchHeader == "entryType") {
            //            return;
            //        } else if (searchHeader == "tbl_asset.name") {
            //            data[n]["assetName"] = this.highlight(searchValue, data[n]["assetName"]);
            //        } else if (searchHeader == "tbl_location.code") {
            //            data[n]["locationCode"] = this.highlight(searchValue, data[n]["locationCode"]);
            //        } else if (searchHeader == "tbl_user.username") {
            //            data[n]["createdBy"] = this.highlight(searchValue, data[n]["createdBy"]);
            //        } else {
            //            data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
            //        }
            //    }
            //}
        },
    }
})

$(document).ready(function () {
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > STOCK CONTROL > MOVEMENT HISTORY");

    //reloadSlSearch(slSearchOptions);
    setFilterVuetable(mhVuetable);

    $(function () {
        $("#selectUserLocation").on('change', function () {
            waitingDialog2.show();
            setWarehouseIdSession();
            setWarehouseCodeSession();
            filterTable();
            waitingDialog2.hide();
        });
    });

    setElementChkOnChange("chkMHDocId", "tbMHDocId");
    setElementChkOnChange("chkMHSubDocId", "tbMHSubDocId");
    setElementChkOnChange("chkMHLine", "tbMHLine");
    setElementChkOnChange("chkMHItemId", "tbMHItemId");
    setElementChkOnChange("chkMHPartCode", "tbMHPartCode");
    setElementChkOnChange("chkMHLocFrom", "tbMHParttbMHLocFromCode");
    setElementChkOnChange("chkMHLocTo", "tbMHLocTo");
    setElementChkOnChange("chkMHQty", "tbMHQty");
    setElementChkOnChange("chkMHUser", "tbMHUser");
    setElementChkOnChange("chkMHType", "tbMHType");
    setElementChkOnChange("chkMHDate", "divMHDate");

});

function setElementChkOnChange(cId,dId) {

    $(function () {
        $("#" + cId).on('change', function () {
            if (document.getElementById(cId).checked) {
                setElementIdVisible("#" + dId, true);
            } else {
                setElementIdVisible("#" + dId, false);
                $("#" + dId).val("");
            }
        }).change();

    });

    $('#btnMHSearch').click(function () {
        filterTable();
    })
}

function filterTable() {
    var tempSelectUserLocation = document.getElementById('selectUserLocation');
    var selectedUserLocationText = tempSelectUserLocation.options[tempSelectUserLocation.selectedIndex].text;

    var strFilterHeader = 'id';
    var strFilterContent = '';
    var strFilterContent2 = '';

    if (document.getElementById('chkMHDocId').checked) {
        strFilterHeader += ',axDOCUMENTID';
        strFilterContent += ',' + document.getElementById('tbMHDocId').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHSubDocId').checked) {
        strFilterHeader += ',axSubDOCUMENTID';
        strFilterContent += ',' + document.getElementById('tbMHSubDocId').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHLine').checked) {
        strFilterHeader += ',axLINENUM';
        strFilterContent += ',' + document.getElementById('tbMHLine').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHItemId').checked) {
        strFilterHeader += ',axITEMID';
        strFilterContent += ',' + document.getElementById('tbMHItemId').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHPartCode').checked) {
        strFilterHeader += ',axPART_CODE';
        strFilterContent += ',' + document.getElementById('tbMHPartCode').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHLocFrom').checked) {
        strFilterHeader += ',axWMSLOCATION_FROMID';
        strFilterContent += ',' + document.getElementById('tbMHLocFrom').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHLocTo').checked) {
        strFilterHeader += ',axWMSLOCATION_TOID';
        strFilterContent += ',' + document.getElementById('tbMHLocTo').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHQty').checked) {
        strFilterHeader += ',[qty]';
        strFilterContent += ',' + document.getElementById('tbMHQty').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHUser').checked) {
        strFilterHeader += ',tbl_username';
        strFilterContent += ',' + document.getElementById('tbMHUser').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHType').checked) {
        strFilterHeader += ',movementType';
        strFilterContent += ',' + document.getElementById('tbMHType').value;
        strFilterContent2 += ',';
    }
    if (document.getElementById('chkMHDate').checked) {
        strFilterHeader += ',movementDatetime';
        strFilterContent += ',' + document.getElementById('tbMHDateRangeFrom').value;
        strFilterContent2 += ',' + document.getElementById('tbMHDateRangeTo').value;
    }
    //if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
    //    mhVuetable.moreParams = [
    //        'filter=' + strSearchColumnName + '|' + document.getElementById('tbSearchDateRangeFrom').value + '|' + document.getElementById('tbSearchDateRangeTo').value,
    //        'filterType=' + slSearchOptions[e.selectedIndex].optType
    //    ];
    //} else if ($('#divSearchDateRange').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
    //    mhVuetable.moreParams = [
    //        'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
    //        'filterType=' + slSearchOptions[e.selectedIndex].optType
    //    ];
    //} else if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDateRange').hasClass('hidden')) {
    //    mhVuetable.moreParams = [
    //        'filter=' + strSearchColumnName + '|' + document.getElementById('slSearchDropdown').value,
    //        'filterType=' + slSearchOptions[e.selectedIndex].optType
    //    ];
    //}

    var strFilter = 'filter=' + strFilterHeader + '|' +strFilterContent + '|' + strFilterContent2;
    mhVuetable.moreParams = [
        strFilter, 'warehouseCode=' + selectedUserLocationText, 'showEntries=' + document.getElementById('selectMHShowEntries').value
    ];

    mhVuetable.$nextTick(function () {
        mhVuetable.$broadcast('vuetable:refresh');
    });

    //if ($('#divSearchInput').hasClass('hidden')) {
    //    mhVuetable.moreParams = [
    //        'filter=' + strSearchColumnName + '|' + document.getElementById('tbSearchDateRangeFrom').value + '|' + document.getElementById('tbSearchDateRangeTo').value
    //    ];
    //} else {
    //    mhVuetable.moreParams = [
    //        'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value
    //    ];
    //}
}
