﻿Vue.config.async = false;
var viewGRVue = new Vue({
    el: "#appViewGR",
    data: {
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        addRow: function (item_Id, item_code, description, uom, receive_quantity, location) {
            this.rows.push({
                item_Id: item_Id, item_code: item_code, description: description, uom: uom, receive_quantity: receive_quantity, location: location
            });
        },
        removeRow: function (row) {
            this.rows.$remove(row);
        }
    }
});


$(document).ready(function () {

    //$("#btnGRUndo").click(function () {
    //    undoGR();
    //});

    $('#divViewGRModalId').on('hidden.bs.modal', function () {
        clearAllViewGR();
    })
});

function clearAllViewGR() {
    clearViewGRInput();
    for (i = viewGRVue.rows.length - 1; i >= 0; i--) {
        viewGRVue.rows.$remove(viewGRVue.rows[i]);
    }
}

function clearViewGRInput() {
    document.getElementById('tbViewGRNumber').value = "";
    document.getElementById('tbViewGRPONumber').value = "";
    document.getElementById('tbViewGRReceiveDate').value = "";
    document.getElementById('tbEditGRCreatedBy').value = "";
    document.getElementById('tbViewGRDONumber').value = "";
}
