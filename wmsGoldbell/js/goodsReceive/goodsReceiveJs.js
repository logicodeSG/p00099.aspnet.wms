﻿
// fields definition
var tableColumns = [
    {
        name: 'id',
        title: 'ID',
        sortField: 'tbl_goodsReceive.id',
        dataClass: 'text-center',
        visible: false
        //callback: 'showDetailRow'
    },
    {
        name: 'referenceNo',
        title: 'GR#',
        sortField: 'referenceNo',
    },
    {
        name: 'poReferenceNo',
        title: 'PO#',
        sortField: 'poReferenceNo',
    },
    {
        name: 'createdBy',
        title: 'Created By',
        sortField: 'createdBy'
    },
    {
        name: 'receiveDatetime',
        title: 'Received Date',
        sortField: 'receiveDatetime',
        callback: 'formatDate'
    },
    {
        name: 'tbl_status_Id',
        title: 'Status',
        sortField: 'tbl_status_Id',
        callback: 'statusCallback'
    },
    {
        name: 'doReferenceNo',
        title: 'DO#',
        sortField: 'doReferenceNo',
    },
    {
        name: '__actions',
        title: 'ACTION',
        dataClass: 'text-center',
    }
]

var slSearchOptions = [
    {
        optValue: "tbl_goodsReceive.referenceNo",
        optText: tableColumns[1].title,
        optType: ''
    },
    {
        optValue: "tbl_ax_po.referenceNo",
        optText: tableColumns[2].title,
        optType: ''
    },
    {
        optValue: "tbl_user.username",
        optText: tableColumns[3].title,
        optType: ''
    },
    {
        optValue: "receiveDatetime",
        optText: tableColumns[4].title,
        optType: 'DATE'
    },
    {
        optValue: 'tbl_goodsReceive.tbl_status_Id',
        optText: tableColumns[5].title,
        optType: 'DROPDOWN',
        optDDData: ['COMPLETED', 'UNDO']
    },
    {
        optValue: "tbl_goodsReceive.doReferenceNo",
        optText: tableColumns[6].title,
        optType: ''
    }
]

var grVuetable = new Vue({
    el: '#appGRDivId',
    data: {
        columns: tableColumns,
        sortOrder: [{
            field: 'tbl_goodsReceive.tbl_status_Id asc, tbl_goodsReceive.id ',
            direction: 'desc'
        }],
        multiSort: true,
        perPage: 10,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        actions: [
            //{ name: 'assign-item', label: '', icon: 'glyphicon glyphicon-paperclip', class: 'btn btn-primary', extra: { 'title': 'Assign', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            //{ name: 'print-item', label: '', icon: 'glyphicon glyphicon-print', class: 'btn btn-info', extra: { 'title': 'Print', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            {
                name: 'view-goodsReceive', label: '', icon: 'glyphicon glyphicon-info-sign', class: 'label label-info'
                , extra: { 'title': 'View', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },
        ],
        moreParams: [
        'filter=tbl_goodsReceive.id|',
        'filterType='
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        /**
         * Callback functions
         */
        allCap: function (value) {
            return value.toUpperCase()
        },
        gender: function (value) {
            return value == 'True'
              ? '<span class="label label-info"><i class="glyphicon glyphicon-star"></i> TRUE</span>'
              : '<span class="label label-success"><i class="glyphicon glyphicon-heart"></i> FALSE</span>'
        },
        formatDate: function (value, fmt) {
            if (value == null) return ''
            fmt = (typeof fmt == 'undefined') ? 'DD/MM/YYYY' : fmt
            return moment(value, 'DD/MM/YYYY hh:mm:ss A').format(fmt)
        },
        statusCallback: function (value) {
            switch (value) {
                case "1":
                    return '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> COMPLETED</span>'
                    break;
                case "3":
                    return '<span class="label label-danger"><i class="glyphicon glyphicon-arrow-left"></i> UNDO</span>'
                    break;

            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        // -------------------------------------------------------------------------------------------
        // You can change how sort params string is constructed by overriding getSortParam() like this
        // -------------------------------------------------------------------------------------------
        // getSortParam: function(sortOrder) {
        //     console.log('parent getSortParam:', JSON.stringify(sortOrder))
        //     return sortOrder.map(function(sort) {
        //         return (sort.direction === 'desc' ? '+' : '') + sort.field
        //     }).join(',')
        // }
    },
    events: {
        'vuetable:action': function (action, data) {
            //console.log('vuetable:action', action, data)

            if (action == 'view-goodsReceive') {
                loadViewGR(data.id, data.tbl_status_Id);
            }
        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            var searchValue = document.getElementById('inputSearchKeyId').value;
            var searchHeader = document.getElementById('slSearch').value;

            if (searchValue !== '') {
                for (n in data) {
                    if (searchHeader == "tbl_goodsReceive.referenceNo") {
                        data[n]["referenceNo"] = this.highlight(searchValue, data[n]["referenceNo"]);
                    } else if (searchHeader == "tbl_ax_po.referenceNo") {
                        data[n]["poReferenceNo"] = this.highlight(searchValue, data[n]["poReferenceNo"]);
                    } else if (searchHeader == "tbl_user.username") {
                        data[n]["createdBy"] = this.highlight(searchValue, data[n]["createdBy"]);
                    } else if (searchHeader == "tbl_goodsReceive.doReferenceNo") {
                        data[n]["doReferenceNo"] = this.highlight(searchValue, data[n]["doReferenceNo"]);
                    } else {
                        data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
                    }
                }
            }
        },
    }
})

$(document).ready(function () {
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > STOCK CONTROL > GOODS RECEIVE");

    reloadSlSearch(slSearchOptions);
    setFilterVuetable(grVuetable);

    $('#inputSearchKeyId').keypress(function (event) {
        if (event.keyCode == 13) {
            filterTable();
        }
    });

    $(function () {
        $("#selectUserLocation").on('change', function () {
            setWarehouseIdSession();
            setWarehouseCodeSession();
            filterTable();
        });

    });


    //$('#divNewGRModalId').on('hidden.bs.modal', function () {
    //    grVuetable.$broadcast('vuetable:refresh');
    //})


});

function loadViewGR(grId, statusId) {
    $.ajax({
        url: '/ws/ims-ws.asmx/getGRByGRId',
        contentType: "application/json",
        data: JSON.stringify({ argGRId: grId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                $('#hfViewGRId').val(returnData.d[0].grId);
                $('#tbViewGRNumber').val(returnData.d[0].grReferenceNo);
                $('#tbViewGRPONumber').val(returnData.d[0].grPOReferenceNo);
                $('#tbViewGRDONumber').val(returnData.d[0].grDOReferenceNo);
                $('#tbViewGRReceiveDate').val(formatDate(returnData.d[0].grReceiveDate));
                $('#tbEditGRCreatedBy').val(returnData.d[0].grCreatedBy);

                loadViewGRDetail(grId, statusId);
            } else {
                swal(
                        'Load Goods Receive - Fail',
                        'Failed in loading selected goods receive',
                        'error'
                        );
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function loadViewGRDetail(grId, statusId) {
    $.ajax({
        url: '/ws/ims-ws.asmx/getGRDetailByGRId',
        contentType: "application/json",
        data: JSON.stringify({ argGRId: grId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                for (var i = 0; i < returnData.d.length; i++) {
                    viewGRVue.addRow(returnData.d[i].grDetailItemId, returnData.d[i].grDetailItemCode, returnData.d[i].grDetailItemDescription,
                        returnData.d[i].grDetailItemUOM, returnData.d[i].grDetailItemReceiveQuantity, returnData.d[i].grDetailItemLocationName);
                }
                $("#divViewGRModalId").modal({ backdrop: 'static', keyboard: false });
            } else {
                swal(
                        'Load Goods Receive - Fail',
                        'Failed in loading selected goods receive',
                        'error'
                        );
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function filterTable() {
    var e = document.getElementById("slSearch");
    var strSearchColumnName = e.options[e.selectedIndex].value;

    if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        grVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('tbSearchDateRangeFrom').value + '|' + document.getElementById('tbSearchDateRangeTo').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType
        ];
    } else if ($('#divSearchDateRange').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        grVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType
        ];
    } else if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDateRange').hasClass('hidden')) {
        grVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('slSearchDropdown').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType
        ];
    }

    grVuetable.$nextTick(function () {
        grVuetable.$broadcast('vuetable:refresh');
    });
}


function chkGRShowAll_onChanged() {
    filterTable();
}

