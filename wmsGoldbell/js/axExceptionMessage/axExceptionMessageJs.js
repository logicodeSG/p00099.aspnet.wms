﻿
// fields definition
var tableColumns = [
    //{
    //    name: 'id',
    //    title: 'ID',
    //    sortField: 'tbl_ax_aj.id',
    //    dataClass: 'text-center',
    //    visible: false
    //    //callback: 'showDetailRow'
    //},
    {
        name: 'MESSAGEID',
        title: 'MESSAGE ID',
        sortField: 'MESSAGEID',
    },
    {
        name: 'EXCEPTIONID',
        title: 'EXCEPTION ID',
        sortField: 'EXCEPTIONID',
    },
    {
        name: 'PORTNAME',
        title: 'PORT NAME',
        sortField: 'PORTNAME',
    },
    {
        name: 'DESCRIPTION',
        title: 'DESCRIPTION',
        sortField: 'DESCRIPTION',
    },
    {
        name: 'SUBSYSTEM',
        title: 'SUB SYSTEM',
        sortField: 'SUBSYSTEM',
    },
    {
        name: 'CREATEDDATETIME',
        title: 'CREATED DATETIME',
        sortField: 'CREATEDDATETIME',
        callback: 'formatDatetime'
    },
    //{
    //    name: '__actions',
    //    title: 'ACTION',
    //    dataClass: 'text-center',
    //}
]

var slSearchOptions = [
    {
        optValue: tableColumns[0].name,
        optText: tableColumns[0].title,
        optType: ''
    },
    {
        optValue: tableColumns[1].name,
        optText: tableColumns[1].title,
        optType: ''
    },
    {
        optValue: tableColumns[2].name,
        optText: tableColumns[2].title,
        optType: ''
    },
    {
        optValue: tableColumns[3].name,
        optText: tableColumns[3].title,
        optType: ''
    },
    {
        optValue: tableColumns[4].name,
        optText: tableColumns[4].title,
        optType: ''
    },
    {
        optValue: "CREATEDDATETIME",
        optText: tableColumns[5].title,
        optType: 'DATE'
    }
]

var axExceptionMessageVuetable = new Vue({
    el: '#appAXExceptionMessageDivId',
    data: {
        columns: tableColumns,
        sortOrder: [{
            field: 'EXCEPTIONID ',
            direction: 'desc'
        }],
        multiSort: true,
        perPage: 7,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        actions: [
            //{ name: 'assign-item', label: '', icon: 'glyphicon glyphicon-paperclip', class: 'btn btn-primary', extra: { 'title': 'Assign', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            //{ name: 'print-item', label: '', icon: 'glyphicon glyphicon-print', class: 'btn btn-info', extra: { 'title': 'Print', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            //{
            //    name: 'view-ajGoodsReceive', label: '', icon: 'glyphicon glyphicon-info-sign', class: 'label label-info'
            //    , extra: { 'title': 'View', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            //},
        ],
        moreParams: [
            'filter=EXCEPTIONID|',
            'filterType='
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        /**
         * Callback functions
         */
        allCap: function (value) {
            return value.toUpperCase()
        },
        gender: function (value) {
            return value == 'True'
              ? '<span class="label label-info"><i class="glyphicon glyphicon-star"></i> TRUE</span>'
              : '<span class="label label-success"><i class="glyphicon glyphicon-heart"></i> FALSE</span>'
        },
        formatDatetime: function (value, fmt) {
            if (value == null) return ''
            fmt = (typeof fmt == 'undefined') ? 'DD/MM/YYYY hh:mm:ss A' : fmt
            return moment(value, 'DD/MM/YYYY hh:mm:ss A').format(fmt)
        },
        statusCallback: function (value) {
            switch (value) {
                case "1":
                    return '<span class="label label-info"><i class="glyphicon glyphicon-refresh"></i> PENDING</span>'
                    break;
                case "9":
                    return '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> COMPLETED</span>'
                    break;

            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        // -------------------------------------------------------------------------------------------
        // You can change how sort params string is constructed by overriding getSortParam() like this
        // -------------------------------------------------------------------------------------------
        // getSortParam: function(sortOrder) {
        //     console.log('parent getSortParam:', JSON.stringify(sortOrder))
        //     return sortOrder.map(function(sort) {
        //         return (sort.direction === 'desc' ? '+' : '') + sort.field
        //     }).join(',')
        // }
    },
    events: {
        'vuetable:action': function (action, data) {
            //console.log('vuetable:action', action, data)

            if (action == 'view-ajGoodsReceive') {
                loadViewAJGR(data.axJOURNALID, data.tbl_status_Id);
            }
        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            var searchValue = document.getElementById('inputSearchKeyId').value;
            var searchHeader = document.getElementById('slSearch').value;

            if (searchValue !== '') {
                for (n in data) {
                    if (searchHeader == "tbl_ax_aj.axJOURNALID") {
                        //data[n]["axJOURNALID"] = this.highlight(searchValue, data[n]["axJOURNALID"]);
                    } else {
                        data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
                    }
                }
            }
        },
    }
})

$(document).ready(function () {
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > AX EXCEPTION MESSAGE");

    reloadSlSearch(slSearchOptions);
    setFilterVuetable(axExceptionMessageVuetable);

    $('#inputSearchKeyId').keypress(function (event) {
        if (event.keyCode == 13) {
            filterTable();
        }
    });


});


function filterTable() {
    var e = document.getElementById("slSearch");
    var strSearchColumnName = e.options[e.selectedIndex].value;


    if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        axExceptionMessageVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('tbSearchDateRangeFrom').value + '|' + document.getElementById('tbSearchDateRangeTo').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType
        ];
    } else if ($('#divSearchDateRange').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        axExceptionMessageVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType
        ];
    } else if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDateRange').hasClass('hidden')) {
        axExceptionMessageVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('slSearchDropdown').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType
        ];
    }

    axExceptionMessageVuetable.$nextTick(function () {
        axExceptionMessageVuetable.$broadcast('vuetable:refresh');
    });
}