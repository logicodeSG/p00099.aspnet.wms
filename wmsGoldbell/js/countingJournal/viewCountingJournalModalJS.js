﻿Vue.config.async = false;
var viewCJVue = new Vue({
    el: "#appViewCJ",
    data: {
        //cb_print: [],
        total2Print: 0,
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        cbPrintCheck: function (e, row) {
            calculateTotal2Print();
            //if (e.target.checked) {
            //    this.rows.color = 'lightblue';
            //} else {
            //    this.rows.color = '';
            //}
        },
        addRow: function (line_num, item_id, part_code, uom, qty, scan_qty, scan_datetime, user) {
            this.rows.push({
                line_num: line_num, item_id: item_id, part_code: part_code, uom: uom,
                qty: qty, scan_qty: scan_qty, scan_datetime: scan_datetime, user: user
            });
        },
        removeRow: function (row) {
            this.rows.$remove(row);
        }
    }
});


$(document).ready(function () {

    $("#btnCJPrint").click(function () {
        printCJ();
    });

    $('#divViewCJModalId').on('hidden.bs.modal', function () {
        clearAllViewCJ();
    })
});

function cbAllOnClick() {
    var isChecked = document.getElementById('cbAll').checked;
    for (i = 0; i < viewCJVue.rows.length; i++) {
        document.getElementById('cbPrint' + (i + 1)).checked = isChecked;
        //document.getElementById('cbPrint' + (i + 1)).dispatchEvent(new Event('change'));
    }
    calculateTotal2Print();
}


function calculateTotal2Print() {
    var tempTotal = 0;
    for (i = 0; i < viewCJVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            tempTotal += 1;
        }
    }
    viewCJVue.total2Print = tempTotal;
}

function printCJ() {
    for (i = 0; i < viewCJVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            console.log("Printing item Id =" + document.getElementById('tbViewCJItemId' + (i + 1)).value +
               " Part Code = " + document.getElementById('tbViewCJPartCode' + (i + 1)).value)
        }
    }
}

function clearAllViewCJ() {
    clearViewCJInput();
    for (i = viewCJVue.rows.length - 1; i >= 0; i--) {
        viewCJVue.rows.$remove(viewCJVue.rows[i]);
    }
}

function clearViewCJInput() {
    document.getElementById('tbViewCJJournalId').value = "";
    document.getElementById('hfViewAXCJId').value = "";
    document.getElementById('tbViewCJPostedDate').value = "";
    document.getElementById('cbAll').checked = false;
}


