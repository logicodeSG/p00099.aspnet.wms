﻿
// fields definition
var tableColumns = [
    {
        name: 'id',
        title: 'ID',
        sortField: 'tbl_ax_aj.id',
        dataClass: 'text-center',
        visible: false
        //callback: 'showDetailRow'
    },
    {
        name: 'axJOURNALID',
        title: 'JOURNAL ID',
        sortField: 'tbl_ax_aj.axJOURNALID',
    },
    {
        name: 'axCREATEDDATETIME',
        title: 'Created Date',
        sortField: 'tbl_ax_aj.axCREATEDDATETIME',
        callback: 'formatDate'
    },
    {
        name: 'tbl_status_Id',
        title: 'Status',
        sortField: 'tbl_ax_aj.tbl_status_Id',
        callback: 'statusCallback'
    },
    {
        name: '__actions',
        title: 'ACTION',
        dataClass: 'text-center',
    }
]

var slSearchOptions = [
    {
        optValue: "tbl_ax_aj.axJOURNALID",
        optText: tableColumns[1].title,
        optType: ''
    },
    {
        optValue: "tbl_ax_aj.axCREATEDDATETIME",
        optText: tableColumns[2].title,
        optType: 'DATE'
    },
    {
        optValue: 'tbl_ax_aj.tbl_status_Id',
        optText: tableColumns[3].title,
        optType: 'DROPDOWN',
        optDDData: ['PENDING', 'COMPLETED']
    }
]

var ajgrVuetable = new Vue({
    el: '#appAJGRDivId',
    data: {
        columns: tableColumns,
        sortOrder: [{
            field: 'tbl_ax_aj.tbl_status_Id asc, tbl_ax_aj.id ',
            direction: 'desc'
        }],
        multiSort: true,
        perPage: 10,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        actions: [
            //{ name: 'assign-item', label: '', icon: 'glyphicon glyphicon-paperclip', class: 'btn btn-primary', extra: { 'title': 'Assign', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            //{ name: 'print-item', label: '', icon: 'glyphicon glyphicon-print', class: 'btn btn-info', extra: { 'title': 'Print', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            {
                name: 'view-ajGoodsReceive', label: '', icon: 'glyphicon glyphicon-info-sign', class: 'label label-info'
                , extra: { 'title': 'View', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },
        ],
        moreParams: [
            'filter=tbl_ax_aj.id|',
            'filterType=', 'warehouseCode=-1',
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        /**
         * Callback functions
         */
        allCap: function (value) {
            return value.toUpperCase()
        },
        gender: function (value) {
            return value == 'True'
              ? '<span class="label label-info"><i class="glyphicon glyphicon-star"></i> TRUE</span>'
              : '<span class="label label-success"><i class="glyphicon glyphicon-heart"></i> FALSE</span>'
        },
        formatDate: function (value, fmt) {
            if (value == null) return ''
            fmt = (typeof fmt == 'undefined') ? 'DD/MM/YYYY' : fmt
            return moment(value, 'DD/MM/YYYY hh:mm:ss A').format(fmt)
        },
        statusCallback: function (value) {
            switch (value) {
                case "1":
                    return '<span class="label label-info"><i class="glyphicon glyphicon-refresh"></i> PENDING</span>'
                    break;
                case "9":
                    return '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> COMPLETED</span>'
                    break;

            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        // -------------------------------------------------------------------------------------------
        // You can change how sort params string is constructed by overriding getSortParam() like this
        // -------------------------------------------------------------------------------------------
        // getSortParam: function(sortOrder) {
        //     console.log('parent getSortParam:', JSON.stringify(sortOrder))
        //     return sortOrder.map(function(sort) {
        //         return (sort.direction === 'desc' ? '+' : '') + sort.field
        //     }).join(',')
        // }
    },
    events: {
        'vuetable:action': function (action, data) {
            //console.log('vuetable:action', action, data)

            if (action == 'view-ajGoodsReceive') {
                loadViewAJGR(data.axJOURNALID, data.tbl_status_Id);
            }
        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            var searchValue = document.getElementById('inputSearchKeyId').value;
            var searchHeader = document.getElementById('slSearch').value;

            if (searchValue !== '') {
                for (n in data) {
                    if (searchHeader == "tbl_ax_aj.axJOURNALID") {
                        //data[n]["axJOURNALID"] = this.highlight(searchValue, data[n]["axJOURNALID"]);
                    } else {
                        data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
                    }
                }
            }
        },
    }
})

$(document).ready(function () {
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > STOCK CONTROL > GOODS RECEIVE (AJ)");

    reloadSlSearch(slSearchOptions);
    setFilterVuetable(ajgrVuetable);

    $('#inputSearchKeyId').keypress(function (event) {
        if (event.keyCode == 13) {
            filterTable();
        }
    });

    $(function () {
        $("#selectUserLocation").on('change', function () {
            setWarehouseIdSession();
            setWarehouseCodeSession();
            filterTable();
        });

    });


    //$('#divNewGRModalId').on('hidden.bs.modal', function () {
    //    ajgrVuetable.$broadcast('vuetable:refresh');
    //})


});

function loadViewAJGR(journalId, statusId) {
    $.ajax({
        url: '/ws/ims-ws.asmx/getAXAJByAXJournalId',
        contentType: "application/json",
        data: JSON.stringify({ argAXJOURNALID: journalId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                $('#hfViewAXAJId').val(returnData.d[0].ajId);
                $('#tbViewAJGRJournalId').val(returnData.d[0].ajAX_JournalId);
                $('#tbViewAJGRCreatedDate').val(returnData.d[0].ajAX_CreationDatetime);
                $('#tbViewAJGRPackingSlip').val(returnData.d[0].ajAX_PackingSlip);
                
                loadViewAJGRDetail(journalId, statusId);
            } else {
                swal(
                        'Load Goods Receive (AJ) - Fail',
                        'Failed in loading selected goods receive (AJ)',
                        'error'
                        );
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function loadViewAJGRDetail(journalId, statusId) {
    waitingDialog.show();
    $.ajax({
        url: '/ws/ims-ws.asmx/getAXAJDetailByAXJournalId',
        contentType: "application/json",
        data: JSON.stringify({ argAXJOURNALID: journalId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                for (var i = 0; i < returnData.d.length; i++) {
                    viewAJGRVue.addRow(
                        returnData.d[i].ajdAX_LineNumber,
                        returnData.d[i].ajdAX_PurchId,
                        returnData.d[i].ajdAX_ItemId,
                        returnData.d[i].ajdAX_God_PartCode,
                        returnData.d[i].ajdAX_Qty,
                        returnData.d[i].ajdScanQuantity,
                        returnData.d[i].ajdScanDatetime,
                        returnData.d[i].ajdUsername);
                }
                $("#divViewAJGRModalId").modal({ backdrop: 'static', keyboard: false });
            } else {
                swal(
                        'Load Goods Receive (AJ) - Fail',
                        'Failed in loading selected goods receive (AJ)',
                        'error'
                        );
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        },
        complete: function (data) {
            waitingDialog.hide();
        }
    });
}

function filterTable() {
    var e = document.getElementById("slSearch");
    var strSearchColumnName = e.options[e.selectedIndex].value;

    var tempSelectUserLocation = document.getElementById('selectUserLocation');
    var selectedUserLocationText = tempSelectUserLocation.options[tempSelectUserLocation.selectedIndex].text;

    if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        ajgrVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('tbSearchDateRangeFrom').value + '|' + document.getElementById('tbSearchDateRangeTo').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType,
            'warehouseCode=' + selectedUserLocationText
        ];
    } else if ($('#divSearchDateRange').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        ajgrVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType,
            'warehouseCode=' + selectedUserLocationText
        ];
    } else if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDateRange').hasClass('hidden')) {
        ajgrVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('slSearchDropdown').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType,
            'warehouseCode=' + selectedUserLocationText
        ];
    }

    ajgrVuetable.$nextTick(function () {
        ajgrVuetable.$broadcast('vuetable:refresh');
    });
}


function chkAJGRShowAll_onChanged() {
    filterTable();
}

