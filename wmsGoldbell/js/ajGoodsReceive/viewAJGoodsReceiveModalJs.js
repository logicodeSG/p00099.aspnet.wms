﻿Vue.config.async = false;
var viewAJGRVue = new Vue({
    el: "#appViewAJGR",
    data: {
        //cb_print: [],
        total2Print: 0,
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        cbPrintCheck: function (e, row) {
            calculateTotal2Print();
            //if (e.target.checked) {
            //    this.rows.color = 'lightblue';
            //} else {
            //    this.rows.color = '';
            //}
        },
        addRow: function (line_num, purch_id, item_id, part_code, qty, scan_qty, scan_datetime, user) {
            this.rows.push({
                line_num: line_num, purch_id: purch_id, item_id: item_id, part_code: part_code,
                qty: qty, scan_qty: scan_qty, scan_datetime: scan_datetime, user: user
            });
        },
        removeRow: function (row) {
            this.rows.$remove(row);
        }
    }
});


$(document).ready(function () {

    $("#btnAJGRPrint").click(function () {
        printAJGR();
    });

    $('#divViewAJGRModalId').on('hidden.bs.modal', function () {
        clearAllViewAJGR();
    })
});

function cbAllOnClick() {
    var isChecked = document.getElementById('cbAll').checked;
    for (i = 0; i < viewAJGRVue.rows.length; i++) {
        document.getElementById('cbPrint' + (i + 1)).checked = isChecked;
        //document.getElementById('cbPrint' + (i + 1)).dispatchEvent(new Event('change'));
    }
    calculateTotal2Print();
}


function calculateTotal2Print() {
    var tempTotal = 0;
    for (i = 0; i < viewAJGRVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            tempTotal += 1;
        }
    }
    viewAJGRVue.total2Print = tempTotal;
}

function printAJGR() {
    for (i = 0; i < viewAJGRVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            console.log("Printing item Id =" + document.getElementById('tbViewAJGRItemId' + (i + 1)).value +
               " Part Code = " + document.getElementById('tbViewAJGRPartCode' + (i + 1)).value)
        }
    }
}

function clearAllViewAJGR() {
    clearViewAJGRInput();
    for (i = viewAJGRVue.rows.length - 1; i >= 0; i--) {
        viewAJGRVue.rows.$remove(viewAJGRVue.rows[i]);
    }
}

function clearViewAJGRInput() {
    document.getElementById('tbViewAJGRJournalId').value = "";
    document.getElementById('hfViewAXAJId').value = "";
    document.getElementById('tbViewAJGRCreatedDate').value = "";
    document.getElementById('tbViewAJGRPackingSlip').value = "";
    document.getElementById('cbAll').checked = false;
}


