﻿
// fields definition
var tableColumns = [
    {
        name: 'id',
        title: 'ID',
        sortField: 'tbl_purchaseOrder.id',
        dataClass: 'text-center',
        visible: false
        //callback: 'showDetailRow'
    },
    {
        name: 'referenceNo',
        title: 'PO#',
        sortField: 'referenceNo',
    },
    {
        name: 'poDatetime',
        title: 'Date',
        sortField: 'poDatetime',
        callback: 'formatDate'
    },
    {
        name: 'tbl_status_Id',
        title: 'Status',
        sortField: 'tbl_status_Id',
        callback: 'statusCallback',
        visible: false
    },
    {
        name: 'customizeStatus',
        title: 'Status',
        sortField: 'customizeStatus',
        callback: 'customizeStatusCallback'
    },
    {
        name: '__actions',
        title: 'ACTION',
        dataClass: 'text-center',
    }
]

var slSearchOptions = [
    {
        optValue: tableColumns[1].name,
        optText: tableColumns[1].title,
        optType: ''
    },
    {
        optValue: "poDatetime",
        optText: tableColumns[2].title,
        optType: 'DATE'
    },
    {
        optValue: "tbl_ax_po.tbl_status_Id",
        optText: tableColumns[4].title,
        optType: 'DROPDOWN',
        optDDData: ['NEW', 'CANCELED', 'PARTIAL', 'COMPLETED','SYNCED']
    }
]

var poVuetable = new Vue({
    el: '#appPODivId',
    data: {
        columns: tableColumns,
        sortOrder: [{
            field: 'tbl_ax_po.tbl_status_Id asc, tbl_ax_po.id ',
            direction: 'desc'
        }],
        multiSort: true,
        perPage: 10,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        actions: [
            //{ name: 'assign-item', label: '', icon: 'glyphicon glyphicon-paperclip', class: 'btn btn-primary', extra: { 'title': 'Assign', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            //{ name: 'print-item', label: '', icon: 'glyphicon glyphicon-print', class: 'btn btn-info', extra: { 'title': 'Print', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" } },
            {
                name: 'view-purchaseOrder', label: '', icon: 'glyphicon glyphicon-info-sign', class: 'label label-info'
                , extra: { 'title': 'View', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },
        ],
        moreParams: [
        'filter=tbl_ax_po.id|',
        'filterType='
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        /**
         * Callback functions
         */
        allCap: function (value) {
            return value.toUpperCase()
        },
        gender: function (value) {
            return value == 'True'
              ? '<span class="label label-info"><i class="glyphicon glyphicon-star"></i> TRUE</span>'
              : '<span class="label label-success"><i class="glyphicon glyphicon-heart"></i> FALSE</span>'
        },
        formatDate: function (value, fmt) {
            if (value == null) return ''
            fmt = (typeof fmt == 'undefined') ? 'DD/MM/YYYY' : fmt
            return moment(value, 'DD/MM/YYYY hh:mm:ss A').format(fmt)
        },
        statusCallback: function (value) {
            switch (value) {
                case "1":
                    return '<span class="label label-info"><i class="glyphicon glyphicon-repeat"></i> NEW</span>'
                    break;
                case "2":
                    return '<span class="label label-warning"><i class="glyphicon glyphicon-repeat"></i> PENDING</span>'
                    break;
                case "3":
                    return '<span class="label label-danger"><i class="glyphicon glyphicon-remove"></i> CANCELED</span>'
                    break;
                case "4":
                    return '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> READY</span>'
                    break;

            }
        },
        customizeStatusCallback: function (value) {
            var subValue = value.split("|");
            //receive is 0

            if (subValue[0] == "3") {
                return '<span class="label label-danger"><i class="glyphicon glyphicon-remove"></i> CANCELED</span>'
            }else if (subValue[0] == "4") {
                return '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> COMPLETED</span>'
            } else if (subValue[0] == "5") {
                return '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> SYNCED</span>'
            }


            if (parseInt(subValue[2]) == 0) {
                switch (subValue[0]) {
                    case "1":
                        return '<span class="label label-info"><i class="glyphicon glyphicon-repeat"></i> NEW</span>'
                        break;
                }
            }
            //    //if order quantity and receive quantity is the same
            //else if (parseInt(subValue[1]) == parseInt(subValue[2])) {
            //    switch (subValue[0]) {
            //        case "1":
            //            return '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> COMPLETED</span>'
            //            break;

            //    }
            //}
            else if (parseInt(subValue[2]) > 0) { //receive more then 0
                switch (subValue[0]) {
                    case "1":
                        return '<span class="label label-warning"><i class="glyphicon glyphicon-lock"></i> PARTIAL RECEIVE</span> '
                        break;

                }
            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        // -------------------------------------------------------------------------------------------
        // You can change how sort params string is constructed by overriding getSortParam() like this
        // -------------------------------------------------------------------------------------------
        // getSortParam: function(sortOrder) {
        //     console.log('parent getSortParam:', JSON.stringify(sortOrder))
        //     return sortOrder.map(function(sort) {
        //         return (sort.direction === 'desc' ? '+' : '') + sort.field
        //     }).join(',')
        // }
    },
    events: {
        'vuetable:action': function (action, data) {
            //console.log('vuetable:action', action, data)

            if (action == 'view-purchaseOrder') {
                waitingDialog.show();
                loadViewPO(data.id);
            }
        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            var searchValue = document.getElementById('inputSearchKeyId').value;
            var searchHeader = document.getElementById('slSearch').value;

            if (searchValue !== '') {
                for (n in data) {
                    if (searchHeader == "tbl_supplier.code") {
                        data[n]["supplierCode"] = this.highlight(searchValue, data[n]["supplierCode"]);
                    } else {
                        data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
                    }
                }
            }
        },
    }
})

$(document).ready(function () {
    setSystemTitle("INVENTORY MANAGEMENT SYSTEM > STOCK CONTROL > PURCHASE ORDER");

    reloadSlSearch(slSearchOptions);
    setFilterVuetable(poVuetable);

    $('#inputSearchKeyId').keypress(function (event) {
        if (event.keyCode == 13) {
            filterTable();
        }
    });

    $(function () {
        $("#selectUserLocation").on('change', function () {
            setWarehouseIdSession();
            setUserSelectedLocationByUserId();
            filterTable();
        });

    });

});

function loadViewPO(poId) {
    $.ajax({
        url: '/ws/ims-ws.asmx/getAXPOByAXPOId',
        contentType: "application/json",
        data: JSON.stringify({ argAXPOId: poId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                $('#hfViewPOId').val(returnData.d[0].poId);
                $('#tbViewPONumber').val(returnData.d[0].poReferenceNo);
                $('#tbViewPODate').val(formatDate(returnData.d[0].poDate));

                loadViewPODetail(poId, returnData.d[0].poStatusId);
            } else {
                swal(
                        'Load Purchase Order - Fail',
                        'Failed in loading selected purchase order',
                        'error'
                        );
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function loadViewPODetail(poId, statusId) {

    $.ajax({
        url: '/ws/ims-ws.asmx/getAXPODetailByAXPOId',
        contentType: "application/json",
        data: JSON.stringify({ argAXPOId: poId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                var remainingQty = 0;
                var hasReceiveQty = false;
                for (var i = 0; i < returnData.d.length; i++) {
                    viewPOVue.addRow(returnData.d[i].poDetailItemId, returnData.d[i].poDetailItemCode, returnData.d[i].poDetailItemDescription,
                        returnData.d[i].poDetailItemUOM, returnData.d[i].poDetailItemQuantity, returnData.d[i].poDetailItemReceivedQuantity);
                }

                $("#divViewPOModalId").modal({ backdrop: 'static', keyboard: false });
            } else {
                swal(
                        'Load Purchase Order - Fail',
                        'Failed in loading selected purchase order',
                        'error'
                        );
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        },
        complete: function (data) {
            waitingDialog.hide();
            //A function to be called when the request finishes 
            // (after success and error callbacks are executed). 
        }
    });
}

function filterTable() {
    var e = document.getElementById("slSearch");
    var strSearchColumnName = e.options[e.selectedIndex].value;

    if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        poVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('tbSearchDateRangeFrom').value + '|' + document.getElementById('tbSearchDateRangeTo').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType
        ];
    } else if ($('#divSearchDateRange').hasClass('hidden') && $('#divSearchDropdown').hasClass('hidden')) {
        poVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType
        ];
    } else if ($('#divSearchInput').hasClass('hidden') && $('#divSearchDateRange').hasClass('hidden')) {
        poVuetable.moreParams = [
            'filter=' + strSearchColumnName + '|' + document.getElementById('slSearchDropdown').value,
            'filterType=' + slSearchOptions[e.selectedIndex].optType
        ];
    }

    poVuetable.$nextTick(function () {
        poVuetable.$broadcast('vuetable:refresh');
    });
}


function chkPOShowAll_onChanged() {
    filterTable();
}
