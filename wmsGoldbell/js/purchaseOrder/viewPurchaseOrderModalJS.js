﻿Vue.config.async = false;
var viewPOVue = new Vue({
    el: "#appViewPO",
    data: {
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        addRow: function (item_Id, item_code, description, uom, quantity, received_quantity) {
            this.rows.push({
                item_Id: item_Id, item_code: item_code, description: description, uom: uom, quantity: quantity, received_quantity: received_quantity
            });
        },
        removeRow: function (row) {
            this.rows.$remove(row);
        }
    }
});


$(document).ready(function () {

    $('#divViewPOModalId').on('hidden.bs.modal', function () {
        clearAllViewPO();
    })
});

function clearAllViewPO() {
    clearViewPOInput();
    for (i = viewPOVue.rows.length - 1; i >= 0; i--) {
        viewPOVue.rows.$remove(viewPOVue.rows[i]);
    }
}

function clearViewPOInput() {
    document.getElementById('hfViewPOId').value = "";
    document.getElementById('tbViewPONumber').value = "";
    document.getElementById('tbViewPODate').value = "";
}
