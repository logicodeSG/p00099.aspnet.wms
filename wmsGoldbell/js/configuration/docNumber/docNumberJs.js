﻿Vue.config.async = false;
var settingDocNumberVue = new Vue({
    el: "#appDocNumber",
    data: {
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ],
        total: 0,
        gst: 0
    },
    computed: {
        total: function () {
            var t = 0;
            $.each(this.rows, function (i, e) {
                t += parseInt(e.quantity) * parseFloat(e.price);
            });
            return t;
        }
    },
    methods: {
        addRow: function (index) {
            this.rows.push({ price: 0 });
        },
        addRowNValue: function (id, type, prefix, numberLength, nextNumber) {
            this.rows.push({ id: id, type: type, prefix: prefix, numberLength: numberLength, nextNumber: nextNumber });
        },
        removeRow: function (row) {
            //console.log(row);
            this.rows.$remove(row);
        },
        resetRow: function (index) {

        }
    }

});

$(document).ready(function () {
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > CONFIGURATION > Document Number");

    initialAccessControl();
    loadDocNumber();

    $('#btnSettingDocNumberSave').click(function () {
        saveDocNumber();
    });
});

function initialAccessControl() {
    enableElementById("#btnSettingDocNumberSave", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
}

function saveDocNumber() {

    if (!inputVerification()) { return false };

    var docNumberIdList = [];
    var docNumberPrefixList = [];
    var docNumberLengthList = [];
    var docNumberNextNumberList = [];

    for (i = 1; i <= settingDocNumberVue.rows.length; i++) {
        docNumberIdList.push(document.getElementById("hfDocNumberId" + i).value);
        docNumberPrefixList.push(document.getElementById("tbDocNumberPrefix" + i).value);
        docNumberLengthList.push(document.getElementById("tbDocNumberLength" + i).value);
        docNumberNextNumberList.push(document.getElementById("tbDocNumberNextNumber" + i).value);
    }

    var ce = {
        "argDocNumberId": docNumberIdList
        , "argDocNumberPrefix": docNumberPrefixList
        , "argDocNumberLength": docNumberLengthList
        , "argDocNumberNextNumber": docNumberNextNumberList
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/saveDocNumber',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == "true") {
                swal(
                    'Save Document Number - Successful',
                    '',
                    'success'
                )
                //loadCompany();
            } else {
                swal(
                    'Save Document Number - Fail',
                    data.d,
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function loadDocNumber() {
    $.ajax({
        url: '/ws/ims-ws.asmx/getDocNumber',
        contentType: "application/json",
        data: JSON.stringify({}),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            for (var i = 0; i < returnData.d.length; i++) {
                settingDocNumberVue.addRowNValue(returnData.d[i].docNumberId, returnData.d[i].docNumberType, returnData.d[i].docNumberPrefix,
                    returnData.d[i].docNumberLength, returnData.d[i].docNumberNextNumber)
                setNextNumberPreview(i + 1);
                initialRowAccessControl(i + 1);
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function initialRowAccessControl(rowIndex) {
    enableElementById("#tbDocNumberPrefix" + rowIndex, !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbDocNumberLength" + rowIndex, !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbDocNumberNextNumber" + rowIndex, !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
}



function setNextNumberPreview(rowIndex) {
    var prefix = document.getElementById('tbDocNumberPrefix' + rowIndex).value;
    var nextNumber = document.getElementById('tbDocNumberNextNumber' + rowIndex).value;
    var numberLength = document.getElementById('tbDocNumberLength' + rowIndex).value;

    $("#tbDocNumberPreview" + (rowIndex)).val(prefix + pad(nextNumber, numberLength))
    //return prefix + pad(nextNumber, numberLength);
}

function columnInputOnChanged(rowIndex) {
    setNextNumberPreview(rowIndex);
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function clearRowDivErrorMsg(index) {
    //$("#divDocNumberPrefix" + index).removeClass('has-error');
    $("#divDocNumberLength" + index).removeClass('has-error');
    $("#divDocNumberNextNumber" + index).removeClass('has-error');
}

function inputVerification() {
    for (i = 1; i <= settingDocNumberVue.rows.length; i++) {
        //clear All Error Div
        clearRowDivErrorMsg(i);

        if (document.getElementById('tbDocNumberLength' + i).value.length == 0) {
            showSwalErrorMsg('Invalid Length of Document Number at row #' + i, "#divDocNumberLength" + i);
            return false;
        }

        if (document.getElementById('tbDocNumberNextNumber' + i).value.length == 0) {
            showSwalErrorMsg('Invalid Next Document Number at row #' + i, "#divDocNumberNextNumber" + i);
            return false;
        }
    }
    return true;
}