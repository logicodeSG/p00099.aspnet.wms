﻿
$(document).ready(function () {
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > CONFIGURATION > COMPANY");
    loadCompany();
    initialAccessControl();

});

function initialAccessControl() {
    enableElementById("#tbCompanyName", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbCompanyAddress1", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbCompanyAddress2", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbCompanyAddress3", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbCompanyPhoneNumber", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbCompanyFaxNumber", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbCompanyEmail", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbCompanyWebsite", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbCompanyRegNumber", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#tbCompanyGSTRegNumber", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));
    enableElementById("#btnSaveCompany", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));

    if (!!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT))) {
        $('#btnSaveCompany').click(function () {
            saveCompany();
        });
    } else {
        $('#btnSaveCompany').attr('onclick', '').unbind('click');
    }
}

function saveCompany() {
    clearAllError();
    if (!InputVerification()) { return false };
    var ce = {
        "argCompanyName": document.getElementById('tbCompanyName').value
        , "argCompanyAddress1": document.getElementById('tbCompanyAddress1').value
        , "argCompanyAddress2": document.getElementById('tbCompanyAddress2').value
        , "argCompanyAddress3": document.getElementById('tbCompanyAddress3').value
        , "argCompanyPhoneNumber": document.getElementById('tbCompanyPhoneNumber').value
        , "argCompanyFaxNumber": document.getElementById('tbCompanyFaxNumber').value
        , "argCompanyEmail": document.getElementById('tbCompanyEmail').value
        , "argCompanyWebsite": document.getElementById('tbCompanyWebsite').value
        , "argCompanyRegNumber": document.getElementById('tbCompanyRegNumber').value
        , "argCompanyGSTRegNumber": document.getElementById('tbCompanyGSTRegNumber').value
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/saveCompany',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == "true") {
                swal(
                    'Save Company - Successful',
                    '',
                    'success'
                )
                loadCompany();
            } else {
                swal(
                    'Save Company - Fail',
                    data.d,
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function loadCompany() {
    $.ajax({
        url: '/ws/ims-ws.asmx/getCompany',
        contentType: "application/json",
        data: JSON.stringify({}),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            $("#tbCompanyName").val(returnData.d[0].companyName);
            $("#tbCompanyAddress1").val(returnData.d[0].companyAddress1);
            $("#tbCompanyAddress2").val(returnData.d[0].companyAddress2);
            $("#tbCompanyAddress3").val(returnData.d[0].companyAddress3);
            $("#tbCompanyPhoneNumber").val(returnData.d[0].companyPhoneNumber);
            $("#tbCompanyFaxNumber").val(returnData.d[0].companyFaxNumber);
            $("#tbCompanyEmail").val(returnData.d[0].companyEmail);
            $("#tbCompanyWebsite").val(returnData.d[0].companyWebsite);
            $("#tbCompanyRegNumber").val(returnData.d[0].companyRegNumber);
            $("#tbCompanyGSTRegNumber").val(returnData.d[0].companyGSTRegNumber);
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function InputVerification() {
    if (document.getElementById('tbCompanyName').value.length == 0) {
        showSwalErrorMsg('Invalid Company Name', "#divCompanyName");
        return false;
    }
    return true;
}

function clearAllError() {

    clearDivErrorMsg('#divCompanyName');
}