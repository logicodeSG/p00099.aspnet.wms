﻿
var printerVuetable = new Vue({
    el: '#printerVueDivId',
    data: {
        apiUrl: '/ws/ims-ws.asmx/getAllPrinterInfo',
        columns: [
            {
                name: 'id',
                title: 'printerId',
                sortField: 'id',
                dataClass: 'text-center',
                visible: false
            },
            {
                name: 'name',
                title: 'Name',
                sortField: 'name',
            },
            {
                name: 'active',
                title: 'Active',
                sortField: 'active',
                callback: 'isActive'
            },
            {
                name: 'isDefault',
                title: 'Default',
                sortField: 'isDefault',
                callback: 'isDefaultCallback',
                visible: false
            },
            {
                name: '__actions',
                title: 'ACTION',
                dataClass: 'text-center',
            }


        ],
        sortOrder: [{
            field: 'tbl_printer.id',
            direction: 'asc'
        }],
        multiSort: true,
        perPage: 10,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        printerActions: [
            {
                name: 'edit-printer', label: '', icon: 'glyphicon glyphicon-edit', class: 'label label-info' + setActionVisible(!!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)))
                , extra: { 'title': 'Edit', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },
        ],
        moreParams: [
        'showAll=' + document.getElementById('chkPrinterShowAll').checked,
        'filter=tbl_printer.id|'
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        isActive: function (value) {
            return value == 'True'
              ? '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> </span>'
              : '<span class="label label-danger"><i class="glyphicon glyphicon-minus"></i> </span>'
        },
        isDefaultCallback: function (value) {
            return value == 'True'
              ? '<span class="label label-warning"><i class="glyphicon glyphicon-star"></i> </span>'
              : ''
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
    },
    events: {

        'vuetable:action': function (action, data) {
            console.log('vuetable:action', action, data)

            if (action == 'edit-printer') {
                $.ajax({
                    url: '/ws/ims-ws.asmx/getPrinterByPrinterId',
                    contentType: "application/json",
                    data: JSON.stringify({ argPrinterId: data.id }),
                    dataType: 'JSON',
                    cache: false,
                    context: document.body,
                    type: 'POST',
                    success: function (returnData) {
                        $("#tbEditPrinterId").val(returnData.d[0].printerId);
                        $("#tbEditPrinterName").val(returnData.d[0].printerName);
                        document.getElementById("chkEditPrinterActive").checked = returnData.d[0].printerActive;
                        document.getElementById("chkEditPrinterIsDefault").checked = returnData.d[0].printerIsDefault;
                    },
                    error: function (err) {
                        showSystemErrorMsg(err.responseText);
                    }
                });
                $("#divEditPrinterModalId").modal({ backdrop: 'static', keyboard: false })


            }
        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            var searchValue = document.getElementById('inputSearchKeyId').value;
            var searchHeader = document.getElementById('slSearch').value;

            if (searchValue !== '') {
                for (n in data) {
                    data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
                }
            }
        },
    }
})

var slSearchOptions = [
    {
        optValue: printerVuetable.columns[1].name,
        optText: printerVuetable.columns[1].title,
        optType: ''
    }
]



$(document).ready(function () {
    initialAccessControl();
    setSystemTitle("ASSET MANAGEMENT SYSTEM > CONFIGURATION > Device > Printer");

    reloadSlSearch(slSearchOptions);
    setFilterVuetable(printerVuetable);

    $('#inputSearchKeyId').keypress(function (event) {
        if (event.keyCode == 13) {
            filterTable();
        }
    });

    $('#btnPrinterReset').click(function () {
        resetAllEntry();
        clearAllError();
    });

    $('#btnPrinterAdd').click(function () {
        createPrinter();
    });

    //$('#btnEditPrinterReset').click(function () {
    //    editResetAllEntry();
    //});

    $('#btnEditPrinterUpdate').click(function () {
        updatePrinter();
    })

    $('#divNewPrinterModalId').on('hidden.bs.modal', function () {
        resetAllEntry();
        clearAllError();
    });

    $('#divEditPrinterModalId').on('hidden.bs.modal', function () {
        clearAllEditPrinterError();
        editResetAllEntry();
    });

});

function initialAccessControl() {
    //setElementIdVisible("#btnOpenDivCreateItem", !!parseInt(getAccessControl(INVENTORY_AC_ID, CAN_ADD)));
    if (!!parseInt(getAccessControl(SETTING_AC_ID, CAN_ADD))) {
        enableElementById("#btnCreateNewPrinter", true);
        $('#btnCreateNewPrinter').attr('onclick', "$('#divNewPrinterModalId').modal({ backdrop: 'static', keyboard: false })").unbind('click');
    } else {
        enableElementById("#btnCreateNewPrinter", false);
        $('#btnCreateNewPrinter').attr('onclick', '').unbind('click');
    }

    enableElementById("#chkPrinterActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
    enableElementById("#chkEditPrinterActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
}


function createPrinter() {
    clearAllError();
    if (!InputVerification()) { return false };
    var ce = {
        "argPrinterName": document.getElementById('tbPrinterName').value
        , "argPrinterActive": document.getElementById('chkPrinterActive').checked
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/createPrinter',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == "true") {
                swal(
                    'Create Printer - Successful',
                    ce.argPrinterName,
                    'success'
                )
                $("#divNewPrinterModalId").modal('hide');
                printerVuetable.$broadcast('vuetable:refresh')
            } else {
                swal(
                    'Create Printer - Fail',
                    data.d,
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function updatePrinter() {
    clearAllEditPrinterError();
    if (!InputVerificationEditPrinter()) { return false };
    var ce = {
        "argPrinterId": document.getElementById('tbEditPrinterId').value
        , "argPrinterName": document.getElementById('tbEditPrinterName').value
        , "argPrinterActive": document.getElementById('chkEditPrinterActive').checked
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/setPrinter',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == "true") {
                swal(
                    'Edit Printer - Success',
                    ce.argPrinterName,
                    'success'
                )
                $("#divEditPrinterModalId").modal('hide');
                printerVuetable.$broadcast('vuetable:refresh')
            } else {
                swal(
                    'Edit Printer - Fail',
                    data.d,
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function InputVerification() {

    if (document.getElementById('tbPrinterName').value.length == 0) {
        showSwalErrorMsg('Invalid Printer Name', "#divPrinterName");
        return false;
    }
    return true;
}

function InputVerificationEditPrinter() {
    if (document.getElementById('tbEditPrinterName').value.length == 0) {
        showSwalErrorMsg('Invalid Printer Name', "#divEditPrinterName");
        return false;
    }

    return true;
}

function clearAllError() {

    clearDivErrorMsg('#divPrinterName');
}

function clearAllEditPrinterError() {
    clearDivErrorMsg('#divEditPrinterName');
}

function resetAllEntry() {
    $("#tbPrinterName").val("");
    document.getElementById("chkPrinterActive").checked = true;
}

function editResetAllEntry() {
    $("#tbEditPrinterId").val("");
    $("#tbEditPrinterName").val("");
    document.getElementById("chkEditPrinterActive").checked = true;
    document.getElementById("chkEditPrinterIsDefault").checked = false;

}

function filterTable() {
    var e = document.getElementById("slSearch");
    var strSearchColumnName = e.options[e.selectedIndex].value;

    printerVuetable.moreParams = [
        'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
        'showAll=' + document.getElementById('chkPrinterShowAll').checked
    ];

    printerVuetable.$nextTick(function () {
        printerVuetable.$broadcast('vuetable:refresh');
    });
}

function chkPrinterShowAll_onChanged() {
    filterTable();
}
