﻿//document.getElementById('itemEntryVueTableId').setAttribute('api-url'
//    , '/ws/ims-ws.asmx/getItemEntryByItemCode?argItemCode=' + curItemCode)

var curSelectLvlOneLocation;


var locationVuetable = new Vue({
    el: '#appLocation',
    data: {
        //apiUrl: '/ws/ims-ws.asmx/getItemEntryByItemCode?argItemCode=' + curItemCode,
        columns: [
            {
                name: 'id',
                title: 'Location ID',
                sortField: 'id',
                visible: false
            },
            {
                name: 'code',
                title: 'Location Code',
                sortField: 'code'
            },
            {
                name: 'description',
                title: 'Location',
                sortField: 'description'
            },
            {
                name: 'active',
                title: 'Active',
                sortField: 'active',
                callback: 'isActive'
            },
            {
                name: '__actions',
                dataClass: 'text-center',
                callback: 'actionCallBack'
            }
        ],
        sortOrder: [{
            field: 'id',
            direction: 'desc'
        }],
        multiSort: true,
        perPage: 10,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        locationActions: [
            //{
              //  name: 'print-item', label: '', icon: 'glyphicon glyphicon-print', class: 'btn btn-info'
                //, extra: { 'title': 'Print', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            //},
            //{
            //    name: 'edit-lvone-location', label: '', icon: 'glyphicon glyphicon-edit', class: 'label label-info edit-location' + setActionVisible(!!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)))
            //    , extra: { 'title': 'Edit', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            //},
            {
                name: 'view-lvtwo-location', label: '', icon: 'glyphicon glyphicon-info-sign', class: 'label label-info'
                , extra: { 'title': 'View Location LV2', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },
        ],
        moreParams: [
        'showAll=' + document.getElementById('chkLocationShowAll').checked,
        'filter=tbl_location.id|'
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    mounted() {
        this.$events.$on('filter-set', eventData => this.onFilterSet(eventData))
        this.$events.$on('filter-reset', e => this.onFilterReset())
    },
    methods: {
        isActive: function (value) {
            return value == 'True'
              ? '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> </span>'
              : '<span class="label label-danger"><i class="glyphicon glyphicon-minus"></i> </span>'
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
    },
    events: {
        'vuetable:action': function (action, data) {
            if (action == 'edit-lvone-location') {
                $("#divEditLvOneLocId").modal({ backdrop: 'static', keyboard: false })

                $.ajax({
                    url: '/ws/ims-ws.asmx/getLocationByLocationId',
                    contentType: "application/json",
                    data: JSON.stringify({ argLocationId: data.id }),
                    dataType: 'JSON',
                    cache: false,
                    context: document.body,
                    type: 'POST',
                    success: function (returnData) {
                        $("#tbEditLocationID").val(returnData.d[0].locationId)
                        $("#tbEditLocationCode").val(returnData.d[0].locationCode)
                        $("#tbEditLocationDesc").val(returnData.d[0].locationDescription)
                        document.getElementById("chkEditLocationActive").checked = returnData.d[0].locationActive;
                    },
                    error: function (err) {
                        showSystemErrorMsg(err.responseText);
                    }
                });
            }
            else if (action == 'view-lvtwo-location') {
                curSelectLvlOneLocation = data.id

                $('#lblLvTwoLocTitleId').text("LV-1 Location: " + data.code)
                $("#hfLocationCode").val(data.code)
                activateLvTwoTable(data.code)
                $("#divLvTwoLocModal").modal({ backdrop: 'static', keyboard: false })
            }
        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            var searchValue = document.getElementById('inputSearchKeyId').value;
            var searchHeader = document.getElementById('slSearch').value;

            if (searchValue !== '') {
                for (n in data) {
                    if (searchHeader == "tbl_location.description") {
                        data[n]["description"] = this.highlight(searchValue, data[n]["description"]);
                    } else {
                        data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
                    }
                }
            }
        },
    }
})

var slSearchOptions = [
    {
        optValue: locationVuetable.columns[1].name,
        optText: locationVuetable.columns[1].title,
        optType: ''
    },
    {
        optValue: "tbl_location.description",
        optText: locationVuetable.columns[2].title,
        optType: ''
    }
]

$(document).ready(function () {
    initialAccessControl();
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > CONFIGURATION > LOCATION");

    reloadSlSearch(slSearchOptions);
    setFilterVuetable(locationVuetable);

    $('#inputSearchKeyId').keypress(function (event) {
        if (event.keyCode == 13) {
            filterTable();
        }
    });

    $('#btnOpenDivLvOneLoc').click(function () {
        document.getElementById('tbLocationCode').value = ''
        document.getElementById('tbLocationDesc').value = ''
    });

    $('#btnAddLvOneLoc').click(function () {
        createLvOneLoc();
    });

    $('#btnOpenDivLvTwoLoc').click(function () {
        document.getElementById('tbL2LocationCode').value = ''
        document.getElementById('tbL2LocationDesc').value = ''
    });


    $('#btnAddLvTwoLoc').click(function () {
        createLvTwoLoc();
    });

    $('#btnOpenDivLvThreeLoc').click(function () {
        document.getElementById('tbL3LocationCode').value = ''
        document.getElementById('tbL3LocationDesc').value = ''
    });


    $('#btnAddLvThreeLoc').click(function () {
        createLvThreeLoc();
    });

    $('#btnEditLvOneLoc').click(function () {
        updateLvOneLoc();
    })

    $('#btnEditLvTwoLoc').click(function () {
        updateLvTwoLoc();
    })

    $('#btnEditLvThreeLoc').click(function () {
        updateLvThreeLoc();
    })

    $('#divLvOneLocId').on('hidden.bs.modal', function () {
        clearAllLvOneLocError();
    });
    $('#divLvTwoLocId').on('hidden.bs.modal', function () {
        clearAllLvTwoLocError();
    });
    $('#divLvThreeLocId').on('hidden.bs.modal', function () {
        clearAllLvThreeLocError();
    });

    $('#divEditLvOneLocId').on('hidden.bs.modal', function () {
        clearAllEditLvOneLocError();
    });
    $('#divEditLvTwoLocId').on('hidden.bs.modal', function () {
        clearAllEditLvTwoLocError();
    });
    $('#divEditLvThreeLocId').on('hidden.bs.modal', function () {
        clearAllEditLvThreeLocError();
    });

});

function initialAccessControl() {
    //setElementIdVisible("#btnOpenDivCreateItem", !!parseInt(getAccessControl(INVENTORY_AC_ID, CAN_ADD)));
    if (!!parseInt(getAccessControl(SETTING_AC_ID, CAN_ADD))) {
        enableElementById("#btnOpenDivLvOneLoc", true);
        $('#btnOpenDivLvOneLoc').attr('onclick', "$('#divLvOneLocId').modal({ backdrop: 'static', keyboard: false })").unbind('click');
        enableElementById("#btnOpenDivLvTwoLoc", true);
        $('#btnOpenDivLvTwoLoc').attr('onclick', "$('#divLvTwoLocId').modal({ backdrop: 'static', keyboard: false })").unbind('click');
        enableElementById("#btnOpenDivLvThreeLoc", true);
        $('#btnOpenDivLvThreeLoc').attr('onclick', "$('#divLvThreeLocId').modal({ backdrop: 'static', keyboard: false })").unbind('click');
    } else {
        enableElementById("#btnOpenDivLvOneLoc", false);
        $('#btnOpenDivLvOneLoc').attr('onclick', '').unbind('click');
        enableElementById("#btnOpenDivLvTwoLoc", false);
        $('#btnOpenDivLvTwoLoc').attr('onclick', '').unbind('click');
        enableElementById("#btnOpenDivLvThreeLoc", false);
        $('#btnOpenDivLvThreeLoc').attr('onclick', '').unbind('click');
    }
    //setElementIdVisible(".edit-location", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)));

    enableElementById("#chkLocationActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
    enableElementById("#chkEditLocationActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
    enableElementById("#chkL2LocationActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
    enableElementById("#chkEditLocation2Active", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
    enableElementById("#chkL3LocationActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
    enableElementById("#chkEditLocation3Active", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
}

function createLvOneLoc() {
    clearAllLvOneLocError();
    if (!InputVerificationLvOneLoc()) { return false };
    var ce = {
        "argLocCode": document.getElementById('tbLocationCode').value
        , "argLocDesc": document.getElementById('tbLocationDesc').value
        , "argLocLevel": "1"
        , "argLocParentId": "1"
        , "argLocActive": document.getElementById('chkLocationActive').checked
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/createLocation',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == 'true') {
                swal(
                    'Create Location - Successful',
                    ce.argLocCode,
                    'success'
                )
            } else {
                swal(
                    'Create Location - Fail',
                    ce.argLocCode + ' is already existed',
                    'error'
                )
            }
            locationVuetable.$broadcast('vuetable:refresh')
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });


}

function createLvTwoLoc() {
    clearAllLvTwoLocError();
    if (!InputVerificationLvTwoLoc()) { return false };
    var ce = {
        "argLocCode": document.getElementById('tbL2LocationCode').value
        , "argLocDesc": document.getElementById('tbL2LocationDesc').value
        , "argLocLevel": "2"
        , "argLocParentId": curSelectLvlOneLocation
        , "argLocActive": document.getElementById('chkL2LocationActive').checked
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/createLocation',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == 'true') {
                swal(
                    'Create LV2 Location - Successful',
                    ce.argLocCode,
                    'success'
                )
            } else {
                swal(
                    'Create LV2 Location - Fail',
                    ce.argLocCode + ' is already existed',
                    'error'
                )
            }
            lvTwoLocVuetable.$broadcast('vuetable:refresh')
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function createLvThreeLoc() {
    clearAllLvThreeLocError();
    if (!InputVerificationLvThreeLoc()) { return false };
    var ce = {
        "argLocCode": document.getElementById('tbL3LocationCode').value
        , "argLocDesc": document.getElementById('tbL3LocationDesc').value
        , "argLocLevel": "3"
        , "argLocParentId": curSelectLvlTwoLocation
        , "argLocActive": document.getElementById('chkL3LocationActive').checked

    }

    $.ajax({
        url: '/ws/ims-ws.asmx/createLocation',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == 'true') {
                swal(
                    'Create LV3 Location - Successful',
                    ce.argLocCode,
                    'success'
                )
            } else {
                swal(
                    'Create LV3 Location - Fail',
                    ce.argLocCode + ' is already existed',
                    'error'
                )
            }
            lvThreeLocVuetable.$broadcast('vuetable:refresh')
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function updateLvOneLoc() {
    clearAllEditLvOneLocError();
    if (!InputVerificationEditLvOneLoc()) { return false };

    var ce = {
        "argLocationId": document.getElementById('tbEditLocationID').value
        , "argLocationCode": document.getElementById('tbEditLocationCode').value
        , "argLocationDesc": document.getElementById('tbEditLocationDesc').value
        , "argLocationActive": document.getElementById('chkEditLocationActive').checked
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/setLocation',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == true) {
                swal(
                    'Edit Location - Success',
                    ce.argLocationCode,
                    'success'
                )
                locationVuetable.$broadcast('vuetable:refresh')
            } else {
                swal(
                    'Edit Location - Fail',
                    '',
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function updateLvTwoLoc() {
    clearAllEditLvTwoLocError();
        if (!InputVerificationEditLvTwoLoc()) { return false };

        var ce = {
            "argLocationId": document.getElementById('tbEditLocation2ID').value
            , "argLocationCode": document.getElementById('tbEditLocation2Code').value
            , "argLocationDesc": document.getElementById('tbEditLocation2Desc').value
            , "argLocationActive": document.getElementById('chkEditLocation2Active').checked
        }

        $.ajax({
            url: '/ws/ims-ws.asmx/setLocation',
            contentType: "application/json",
            data: JSON.stringify(ce),
            dataType: 'JSON',
            cache: false,
            context: document.body,
            type: 'POST',
            success: function (data) {
                if (data.d == true) {
                    swal(
                        'Edit Location - Success',
                        ce.argLocationCode,
                        'success'
                    )
                    lvTwoLocVuetable.$broadcast('vuetable:refresh')
                } else {
                    swal(
                        'Edit Location - Fail',
                        '',
                        'error'
                    )
                }
            },
            error: function (err) {
                showSystemErrorMsg(err.responseText);
            }
        });
}

function updateLvThreeLoc() {
    clearAllEditLvThreeLocError();
    if (!InputVerificationEditLvThreeLoc()) { return false };

    var ce = {
        "argLocationId": document.getElementById('tbEditLocation3ID').value
        , "argLocationCode": document.getElementById('tbEditLocation3Code').value
        , "argLocationDesc": document.getElementById('tbEditLocation3Desc').value
         , "argLocationActive": document.getElementById('chkEditLocation3Active').checked
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/setLocation',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == true) {
                swal(
                    'Edit Location - Success',
                    ce.argLocationCode,
                    'success'
                )
                lvThreeLocVuetable.$broadcast('vuetable:refresh')
            } else {
                swal(
                    'Edit Location - Fail',
                    '',
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function InputVerificationLvOneLoc() {

    if (document.getElementById('tbLocationCode').value.length == 0) {
        showSwalErrorMsg('Invalid Location Code', "#divLocationCode");
        return false;
    }
    if (document.getElementById('tbLocationDesc').value.length == 0) {
        showSwalErrorMsg('Invalid Location Description Name', "#divLocationDesc");
        return false;
    }
    return true;
}

function InputVerificationLvTwoLoc() {

    if (document.getElementById('tbL2LocationCode').value.length == 0) {
        showSwalErrorMsg('Invalid Location Code', "#divL2LocationCode");
        return false;
    }
    if (document.getElementById('tbL2LocationDesc').value.length == 0) {
        showSwalErrorMsg('Invalid Location Description Name', "#divL2LocationDesc");
        return false;
    }
    return true;
}

function InputVerificationLvThreeLoc() {

    if (document.getElementById('tbL3LocationCode').value.length == 0) {
        showSwalErrorMsg('Invalid Location Code', "#divL3LocationCode");
        return false;
    }
    if (document.getElementById('tbL3LocationDesc').value.length == 0) {
        showSwalErrorMsg('Invalid Location Description Name', "#divL3LocationDesc");
        return false;
    }
    return true;
}

function InputVerificationEditLvOneLoc() {

    if (document.getElementById('tbEditLocationCode').value.length == 0) {
        showSwalErrorMsg('Invalid Location Code', "#divEditLocationCode");
        return false;
    }
    if (document.getElementById('tbEditLocationDesc').value.length == 0) {
        showSwalErrorMsg('Invalid Location Description Name', "#divEditLocationDesc");
        return false;
    }
    return true;
}

function InputVerificationEditLvTwoLoc() {

    if (document.getElementById('tbEditLocation2Code').value.length == 0) {
        showSwalErrorMsg('Invalid Location Code', "#divEditLocation2Code");
        return false;
    }
    if (document.getElementById('tbEditLocation2Desc').value.length == 0) {
        showSwalErrorMsg('Invalid Location Description Name', "#divEditLocation2Desc");
        return false;
    }
    return true;
}

function InputVerificationEditLvThreeLoc() {

    if (document.getElementById('tbEditLocation3Code').value.length == 0) {
        showSwalErrorMsg('Invalid Location Code', "#divEditLocation3Code");
        return false;
    }
    if (document.getElementById('tbEditLocation3Desc').value.length == 0) {
        showSwalErrorMsg('Invalid Location Description Name', "#divEditLocation3Desc");
        return false;
    }
    return true;
}

function clearAllLvOneLocError() {

    clearDivErrorMsg('#divLocationCode');
    clearDivErrorMsg('#divLocationDesc');
}

function clearAllLvTwoLocError() {

    clearDivErrorMsg('#divL2LocationCode');
    clearDivErrorMsg('#divL2LocationDesc');
}

function clearAllLvThreeLocError() {

    clearDivErrorMsg('#divL3LocationCode');
    clearDivErrorMsg('#divL3LocationDesc');
}

function clearAllEditLvOneLocError() {

    clearDivErrorMsg('#divEditLocationCode');
    clearDivErrorMsg('#divEditLocationDesc');
}

function clearAllEditLvTwoLocError() {

    clearDivErrorMsg('#divEditLocation2Code');
    clearDivErrorMsg('#divEditLocation2Desc');
}

function clearAllEditLvThreeLocError() {

    clearDivErrorMsg('#divEditLocation3Code');
    clearDivErrorMsg('#divEditLocation3Desc');
}

function filterTable() {
    var e = document.getElementById("slSearch");
    var strSearchColumnName = e.options[e.selectedIndex].value;

    locationVuetable.moreParams = [
        'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
        'showAll=' + document.getElementById('chkLocationShowAll').checked
    ];

    locationVuetable.$nextTick(function () {
        locationVuetable.$broadcast('vuetable:refresh');
    });
}

function chkLocationShowAll_onChanged() {
    filterTable();
}
