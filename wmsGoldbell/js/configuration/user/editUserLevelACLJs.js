﻿Vue.config.async = false;
var editUserLevelACLVue = new Vue({
    el: "#appEditUserLevelACL",
    data: {
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ],
        total: 0,
        gst: 0
    },
    computed: {
        total: function () {
            var t = 0;
            $.each(this.rows, function (i, e) {
                t += parseInt(e.quantity) * parseFloat(e.price);
            });
            return t;
        }
    },
    methods: {
        addRow: function (index) {
            this.rows.push({ price: 0 });
        },
        addRowNValue: function (id, action) {
            this.rows.push({ id: id, action: action});
        },
        removeRow: function (row) {
            //console.log(row);
            this.rows.$remove(row);
        },
        resetRow: function (index) {

        }
    }

});

function chkEditHeaderOnChanged(headerName) {

    switch(headerName) {
        case 'canRead':
            var v = document.getElementById('chkEditCanRead0').checked;
            for (i = 1; i <= editUserLevelACLVue.rows.length; i++) {
                document.getElementById("chkEditCanRead" + (i)).checked = v;
            }
            break;
        case 'canAdd':
            var v = document.getElementById('chkEditCanAdd0').checked;
            for (i = 1; i <= editUserLevelACLVue.rows.length; i++) {
                document.getElementById("chkEditCanAdd" + (i)).checked = v;
            }
            break;
        case 'canEdit':
            var v = document.getElementById('chkEditCanEdit0').checked;
            for (i = 1; i <= editUserLevelACLVue.rows.length; i++) {
                document.getElementById("chkEditCanEdit" + (i)).checked = v;
            }
            break;
        case 'canDelete':
            var v = document.getElementById('chkEditCanDelete0').checked;
            for (i = 1; i <= editUserLevelACLVue.rows.length; i++) {
                document.getElementById("chkEditCanDelete" + (i)).checked = v;
            }
            break;
        case 'canPrint':
            var v = document.getElementById('chkEditCanPrint0').checked;
            for (i = 1; i <= editUserLevelACLVue.rows.length; i++) {
                document.getElementById("chkEditCanPrint" + (i)).checked = v;
            }
            break;
        default:
            break;
    }
    
}

