﻿//document.getElementById('itemEntryVueTableId').setAttribute('api-url'
//    , '/ws/ims-ws.asmx/getItemEntryByItemCode?argItemCode=' + curItemCode)

var userLevelVuetable = new Vue({
    el: '#appUserLevelDivId',
    data: {
        //apiUrl: '/ws/ims-ws.asmx/getItemEntryByItemCode?argItemCode=' + curItemCode,
        columns: [
            {
                name: 'id',
                title: 'User Group ID',
                sortField: 'id',
                visible: false,

            },
            {
                name: 'description',
                title: 'User Group',
                sortField: 'description',
            },
            {
                name: 'active',
                title: 'Active',
                sortField: 'active',
                callback: 'isActive',
            },
            {
                name: '__actions',
                dataClass: 'text-center',
            }
            //{
            //    name: '__actions',
            //    dataClass: 'text-center',
            //}
        ],
        sortOrder: [{
            field: 'id',
            direction: 'asc'
        }],
        multiSort: true,
        perPage: 10,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        actions: [
            {
                name: 'edit-userLevel', label: '', icon: 'glyphicon glyphicon-edit', class: 'label label-info' + setActionVisible(!!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)))
                , extra: { 'title': 'Edit', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },
            /*{
                name: 'delete-user', label: '', icon: 'glyphicon glyphicon-remove', class: 'label label-danger'
                , extra: { 'title': 'Delete', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },*/
        ],
        moreParams: [
        'showAll=' + document.getElementById('chkUserLevelShowAll').checked,
        'filter=tbl_userLevel.id|'
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        isActive: function (value) {
            return value == 'True'
              ? '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> </span>'
              : '<span class="label label-danger"><i class="glyphicon glyphicon-minus"></i> </span>'
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
    },
    events: {
        'vuetable:action': function (action, data) {
            if (action == 'edit-userLevel') {
                loadUserLevel(data.id);
            }
        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            var searchValue = document.getElementById('inputSearchKeyId').value;
            var searchHeader = document.getElementById('slSearch').value;

            if (searchValue !== '') {
                for (n in data) {
                    if (searchHeader == "tbl_userLevel.description") {
                        data[n]["description"] = this.highlight(searchValue, data[n]["description"]);
                    } else {
                        data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
                    }
                }
            }
        },
    }
})

var slSearchOptions = [

{
    optValue: userLevelVuetable.columns[1].name,
    optText: userLevelVuetable.columns[1].title,
    optType: ''
},
]

$(document).ready(function () {
    initialAccessControl();
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > CONFIGURATION > USER GROUP");

    reloadSlSearch(slSearchOptions);
    setFilterVuetable(userLevelVuetable);
    loadUserLevelAccess();

    $('#inputSearchKeyId').keypress(function (event) {
        if (event.keyCode == 13) {
            filterTable();
        }
    });

    $('#btnOpenDivCreateUserLevel').click(function () {
        clearAllError();
        resetAllEntry();
    })

    $('#btnEditUserLevel').click(function () {
        editUserLevel();
    })


    $('#btnAddUserLevel').click(function () {
        saveNewUserLevel();
    });

});

function initialAccessControl() {
    //setElementIdVisible("#btnOpenDivCreateItem", !!parseInt(getAccessControl(INVENTORY_AC_ID, CAN_ADD)));
    if (!!parseInt(getAccessControl(SETTING_AC_ID, CAN_ADD))) {
        enableElementById("#btnOpenDivCreateUserLevel", true);
        $('#btnOpenDivCreateUserLevel').attr('onclick', "$('#divNewUserLevelModalId').modal({ backdrop: 'static', keyboard: false })").unbind('click');
    } else {
        enableElementById("#btnOpenDivCreateUserLevel", false);
        $('#btnOpenDivCreateUserLevel').attr('onclick', '').unbind('click');
    }

    enableElementById("#chkUserLevelActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
    enableElementById("#chkEditUserLevelActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
}

function loadUserLevel(id) {
    resetAllEditEntry();
    $.ajax({
        url: '/ws/ims-ws.asmx/getUserLevelByUserLevelId',
        contentType: "application/json",
        data: JSON.stringify({ argUserLevelId: id }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            $("#tbEditUserLevelId").val(returnData.d[0].userLevelId)
            $("#tbEditUserLevelDescription").val(returnData.d[0].userLevelDesc)
            document.getElementById("chkEditUserLevelActive").checked = returnData.d[0].userLevelActive;
            loadUserLevelXAccess(id);
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function loadUserLevelXAccess(userLevelId) {

    $.ajax({
        url: '/ws/ims-ws.asmx/getUserLevelXAccessByUserLevelId',
        contentType: "application/json",
        data: JSON.stringify({ argUserLevelId: userLevelId }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            if (returnData.d.length > 0) {
                for (var i = 0; i < returnData.d.length; i++) {
                    for (j = 1; j <= editUserLevelACLVue.rows.length; j++) {
                        if (document.getElementById("hfEditActionId" + j).value == returnData.d[i].userAccessId) {
                            document.getElementById("chkEditCanRead" + j).checked = (returnData.d[i].canRead === 'True') ? true : false;
                            document.getElementById("chkEditCanAdd" + j).checked = (returnData.d[i].canAdd === 'True') ? true : false;
                            document.getElementById("chkEditCanEdit" + j).checked = (returnData.d[i].canEdit === 'True') ? true : false;
                            document.getElementById("chkEditCanDelete" + j).checked = (returnData.d[i].canDelete === 'True') ? true : false;
                            document.getElementById("chkEditCanPrint" + j).checked = (returnData.d[i].canPrint === 'True') ? true : false;
                            break;
                        }
                    }
                }
            }
            $("#divEditUserLevelModalId").modal({ backdrop: 'static', keyboard: false })
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function saveNewUserLevel() {
    if (!InputVerification()) { return false };
    var actionIdList = [];
    var canReadList = [];
    var canAddList = [];
    var canEditList = [];
    var canDeleteList = [];
    var canPrintList = [];

    for (i = 1; i <= userLevelACLVue.rows.length; i++) {
        actionIdList.push(document.getElementById("hfActionId" + i).value);
        canReadList.push(document.getElementById('chkCanRead' + i).checked);
        canAddList.push(document.getElementById('chkCanAdd' + i).checked);
        canEditList.push(document.getElementById('chkCanEdit' + i).checked);
        canDeleteList.push(document.getElementById('chkCanDelete' + i).checked);
        canPrintList.push(document.getElementById('chkCanPrint' + i).checked);
    }

    var ce = {
        "argUserLevelDescription": document.getElementById('tbUserLevelDescription').value
        , "argUserLevelActive": document.getElementById('chkUserLevelActive').checked
        , "argActionIdList": actionIdList
        , "argCanReadList": canReadList
        , "argCanAddList": canAddList
        , "argCanEditList": canEditList
        , "argCanDeleteList": canDeleteList
        , "argCanPrintList": canPrintList
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/createUserLevel',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == "true") {
                swal(
                    'Create User Group - Successful',
                    ce.argUserLevelDescription,
                    'success'
                )
                userLevelVuetable.$broadcast('vuetable:refresh')
            } else {
                swal(
                    'Create User Group - Fail',
                    data.d,
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function editUserLevel() {
    if (!InputVerificationEditUserLevel()) { return false };

    var actionIdList = [];
    var canReadList = [];
    var canAddList = [];
    var canEditList = [];
    var canDeleteList = [];
    var canPrintList = [];

    for (i = 1; i <= editUserLevelACLVue.rows.length; i++) {
        actionIdList.push(document.getElementById("hfEditActionId" + i).value);
        canReadList.push(document.getElementById('chkEditCanRead' + i).checked);
        canAddList.push(document.getElementById('chkEditCanAdd' + i).checked);
        canEditList.push(document.getElementById('chkEditCanEdit' + i).checked);
        canDeleteList.push(document.getElementById('chkEditCanDelete' + i).checked);
        canPrintList.push(document.getElementById('chkEditCanPrint' + i).checked);
    }

    var ce = {
        "argUserLevelId": document.getElementById('tbEditUserLevelId').value
        , "argUserLevelDescription": document.getElementById('tbEditUserLevelDescription').value
        , "argUserLevelActive": document.getElementById('chkEditUserLevelActive').checked
        , "argActionIdList": actionIdList
        , "argCanReadList": canReadList
        , "argCanAddList": canAddList
        , "argCanEditList": canEditList
        , "argCanDeleteList": canDeleteList
        , "argCanPrintList": canPrintList
    }

    $.ajax({
        url: '/ws/ims-ws.asmx/setUserLevel',
        contentType: "application/json",
        data: JSON.stringify(ce),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (data) {
            if (data.d == 'true') {
                swal(
                    'Edit User Group - Success',
                    ce.argUserLevelDescription,
                    'success'
                )
                userLevelVuetable.$broadcast('vuetable:refresh')
            } else {
                swal(
                    'Edit User Group - Fail',
                    data.d + '',
                    'error'
                )
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function InputVerification() {
    clearAllError();
    if (document.getElementById('tbUserLevelDescription').value.length == 0) {
        showSwalErrorMsg('Invalid User Group Description', "#divUserLevelDescription");
        return false;
    }

    return true;
}

function InputVerificationEditUserLevel() {
    clearAllEditUserLevelError();
    if (document.getElementById('tbEditUserLevelDescription').value.length == 0) {
        showSwalErrorMsg('Invalid User Group Description', "#divEditUserLevelDescription");
        return false;
    }
    return true;
}

function clearAllError() {

    clearDivErrorMsg('#divUserLevelDescription');
}

function clearAllEditUserLevelError() {

    clearDivErrorMsg('#divEditUserLevelDescription');
}

function resetAllEntry() {
    $("#tbUserLevelDescription").val("");
    document.getElementById("chkUserLevelActive").checked = true;
    document.getElementById('chkCanRead0').checked = false;
    document.getElementById('chkCanAdd0').checked = false;
    document.getElementById('chkCanEdit0').checked = false;
    document.getElementById('chkCanDelete0').checked = false;
    document.getElementById('chkCanPrint0').checked = false;
    chkHeaderOnChanged('canRead');
    chkHeaderOnChanged('canAdd');
    chkHeaderOnChanged('canEdit');
    chkHeaderOnChanged('canDelete');
    chkHeaderOnChanged('canPrint');
}

function resetAllEditEntry() {
    $("#tbEditUserLevelDescription").val("");
    document.getElementById("chkEditUserLevelActive").checked = true;
    document.getElementById('chkEditCanRead0').checked = false;
    document.getElementById('chkEditCanAdd0').checked = false;
    document.getElementById('chkEditCanEdit0').checked = false;
    document.getElementById('chkEditCanDelete0').checked = false;
    document.getElementById('chkEditCanPrint0').checked = false;
    chkEditHeaderOnChanged('canRead');
    chkEditHeaderOnChanged('canAdd');
    chkEditHeaderOnChanged('canEdit');
    chkEditHeaderOnChanged('canDelete');
    chkEditHeaderOnChanged('canPrint');
}
function filterTable() {
    var e = document.getElementById("slSearch");
    var strSearchColumnName = e.options[e.selectedIndex].value;

    userLevelVuetable.moreParams = [
        'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
        'showAll=' + document.getElementById('chkUserLevelShowAll').checked
    ];

    userLevelVuetable.$nextTick(function () {
        userLevelVuetable.$broadcast('vuetable:refresh');
    });
}


function chkUserLevelShowAll_onChanged() {
    filterTable();
    //userVuetable.moreParams = [
    //    'showAll=' + document.getElementById('chkUserShowAll').checked,
    //    'userId=' + document.getElementById('hfUserId').value
    //]

    //userVuetable.$nextTick(function () {
    //    userVuetable.$broadcast('vuetable:refresh')
    //})
}

function loadUserLevelAccess() {
    $.ajax({
        url: '/ws/ims-ws.asmx/getUserLevelAccess',
        contentType: "application/json",
        data: JSON.stringify({}),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            for (var i = 0; i < returnData.d.length; i++) {
                userLevelACLVue.addRowNValue(returnData.d[i].userAccessId, returnData.d[i].userAccessName);
                editUserLevelACLVue.addRowNValue(returnData.d[i].userAccessId, returnData.d[i].userAccessName);
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}