﻿//document.getElementById('itemEntryVueTableId').setAttribute('api-url'
//    , '/ws/ims-ws.asmx/getItemEntryByItemCode?argItemCode=' + curItemCode)

var userVuetable = new Vue({
    el: '#appUserDivId',
    data: {
        //apiUrl: '/ws/ims-ws.asmx/getItemEntryByItemCode?argItemCode=' + curItemCode,
        columns: [
            {
                name: 'id',
                title: 'User ID',
                sortField: 'id',
                visible: false,

            },
            {
                name: 'code',
                title: 'User Code',
                sortField: 'code',
            },
            {
                name: 'username',
                title: 'Username',
                sortField: 'username',
            },
            {
                name: 'description',
                title: 'User Level',
                sortField: 'description',
            },
            {
                name: 'active',
                title: 'Active',
                sortField: 'active',
                callback: 'isActive',
            },
            {
                name: '__actions',
                dataClass: 'text-center',
            }
            //{
            //    name: '__actions',
            //    dataClass: 'text-center',
            //}
        ],
        sortOrder: [{
            field: 'id',
            direction: 'asc'
        }],
        multiSort: true,
        perPage: 10,
        paginationComponent: 'vuetable-pagination',
        paginationInfoTemplate: 'แสดง {from} ถึง {to} จากทั้งหมด {total} รายการ',
        actions: [
            {
                name: 'edit-user', label: '', icon: 'glyphicon glyphicon-edit', class: 'label label-info' + setActionVisible(!!parseInt(getAccessControl(SETTING_AC_ID, CAN_EDIT)))
                , extra: { 'title': 'Edit', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },
            /*{
                name: 'delete-user', label: '', icon: 'glyphicon glyphicon-remove', class: 'label label-danger'
                , extra: { 'title': 'Delete', 'data-toggle': "tooltip", 'data-placement': "top", 'onclick': "return false" }
            },*/
        ],
        moreParams: [
        'showAll=' + document.getElementById('chkUserShowAll').checked,
        'userId=' + document.getElementById('hfUserId').value,
        'filter=tbl_user.id|'
        ],
    },
    watch: {
        'perPage': function (val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function (val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        isActive: function (value) {
            return value == 'True'
              ? '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> </span>'
              : '<span class="label label-danger"><i class="glyphicon glyphicon-minus"></i> </span>'
        },
        paginationConfig: function (componentName) {
            //console.log('paginationConfig: ', componentName)
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: '' },
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        preg_quote: function (str) {
            // http://kevin.vanzonneveld.net
            // +   original by: booeyOH
            // +   improved by: Ates Goral (http://magnetiq.com)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   bugfixed by: Onno Marsman
            // *     example 1: preg_quote("$40");
            // *     returns 1: '\$40'
            // *     example 2: preg_quote("*RRRING* Hello?");
            // *     returns 2: '\*RRRING\* Hello\?'
            // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
            // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function (needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
    },
    events: {
        'vuetable:action': function (action, data) {
            if (action == 'edit-user') {
                editResetAllEntry();
                $("#divEditUserModalId").modal({ backdrop: 'static', keyboard: false })

                $.ajax({
                    url: '/ws/ims-ws.asmx/getUserByUserId',
                    contentType: "application/json",
                    data: JSON.stringify({ argUserId: data.id }),
                    dataType: 'JSON',
                    cache: false,
                    context: document.body,
                    type: 'POST',
                    success: function (returnData) {
                        $("#tbEditUserId").val(returnData.d[0].userId)
                        $("#tbEditUsercode").val(returnData.d[0].userCode)
                        $("#tbEditUserPass1").val("")
                        $("#tbEditUserPass2").val("")
                        $("#tbEditUsername").val(returnData.d[0].username)
                        $("#selectEditUserLevel").val(returnData.d[0].userLevelId)
                        document.getElementById("chkEditUserActive").checked = returnData.d[0].userActive;
                    
                        for (var i = 0; i < returnData.d[0].uloList.length; i++) {
                            editUserLocationVue.addRowNValue(returnData.d[0].uloList[i].locationId, returnData.d[0].uloList[i].locationDescription);
                        }
                        loadAllUnanssignedLocation('selectEditUserLocation', editUserLocationVue);
                    },
                    error: function (err) {
                        showSystemErrorMsg(err.responseText);
                    }
                });
            }
            /*else if (action == 'delete-user') {
                swal({
                    title: 'Please confirm this deletion?',
                    text: "Deletion of user cannot be recovered, do you want to continue? User Code: " + data.userId,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Confirm, please delete!'
                }).then(function () {
                    //send delete message back to webservice
                    jnDeleteDialog(data.userId);
                    swal("DELETE!", "User is deleted successfully.", "success");
                    userVuetable.$broadcast('vuetable:refresh')
                })
            }*/
        },
        'vuetable:load-success': function (response) {
            var data = response.data.data
            //console.log(data)
            var searchValue = document.getElementById('inputSearchKeyId').value;
            var searchHeader = document.getElementById('slSearch').value;

            if (searchValue !== '') {
                for (n in data) {
                    if (searchHeader == "tbl_user.code") {
                        data[n]["code"] = this.highlight(searchValue, data[n]["code"]);
                    } else if (searchHeader == "tbl_user.username") {
                        data[n]["username"] = this.highlight(searchValue, data[n]["username"]);
                    } else if (searchHeader == "tbl_userLevel.description") {
                        data[n]["description"] = this.highlight(searchValue, data[n]["description"]);
                    } else {
                        data[n][searchHeader] = this.highlight(searchValue, data[n][searchHeader]);
                    }
                }
            }
        },
    }
})

var slSearchOptions = [
{
    optValue: "tbl_user.code",
    optText: userVuetable.columns[1].title,
    optType: ''
},
{
    optValue: "tbl_user.username",
    optText: userVuetable.columns[2].title,
    optType: ''
},
{
    optValue: "tbl_userLevel.description",
    optText: userVuetable.columns[3].title,
    optType: ''
},
]

var newUserLocationVue = new Vue({
    el: "#appNewUserLocation",
    data: {
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        addRow: function (index) {
            this.rows.push({});
        },
        addRowNValue: function (locationId, locationName) {
            this.rows.push({ locationId: locationId, locationName: locationName });

        },
        removeRow: function (row) {
            //console.log(row);
            this.rows.$remove(row);
            loadAllUnanssignedLocation('selectNewUserLocation', this);
        },
        resetRow: function (index) {

        }
    }

});

var editUserLocationVue = new Vue({
    el: "#appEditUserLocation",
    data: {
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        addRow: function (index) {
            this.rows.push({});
        },
        addRowNValue: function (locationId, locationName) {
            this.rows.push({ locationId: locationId, locationName: locationName });

        },
        removeRow: function (row) {
            //console.log(row);
            this.rows.$remove(row);
            loadAllUnanssignedLocation('selectEditUserLocation', this);
        },
        resetRow: function (index) {

        }
    }

});

$(document).ready(function () {
    initialAccessControl();
    setSystemTitle("WAREHOUSE MANAGEMENT SYSTEM > CONFIGURATION > USER");

    reloadSlSearch(slSearchOptions);
    setFilterVuetable(userVuetable);

    $('#inputSearchKeyId').keypress(function (event) {
        if (event.keyCode == 13) {
            filterTable();
        }
    });

    $('#btnOpenDivCreateUser').click(function () {
        clearAllError();
        resetAllEntry();
    })

    loadAllUserLevel();

    $('#btnEditUser').click(function () {
        updateUser();
    })

    $('#btnAddUser').click(function () {
        createUser();
    });

});

function addLocation() {
    var tempSelect = document.getElementById('selectNewUserLocation');
    if (tempSelect.selectedIndex > -1) {
        newUserLocationVue.addRowNValue(tempSelect.value, tempSelect.options[tempSelect.selectedIndex].text);
        tempSelect.options[tempSelect.selectedIndex] = null;
        $("#selectNewUserLocation").selectpicker('refresh');
    }
}

function editAddLocation() {
    var tempSelect = document.getElementById('selectEditUserLocation');
    if (tempSelect.selectedIndex > -1) {
        editUserLocationVue.addRowNValue(tempSelect.value, tempSelect.options[tempSelect.selectedIndex].text);
        tempSelect.options[tempSelect.selectedIndex] = null;
        $("#selectEditUserLocation").selectpicker('refresh');
    }
}

function initialAccessControl() {
    //setElementIdVisible("#btnOpenDivCreateItem", !!parseInt(getAccessControl(INVENTORY_AC_ID, CAN_ADD)));
    if (!!parseInt(getAccessControl(SETTING_AC_ID, CAN_ADD))) {
        enableElementById("#btnOpenDivCreateUser", true);
        $('#btnOpenDivCreateUser').attr('onclick', "$('#divNewUserModalId').modal({ backdrop: 'static', keyboard: false })").unbind('click');
    } else {
        enableElementById("#btnOpenDivCreateUser", false);
        $('#btnOpenDivCreateUser').attr('onclick', '').unbind('click');
    }

    enableElementById("#chkUserActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
    enableElementById("#chkEditUserActive", !!parseInt(getAccessControl(SETTING_AC_ID, CAN_DELETE)));
}

function loadAllUnanssignedLocation(slctLocation, v) {
    var selectTemp = document.getElementById(slctLocation);
    for (var i = selectTemp.options.length - 1; i >= 0; i--) {
        selectTemp.options[i] = null;
    }
    $("#" + slctLocation).selectpicker('refresh');
    $.ajax({
        url: '/ws/ims-ws.asmx/getAllLocationTwo',
        contentType: "application/json",
        data: JSON.stringify({}),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {

            var isAssigned = false;
            for (var i = 0; i < returnData.d.length; i++) {
                isAssigned = false;
                for (var j = 0; j < v.rows.length; j++) {
                    if (returnData.d[i].locationId == v.rows[j].locationId) {
                        isAssigned = true;
                    }
                }
                if (!isAssigned) {
                    opt = document.createElement('option');
                    opt.value = returnData.d[i].locationId;
                    opt.innerHTML = returnData.d[i].locationDescription;
                    selectTemp.appendChild(opt);
                    $("#" + slctLocation + " > option:eq(" + (i) + ")").data("subtext", returnData.d[i].locationCode);
                }
            }
            $("#" + slctLocation).selectpicker('refresh');

        },
        error: function (err) {
            alert("error:" + err.responseText);
        }
    });
}

function loadAllUserLevel() {
    $.ajax({
        url: '/ws/ims-ws.asmx/getAllUserLevel',
        contentType: "application/json",
        data: JSON.stringify({ "argUserId": document.getElementById('hfUserId').value }),
        dataType: 'JSON',
        cache: false,
        context: document.body,
        type: 'POST',
        success: function (returnData) {
            var selectNewUserLevel = document.getElementById('selectNewUserLevel');
            var selectEditUserLevel = document.getElementById('selectEditUserLevel');
            for (var i = 0; i < returnData.d.length; i++) {
                var opt = document.createElement('option');
                opt.value = returnData.d[i].userLevelId;
                opt.innerHTML = returnData.d[i].userLevelDesc;
                selectEditUserLevel.appendChild(opt);
            }

            for (var i = 0; i < returnData.d.length; i++) {
                var opt = document.createElement('option');
                opt.value = returnData.d[i].userLevelId;
                opt.innerHTML = returnData.d[i].userLevelDesc;
                selectNewUserLevel.appendChild(opt);
            }
        },
        error: function (err) {
            showSystemErrorMsg(err.responseText);
        }
    });
}

function createUser() {
    if (InputVerification()) {

        var locationIdList = [];

        for (i = 1; i <= newUserLocationVue.rows.length; i++) {
            locationIdList.push(document.getElementById("hfNewUserLocationId" + i).value);
        }

        var ce = {
            "argUsername": document.getElementById('tbUsercode').value
            , "argUsercode": document.getElementById('tbUsername').value
            , "argUserPass": document.getElementById('tbUserPass1').value
            , "argUserLevelId": document.getElementById('selectNewUserLevel').value
            , "argUserActive": document.getElementById('chkUserActive').checked
            , "argUserLocation": locationIdList
        }

        $.ajax({
            url: '/ws/ims-ws.asmx/createUser',
            contentType: "application/json",
            data: JSON.stringify(ce),
            dataType: 'JSON',
            cache: false,
            context: document.body,
            type: 'POST',
            success: function (data) {
                if (data.d == "true") {
                    swal(
                        'Create User - Successful',
                        ce.argUsercode,
                        'success'
                    )
                    $("#divNewUserModalId").modal('hide');
                    userVuetable.$broadcast('vuetable:refresh')
                } else {
                    swal(
                        'Create User- Fail',
                        data.d,
                        'error'
                    )
                }
            },
            error: function (err) {
                showSystemErrorMsg(err.responseText);
            }
        });
    };
}

function updateUser() {
    if (InputVerificationEditUser()) {

        var locationIdList = [];

        for (i = 1; i <= editUserLocationVue.rows.length; i++) {
            locationIdList.push(document.getElementById("hfEditUserLocationId" + i).value);
        }

        var ce = {
            "argUserId": document.getElementById('tbEditUserId').value
            , "argUserCode": document.getElementById('tbEditUsercode').value
            , "argUserPass": document.getElementById('tbEditUserPass1').value
            , "argUserName": document.getElementById('tbEditUsername').value
            , "argUserLevelId": document.getElementById('selectEditUserLevel').value
            , "argUserActive": document.getElementById('chkEditUserActive').checked
            , "argUserLocation": locationIdList
        }

        $.ajax({
            url: '/ws/ims-ws.asmx/setUser',
            contentType: "application/json",
            data: JSON.stringify(ce),
            dataType: 'JSON',
            cache: false,
            context: document.body,
            type: 'POST',
            success: function (data) {
                if (data.d == "true") {
                    swal(
                        'Edit User - Success',
                        ce.argUsercode,
                        'success'
                    )
                    $("#divEditUserModalId").modal('hide');
                    userVuetable.$broadcast('vuetable:refresh')
                } else {
                    swal(
                        'Edit User - Fail',
                        data.d + '',
                        'error'
                    )
                }
            },
            error: function (err) {
                showSystemErrorMsg(err.responseText);
            }
        });
    };
}

function InputVerification() {
    clearAllError();
    if (document.getElementById('tbUsercode').value.length == 0) {
        showSwalErrorMsg('Invalid User Code', "#divUserCode");
        return false;
    }
    if (document.getElementById('tbUsername').value.length == 0) {
        showSwalErrorMsg('Invalid User Name', "#divUserName");
        return false;
    }
    if (document.getElementById('tbUserPass1').value.length < 5) {
        showSwalErrorMsg('Password must be at least 5 characters', "#divUserPass1");
        return false;
    }
    if (document.getElementById('tbUserPass1').value.length == 0) {
        showSwalErrorMsg('Invalid Password', "#divUserPass1");
        return false;
    }
    if (document.getElementById('tbUserPass1').value != document.getElementById('tbUserPass2').value) {
        showSwalErrorMsg('Confirmed Password must be the same as Password', "#divUserPass2");
        return false;
    }

    var tempSelectFrom = document.getElementById('selectNewUserLevel');
    if (tempSelectFrom.selectedIndex < 0) {
        showSwalErrorMsg('Invalid User Level', "#divUserLevel");
        return false;
    }

    if (newUserLocationVue.rows.length < 1) {
        showSwalErrorMsg('Invalid User Location', "#divUserLocation");
        return false;
    }

    return true;
}

function InputVerificationEditUser() {
    clearAllEditUserError();
    if (document.getElementById('tbEditUsercode').value.length == 0) {
        showSwalErrorMsg('Invalid User Code', "#divEditUserCode");
        return false;
    }
    if (document.getElementById('tbEditUsername').value.length == 0) {
        showSwalErrorMsg('Invalid User Name', "#divEditUserName");
        return false;
    }
    if (document.getElementById('tbEditUserPass1').value.length < 5) {
        showSwalErrorMsg('Password must be at least 5 characters', "#divEditUserPass1");
        return false;
    }
    if (document.getElementById('tbEditUserPass1').value.length == 0) {
        showSwalErrorMsg('Invalid Password', "#divEditUserPass1");
        return false;
    }
    if (document.getElementById('tbEditUserPass1').value != document.getElementById('tbEditUserPass2').value) {
        showSwalErrorMsg('Confirmed Password must be the same as Password', "#divEditUserPass2");
        return false;
    }

    var tempSelectFrom = document.getElementById('selectEditUserLevel');
    if (tempSelectFrom.selectedIndex < 0) {
        showSwalErrorMsg('Invalid User Level', "#divEditUserLevel");
        return false;
    }

    if (editUserLocationVue.rows.length < 1) {
        showSwalErrorMsg('Invalid User Location', "#divEditUserLocation");
        return false;
    }
    return true;


}

function clearAllError() {

    clearDivErrorMsg('#divUserCode');
    clearDivErrorMsg('#divUserName');
    clearDivErrorMsg('#divUserPass1');
    clearDivErrorMsg('#divUserPass2');
    clearDivErrorMsg('#divUserLevel');
    clearDivErrorMsg('#divUserLocation');
}

function clearAllEditUserError() {

    clearDivErrorMsg('#divEditUserCode');
    clearDivErrorMsg('#divEditUserName');
    clearDivErrorMsg('#divEditUserPass1');
    clearDivErrorMsg('#divEditUserPass2');
    clearDivErrorMsg('#divEditUserLevel');
    clearDivErrorMsg('#divEditUserLocation');
}

function resetAllEntry() {
    $("#tbUsercode").val("");
    $("#tbUsername").val("");
    $("#tbUserPass1").val("");
    $("#tbUserPass2").val("");
    var tempSelect = document.getElementById('selectNewUserLevel');
    if (tempSelect.selectedIndex > -1) {
        $("#selectNewUserLevel").selectedIndex = 0;
        $("#selectNewUserLevel").selectpicker('refresh');
    }

    document.getElementById("chkUserActive").checked = true;
    for (i = newUserLocationVue.rows.length - 1; i >= 0; i--) {
        newUserLocationVue.rows.$remove(newUserLocationVue.rows[i]);
    }
    loadAllUnanssignedLocation('selectNewUserLocation', newUserLocationVue);
}

function editResetAllEntry() {
    $('#tbEditUserId').val("");
    $("#tbEditUsercode").val("");
    $("#tbEditUsername").val("");
    $("#tbEditUserPass1").val("");
    $("#tbEditUserPass2").val("");
    var tempSelect = document.getElementById('selectEditUserLevel');
    if (tempSelect.selectedIndex > -1) {
        $("#selectEditUserLevel").selectedIndex = 0;
        $("#selectEditUserLevel").selectpicker('refresh');
    }

    document.getElementById("chkEditUserActive").checked = true;
    for (i = editUserLocationVue.rows.length - 1; i >= 0; i--) {
        editUserLocationVue.rows.$remove(editUserLocationVue.rows[i]);
    }
    //loadAllUnanssignedLocation('selectEditUserLocation', editUserLocationVue);
}

function filterTable() {
    var e = document.getElementById("slSearch");
    var strSearchColumnName = e.options[e.selectedIndex].value;

    userVuetable.moreParams = [
        'filter=' + strSearchColumnName + '|' + document.getElementById('inputSearchKeyId').value,
        'showAll=' + document.getElementById('chkUserShowAll').checked,
        'userId=' + document.getElementById('hfUserId').value
    ];

    userVuetable.$nextTick(function () {
        userVuetable.$broadcast('vuetable:refresh');
    });
}


function chkUserShowAll_onChanged() {
    filterTable();
    //userVuetable.moreParams = [
    //    'showAll=' + document.getElementById('chkUserShowAll').checked,
    //    'userId=' + document.getElementById('hfUserId').value
    //]

    //userVuetable.$nextTick(function () {
    //    userVuetable.$broadcast('vuetable:refresh')
    //})
}

