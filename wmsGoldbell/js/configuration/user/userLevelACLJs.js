﻿Vue.config.async = false;
var userLevelACLVue = new Vue({
    el: "#appUserLevelACL",
    data: {
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ],
        total: 0,
        gst: 0
    },
    computed: {
        total: function () {
            var t = 0;
            $.each(this.rows, function (i, e) {
                t += parseInt(e.quantity) * parseFloat(e.price);
            });
            return t;
        }
    },
    methods: {
        addRow: function (index) {
            this.rows.push({ price: 0 });
        },
        addRowNValue: function (id, action) {
            this.rows.push({ id: id, action: action});
        },
        removeRow: function (row) {
            //console.log(row);
            this.rows.$remove(row);
        },
        resetRow: function (index) {

        }
    }

});

function chkHeaderOnChanged(headerName) {

    switch(headerName) {
        case 'canRead':
            var v = document.getElementById('chkCanRead0').checked;
            for (i = 1; i <= userLevelACLVue.rows.length; i++) {
                document.getElementById("chkCanRead" + (i)).checked = v;
            }
            break;
        case 'canAdd':
            var v = document.getElementById('chkCanAdd0').checked;
            for (i = 1; i <= userLevelACLVue.rows.length; i++) {
                document.getElementById("chkCanAdd" + (i)).checked = v;
            }
            break;
        case 'canEdit':
            var v = document.getElementById('chkCanEdit0').checked;
            for (i = 1; i <= userLevelACLVue.rows.length; i++) {
                document.getElementById("chkCanEdit" + (i)).checked = v;
            }
            break;
        case 'canDelete':
            var v = document.getElementById('chkCanDelete0').checked;
            for (i = 1; i <= userLevelACLVue.rows.length; i++) {
                document.getElementById("chkCanDelete" + (i)).checked = v;
            }
            break;
        case 'canPrint':
            var v = document.getElementById('chkCanPrint0').checked;
            for (i = 1; i <= userLevelACLVue.rows.length; i++) {
                document.getElementById("chkCanPrint" + (i)).checked = v;
            }
            break;
        default:
            break;
    }
    
}

