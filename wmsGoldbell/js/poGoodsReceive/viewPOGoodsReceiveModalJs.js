﻿Vue.config.async = false;
var viewPOGRVue = new Vue({
    el: "#appViewPOGR",
    data: {
        //cb_print: [],
        total2Print: 0,
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        cbPrintCheck: function (e, row) {
            calculateTotal2Print();
            //if (e.target.checked) {
            //    this.rows.color = 'lightblue';
            //} else {
            //    this.rows.color = '';
            //}
        },
        addRow: function (line_num, company_code, item_id, part_code, uom, qty, scan_qty, scan_datetime, user) {
            this.rows.push({
                line_num: line_num, company_code: company_code, item_id: item_id, part_code: part_code, uom: uom,
                qty: qty, scan_qty: scan_qty, scan_datetime: scan_datetime, user: user
            });
        },
        removeRow: function (row) {
            this.rows.$remove(row);
        }
    }
});


$(document).ready(function () {

    $("#btnPOGRPrint").click(function () {
        printPOGR();
    });

    $('#divViewPOGRModalId').on('hidden.bs.modal', function () {
        clearAllViewPOGR();
    })
});

function cbAllOnClick() {
    var isChecked = document.getElementById('cbAll').checked;
    for (i = 0; i < viewPOGRVue.rows.length; i++) {
        document.getElementById('cbPrint' + (i + 1)).checked = isChecked;
        //document.getElementById('cbPrint' + (i + 1)).dispatchEvent(new Event('change'));
    }
    calculateTotal2Print();
}


function calculateTotal2Print() {
    var tempTotal = 0;
    for (i = 0; i < viewPOGRVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            tempTotal += 1;
        }
    }
    viewPOGRVue.total2Print = tempTotal;
}

function printPOGR() {
    for (i = 0; i < viewPOGRVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            console.log("Printing item Id =" + document.getElementById('tbViewPOGRItemId' + (i + 1)).value +
               " Part Code = " + document.getElementById('tbViewPOGRPartCode' + (i + 1)).value)
        }
    }
}

function clearAllViewPOGR() {
    clearViewPOGRInput();
    for (i = viewPOGRVue.rows.length - 1; i >= 0; i--) {
        viewPOGRVue.rows.$remove(viewPOGRVue.rows[i]);
    }
}

function clearViewPOGRInput() {
    document.getElementById('tbViewPOGRPurchId').value = "";
    document.getElementById('hfViewAXPOId').value = "";
    document.getElementById('tbViewPOGRCompanyCode').value = "";
    document.getElementById('tbViewPOGRVendorCode').value = "";
    document.getElementById('tbViewPOGRCreatedDate').value = "";
    document.getElementById('tbViewPOGRNumber').value = "";
    document.getElementById('cbAll').checked = false;
}


