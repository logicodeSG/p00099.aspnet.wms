﻿Vue.config.async = false;
var viewSOVue = new Vue({
    el: "#appViewSO",
    data: {
        //cb_print: [],
        total2Print: 0,
        rows: [
            // { item: "item1", quantity: 4 },
            //{ item: "item2", quantity: 2 }
        ]
    },
    methods: {
        cbPrintCheck: function (e, row) {
            calculateTotal2Print();
            //if (e.target.checked) {
            //    this.rows.color = 'lightblue';
            //} else {
            //    this.rows.color = '';
            //}
        },
        addRow: function (line_num, item_id, part_code, uom, qty, scan_qty, scan_datetime, user) {
            this.rows.push({
                line_num: line_num, item_id: item_id, part_code: part_code, uom: uom,
                qty: qty, scan_qty: scan_qty, scan_datetime: scan_datetime, user: user
            });
        },
        removeRow: function (row) {
            this.rows.$remove(row);
        }
    }
});


$(document).ready(function () {

    $("#btnSOPrint").click(function () {
        printSO();
    });

    $('#divViewSOModalId').on('hidden.bs.modal', function () {
        clearAllViewSO();
    })
});

function cbAllOnClick() {
    var isChecked = document.getElementById('cbAll').checked;
    for (i = 0; i < viewSOVue.rows.length; i++) {
        document.getElementById('cbPrint' + (i + 1)).checked = isChecked;
        //document.getElementById('cbPrint' + (i + 1)).dispatchEvent(new Event('change'));
    }
    calculateTotal2Print();
}


function calculateTotal2Print() {
    var tempTotal = 0;
    for (i = 0; i < viewSOVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            tempTotal += 1;
        }
    }
    viewSOVue.total2Print = tempTotal;
}

function printSO() {
    for (i = 0; i < viewSOVue.rows.length; i++) {
        if (document.getElementById('cbPrint' + (i + 1)).checked) {
            console.log("Printing item Id =" + document.getElementById('tbViewSOItemId' + (i + 1)).value +
               " Part Code = " + document.getElementById('tbViewSOPartCode' + (i + 1)).value)
        }
    }
}

function clearAllViewSO() {
    clearViewSOInput();
    for (i = viewSOVue.rows.length - 1; i >= 0; i--) {
        viewSOVue.rows.$remove(viewSOVue.rows[i]);
    }
}

function clearViewSOInput() {
    document.getElementById('tbViewSOSalesId').value = "";
    document.getElementById('tbViewSORouteId').value = "";
    document.getElementById('hfViewAXSOId').value = "";
    document.getElementById('tbViewSOCreatedDate').value = "";
    document.getElementById('cbAll').checked = false;
}


