﻿$(document).ready(function () {

    $("#btnSave").click(function () {
        saveGR();
    });
});

function saveGR() {
    swal({
        title: 'Please confirm this Goods Receive Saving?',
        text: "Are you sure you want to save this GR?",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirm'
    }).then(function () {
        var itemIdList = [1,2];
        var itemCodeList = ["ITEM0000001","ITEM0000002"];
        var itemDescriptionList = ["ITEM1","ITEM2"];
        var itemUOMList = ["PCS","PCS"];
        var itemRQuantityList = [5,3];
        var itemLocationIdList = [1,1];
        var macAdd = "SERVER";
        var curLocation = "1";

        var ce = {
            "argItemId": itemIdList
            ,"argItemCode": itemCodeList
            , "argItemDescription": itemDescriptionList
            , "argItemUOM": itemUOMList
            , "argItemRQuantity": itemRQuantityList
            , "argItemLocation": itemLocationIdList
            , "argGRReceiveDate": "07/05/2018"
            , "argGRPOId": 1
            , "argGRPOReferenceVersion": 1
            , "argGRCreatedBy": 1
            , "argGRDONumber": "DO-TEST"
            , "argGRDeviceSN": macAdd
            , "argGRLocationId": curLocation
        };

        $.ajax({
            url: 'ws/ims-ws.asmx/createGoodsReceive',
            contentType: "application/json",
            data: JSON.stringify(ce),
            dataType: 'JSON',
            cache: false,
            context: document.body,
            type: 'POST',
            success: function (data) {
                if (data.d == "true") {
                    swal('Save Goods Receive - Success', '', 'success');

                } else {
                    swal(
                            'Save Goods Receive - Fail',
                            data.d,
                            'error'
                            )
                }
                //stockVuetable.reloadStockTable();
            },
            error: function (err) {
                showSystemErrorMsg(err.responseText);
            }
        });

    })

}
