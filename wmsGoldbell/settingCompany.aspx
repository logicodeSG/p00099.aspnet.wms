﻿<%@ Page Title="" Language="C#" MasterPageFile="~/customSite.Master" AutoEventWireup="true" CodeBehind="settingCompany.aspx.cs" Inherits="wmsGoldbell.settingCompany" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="page-wrapper">
        <div class="row">
            <div id="divError" class="alert alert-danger hidden">
            </div>
            <div id="divCompanyName" class="form-group col-md-6">
                <label class="control-label" for="inputError">Company Name:</label>
                <input type="text" maxlength="50" class="form-control" id="tbCompanyName" disabled="disabled" />
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label>Address1:</label>
                <input type="text" maxlength="50" class="form-control" id="tbCompanyAddress1" disabled="disabled" />
            </div>
            <div class="form-group col-md-6">
                <label>Address2:</label>
                <input type="text" maxlength="50" class="form-control" id="tbCompanyAddress2" disabled="disabled" />
            </div>
            <div class="form-group col-md-6">
                <label>Address3:</label>
                <input type="text" maxlength="50" class="form-control" id="tbCompanyAddress3" disabled="disabled" />
            </div>
            <div class="form-group col-md-6">
                <label>Phone Number:</label>
                <input type="text" maxlength="20" class="form-control" id="tbCompanyPhoneNumber" disabled="disabled" />
            </div>
            <div class="form-group col-md-6">
                <label>Fax Number:</label>
                <input type="text" maxlength="20" class="form-control" id="tbCompanyFaxNumber" disabled="disabled" />
            </div>
            <div class="form-group col-md-6">
                <label>Email:</label>
                <input type="text" maxlength="50" class="form-control" id="tbCompanyEmail" disabled="disabled" />
            </div>
            <div class="form-group col-md-6">
                <label>Website:</label>
                <input type="text" maxlength="50" class="form-control" id="tbCompanyWebsite" disabled="disabled" />
            </div>
            <div class="form-group col-md-6">
                <label>Reg Number:</label>
                <input type="text" maxlength="20" class="form-control" id="tbCompanyRegNumber" disabled="disabled" />
            </div>
            <div class="form-group col-md-6">
                <label>GST Reg Number:</label>
                <input type="text" maxlength="20" class="form-control" id="tbCompanyGSTRegNumber" disabled="disabled" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-9 col-md-3">
                <a class="btn btn-block btn-social btn-linkedin" id="btnSaveCompany" style="text-align:center">
                    <i class="fa fa-save"></i>Save
                </a>
                <%--<button id="btnSaveCompany" type="button" class="btn btn-primary" data-dismiss="modal">Save</button>--%>
            </div>
        </div>
    </div>
    <script src="js/configuration/company/companyJs.js"></script>
</asp:Content>
