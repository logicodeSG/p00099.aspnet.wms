﻿using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.SqlServer;
using System.Web.Configuration;

[assembly: OwinStartupAttribute(typeof(wmsGoldbell.Startup))]
namespace wmsGoldbell
{
    public partial class Startup {

        public void Configuration(IAppBuilder app) {
            

            ConfigureAuth(app);

            GlobalConfiguration.Configuration.UseSqlServerStorage(WebConfigurationManager.AppSettings["DB_CONNECTION_STRING"]);

            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }
    }
}
