﻿<%@ Page Title="" Language="C#" MasterPageFile="~/customSite.Master" AutoEventWireup="true" CodeBehind="location.aspx.cs" Inherits="wmsGoldbell.location" %>
<%@ Register TagPrefix="web" TagName="tnLvTwoLocModal" Src="~/subModule/configuration/location/lvTwoLocModal.ascx"%>
<%@ Register TagPrefix="web" TagName="tnLvThreeLocModal" Src="~/subModule/configuration/location/lvThreeLocModal.ascx"%>
<%@ Register TagPrefix="web" TagName="tnNewLvTwoLocModal" Src="~/subModule/configuration/location/newLvTwoLocModal.ascx"%>
<%@ Register TagPrefix="web" TagName="modalEditLvTwoLoc" Src="~/subModule/configuration/location/editLvTwoLocModal.ascx"%>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <web:tnLvTwoLocModal id="lvTwoLocModalId" runat="server"/>
    <web:tnLvThreeLocModal id="TnLvThreeLocModalId" runat="server"/>
    <web:tnNewLvTwoLocModal id="newLvTwoLocModalId" runat="server"/>
    <web:modalEditLvTwoLoc id="editLvTwoLocModal" runat="server"/>

    <link href="ajnRes/ajnVuetableCss.css" rel="stylesheet" type="text/css" media="screen" />
    <div id="page-wrapper" style="padding:0px;">
        <div class="row">
            <div class="form-group col-lg-2 col-lg-offset-10">
                <div style="margin: 10px 0 0 0;">
                    <label >Show All</label>
                    <input id="chkLocationShowAll" type="checkbox" onchange="chkLocationShowAll_onChanged()" value="" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div id="appLocation">
                    <div class="table-responsive" style="height:92vh">
                        <vuetable
                            <%--id="itemEntryVueTableId"--%>
                            api-url="/ws/ims-ws.asmx/getAllLevelOneLocation"
                            table-class="table table-bordered table-striped table-hover"
                            :fields="columns"
                            :item-actions="locationActions"
                            :append-params="moreParams"      
                            pagination-path=""
                            :sort-order="sortOrder"
                            :multi-sort="multiSort"
                            ascending-icon="glyphicon glyphicon-chevron-up"
                            descending-icon="glyphicon glyphicon-chevron-down"
                            pagination-class=""
                            pagination-info-class=""
                            pagination-component-class=""
                            :pagination-component="paginationComponent"
                            :per-page="perPage"
                            wrapper-class="vuetable-wrapper"
                            table-wrapper=".vuetable-wrapper"
                            loading-class="loading"
                        ></vuetable>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <script src="js/configuration/location/locationJs.js"></script>
</asp:Content>