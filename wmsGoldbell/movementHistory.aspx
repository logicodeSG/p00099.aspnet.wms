﻿<%@ Page Title="" Language="C#" MasterPageFile="~/customSite.Master" AutoEventWireup="true" CodeBehind="movementHistory.aspx.cs" Inherits="wmsGoldbell.movementHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div id="page-wrapper" style="padding: 0px;">

        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Search Option
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label id="lblMHDocId" for="inputError" class="control-label">Doc ID:</label>
                            <input id="chkMHDocId" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control input-sm hidden" id="tbMHDocId" />
                        </div>
                        <div class="col-md-2">
                            <label id="lblMHSubDocId" for="inputError" class="control-label">Sub Doc ID:</label>
                            <input id="chkMHSubDocId" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control input-sm hidden" id="tbMHSubDocId" />
                        </div>
                        <div class="col-md-2">
                            <label id="lblMHLine" for="inputError" class="control-label">Line:</label>
                            <input id="chkMHLine" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control input-sm hidden" id="tbMHLine" />
                        </div>
                        <div class="col-md-2">
                            <label id="lblMHItemId" for="inputError" class="control-label">Item ID:</label>
                            <input id="chkMHItemId" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control hidden" id="tbMHItemId" />
                        </div>
                        <div class="col-md-2">
                            <label id="lblMHPartCode" for="inputError" class="control-label">Part Code:</label>
                            <input id="chkMHPartCode" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control input-sm hidden" id="tbMHPartCode" />
                        </div>
                        <div class="col-md-2">
                            <label id="lblMHLocFrom" for="inputError" class="control-label">Loc. From:</label>
                            <input id="chkMHLocFrom" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control input-sm hidden" id="tbMHLocFrom" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label id="lblMHLocTo" for="inputError" class="control-label">Loc. To:</label>
                            <input id="chkMHLocTo" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control input-sm hidden" id="tbMHLocTo" />
                        </div>
                        <div class="col-md-2">
                            <label id="lblMHQty" for="inputError" class="control-label">Qty:</label>
                            <input id="chkMHQty" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control input-sm hidden" id="tbMHQty" />
                        </div>
                        <div class="col-md-2">
                            <label id="lblMHUser" for="inputError" class="control-label">User:</label>
                            <input id="chkMHUser" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control input-sm hidden" id="tbMHUser" />
                        </div>
                        <div class="col-md-2">
                            <label id="lblMHType" for="inputError" class="control-label">Type:</label>
                            <input id="chkMHType" type="checkbox" value="" />
                            <input type="text" maxlength="50" class="form-control input-sm hidden" id="tbMHType" />
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="inputError">Date</label>
                            <input id="chkMHDate" type="checkbox" value="" />
                            <div id="divMHDate" class="input-group input-daterange hidden">
                                <input type="text" class="form-control input-sm" id="tbMHDateRangeFrom" value="" readonly="readonly" style="background-color: white; padding-left: 3px; padding-right: 3px;" />
                                <div class="input-group-addon input-sm">-</div>
                                <input type="text" class="form-control input-sm" id="tbMHDateRangeTo" value="" readonly="readonly" style="background-color: white; padding-left: 3px; padding-right: 3px;" />
                            </div>
                            <div style="text-align: right">
                            </div>
                        </div>
                    </div>
                    <div class="row form-inline">
                        <div class="col-md-3">
                            <label class="control-label" for="inputError">Show Entries: </label>
                            <select class="form-control" id="selectMHShowEntries" data-style="btn-sm btn-default" >
                                <option value="1000">1000</option>
                                <option value="2000">2000</option>
                                <option value="5000">5000</option>
                                <option value="10000">10000</option>
                                <option value="">All</option>
                            </select>
                            
                        </div>
                        <div class="col-md-offset-6 col-md-3">
                            <a class="btn btn-block btn-social btn-linkedin" id="btnMHSearch">
                                <i class="fa fa-search"></i>Search
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div id="appMHDivId">
                    <div class="table-responsive" style="height:65vh">
                        <vuetable
                            api-url="/ws/ims-ws.asmx/getAllMovementHistoryInfo"
                            table-class="table table-bordered table-striped table-hover"
                            :fields="columns"
                            :item-actions="actions"
                            :append-params="moreParams"      
                            pagination-path=""
                            :sort-order="sortOrder"
                            :multi-sort="multiSort"
                            ascending-icon="glyphicon glyphicon-chevron-up"
                            descending-icon="glyphicon glyphicon-chevron-down"
                            pagination-class=""
                            pagination-info-class=""
                            pagination-component-class=""
                            :pagination-component="paginationComponent"
                            :per-page="perPage"
                            wrapper-class="vuetable-wrapper"
                            table-wrapper="vuetable-wrapper"
                            loading-class="loading"
                            ></vuetable>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <script src="js/movementHistory/movementHistoryJs.js"></script>
</asp:Content>
