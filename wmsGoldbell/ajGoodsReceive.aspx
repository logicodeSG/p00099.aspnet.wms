﻿<%@ Page Title="" Language="C#" MasterPageFile="~/customSite.Master" AutoEventWireup="true" CodeBehind="ajGoodsReceive.aspx.cs" Inherits="wmsGoldbell.ajGoodsReceive" %>
<%@ Register TagPrefix="web" TagName="viewAJGoodsReceiveModal" Src="~/subModule/ajGoodsReceive/viewAJGoodsReceiveModal.ascx"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <web:viewAJGoodsReceiveModal id="viewAJGoodsReceiveModal" runat="server"/>
    <div id="page-wrapper" style="padding:0px;">
        <div class="row">   
            <div class="col-md-2 col-md-offset-7">
                <div style="margin: 10px 0 0 0; display:none;">
                    <label >Show All</label>
                    <input id="chkAJGRShowAll" type="checkbox" onchange="chkAJGRShowAll_onChanged()" value="" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="appAJGRDivId">
                    <div class="table-responsive" style="height:85vh">
                        <vuetable
                            api-url="/ws/ims-ws.asmx/getAllAJGoodsReceiveInfo"
                            table-class="table table-bordered table-striped table-hover"
                            :fields="columns"
                            :item-actions="actions"
                            :append-params="moreParams"      
                            pagination-path=""
                            :sort-order="sortOrder"
                            :multi-sort="multiSort"
                            ascending-icon="glyphicon glyphicon-chevron-up"
                            descending-icon="glyphicon glyphicon-chevron-down"
                            pagination-class=""
                            pagination-info-class=""
                            pagination-component-class=""
                            :pagination-component="paginationComponent"
                            :per-page="perPage"
                            wrapper-class="vuetable-wrapper"
                            table-wrapper="vuetable-wrapper"
                            loading-class="loading"
                            ></vuetable>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <script src="js/ajGoodsReceive/ajGoodsReceiveJs.js"></script>

</asp:Content>
