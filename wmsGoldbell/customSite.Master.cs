﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace wmsGoldbell
{
    public partial class customSite : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfSystemErrorToggle.Value = WebConfigurationManager.AppSettings["SYSTEM_ERROR"];
            //lblSystemTitle.Text = WebConfigurationManager.AppSettings["SYSTEM_NAME"];
            if (Session["curSessionUsername"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                hfUsername.Value = Session["curSessionUsername"].ToString();
                hfUserId.Value = Session["curSessionUserId"].ToString();
                hfWarehouseId.Value = Session["curSessionWarehouseId"].ToString();
                hfWarehouseCode.Value = Session["curSessionWarehouseCode"].ToString();
                hfUserLevelACL.Value = Session["curSessionUserLevelACL"].ToString();
            }
            
        }

        //protected void selectUserLocationOnChanged(object sender, EventArgs e)
        //{
        //    Session["curSessionWarehouseId"] = selectUserLocation.SelectedValue;
        //    hfWarehouseId.Value = Session["curSessionWarehouseId"].ToString();
        //}

        protected void btnUserLogout_Click(object sender, EventArgs e)
        {
            Session.Remove("curSessionUsername");
            Session.Remove("curSessionUserId");
            Session.Remove("curSessionWarehouseId");
            //Session.Remove("curMobileSessionUserLevelId");
            Response.Redirect("~/Login.aspx");
        }
    }
}