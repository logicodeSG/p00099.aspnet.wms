﻿<%@ Page Title="" Language="C#" MasterPageFile="~/customSite.Master" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="wmsGoldbell.dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="page-wrapper" style="overflow-y: scroll; height:650px;">
        <div class="row">
            <div class="col-lg-12">
                <h1>Dashboard</h1>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="btnSave">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-cube fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div id="lblTotalInventoryCost" class="huge">0</div>
                                <div id="lblTotalInventoryCostDescription">Inventory Cost</div>
                            </div>
                        </div>
                    </div>
                    <a href="itemList.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-wpforms fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div id="lblTotalSales" class="huge">0</div>
                                <div id="lblTotalSalesDescription">Current Month Sales</div>
                            </div>
                        </div>
                    </div>
                    <a href="salesOrder.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-handshake-o fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div id="lblTotalProfit" class="huge">0</div>
                                <div id="lblTotalProfitDescription">Current Month Profit</div>
                            </div>
                        </div>
                    </div>
                    <a href="salesOrder.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
      <%--      <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-upload fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div id="lblTotalOutgoingStock" class="huge">0</div>
                                <div>Total Outgoing Stock</div>
                            </div>
                        </div>
                    </div>
                    <a href="salesOrder.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>--%>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-3" style="font-size:20px;">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Sales Chart
                            </div>
                            <div class="col-md-4 col-md-offset-5">
                                <div class="input-group float-right" style="width: 350px;">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <div class="input-group input-daterange" id="divDateRangeSalesChart">
                                        <input type="text" class="form-control" id="tbSalesChartDateRangeFrom" value="" style="background-color: white; padding-left: 3px; padding-right: 3px;" />
                                        <div class="input-group-addon">-</div>
                                        <input type="text" class="form-control" id="tbSalesChartDateRangeTo" value="" style="background-color: white; padding-left: 3px; padding-right: 3px;" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" onclick="drawSalesChartByPeriod()">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body" style="height: 250px;">
                        <div id="chartSales" style="height: 250px;"></div>
                    </div>
                    <div class="panel-bottom">
                        <div id="divChartSalesMessage" style="height: 20px; font-style: italic;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-3" style="font-size:20px;">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Top 10 Sales Item
                            </div>
                            <div class="col-md-4 col-md-offset-5">
                                <div class="input-group float-right" style="width: 350px;">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <div class="input-group input-daterange" id="divDateRangeTopSalesItemChart">
                                        <input type="text" class="form-control" id="tbTopSalesItemChartDateRangeFrom" value="" style="background-color: white; padding-left: 3px; padding-right: 3px;" />
                                        <div class="input-group-addon">-</div>
                                        <input type="text" class="form-control" id="tbTopSalesItemChartDateRangeTo" value="" style="background-color: white; padding-left: 3px; padding-right: 3px;" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" onclick="drawTopSalesItemChartByPeriod()">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body" style="height: 350px;">
                        <div id="chartTopSalesItem" style="height: 350px; padding-bottom:40px;"></div>
                    </div>
                    <div class="panel-bottom">
                        <div id="divChartTopSalesItemMessage" style="height: 20px; font-style: italic;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/dashboard/dashboardJs.js"></script>
</asp:Content>
