﻿<%@ Page Title="" Language="C#" MasterPageFile="~/customSite.Master" AutoEventWireup="true" CodeBehind="settingUser.aspx.cs" Inherits="wmsGoldbell.settingUser" %>
<%@ Register TagPrefix="web" TagName="modal" Src="~/subModule/configuration/user/newUserModal.ascx"%>
<%@ Register TagPrefix="web" TagName="modalEditUser" Src="~/subModule/configuration/user/editUserModal.ascx"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <web:modal id="newUserModalId" runat="server"/>
    <web:modalEditUser id="editUserModalId" runat="server"/>

    <link href="ajnRes/ajnVuetableCss.css" rel="stylesheet" type="text/css" media="screen" />
    <div id="page-wrapper" style="padding:0px;">
        <div class="row">
            <div class="col-lg-3">
                    <a class="btn btn-block btn-social btn-linkedin" id="btnOpenDivCreateUser">
                                    <i class="fa fa-plus"></i> Create New User
                    </a>   
                </div>
            <div class="col-lg-2 col-lg-offset-7">
                <div style="margin: 10px 0 0 0;">
                    <label >Show All</label>
                    <input id="chkUserShowAll" type="checkbox" onchange="chkUserShowAll_onChanged()" value="" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="appUserDivId">
                    <div class="table-responsive" style="height:92vh">
                        <vuetable
                            api-url="/ws/ims-ws.asmx/getAllUserInfo"
                            table-class="table table-bordered table-striped table-hover"
                            :fields="columns"
                            :item-actions="actions"
                            :append-params="moreParams"      
                            pagination-path=""
                            :sort-order="sortOrder"
                            :multi-sort="multiSort"
                            ascending-icon="glyphicon glyphicon-chevron-up"
                            descending-icon="glyphicon glyphicon-chevron-down"
                            pagination-class=""
                            pagination-info-class=""
                            pagination-component-class=""
                            :pagination-component="paginationComponent"
                            :per-page="perPage"
                            wrapper-class="vuetable-wrapper"
                            table-wrapper="vuetable-wrapper"
                            loading-class="loading"
                        ></vuetable>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <script src="js/configuration/user/userJs.js"></script>
</asp:Content>