﻿using wmsGoldbell.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Data.SqlClient;

namespace wmsGoldbell.ws
{
    public class TSCLIB_DLL
    {
        [DllImport("TSCLIB.dll", EntryPoint = "about")]
        public static extern int about();

        [DllImport("TSCLIB.dll", EntryPoint = "openport")]
        public static extern int openport(string printername);

        [DllImport("TSCLIB.dll", EntryPoint = "barcode")]
        public static extern int barcode(string x, string y, string type,
                    string height, string readable, string rotation,
                    string narrow, string wide, string code);

        [DllImport("TSCLIB.dll", EntryPoint = "clearbuffer")]
        public static extern int clearbuffer();

        [DllImport("TSCLIB.dll", EntryPoint = "closeport")]
        public static extern int closeport();

        [DllImport("TSCLIB.dll", EntryPoint = "downloadpcx")]
        public static extern int downloadpcx(string filename, string image_name);

        [DllImport("TSCLIB.dll", EntryPoint = "formfeed")]
        public static extern int formfeed();

        [DllImport("TSCLIB.dll", EntryPoint = "nobackfeed")]
        public static extern int nobackfeed();

        [DllImport("TSCLIB.dll", EntryPoint = "printerfont")]
        public static extern int printerfont(string x, string y, string fonttype,
                        string rotation, string xmul, string ymul,
                        string text);

        [DllImport("TSCLIB.dll", EntryPoint = "printlabel")]
        public static extern int printlabel(string set, string copy);

        [DllImport("TSCLIB.dll", EntryPoint = "sendcommand")]
        public static extern int sendcommand(string printercommand);

        [DllImport("TSCLIB.dll", EntryPoint = "setup")]
        public static extern int setup(string width, string height,
                  string speed, string density,
                  string sensor, string vertical,
                  string offset);

        [DllImport("TSCLIB.dll", EntryPoint = "windowsfont")]
        public static extern int windowsfont(int x, int y, int fontheight,
                        int rotation, int fontstyle, int fontunderline,
                        string szFaceName, string content);

    }

    /// <summary>
    /// Summary description for ims_ws
    /// </summary>
    [WebService(Namespace = "http://ajn.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class ims_ws : System.Web.Services.WebService
    {

        private String XML_FOLDER_PATH = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "xml");

        private String axConnectionString = WebConfigurationManager.AppSettings["AX_DB_CONNECTION_STRING"];
        //Module Master
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        private string getAXMessageId()
        {

            //DateTime todaysDate = DateTime.Now;
            //string day, month, year, hour, minute, second;
            string AXMessageId;

            //day = todaysDate.Day.ToString();
            //month = todaysDate.Month.ToString();
            //year = todaysDate.Year.ToString();
            //hour = todaysDate.Hour.ToString();
            //minute = todaysDate.Minute.ToString();
            //second = todaysDate.Second.ToString();
            //string date;
            //date = day + month + year + hour + minute + second;

            AXMessageId = "{33000000-0000-0000-0000-" + DateTime.Now.ToString("ddMMyyHHmmss") + "}";

            return AXMessageId;
        }

        private String getLastSyncDatetime()
        {

            DateTime dtMinus8hours = DateTime.Now.AddHours(-8);
            String currentSyncDatetime = dtMinus8hours.ToString("yyyy-MM-dd HH:mm:ss");
            return currentSyncDatetime;
        }

        [WebMethod]
        public sysUserS[] getUserByUsernamePassword(String argUsername, String argUserPass, String argUserlevel)
        {
            SHA256Managed h = new SHA256Managed();

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyUsername", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argUsername},
                //new sqlQueryParameter {strParameter="@keyUserPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object)argUserPass}
                new sqlQueryParameter {strParameter="@keyUserPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object) Encoding.Unicode.GetString(h.ComputeHash(Encoding.Unicode.GetBytes(argUserPass)))}
            };

            DataTable curDt = curDb.getDataTable("[selectUserByUsernamePassword]", curQueryParameter);

            List<sysUserS> list = new List<sysUserS>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                if (argUserlevel.Length != 0)
                {
                    string[] separators = { "," };
                    string[] arrayUserlevel = argUserlevel.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    Boolean isValid = false;
                    foreach (var ul in arrayUserlevel)
                    {
                        if (ul == curDt.Rows[i][4].ToString())
                        {
                            isValid = true;
                        }
                    }

                    sysUserS[] emptyItem = { };
                    if (!isValid) return emptyItem;
                }

                list.Add(new sysUserS(curDt.Rows[i][0].ToString()
                , curDt.Rows[i][1].ToString()
                , curDt.Rows[i][2].ToString()
                , curDt.Rows[i][7].ToString()
                , curDt.Rows[i][9].ToString()
                , (Boolean)curDt.Rows[i][5]
                , curDt.Rows[i][6].ToString()
                ));
            }

            sysUserS[] curItem = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curItem;
        }

        [WebMethod]
        public String updateUserPassword(String argUserId, String argUsername, String argUserOldPass, String argUserNewPass)
        {
            database curDb = new database();
            curDb.openConnection();
            try
            {
                Int64 bigintUserId = Int64.Parse(argUserId);
                SHA256Managed h = new SHA256Managed();



                if (getUserByUsernamePassword(argUsername, argUserOldPass, "").Length > 0)
                {

                    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserId)},
                        //new sqlQueryParameter {strParameter="@keyUserOldPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object) argUserOldPass},
                        //new sqlQueryParameter {strParameter="@keyUserNewPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object) argUserNewPass}
                        new sqlQueryParameter {strParameter="@keyUserOldPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object) Encoding.Unicode.GetString(h.ComputeHash(Encoding.Unicode.GetBytes(argUserOldPass)))},
                        new sqlQueryParameter {strParameter="@keyUserNewPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object) Encoding.Unicode.GetString(h.ComputeHash(Encoding.Unicode.GetBytes(argUserNewPass)))}
                            };
                    int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateUserPassword", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert/update/delete failed");
                    }
                }
                else
                {
                    throw new Exception("Invalid Old Password");
                }

            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                return e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return "true";
        }

        //Integration - AX INVENTORY
        //+++++++++++++++++++++++++++++++++++++

        //Get All AX INVENTORY From AX DB View
        [WebMethod]
        public String retrieveAllAXInventory()
        {
            String isSuccess = "Failed Retrieve AX Inventory";

            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            DataTable curDt;
            int queryResult = -99;

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    axConn.Open();
                    //Get all AX INVENTORY from AX DB View
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT DISTINCT [DATAAREAID]" +
                          ",[ITEMID]" +
                          ",[PARTCODE]" +
                          "FROM [AX_WMS_INVENTITEMBARCODE]" +
                          "GROUP BY [DATAAREAID],[ITEMID],[PARTCODE]", axConn);
                    axCommand.CommandTimeout = 10000;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX Inventory from AX DB View
                    DataTable axInventoryDt = new DataTable();

                    int noOfAXInventory = sda.Fill(axInventoryDt);
                    //no AX Inventory is found
                    //if (noOfAXInventory <= 0) return "true";

                    //delete all AX Inventory
                    curQueryParameter = new List<sqlQueryParameter>()
                    {
                    };
                    queryResult = curDb.insertUpdateDeleteDataIntoTable("deleteAllInventory", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("Failed to delete All Inventory");
                    }

                    Int64 axInventoryId = -1;
                    //Loop through all AXPO from AX DB View
                    for (int i = 0; i < noOfAXInventory; i++)
                    {
                        String tempAXINVENT_DATAAREAID = axInventoryDt.Rows[i][0].ToString();
                        String tempAXINVENT_ITEMID = axInventoryDt.Rows[i][1].ToString();
                        String tempAXINVENT_DOT_PARTCODE = axInventoryDt.Rows[i][2].ToString();
                        //String tempAXINVENT_NAME = axInventoryDt.Rows[i][3].ToString();
                        //String tempAXINVENT_DESCRIPTION = axInventoryDt.Rows[i][4].ToString();
                        //String tempAXINVENT_ITEMTYPE = axInventoryDt.Rows[i][5].ToString();
                        //String tempAXINVENT_InventUnitID = axInventoryDt.Rows[i][6].ToString();
                        //String tempAXINVENT_PurchUnitID = axInventoryDt.Rows[i][7].ToString();
                        //String tempAXINVENT_SalesUnitID = axInventoryDt.Rows[i][8].ToString();
                        //String tempAXINVENT_isSerialized = axInventoryDt.Rows[i][9].ToString();
                        //String tempAXINVENT_active = axInventoryDt.Rows[i][10].ToString();
                        //String tempAXINVENT_StartDate = axInventoryDt.Rows[i][11] == DBNull.Value ? "" : axInventoryDt.Rows[i][11].ToString();
                        //String tempAXINVENT_PRODUCT = axInventoryDt.Rows[i][12].ToString();
                        //String tempAXINVENT_CREATEDDATETIME = axInventoryDt.Rows[i][13].ToString();
                        //String tempAXINVENT_MODIFIEDDATETIME = axInventoryDt.Rows[i][14].ToString();
                        //String tempAXINVENT_ITEMGROUPID = axInventoryDt.Rows[i][15].ToString();

                        object tempAXINVENT_NAME = DBNull.Value;
                        object tempAXINVENT_DESCRIPTION = DBNull.Value;
                        object tempAXINVENT_ITEMTYPE = DBNull.Value;
                        object tempAXINVENT_InventUnitID = DBNull.Value; ;
                        object tempAXINVENT_PurchUnitID = DBNull.Value;
                        object tempAXINVENT_SalesUnitID = DBNull.Value;
                        object tempAXINVENT_isSerialized = DBNull.Value;
                        object tempAXINVENT_active = DBNull.Value;
                        object tempAXINVENT_StartDate = DBNull.Value;
                        object tempAXINVENT_PRODUCT = DBNull.Value;
                        object tempAXINVENT_CREATEDDATETIME = DBNull.Value;
                        object tempAXINVENT_MODIFIEDDATETIME = DBNull.Value;
                        object tempAXINVENT_ITEMGROUPID = DBNull.Value;


                        curQueryParameter = new List<sqlQueryParameter>()
                        {
                        };

                        curDt = curDb.getDataTable("[selectNextAXInventoryId]", curQueryParameter);

                        List<Int64> list = new List<Int64>();
                        for (int j = 0; j < curDt.Rows.Count; j++)
                        {
                            list.Add(Convert.ToInt64(curDt.Rows[j][0]));
                        }
                        axInventoryId = list[0];

                        curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axInventoryId)},
                                new sqlQueryParameter {strParameter="@keyDATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXINVENT_DATAAREAID},
                                new sqlQueryParameter {strParameter="@keyITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXINVENT_ITEMID},
                                new sqlQueryParameter {strParameter="@keyDOT_PARCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXINVENT_DOT_PARTCODE},
                                new sqlQueryParameter {strParameter="@keyNAME", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXINVENT_NAME},
                                new sqlQueryParameter {strParameter="@keyDESCRIPTION", ParameterType=SqlDbType.NVarChar, ParameterLength=1000, keyData = (Object)tempAXINVENT_DESCRIPTION},
                                new sqlQueryParameter {strParameter="@keyITEMTYPE", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                //new sqlQueryParameter {strParameter="@keyITEMTYPE", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(tempAXINVENT_ITEMTYPE)},
                                new sqlQueryParameter {strParameter="@keyInventUnitID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXINVENT_InventUnitID},
                                new sqlQueryParameter {strParameter="@keyPurhUnitID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXINVENT_PurchUnitID},
                                new sqlQueryParameter {strParameter="@keySalesUnitID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXINVENT_SalesUnitID},
                                new sqlQueryParameter {strParameter="@keyIsSerialized", ParameterType=SqlDbType.Int, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyActive", ParameterType=SqlDbType.Int, keyData = (Object)DBNull.Value},
                                //new sqlQueryParameter {strParameter="@keyIsSerialized", ParameterType=SqlDbType.Int, keyData = Convert.ToInt32(tempAXINVENT_isSerialized)},
                                //new sqlQueryParameter {strParameter="@keyActive", ParameterType=SqlDbType.Int, keyData = Convert.ToInt32(tempAXINVENT_active)},
                                new sqlQueryParameter {strParameter="@keyStartDate", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                //new sqlQueryParameter {strParameter="@keyStartDate", ParameterType=SqlDbType.DateTime, keyData = tempAXINVENT_StartDate == "" ? (Object)DBNull.Value : tempAXINVENT_StartDate},
                                new sqlQueryParameter {strParameter="@keyPRODUCT", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                //new sqlQueryParameter {strParameter="@keyPRODUCT", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(tempAXINVENT_PRODUCT)},
                                new sqlQueryParameter {strParameter="@keyCREATEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXINVENT_CREATEDDATETIME},
                                new sqlQueryParameter {strParameter="@keyMODIFIEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXINVENT_MODIFIEDDATETIME},
                                new sqlQueryParameter {strParameter="@keyITEMGROUPID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXINVENT_ITEMGROUPID},
                            };
                        queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXInventory", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("insert ax inventory failed id=" + axInventoryId + " item id =" + tempAXINVENT_ITEMID);
                        }

                    }
                    isSuccess = "true";
                }
                catch (Exception e)
                {
                    curDb.rollbackNCloseConnection();
                    isSuccess = e.Message;
                    return isSuccess;
                }
                if (!curDb.checkIsConnectionClosed())
                    curDb.closeConnection();
                return isSuccess;
            }
        }

        [WebMethod]
        public axInventoryObject[] getAllAXInventory()
        {
            axInventoryObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            try
            {
                //if (!(retrieveAllAXInventory() == "true"))
                //    throw new Exception("Failed Retrieve AX Inventory");

                DataTable curDt = curDb.getDataTable("[selectAllAXInventory]", curQueryParameter);

                List<axInventoryObject> list = new List<axInventoryObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axInventoryObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        ));
                    //list.Add(new axInventoryObject(curDt.Rows[i][0].ToString()
                    //    , curDt.Rows[i][1].ToString()
                    //    , curDt.Rows[i][2].ToString()
                    //    , curDt.Rows[i][3].ToString()
                    //    , curDt.Rows[i][4].ToString()
                    //    , curDt.Rows[i][5].ToString()
                    //    , curDt.Rows[i][6].ToString()
                    //    , curDt.Rows[i][7].ToString()
                    //    , curDt.Rows[i][8].ToString()
                    //    , curDt.Rows[i][9].ToString()
                    //    , curDt.Rows[i][10].ToString()
                    //    , curDt.Rows[i][11].ToString()
                    //    , curDt.Rows[i][12].ToString()
                    //    , curDt.Rows[i][13].ToString()
                    //    , curDt.Rows[i][14].ToString()
                    //    , curDt.Rows[i][15].ToString()
                    //    , curDt.Rows[i][16].ToString()
                    //    ));
                }
                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axInventoryObject[] getAXInventory4Printing(String argItemId, String argPartcode, String argDescription)
        {
            axInventoryObject[] curList = null;

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    axConn.Open();
                    //Get all AX INVENTORY from AX DB View
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT DISTINCT [DATAAREAID] " +
                          ",[ITEMID]" +
                          ",[PARTCODE]" +
                          ",[PRODUCT_NAME] " +
                          "FROM [AX_WMS_INVENTITEMBARCODE] " +
                          "WHERE [ITEMID] LIKE '%" + argItemId + "%' " +
                          "AND [PARTCODE] LIKE '%" + argPartcode + "%' " +
                          "AND [PRODUCT_NAME] LIKE '%" + argDescription + "%' " +
                          "GROUP BY [DATAAREAID],[ITEMID],[PARTCODE],[PRODUCT_NAME]", axConn);
                    axCommand.CommandTimeout = 10000;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX Inventory from AX DB View
                    DataTable axInventoryDt = new DataTable();

                    int noOfAXInventory = sda.Fill(axInventoryDt);

                    List<axInventoryObject> list = new List<axInventoryObject>();
                    for (int i = 0; i < noOfAXInventory; i++)
                    {
                        list.Add(new axInventoryObject(
                            "-1",
                            axInventoryDt.Rows[i][0].ToString()
                            , axInventoryDt.Rows[i][1].ToString()
                            , axInventoryDt.Rows[i][2].ToString()
                            , axInventoryDt.Rows[i][3].ToString()
                            ));
                    }
                    curList = list.ToArray();
                }
                catch (Exception e)
                {
                    //curDb.rollbackNCloseConnection();
                }
            }

            return curList;
        }

        [WebMethod]
        public String getCountAXInventory()
        {
            String result = "Failed";
            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            try
            {
                DataTable curDt = curDb.getDataTable("[selectCountAXInventory]", curQueryParameter);
                result = curDt.Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return result;
        }

        [WebMethod]
        public String getAXInventoryMaxId()
        {
            String result = "Failed";
            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            try
            {
                //if (!(retrieveAllAXInventory() == "true"))
                //    throw new Exception("Failed Retrieve AX Inventory");

                DataTable curDt = curDb.getDataTable("[selectAXInventoryMaxId]", curQueryParameter);
                if (curDt.Rows.Count <= 0)
                {
                    result = "0";
                }
                else
                {
                    result = curDt.Rows[0][0].ToString();
                }
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return result;
        }

        [WebMethod]
        public axInventoryObject[] getAllAXInventoryByIdRange(String argIdFrom, String argIdTo)
        {
            axInventoryObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            try
            {

                curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyIdFrom", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argIdFrom)},
                                    new sqlQueryParameter {strParameter="@keyIdTo", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argIdTo)},
                                    };

                DataTable curDt = curDb.getDataTable("[selectAXInventoryByIdRange]", curQueryParameter);

                List<axInventoryObject> list = new List<axInventoryObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axInventoryObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        ));
                    //list.Add(new axInventoryObject(curDt.Rows[i][0].ToString()
                    //    , curDt.Rows[i][1].ToString()
                    //    , curDt.Rows[i][2].ToString()
                    //    , curDt.Rows[i][3].ToString()
                    //    , curDt.Rows[i][4].ToString()
                    //    , curDt.Rows[i][5].ToString()
                    //    , curDt.Rows[i][6].ToString()
                    //    , curDt.Rows[i][7].ToString()
                    //    , curDt.Rows[i][8].ToString()
                    //    , curDt.Rows[i][9].ToString()
                    //    , curDt.Rows[i][10].ToString()
                    //    , curDt.Rows[i][11].ToString()
                    //    , curDt.Rows[i][12].ToString()
                    //    , curDt.Rows[i][13].ToString()
                    //    , curDt.Rows[i][14].ToString()
                    //    , curDt.Rows[i][15].ToString()
                    //    , curDt.Rows[i][16].ToString()
                    //    ));
                }
                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        //Integration - AX PO
        //+++++++++++++++++++++++++++++++++++++

        //Get All Pending AX PO From AX DB View
        [WebMethod]
        public String retrieveAllAXPurchaseOrder()
        {
            String isSuccess = "Failed Retrieve AX Purchase Order";

            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            DataTable curDt;
            int queryResult = -99;
            String lastSyncDatetime = "";
            String currentSyncDatetime = getLastSyncDatetime();

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST PO SYNC"},
                    };


                    curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                    lastSyncDatetime = curDt.Rows[0][2].ToString();
                    if (String.IsNullOrEmpty(lastSyncDatetime))
                        lastSyncDatetime = "1999-01-01 00:00:00";

                    axConn.Open();
                    //Get all AX PO from AX DB View to update status to open
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT DISTINCT AX_WMS_OPN_PURCHTABLE.[DATAAREAID]" +
                        ", AX_WMS_OPN_PURCHTABLE.[PURCHID]" +
                        ", AX_WMS_OPN_PURCHTABLE.[PURCHNAME]" +
                        ", AX_WMS_OPN_PURCHTABLE.[ORDERACCOUNT]" +
                        ", AX_WMS_OPN_PURCHTABLE.[INVOICEACCOUNT]" +
                        ", '' AS [LastConfirmationDate]" +
                        ", AX_WMS_OPN_PURCHTABLE.[DELIVERYDATE]" +
                        ", AX_WMS_OPN_PURCHTABLE.[CREATEDDATETIME] " +
                        "FROM AX_WMS_OPN_PURCHTABLE " +
                        "JOIN AX_WMS_OPN_PURCHLINE ON AX_WMS_OPN_PURCHTABLE.PURCHID = AX_WMS_OPN_PURCHLINE.PURCHID ", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX PO from AX DB View
                    DataTable axPODt = new DataTable();

                    int noOfAXPO = sda.Fill(axPODt);


                    Int64 axpoId = -1;
                    for (int i = 0; i < noOfAXPO; i++)
                    {
                        String tempAXPO_DATAAREAID = axPODt.Rows[i][0].ToString();
                        String tempAXPO_PURCHID = axPODt.Rows[i][1].ToString();

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXPO_PURCHID}
                        };
                        curDt = curDb.getDataTable("selectAXPOByAXPurchId", curQueryParameter);


                        //++++++++++++++++++++++++++++++
                        //If AX PO Existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        if (curDt.Rows.Count > 0)
                        {
                            axpoId = Convert.ToInt64(curDt.Rows[0][0].ToString());
                            //Update PO to Pending status if PO is still in AX View
                            curQueryParameter = new List<sqlQueryParameter>()
                                        {
                                            new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPO_PURCHID},
                                            new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                                            new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                        };
                            queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXPOCompletedByAXPurchId", curQueryParameter);
                            if (queryResult == -99)
                            {
                                throw new Exception("Update AX Purchase Order Detail Status Failed");
                            }
                        }

                    }

                    //++++++++++++++++++++++++++++++
                    //Select PO with modified data
                    //++++++++++++++++++++++++++++++
                    axCommand = new SqlCommand(
                        "SELECT DISTINCT AX_WMS_OPN_PURCHTABLE.[DATAAREAID]" +
                        ", AX_WMS_OPN_PURCHTABLE.[PURCHID]" +
                        ", AX_WMS_OPN_PURCHTABLE.[PURCHNAME]" +
                        ", AX_WMS_OPN_PURCHTABLE.[ORDERACCOUNT]" +
                        ", AX_WMS_OPN_PURCHTABLE.[INVOICEACCOUNT]" +
                        ", '' AS [LastConfirmationDate]" +
                        ", AX_WMS_OPN_PURCHTABLE.[DELIVERYDATE]" +
                        ", AX_WMS_OPN_PURCHTABLE.[CREATEDDATETIME] " +
                        "FROM AX_WMS_OPN_PURCHTABLE " +
                        "JOIN AX_WMS_OPN_PURCHLINE ON AX_WMS_OPN_PURCHTABLE.PURCHID = AX_WMS_OPN_PURCHLINE.PURCHID " +
                        "WHERE AX_WMS_OPN_PURCHLINE.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX PO from AX DB View
                    axPODt = new DataTable();

                    noOfAXPO = sda.Fill(axPODt);


                    for (int i = 0; i < noOfAXPO; i++)
                    {
                        String tempAXPO_DATAAREAID = axPODt.Rows[i][0].ToString();
                        String tempAXPO_PURCHID = axPODt.Rows[i][1].ToString();
                        String tempAXPO_PURCHNAME = axPODt.Rows[i][2].ToString();
                        String tempAXPO_ORDERACCOUNT = axPODt.Rows[i][3].ToString();
                        String tempAXPO_INVOICEACCOUNT = axPODt.Rows[i][4].ToString();
                        String tempAXPO_LastConfirmationDate = axPODt.Rows[i][5].ToString();
                        String tempAXPO_DELIVERYDATE = axPODt.Rows[i][6].ToString();
                        String tempAXPO_CREATEDDATETIME = axPODt.Rows[i][7].ToString();

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXPO_PURCHID}
                        };
                        curDt = curDb.getDataTable("selectAXPOByAXPurchId", curQueryParameter);


                        //++++++++++++++++++++++++++++++
                        //If AX PO Existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        if (curDt.Rows.Count > 0)
                        {
                            axpoId = Convert.ToInt64(curDt.Rows[0][0].ToString());

                            axCommand = new SqlCommand(
                            "SELECT DISTINCT [DATAAREAID] " +
                              ",[PURCHID] " +
                              ",[LINENUMBER] " +
                              ",[ITEMID] " +
                              ",[NAME] " +
                              ",[DOT_PARTCODE] " +
                              ",[ORDERACCOUNT] " +
                              ",[INVOICEACCOUNT] " +
                              ",[PURCHUNIT] " +
                              ",[INVENTLOCATIONID] " +
                              ",[INVENTSITEID] " +
                              ",[WMSLOCATIONID] " +
                              ",[QTYORDERED] " +
                              ",[PURCHRECEIVEDNOW] " +
                              "FROM [AX_WMS_OPN_PURCHLINE] " +
                              "WHERE [PURCHID] = @keyAXPO_PURCHID " +
                              "AND AX_WMS_OPN_PURCHLINE.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXPO_PURCHID", tempAXPO_PURCHID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axPODetailDt = new DataTable();
                            //AX PO Detail in AX DB View
                            sda.Fill(axPODetailDt);


                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXPO_PURCHID}
                            };
                            //AX PO Detail in WMS DB
                            DataTable curAXPODetailDt = curDb.getDataTable("selectAXPODetailByAXPurchId", curQueryParameter);
                            
                            Boolean isPOStatusUpdated = false; //to make PO status change to pending if there is at least one new line added
                            //Loop through each of the AXPO Detail
                            for (int j = 0; j < axPODetailDt.Rows.Count; j++)
                            {
                                String tempAXPOD_DATAAREAID = axPODetailDt.Rows[j][0].ToString();
                                String tempAXPOD_PURCHID = axPODetailDt.Rows[j][1].ToString();
                                String tempAXPOD_LINENUMBER = axPODetailDt.Rows[j][2].ToString();
                                String tempAXPOD_ITEMID = axPODetailDt.Rows[j][3].ToString();
                                String tempAXPOD_NAME = axPODetailDt.Rows[j][4].ToString();
                                String tempAXPOD_DOT_PARTCODE = axPODetailDt.Rows[j][5].ToString();
                                String tempAXPOD_ORDERACCOUNT = axPODetailDt.Rows[j][6].ToString();
                                String tempAXPOD_INVOICEACCOUNT = axPODetailDt.Rows[j][7].ToString();
                                String tempAXPOD_PURCHUNIT = axPODetailDt.Rows[j][8].ToString();
                                String tempAXPOD_INVENTLOCATIONID = axPODetailDt.Rows[j][9].ToString();
                                String tempAXPOD_INVENTSITEID = axPODetailDt.Rows[j][10].ToString();
                                String tempAXPOD_WMSLOCATIONID = axPODetailDt.Rows[j][11].ToString();
                                String tempAXPOD_QTYORDERED = axPODetailDt.Rows[j][12].ToString();
                                String tempAXPOD_PURCHRECEIVEDNOW = axPODetailDt.Rows[j][13].ToString();

                                Boolean poDetailExisted = false;
                                for (int k = 0; k < curAXPODetailDt.Rows.Count; k++)
                                {

                                    String tempWMSPOD_id = curAXPODetailDt.Rows[k][0].ToString();
                                    String tempWMSPOD_LINENUMBER = curAXPODetailDt.Rows[k][4].ToString();
                                    String tempWMSPOD_ITEMID = curAXPODetailDt.Rows[k][5].ToString().Trim();
                                    String tempWMSPOD_DOT_PARTCODE = curAXPODetailDt.Rows[k][7].ToString().Trim();
                                    String tempWMSPOD_scanQuantity = String.IsNullOrEmpty(curAXPODetailDt.Rows[k][16].ToString()) ? "0" : curAXPODetailDt.Rows[k][16].ToString();

                                    //If AX PO Detail Existed in WMS DB
                                    if (tempAXPOD_ITEMID.Trim() == tempWMSPOD_ITEMID.Trim() &&
                                        tempAXPOD_DOT_PARTCODE.Trim() == tempWMSPOD_DOT_PARTCODE.Trim() &&
                                        (Convert.ToDecimal(tempAXPOD_LINENUMBER) == Convert.ToDecimal(tempWMSPOD_LINENUMBER)))
                                    {
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\
                                        //Update quantity changes
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                        queryResult = setAXPODetail(curDb, tempAXPOD_DATAAREAID, tempAXPOD_PURCHID, tempAXPOD_LINENUMBER, tempAXPOD_ITEMID,
                                        tempAXPOD_NAME, tempAXPOD_DOT_PARTCODE, tempAXPOD_ORDERACCOUNT, tempAXPOD_INVOICEACCOUNT, tempAXPOD_PURCHUNIT, tempAXPOD_INVENTLOCATIONID,
                                        tempAXPOD_INVENTSITEID, tempAXPOD_WMSLOCATIONID, tempAXPOD_QTYORDERED, tempAXPOD_PURCHRECEIVEDNOW);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("update ax po detail failed");
                                        }

                                        Int64 curStatusId = 9;
                                        if (Convert.ToDecimal(tempWMSPOD_scanQuantity) == 0)
                                        {
                                            curStatusId = 1;
                                        }
                                        else if (Convert.ToDecimal(tempAXPOD_QTYORDERED) > Convert.ToDecimal(tempWMSPOD_scanQuantity))
                                        {
                                            curStatusId = 7;
                                        }

                                        curQueryParameter = new List<sqlQueryParameter>()
                                        {
                                                new sqlQueryParameter {strParameter="@keyTableName", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)"tbl_ax_poDetail"},
                                                new sqlQueryParameter {strParameter="@keyFieldName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"tbl_status_Id"},
                                                new sqlQueryParameter {strParameter="@keyFieldValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)curStatusId.ToString()},
                                                new sqlQueryParameter {strParameter="@keyIDName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"id"},
                                                new sqlQueryParameter {strParameter="@keyIDValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)tempWMSPOD_id}
                                            };
                                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateTableSingleField", curQueryParameter);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("Update AX Purchase Order Detail Status Failed");
                                        }

                                        queryResult = insertMovementHistory(curDb, tempAXPO_DATAAREAID, tempAXPO_PURCHID, "", tempAXPOD_ITEMID, tempAXPOD_DOT_PARTCODE,
                                            tempAXPOD_INVENTLOCATIONID, tempAXPOD_INVENTSITEID, tempAXPOD_WMSLOCATIONID, "", "", "", tempAXPOD_QTYORDERED, "-1", "SYSTEM", sharedRes.AX2LGC_PO_DETAIL_UPDATE,
                                            tempAXPOD_LINENUMBER);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("insert movement history failed");
                                        }

                                        poDetailExisted = true;
                                        break;
                                    }
                                }

                                //if AX PO Detail no existed in WMS DB
                                if (!poDetailExisted)
                                {
                                    queryResult = insertAXPODetail(curDb, axpoId.ToString(), tempAXPOD_DATAAREAID, tempAXPOD_PURCHID, tempAXPOD_LINENUMBER, tempAXPOD_ITEMID,
                                        tempAXPOD_NAME, tempAXPOD_DOT_PARTCODE, tempAXPOD_ORDERACCOUNT, tempAXPOD_INVOICEACCOUNT, tempAXPOD_PURCHUNIT, tempAXPOD_INVENTLOCATIONID,
                                        tempAXPOD_INVENTSITEID, tempAXPOD_WMSLOCATIONID, tempAXPOD_QTYORDERED, tempAXPOD_PURCHRECEIVEDNOW);
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert ax po detail failed");
                                    }

                                    queryResult = insertMovementHistory(curDb, tempAXPO_DATAAREAID, tempAXPO_PURCHID, "", tempAXPOD_ITEMID, tempAXPOD_DOT_PARTCODE,
                                        tempAXPOD_INVENTLOCATIONID, tempAXPOD_INVENTSITEID, tempAXPOD_WMSLOCATIONID, "", "", "", tempAXPOD_QTYORDERED, "-1", "SYSTEM", sharedRes.AX2LGC_PO_DETAIL_INSERT, tempAXPOD_LINENUMBER);
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert movement history failed");
                                    }

                                }

                            }


                        }
                        //++++++++++++++++++++++++++++++
                        //if AX PO no existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        else
                        {
                            axCommand = new SqlCommand(
                            "SELECT DISTINCT [DATAAREAID] " +
                              ",[PURCHID] " +
                              ",[LINENUMBER] " +
                              ",[ITEMID] " +
                              ",[NAME] " +
                              ",[DOT_PARTCODE] " +
                              ",[ORDERACCOUNT] " +
                              ",[INVOICEACCOUNT] " +
                              ",[PURCHUNIT] " +
                              ",[INVENTLOCATIONID] " +
                              ",[INVENTSITEID] " +
                              ",[WMSLOCATIONID] " +
                              ",[QTYORDERED] " +
                              ",[PURCHRECEIVEDNOW] " +
                              "FROM [AX_WMS_OPN_PURCHLINE] " +
                              "WHERE [PURCHID] = @keyAXPO_PURCHID", axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXPO_PURCHID", tempAXPO_PURCHID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axPODetailDt = new DataTable();
                            //AX PO Detail in AX DB View
                            sda.Fill(axPODetailDt);

                            curQueryParameter = new List<sqlQueryParameter>()
                            {
                            };

                            curDt = curDb.getDataTable("[selectNextAXPOId]", curQueryParameter);

                            List<Int64> list = new List<Int64>();
                            for (int j = 0; j < curDt.Rows.Count; j++)
                            {
                                list.Add(Convert.ToInt64(curDt.Rows[j][0]));
                            }
                            axpoId = list[0];

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXPO_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axpoId)},
                                new sqlQueryParameter {strParameter="@keyAXPO_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXPO_DATAAREAID},
                                new sqlQueryParameter {strParameter="@keyAXPO_PURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPO_PURCHID},
                                new sqlQueryParameter {strParameter="@keyAXPO_PURCHNAME", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXPO_PURCHNAME},
                                new sqlQueryParameter {strParameter="@keyAXPO_ORDERACCOUNT", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPO_ORDERACCOUNT},
                                new sqlQueryParameter {strParameter="@keyAXPO_INVOIRCEACCOUNT", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPO_INVOICEACCOUNT},
                                new sqlQueryParameter {strParameter="@keyAXPO_LastConfirmationDate", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXPO_LastConfirmationDate == "" ? (Object)DBNull.Value: tempAXPO_LastConfirmationDate},
                                new sqlQueryParameter {strParameter="@keyAXPO_DELIVERYDATE", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXPO_DELIVERYDATE},
                                new sqlQueryParameter {strParameter="@keyAXPO_CREATEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXPO_CREATEDDATETIME},
                                new sqlQueryParameter {strParameter="@keyReferenceNo", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)""},
                                new sqlQueryParameter {strParameter="@keyCreationDateTime", ParameterType=SqlDbType.DateTime, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyDocType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"po"},
                                new sqlQueryParameter {strParameter="@keyReferenceVersion", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                            };
                            queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXPO", curQueryParameter);
                            if (queryResult == -99)
                            {
                                throw new Exception("insert ax purchase order failed");
                            }

                            queryResult = insertMovementHistory(curDb, tempAXPO_DATAAREAID, tempAXPO_PURCHID, "", "", "", "", "", "", "", "", "", "", "-1", "SYSTEM", sharedRes.AX2LGC_PO_INSERT, "");
                            if (queryResult == -99)
                            {
                                throw new Exception("insert movement history failed");
                            }

                            //Loop through each of the AXPO Detail
                            for (int j = 0; j < axPODetailDt.Rows.Count; j++)
                            {
                                String tempAXPOD_DATAAREAID = axPODetailDt.Rows[j][0].ToString();
                                String tempAXPOD_PURCHID = axPODetailDt.Rows[j][1].ToString();
                                String tempAXPOD_LINENUMBER = axPODetailDt.Rows[j][2].ToString();
                                String tempAXPOD_ITEMID = axPODetailDt.Rows[j][3].ToString();
                                String tempAXPOD_NAME = axPODetailDt.Rows[j][4].ToString();
                                String tempAXPOD_DOT_PARTCODE = axPODetailDt.Rows[j][5].ToString();
                                String tempAXPOD_ORDERACCOUNT = axPODetailDt.Rows[j][6].ToString();
                                String tempAXPOD_INVOICEACCOUNT = axPODetailDt.Rows[j][7].ToString();
                                String tempAXPOD_PURCHUNIT = axPODetailDt.Rows[j][8].ToString();
                                String tempAXPOD_INVENTLOCATIONID = axPODetailDt.Rows[j][9].ToString();
                                String tempAXPOD_INVENTSITEID = axPODetailDt.Rows[j][10].ToString();
                                String tempAXPOD_WMSLOCATIONID = axPODetailDt.Rows[j][11].ToString();
                                String tempAXPOD_QTYORDERED = axPODetailDt.Rows[j][12].ToString();
                                String tempAXPOD_PURCHRECEIVEDNOW = axPODetailDt.Rows[j][13].ToString();

                                queryResult = insertAXPODetail(curDb, axpoId.ToString(), tempAXPOD_DATAAREAID, tempAXPOD_PURCHID, tempAXPOD_LINENUMBER, tempAXPOD_ITEMID,
                                    tempAXPOD_NAME, tempAXPOD_DOT_PARTCODE, tempAXPOD_ORDERACCOUNT, tempAXPOD_INVOICEACCOUNT, tempAXPOD_PURCHUNIT, tempAXPOD_INVENTLOCATIONID,
                                    tempAXPOD_INVENTSITEID, tempAXPOD_WMSLOCATIONID, tempAXPOD_QTYORDERED, tempAXPOD_PURCHRECEIVEDNOW);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert ax po detail failed");
                                }

                                queryResult = insertMovementHistory(curDb, tempAXPO_DATAAREAID, tempAXPO_PURCHID, "", tempAXPOD_ITEMID, tempAXPOD_DOT_PARTCODE,
                                    tempAXPOD_INVENTLOCATIONID, tempAXPOD_INVENTSITEID, tempAXPOD_WMSLOCATIONID, "", "", "", tempAXPOD_QTYORDERED, "-1", "SYSTEM", sharedRes.AX2LGC_PO_DETAIL_INSERT, tempAXPOD_LINENUMBER);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert movement history failed");
                                }
                            }
                        }
                    }
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST PO SYNC"},
                        new sqlQueryParameter {strParameter="@keyDataValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)currentSyncDatetime}
                    };

                    queryResult = curDb.insertUpdateDeleteDataIntoTable("updateSystemData", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert/update/delete failed");
                    }

                    isSuccess = "true";
                }
                catch (Exception e)
                {
                    curDb.rollbackNCloseConnection();
                    isSuccess = e.Message;
                    return isSuccess;
                }
                if (!curDb.checkIsConnectionClosed())
                    curDb.closeConnection();
                return isSuccess;
            }
        }

        private int insertAXPODetail(database curDb, String axpoId, String tempAXPOD_DATAAREAID,
            String tempAXPOD_PURCHID, String tempAXPOD_LINENUMBER, String tempAXPOD_ITEMID, String tempAXPOD_NAME,
            String tempAXPOD_DOT_PARTCODE, String tempAXPOD_ORDERACCOUNT, String tempAXPOD_INVOICEACCOUNT,
            String tempAXPOD_PURCHUNIT, String tempAXPOD_INVENTLOCATIONID, String tempAXPOD_INVENTSITEID,
            String tempAXPOD_WMSLOCATIONID, String tempAXPOD_QTYORDERED, String tempAXPOD_PURCHRECEIVEDNOW)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };

                DataTable curDt = curDb.getDataTable("[selectNextAXPODetailId]", curQueryParameter);

                List<Int64> list = new List<Int64>();
                for (int k = 0; k < curDt.Rows.Count; k++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[k][0]));
                }
                Int64 axpodId = list[0];

                curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXPOD_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axpodId)},
                                    new sqlQueryParameter {strParameter="@keyAXPO_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axpoId)},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXPOD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_PURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPOD_PURCHID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_LINENUMBER", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(tempAXPOD_LINENUMBER)},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXPOD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_NAME", ParameterType=SqlDbType.NVarChar, ParameterLength=1000, keyData = (Object)tempAXPOD_NAME},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXPOD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_ORDERACCOUNT", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPOD_ORDERACCOUNT},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_INVOICEACCOUNT", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPOD_INVOICEACCOUNT},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_PURCHUNIT", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXPOD_PURCHUNIT},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPOD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXPOD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXPOD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_QTYORDERED", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXPOD_QTYORDERED)},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_PURCHRECEIVEDNOW", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXPOD_PURCHRECEIVEDNOW)},
                                    new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Int, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                    new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXPODetail", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }

        private int setAXPODetail(database curDb, String tempAXPOD_DATAAREAID,
            String tempAXPOD_PURCHID, String tempAXPOD_LINENUMBER, String tempAXPOD_ITEMID, String tempAXPOD_NAME,
            String tempAXPOD_DOT_PARTCODE, String tempAXPOD_ORDERACCOUNT, String tempAXPOD_INVOICEACCOUNT,
            String tempAXPOD_PURCHUNIT, String tempAXPOD_INVENTLOCATIONID, String tempAXPOD_INVENTSITEID,
            String tempAXPOD_WMSLOCATIONID, String tempAXPOD_QTYORDERED, String tempAXPOD_PURCHRECEIVEDNOW)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXPOD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXPOD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_PURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPOD_PURCHID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_LINENUMBER", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(tempAXPOD_LINENUMBER)},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXPOD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_NAME", ParameterType=SqlDbType.NVarChar, ParameterLength=1000, keyData = (Object)tempAXPOD_NAME},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXPOD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_ORDERACCOUNT", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPOD_ORDERACCOUNT},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_INVOICEACCOUNT", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPOD_INVOICEACCOUNT},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_PURCHUNIT", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXPOD_PURCHUNIT},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXPOD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXPOD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXPOD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_QTYORDERED", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXPOD_QTYORDERED)},
                                    new sqlQueryParameter {strParameter="@keyAXPOD_PURCHRECEIVEDNOW", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXPOD_PURCHRECEIVEDNOW)},
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXPODetailByAXLineNum_PurchId_ItemId_PartCode", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }

        //AX Purchase Order
        //+++++++++++++++++++++++++++++++++++++

        //[WebMethod(EnableSession = true)]
        //public void getAllAXPOInfo(String page, String per_page)
        //{
        //    Int32 total_number_page = 1;
        //    Int32 cur_start_item = 1;
        //    Int32 cur_end_item = 1;
        //    DataTable splitDt = new DataTable();

        //    String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
        //    String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];

        //    String searchHeader = "";
        //    String searchContent = "";
        //    String searchContent2 = "";
        //    String searchType = HttpContext.Current.Request.QueryString["filterType"];
        //    String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');
        //    searchHeader = searchArray[0];
        //    if (searchArray.Length == 3)
        //    {
        //        searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
        //        searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
        //    }
        //    else
        //    {
        //        searchContent = searchArray[1] == "" ? "%" : searchArray[1];
        //    }

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
        //        new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
        //        new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
        //        new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchType},
        //        new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
        //        new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent},
        //        new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent2},
        //        new sqlQueryParameter {strParameter="@keySearchLocationId", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)Session["curSessionWarehouseId"]}
        //    };

        //    database curDb = new database();
        //    curDb.openConnection();
        //    DataTable curDt = curDb.getDataTable("selectAllAXPOInfo", curQueryParameter);
        //    curDb.closeConnection();

        //    if (curDt.Rows.Count != 0)
        //    {
        //        splitDt = splitDataTable(curDt, page, per_page);
        //        total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
        //        cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
        //        cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
        //    }

        //    Context.Response.ContentType = "text/plain";
        //    //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
        //    Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
        //    Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
        //    Context.Response.Write("}");

        //}

        //changes
        [WebMethod]
        public axPOObject[] getAllAXPO()
        {

            axPOObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            try
            {
                //if (!(retrieveAllAXPurchaseOrder() == "true"))
                //    throw new Exception("Failed to retrieve AX Purchase Order");

                DataTable curDt = curDb.getDataTable("[selectAllAXPO]", curQueryParameter);
                List<axPOObject> list = new List<axPOObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axPOObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        , curDt.Rows[i][4].ToString()
                        , curDt.Rows[i][5].ToString()
                        , curDt.Rows[i][6].ToString()
                        , curDt.Rows[i][7].ToString()
                        , curDt.Rows[i][8].ToString()
                        , curDt.Rows[i][9].ToString()
                        , curDt.Rows[i][10].ToString()
                        , curDt.Rows[i][11].ToString()
                        , curDt.Rows[i][12].ToString()
                        , curDt.Rows[i][13].ToString()
                        , curDt.Rows[i][14].ToString()
                        , curDt.Rows[i][15].ToString()
                        , curDt.Rows[i][16].ToString()
                        , curDt.Rows[i][17].ToString()
                        , curDt.Rows[i][18].ToString()
                        ));
                }

                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod(EnableSession = true)]
        public axPOObject[] getAllPendingAXPO()
        {

            axPOObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
                //new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argLocationId)},
                //new sqlQueryParameter {strParameter="@keyDocType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argDocType)}
                //new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(Session["curSessionWarehouseId"])}
            };

            try
            {
                if (!(retrieveAllAXPurchaseOrder() == "true"))
                    throw new Exception("Failed to retrieve AX Purchase Order");

                DataTable curDt = curDb.getDataTable("[selectAllPendingAXPO]", curQueryParameter);

                List<axPOObject> list = new List<axPOObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axPOObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        , curDt.Rows[i][4].ToString()
                        , curDt.Rows[i][5].ToString()
                        , curDt.Rows[i][6].ToString()
                        , curDt.Rows[i][7].ToString()
                        , curDt.Rows[i][8].ToString()
                        , curDt.Rows[i][9].ToString()
                        , curDt.Rows[i][10].ToString()
                        , curDt.Rows[i][11].ToString()
                        , curDt.Rows[i][12].ToString()
                        , curDt.Rows[i][13].ToString()
                        , curDt.Rows[i][14].ToString()
                        , curDt.Rows[i][15].ToString()
                        , curDt.Rows[i][16].ToString()
                        , curDt.Rows[i][17].ToString()
                        , curDt.Rows[i][18].ToString()
                        ));
                }
                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axPODetailObject[] getAllAXPODetail()
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectAllAXPODetail]", curQueryParameter);

            List<axPODetailObject> list = new List<axPODetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axPODetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    , curDt.Rows[i][21].ToString()
                    , curDt.Rows[i][22].ToString()
                    , curDt.Rows[i][23].ToString()
                    ));
            }

            axPODetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axPODetailObject[] getAllPendingAXPODetail()
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectAllPendingAXPODetail]", curQueryParameter);

            List<axPODetailObject> list = new List<axPODetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axPODetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    , curDt.Rows[i][21].ToString()
                    , curDt.Rows[i][22].ToString()
                    , curDt.Rows[i][23].ToString()
                    ));
            }

            axPODetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axPOObject[] getAXPOByAXPurchId(String argAXPURCHID)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength = 20, keyData = (Object)argAXPURCHID},
            };

            DataTable curDt = curDb.getDataTable("[selectAXPOByAXPurchId]", curQueryParameter);

            List<axPOObject> list = new List<axPOObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axPOObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    ));
            }

            axPOObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axPODetailObject[] getAXPODetailByAXPurchId(String argAXPURCHID)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength = 20, keyData = (Object)argAXPURCHID},
            };

            DataTable curDt = curDb.getDataTable("[selectAXPODetailByAXPurchId]", curQueryParameter);

            List<axPODetailObject> list = new List<axPODetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axPODetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    , curDt.Rows[i][21].ToString()
                    , curDt.Rows[i][22].ToString()
                    , curDt.Rows[i][23].ToString()
                    ));
            }

            axPODetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public String setAXPODetailScanInput(List<AXPODetailScanInput> aXPODetailScanInputList)
        {
            String isSuccess = "AX POD Update failed";
            int queryResult = -99;
            int count = 0;
            database curDb = new database();
            curDb.openConnection();
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            String xmlMessage = "";
            String messageId = getAXMessageId();
            String isDevelopmentMode = "0";

            try
            {
                xmlMessage = "<?xml version =\"1.0\" encoding=\"UTF-8\"?>\r\n" +
                    "<GRNPO>\r\n";
                int counter = 0;

                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)aXPODetailScanInputList[0].argAXPURCHID}
                        };
                curDt = curDb.getDataTable("selectAXPOByAXPurchId", curQueryParameter);

                String tempAXPO_VEND_CODE = curDt.Rows[0][4].ToString();

                foreach (var aXPODetailScanInput in aXPODetailScanInputList)
                {
                    count++;
                    if (count == 1)
                    {
                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXPODetailScanInput.argAXPURCHID},
                            new sqlQueryParameter {strParameter="@keyGRNDocNo", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData =  (Object)aXPODetailScanInput.argGRNDocumentNo}
                        };

                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXPOGRNoByAXPurchId", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Update AX PO GR# Failed");
                        }
                    }

                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(aXPODetailScanInput.argAXLINENUMBER)},
                        new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXPODetailScanInput.argAXPURCHID},
                        new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXPODetailScanInput.argAXITEMID},
                        new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXPODetailScanInput.argAXDOT_PARTCODE},
                    };

                    curDt = curDb.getDataTable("[selectAXPODetailByAXLineNum_PurchId_ItemId_PartCode]", curQueryParameter);

                    if (curDt.Rows.Count > 0)
                    {
                        String tempAXPOD_DATAAREAID = curDt.Rows[0][2].ToString();
                        String tempAXPOD_PURCHID = curDt.Rows[0][3].ToString();
                        String tempAXPOD_LINENUMBER = curDt.Rows[0][4].ToString();
                        String tempAXPOD_ITEMID = curDt.Rows[0][5].ToString();
                        String tempAXPOD_NAME = curDt.Rows[0][6].ToString();
                        String tempAXPOD_DOT_PARTCODE = curDt.Rows[0][7].ToString();
                        String tempAXPOD_ORDERACCOUNT = curDt.Rows[0][8].ToString();
                        String tempAXPOD_INVOICEACCOUNT = curDt.Rows[0][9].ToString();
                        String tempAXPOD_PURCHUNIT = curDt.Rows[0][10].ToString();
                        String tempAXPOD_INVENTLOCATIONID = curDt.Rows[0][11].ToString();
                        String tempAXPOD_INVENTSITEID = curDt.Rows[0][12].ToString();
                        String tempAXPOD_WMSLOCATIONID = curDt.Rows[0][13].ToString();
                        String tempAXPOD_QTYORDERED = curDt.Rows[0][14].ToString();
                        String tempAXPOD_PURCHRECEIVEDNOW = curDt.Rows[0][15].ToString();

                        Int64 curStatusId = 9;
                        if (Convert.ToDecimal(tempAXPOD_QTYORDERED) < Convert.ToDecimal(aXPODetailScanInput.argScanQuantity))
                        {
                            throw new Exception("Update Failed - " + aXPODetailScanInput.argAXITEMID +
                                " - Scan Quantity[" + aXPODetailScanInput.argScanQuantity + "] more then Order Quantity [" + tempAXPOD_QTYORDERED + "]");
                        }
                        else if (Convert.ToDecimal(tempAXPOD_QTYORDERED) > Convert.ToDecimal(aXPODetailScanInput.argScanQuantity))
                        {
                            curStatusId = 7;
                        }

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(aXPODetailScanInput.argAXLINENUMBER)},
                            new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXPODetailScanInput.argAXPURCHID},
                            new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXPODetailScanInput.argAXITEMID},
                            new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXPODetailScanInput.argAXDOT_PARTCODE},
                            new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Decimal, keyData =  Convert.ToDecimal(aXPODetailScanInput.argScanQuantity)},
                            new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData =  (Object)(aXPODetailScanInput.argScanDatetime)},
                            new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData =  (Object)aXPODetailScanInput.argRemarks},
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(aXPODetailScanInput.argUserId) },
                            new sqlQueryParameter {strParameter="@keyGRNDocNo", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData =  (Object)aXPODetailScanInput.argGRNDocumentNo},
                            new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)aXPODetailScanInput.argDeviceSN},
                            new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(curStatusId) },
                        };

                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXPODetailScanInputByAXLineNum_PurchId_ItemId_PartCode", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Update AX POD Failed");
                        }


                        if (counter == 0)
                        {

                            xmlMessage += "<GRN>\r\n" +
                                             "<Company_Code>" + tempAXPOD_DATAAREAID + "</Company_Code>\r\n" +
                                             "<PO_ID>" + tempAXPOD_PURCHID + "</PO_ID>\r\n" +
                                             "<PO_DOC_NUM></PO_DOC_NUM>\r\n" +
                                             "<Vend_Code>" + tempAXPO_VEND_CODE + "</Vend_Code>\r\n" +
                                             "<LOC></LOC>\r\n" +
                                             "<GRN_DOC_NUM>" + aXPODetailScanInputList[0].argGRNDocumentNo + "</GRN_DOC_NUM>\r\n" +
                                             "<GRN_DATE>" + DateTime.Now.ToString("dd/MM/yyyy") + "</GRN_DATE>\r\n" +
                                             "</GRN>\r\n";
                        }
                        counter++;
                        xmlMessage += "<GRN_DETAIL>\r\n" +
                            "<Company_Code>" + tempAXPOD_DATAAREAID + "</Company_Code>\r\n" +
                            "<LineNumber>" + tempAXPOD_LINENUMBER + "</LineNumber>\r\n" +
                            "<ITEM_CODE>" + tempAXPOD_ITEMID + "</ITEM_CODE>\r\n" +
                            "<PART_CODE>" + tempAXPOD_DOT_PARTCODE + "</PART_CODE>\r\n" +
                            "<QTY>" + aXPODetailScanInput.argScanQuantity + "</QTY>\r\n" +
                            "</GRN_DETAIL>\r\n";

                        queryResult = insertMovementHistory(curDb, tempAXPOD_DATAAREAID, tempAXPOD_PURCHID, "", tempAXPOD_ITEMID, tempAXPOD_DOT_PARTCODE,
                            tempAXPOD_INVENTLOCATIONID, tempAXPOD_INVENTSITEID, tempAXPOD_WMSLOCATIONID, "", "", "", aXPODetailScanInput.argScanQuantity, aXPODetailScanInput.argUserId,
                            aXPODetailScanInput.argDeviceSN, sharedRes.LGC2AX_PO_DETAIL_UPDATE, aXPODetailScanInput.argAXLINENUMBER);
                        if (queryResult == -99)
                        {
                            throw new Exception("insert movement history failed");
                        }


                    }
                    else
                    {
                        //Temporarily skip for those line is not existed in WMS DB
                        //continue;
                        throw new Exception("Update Failed - " + aXPODetailScanInput.argAXITEMID + " does not found in PO - " + aXPODetailScanInput.argAXPURCHID);
                    }
                }

                xmlMessage += "</GRNPO>";


                //curQueryParameter = new List<sqlQueryParameter>() {
                //            new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData =  (Object)aXPODetailScanInputList[0].argAXPURCHID },
                //        };

                //curDt = curDb.getDataTable("[checkExistenceAXPODetailWRemainingQuantityByAXPurchId]", curQueryParameter);
                ////No more pending line for this AX PO
                //if (curDt.Rows.Count <= 0)
                //{
                curQueryParameter = new List<sqlQueryParameter>()
                            {
                                new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXPODetailScanInputList[0].argAXPURCHID},
                                new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(9)},
                            };
                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXPOCompletedByAXPurchId", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("Update AX Purchase Order Detail Status Failed");
                }
                //}

                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"DEVELOPMENT MODE"},
                };


                curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                isDevelopmentMode = curDt.Rows[0][2].ToString();

                if (isDevelopmentMode == "0")
                {
                    POPostService.GRNPostServiceClient grnObject = new POPostService.GRNPostServiceClient();

                    POPostService.CallContext context = new POPostService.CallContext();
                    context.MessageId = messageId; // "{33000000-0000-0000-0000-000000000012}";

                    String result = "";
                    try
                    {
                        result = grnObject.ReadXMLFile(context, xmlMessage);
                    }
                    catch (Exception axEx)
                    {
                        throw new Exception("Failed to post data to AX - " + axEx.Message + " " + messageId);
                    }

                    Boolean isAXPostSuccess = false;
                    if (!String.IsNullOrEmpty(result))
                    {
                        string[] words = result.Split(' ');
                        if (words[0].Equals("Successfully", StringComparison.OrdinalIgnoreCase))
                        {
                            isAXPostSuccess = true;
                        }
                    }

                    if (!isAXPostSuccess)
                    {
                        throw new Exception("Failed to post data to AX - " + result + " " + messageId);
                        //String axExceptionMsg = retrieveAXExceptionMsg(messageId);
                        //if (axExceptionMsg == "")
                        //{
                        //    throw new Exception("Failed to post data to AX - " + result);
                        //}
                        //else
                        //{
                        //    throw new Exception("Failed to post data to AX - " + axExceptionMsg);
                        //}
                    }
                }
                //else
                //{
                //    ////Testing Purpose
                //    string pathString = XML_FOLDER_PATH;

                //    bool exists = System.IO.Directory.Exists(pathString);

                //    if (!exists)
                //        System.IO.Directory.CreateDirectory(pathString);

                //    string fileName = "PO_" + getAXMessageId() + ".xml";
                //    pathString = System.IO.Path.Combine(pathString, fileName);

                //    if (!System.IO.File.Exists(pathString))
                //    {
                //        using (System.IO.StreamWriter file =
                //            new System.IO.StreamWriter(pathString, true))
                //        {
                //            file.WriteLine(xmlMessage);
                //        }
                //    }
                //    throw new Exception("XML Generated");
                //}

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                insertExceptionMsg(aXPODetailScanInputList[0].argAXPURCHID, aXPODetailScanInputList[0].argUserId,
                    aXPODetailScanInputList[0].argDeviceSN, "PO", e.Message, messageId);
                isSuccess = e.Message;
                try
                {
                    ////Testing Purpose
                    string pathString = XML_FOLDER_PATH;
                    pathString = System.IO.Path.Combine(pathString, "PO");

                    bool exists = System.IO.Directory.Exists(pathString);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(pathString);

                    string fileName = "PO_" + messageId + ".xml";
                    pathString = System.IO.Path.Combine(pathString, fileName);

                    if (!System.IO.File.Exists(pathString))
                    {
                        using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(pathString, true))
                        {
                            file.WriteLine(xmlMessage);
                        }
                    }
                }
                catch (Exception e2) { }
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod]
        public void getAllPOGoodsReceiveInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];

            String searchHeader = "";
            String searchContent = "";
            String searchContent2 = "";
            String searchType = HttpContext.Current.Request.QueryString["filterType"];
            String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');
            String warehouseCode = HttpContext.Current.Request.QueryString["warehouseCode"].ToString();

            searchHeader = searchArray[0];
            if (searchArray.Length == 3)
            {
                searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
                searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
            }
            else
            {
                searchContent = searchArray[1] == "" ? "%" : searchArray[1];
            }

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchType},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent},
                new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent2},
                new sqlQueryParameter {strParameter="@keySearchWarehouseCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)warehouseCode}
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllAXPOGoodsReceiveInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");

        }

        [WebMethod]
        public string printAssetLabel(String[] argBarcode, String argPrinterName, String argLabelSize)
        {
            String isSuccess = "Print Label failed";
            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            try
            {
                //for (int i = 0; i < argBarcode.Length; i++)
                //{

                //    string text = File.ReadAllText(Server.MapPath("/_barcode_template/asset_barcode_template" + argLabelSize + ".prn"));
                //    text = text.Replace("T0000000", argBarcode[i]);
                //    text = text.Replace("Asset Name", curItem[0].assetName);

                //    //PrinterList();
                //    if (RawPrinterHelper.isPrinterReady(argPrinterName))
                //    {
                //        RawPrinterHelper.SendStringToPrinter(argPrinterName, text);
                //    }
                //    else
                //    {
                //        throw new Exception("Print Label failed");
                //    }

                //}
                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
                return isSuccess;
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        //Integration - AX AJ
        //+++++++++++++++++++++++++++++++++++++

        //Get All Pending AX AJ From AX DB View
        [WebMethod]
        public String retrieveAllAXArrivalJournal()
        {
            String isSuccess = "Failed Retrieve AX Arrival Journal";

            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            DataTable curDt;
            int queryResult = -99;

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    axConn.Open();
                    //Get all AX PO from AX DB View
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT DISTINCT [AX_WMS_ARRIVALJOURNALTABLE].[JOURNALID]" +
                           ",[AX_WMS_ARRIVALJOURNALTABLE].[DESCRIPTION]" +
                           ",[AX_WMS_ARRIVALJOURNALTABLE].[POSTED]" +
                           ",[AX_WMS_ARRIVALJOURNALTABLE].[JOURNALNAMEID]" +
                           ",[AX_WMS_ARRIVALJOURNALTABLE].[INVENTDIMID]" +
                           ",[AX_WMS_ARRIVALJOURNALTABLE].[PACKINGSLIP]" +
                           ",[AX_WMS_ARRIVALJOURNALTABLE].[CREATEDDATETIME]" +
                           ",[AX_WMS_ARRIVALJOURNALTABLE].[DATAAREAID] " +
                           "FROM [AX_WMS_ARRIVALJOURNALTABLE] " +
                           "JOIN AX_WMS_ARRIVALJOURNALTRANS ON[AX_WMS_ARRIVALJOURNALTABLE].JOURNALID = AX_WMS_ARRIVALJOURNALTRANS.JOURNALID", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX AJ from AX DB View
                    DataTable axAJDt = new DataTable();

                    int noOfAXAJ = sda.Fill(axAJDt);
                    //no AX AJ is found
                    //if (noOfAXAJ <= 0) return "true";

                    curQueryParameter = new List<sqlQueryParameter>()
                    {
                    };
                    curDt = curDb.getDataTable("selectAllPendingAXAJwAllPendingAXAJDetail", curQueryParameter);

                    //Loop through all pending AX PO with all pending AX PO Detail (from WMS DB)
                    for (int i = 0; i < curDt.Rows.Count; i++)
                    {
                        String tempAXAJ_DATAAREAID = curDt.Rows[i][8].ToString();
                        String tempAXAJ_JOURNALID = curDt.Rows[i][1].ToString();

                        //delete all Pending AX PO (AX PO would not be deleted if there is at least 1 line is scanned)
                        curQueryParameter = new List<sqlQueryParameter>()
                        {
                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXAJ_JOURNALID}
                        };
                        queryResult = curDb.insertUpdateDeleteDataIntoTable("deleteAXAJByAXJournalId", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Failed to delete Pending AX AJ (" + tempAXAJ_JOURNALID + ")");
                        }
                        queryResult = insertMovementHistory(curDb, tempAXAJ_DATAAREAID, tempAXAJ_JOURNALID, "", "", "", "", "", "", "", "", "", "", "-1", "SYSTEM", sharedRes.AX2LGC_AJ_DELETE_PENDING, "");
                        if (queryResult == -99)
                        {
                            throw new Exception("insert movement history failed");
                        }
                    }
                    //delete all Pending AX PO (AX PO would not be deleted if there is at least 1 line is scanned)
                    //curQueryParameter = new List<sqlQueryParameter>() {
                    //        };
                    //queryResult = curDb.insertUpdateDeleteDataIntoTable("deleteAllPendingAXPO", curQueryParameter);
                    //if (queryResult == -99)
                    //{
                    //    throw new Exception("Failed to delete All Pending AXPO");
                    //}

                    Int64 axajId = -1;
                    //Loop through all AXPO from AX DB View
                    for (int i = 0; i < noOfAXAJ; i++)
                    {
                        String tempAXAJ_JOURNALID = axAJDt.Rows[i][0].ToString();
                        String tempAXAJ_DESCRIPTION = axAJDt.Rows[i][1].ToString();
                        String tempAXAJ_POSTED = axAJDt.Rows[i][2].ToString();
                        String tempAXAJ_JOURNALNAMEID = axAJDt.Rows[i][3].ToString();
                        String tempAXAJ_INVENTDIMID = axAJDt.Rows[i][4].ToString();
                        String tempAXAJ_PACKINGSLIP = axAJDt.Rows[i][5].ToString();
                        String tempAXAJ_CREATEDDATETIME = axAJDt.Rows[i][6].ToString();
                        String tempAXAJ_DATAAREAID = axAJDt.Rows[i][7].ToString();

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXAJ_JOURNALID}
                        };
                        curDt = curDb.getDataTable("selectAXAJByAXJournalId", curQueryParameter);

                        //Get all AX PO Detail By Purch_ID from AX DB View
                        axCommand = new SqlCommand(
                        "SELECT DISTINCT [DATAAREAID]" +
                          ",[JOURNALID]" +
                          ",[VENDACCOUNT]" +
                          ",[PurchID]" +
                          ",[LINENUM]" +
                          ",[ITEMID]" +
                          ",[GOD_PARTCODE]" +
                          ",[QTY]" +
                          ",[TRANSDATE]" +
                          ",[INVENTTRANSID]" +
                          ",[INVENTDIMID]" +
                          ",[INVENTLOCATIONID]" +
                          ",[INVENTSITEID]" +
                          ",[WMSLOCATIONID]" +
                          ",[CREATEDDATETIME]" +
                          ",[CREATEDBY]" +
                          ",[BOX_NO]" +
                          ",[CASE]" +
                          ",[PRODUCT_NAME]" +
                          ",'' AS [SO_NUMBER]" +
                          ",'' AS [CUSTOMERNAME]" +
                        "FROM [AX_WMS_ARRIVALJOURNALTRANS] " +
                        "WHERE [JOURNALID] = @keyAXAJ_JOURNALID", axConn);
                        axCommand.Parameters.Add(new SqlParameter("@keyAXAJ_JOURNALID", tempAXAJ_JOURNALID));
                        axCommand.CommandTimeout = 1800;

                        sda.SelectCommand = axCommand;
                        DataTable axAJDetailDt = new DataTable();
                        //AX AJ Detail in AX DB View
                        sda.Fill(axAJDetailDt);

                        //++++++++++++++++++++++++++++++
                        //If AX AJ Existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        if (curDt.Rows.Count > 0)
                        {
                            //Skip if existed
                            continue;
                            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            //Commented below as Arrival Journal only allow one submission per document,
                            //If document is not in status 1 means already scan and cannot be updated
                            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                            //axajId = Convert.ToInt64(curDt.Rows[0][0].ToString());

                            ////delete all Pending AX PO Detail
                            //curQueryParameter = new List<sqlQueryParameter>() {
                            //    new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXAJ_JOURNALID}
                            //};
                            //queryResult = curDb.insertUpdateDeleteDataIntoTable("deleteAllPendingAXAJDetailByAXJournalId", curQueryParameter);
                            //if (queryResult == -99)
                            //{
                            //    throw new Exception("Failed to delete All Pending AXAJ Detail");
                            //}
                            //queryResult = insertMovementHistory(curDb, tempAXAJ_DATAAREAID, tempAXAJ_JOURNALID, "", "", "", "", "", "", "", "", "", "", "-1", "SYSTEM", "SYNC AX AJ DETAIL DELETE PENDING");
                            //if (queryResult == -99)
                            //{
                            //    throw new Exception("insert movement history failed");
                            //}


                            //curQueryParameter = new List<sqlQueryParameter>() {
                            //    new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXAJ_JOURNALID}
                            //};
                            ////AX AJ Detail in WMS DB
                            //DataTable curAXAJDetailDt = curDb.getDataTable("selectAXAJDetailByAXJournalId", curQueryParameter);

                            //Boolean isAJStatusUpdated = false; //to make AJ status change to pending if there is at least one new line added
                            ////Loop through each of the AXAJ Detail
                            //for (int j = 0; j < axAJDetailDt.Rows.Count; j++)
                            //{
                            //    String tempAXAJD_DATAAREAID = axAJDetailDt.Rows[j][0].ToString();
                            //    String tempAXAJD_JOURNALID = axAJDetailDt.Rows[j][1].ToString();
                            //    String tempAXAJD_VENDACCOUNT = axAJDetailDt.Rows[j][2].ToString();
                            //    String tempAXAJD_PurchID = axAJDetailDt.Rows[j][3].ToString();
                            //    String tempAXAJD_LINENUM = axAJDetailDt.Rows[j][4].ToString();
                            //    String tempAXAJD_ITEMID = axAJDetailDt.Rows[j][5].ToString();
                            //    String tempAXAJD_GOD_PARTCODE = axAJDetailDt.Rows[j][6].ToString();
                            //    String tempAXAJD_QTY = axAJDetailDt.Rows[j][7].ToString();
                            //    String tempAXAJD_TRANSDATE = axAJDetailDt.Rows[j][8].ToString();
                            //    String tempAXAJD_INVENTTRANSID = axAJDetailDt.Rows[j][9].ToString();
                            //    String tempAXAJD_INVENTDIMID = axAJDetailDt.Rows[j][10].ToString();
                            //    String tempAXAJD_INVENTLOCATIONID = axAJDetailDt.Rows[j][11].ToString();
                            //    String tempAXAJD_INVENTSITEID = axAJDetailDt.Rows[j][12].ToString();
                            //    String tempAXAJD_WMSLOCATIONID = axAJDetailDt.Rows[j][13].ToString();
                            //    String tempAXAJD_CREATEDDATETIME = axAJDetailDt.Rows[j][14].ToString();
                            //    String tempAXAJD_CREATEDBY = axAJDetailDt.Rows[j][15].ToString();

                            //    Boolean ajDetailExisted = false;
                            //    for (int k = 0; k < curAXAJDetailDt.Rows.Count; k++)
                            //    {
                            //        //If AX AJ Detail Existed in WMS DB
                            //        if (tempAXAJD_PurchID.Trim() == curAXAJDetailDt.Rows[k][5].ToString().Trim() &&
                            //            tempAXAJD_ITEMID.Trim() == curAXAJDetailDt.Rows[k][7].ToString().Trim() &&
                            //            tempAXAJD_GOD_PARTCODE.Trim() == curAXAJDetailDt.Rows[k][8].ToString().Trim())
                            //        {

                            //            queryResult = setAXAJDetail(curDb, tempAXAJD_DATAAREAID, tempAXAJD_JOURNALID, tempAXAJD_VENDACCOUNT,
                            //                tempAXAJD_PurchID, tempAXAJD_LINENUM, tempAXAJD_ITEMID, tempAXAJD_GOD_PARTCODE, tempAXAJD_QTY,
                            //                tempAXAJD_TRANSDATE, tempAXAJD_INVENTTRANSID, tempAXAJD_INVENTDIMID, tempAXAJD_INVENTLOCATIONID,
                            //                tempAXAJD_INVENTSITEID, tempAXAJD_WMSLOCATIONID, tempAXAJD_CREATEDDATETIME, tempAXAJD_CREATEDBY);
                            //            if (queryResult == -99)
                            //            {
                            //                throw new Exception("update ax aj detail failed");
                            //            }

                            //            queryResult = insertMovementHistory(curDb, tempAXAJD_DATAAREAID, tempAXAJD_JOURNALID, tempAXAJD_PurchID, tempAXAJD_ITEMID, tempAXAJD_GOD_PARTCODE,
                            //                tempAXAJD_INVENTLOCATIONID, tempAXAJD_INVENTSITEID, tempAXAJD_WMSLOCATIONID, "", "", "", tempAXAJD_QTY, "-1", "SYSTEM", "SYNC AX AJ DETAIL UPDATE");
                            //            if (queryResult == -99)
                            //            {
                            //                throw new Exception("insert movement history failed");
                            //            }

                            //            ajDetailExisted = true;
                            //            break;
                            //        }
                            //    }

                            //    //if AX AJ Detail no existed in WMS DB
                            //    if (!ajDetailExisted)
                            //    {
                            //        queryResult = insertAXAJDetail(curDb, axajId.ToString(), tempAXAJD_DATAAREAID, tempAXAJD_JOURNALID, tempAXAJD_VENDACCOUNT,
                            //                tempAXAJD_PurchID, tempAXAJD_LINENUM, tempAXAJD_ITEMID, tempAXAJD_GOD_PARTCODE, tempAXAJD_QTY,
                            //                tempAXAJD_TRANSDATE, tempAXAJD_INVENTTRANSID, tempAXAJD_INVENTDIMID, tempAXAJD_INVENTLOCATIONID,
                            //                tempAXAJD_INVENTSITEID, tempAXAJD_WMSLOCATIONID, tempAXAJD_CREATEDDATETIME, tempAXAJD_CREATEDBY);
                            //        if (queryResult == -99)
                            //        {
                            //            throw new Exception("insert ax aj detail failed");
                            //        }

                            //        queryResult = insertMovementHistory(curDb, tempAXAJD_DATAAREAID, tempAXAJD_JOURNALID, tempAXAJD_PurchID, tempAXAJD_ITEMID, tempAXAJD_GOD_PARTCODE,
                            //                tempAXAJD_INVENTLOCATIONID, tempAXAJD_INVENTSITEID, tempAXAJD_WMSLOCATIONID, "", "", "", tempAXAJD_QTY, "-1", "SYSTEM", "SYNC AX AJ DETAIL INSERT");
                            //        if (queryResult == -99)
                            //        {
                            //            throw new Exception("insert movement history failed");
                            //        }

                            //        //Update AJ Status to pending
                            //        if (!isAJStatusUpdated)
                            //        {

                            //            curQueryParameter = new List<sqlQueryParameter>()
                            //            {
                            //                new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_JOURNALID},
                            //                new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                            //                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                            //            };
                            //            queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXAJCompletedByAXJournalId", curQueryParameter);
                            //            if (queryResult == -99)
                            //            {
                            //                throw new Exception("Update AX Arrival Journal Detail Status Failed");
                            //            }
                            //            isAJStatusUpdated = true;
                            //        }
                            //    }

                            //}
                        }
                        //++++++++++++++++++++++++++++++
                        //if AX AJ no existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        else
                        {

                            curQueryParameter = new List<sqlQueryParameter>()
                            {
                            };

                            curDt = curDb.getDataTable("[selectNextAXAJId]", curQueryParameter);

                            List<Int64> list = new List<Int64>();
                            for (int j = 0; j < curDt.Rows.Count; j++)
                            {
                                list.Add(Convert.ToInt64(curDt.Rows[j][0]));
                            }
                            axajId = list[0];

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXAJ_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axajId)},
                                new sqlQueryParameter {strParameter="@keyAXAJ_JOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJ_JOURNALID},
                                new sqlQueryParameter {strParameter="@keyAXAJ_DESCRIPTION", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXAJ_DESCRIPTION},
                                new sqlQueryParameter {strParameter="@keyAXAJ_POSTED", ParameterType=SqlDbType.Int, keyData = Convert.ToInt32(tempAXAJ_POSTED)},
                                new sqlQueryParameter {strParameter="@keyAXAJ_JOURNALNAMEID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJ_JOURNALNAMEID},
                                new sqlQueryParameter {strParameter="@keyAXAJ_INVENTDIMID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJ_INVENTDIMID},
                                new sqlQueryParameter {strParameter="@keyAXAJ_PACKINGSLIP", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJ_PACKINGSLIP},
                                new sqlQueryParameter {strParameter="@keyAXAJ_CREATEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXAJ_CREATEDDATETIME},
                                new sqlQueryParameter {strParameter="@keyAXAJ_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXAJ_DATAAREAID},
                                new sqlQueryParameter {strParameter="@keyReferenceNo", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)""},
                                new sqlQueryParameter {strParameter="@keyCreationDateTime", ParameterType=SqlDbType.DateTime, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyDocType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"aj"},
                                new sqlQueryParameter {strParameter="@keyReferenceVersion", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                            };
                            queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXAJ", curQueryParameter);
                            if (queryResult == -99)
                            {
                                throw new Exception("insert ax arrival journal failed");
                            }

                            queryResult = insertMovementHistory(curDb, tempAXAJ_DATAAREAID, tempAXAJ_JOURNALID, "", "", "", "", "", "", "", "", "", "", "-1", "SYSTEM", sharedRes.AX2LGC_AJ_INSERT, "");
                            if (queryResult == -99)
                            {
                                throw new Exception("insert movement history failed");
                            }

                            //Loop through each of the AXAJ Detail
                            for (int j = 0; j < axAJDetailDt.Rows.Count; j++)
                            {

                                String tempAXAJD_DATAAREAID = axAJDetailDt.Rows[j][0].ToString();
                                String tempAXAJD_JOURNALID = axAJDetailDt.Rows[j][1].ToString();
                                String tempAXAJD_VENDACCOUNT = axAJDetailDt.Rows[j][2].ToString();
                                String tempAXAJD_PurchID = axAJDetailDt.Rows[j][3].ToString();
                                String tempAXAJD_LINENUM = axAJDetailDt.Rows[j][4].ToString();
                                String tempAXAJD_ITEMID = axAJDetailDt.Rows[j][5].ToString();
                                String tempAXAJD_GOD_PARTCODE = axAJDetailDt.Rows[j][6].ToString();
                                String tempAXAJD_QTY = axAJDetailDt.Rows[j][7].ToString();
                                String tempAXAJD_TRANSDATE = axAJDetailDt.Rows[j][8].ToString();
                                String tempAXAJD_INVENTTRANSID = axAJDetailDt.Rows[j][9].ToString();
                                String tempAXAJD_INVENTDIMID = axAJDetailDt.Rows[j][10].ToString();
                                String tempAXAJD_INVENTLOCATIONID = axAJDetailDt.Rows[j][11].ToString();
                                String tempAXAJD_INVENTSITEID = axAJDetailDt.Rows[j][12].ToString();
                                String tempAXAJD_WMSLOCATIONID = axAJDetailDt.Rows[j][13].ToString();
                                String tempAXAJD_CREATEDDATETIME = axAJDetailDt.Rows[j][14].ToString();
                                String tempAXAJD_CREATEDBY = axAJDetailDt.Rows[j][15].ToString();
                                String tempAXAJD_BOX_NO = axAJDetailDt.Rows[j][16].ToString();
                                String tempAXAJD_CASE = axAJDetailDt.Rows[j][17].ToString();
                                String tempAXAJD_PRODUCT_NAME = axAJDetailDt.Rows[j][18].ToString();
                                String tempAXAJD_SO_NUMBER = axAJDetailDt.Rows[j][19].ToString();
                                String tempAXAJD_CUSTOMER_NAME = axAJDetailDt.Rows[j][20].ToString();

                                queryResult = insertAXAJDetail(curDb, axajId.ToString(), tempAXAJD_DATAAREAID, tempAXAJD_JOURNALID, tempAXAJD_VENDACCOUNT,
                                            tempAXAJD_PurchID, tempAXAJD_LINENUM, tempAXAJD_ITEMID, tempAXAJD_GOD_PARTCODE, tempAXAJD_QTY,
                                            tempAXAJD_TRANSDATE, tempAXAJD_INVENTTRANSID, tempAXAJD_INVENTDIMID, tempAXAJD_INVENTLOCATIONID,
                                            tempAXAJD_INVENTSITEID, tempAXAJD_WMSLOCATIONID, tempAXAJD_CREATEDDATETIME, tempAXAJD_CREATEDBY,
                                            tempAXAJD_BOX_NO, tempAXAJD_CASE, tempAXAJD_PRODUCT_NAME, tempAXAJD_SO_NUMBER, tempAXAJD_CUSTOMER_NAME);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert ax aj detail failed");
                                }

                                queryResult = insertMovementHistory(curDb, tempAXAJD_DATAAREAID, tempAXAJD_JOURNALID, tempAXAJD_PurchID, tempAXAJD_ITEMID, tempAXAJD_GOD_PARTCODE,
                                        tempAXAJD_INVENTLOCATIONID, tempAXAJD_INVENTSITEID, tempAXAJD_WMSLOCATIONID, "", "", "", tempAXAJD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_AJ_DETAIL_INSERT, tempAXAJD_LINENUM);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert movement history failed");
                                }

                            }
                        }

                    }
                    isSuccess = "true";
                }
                catch (Exception e)
                {
                    curDb.rollbackNCloseConnection();
                    isSuccess = e.Message;
                    return isSuccess;
                }
                if (!curDb.checkIsConnectionClosed())
                    curDb.closeConnection();
                return isSuccess;
            }
        }

        private int setAXAJDetail(database curDb, String tempAXAJD_DATAAREAID,
            String tempAXAJD_JOURNALID, String tempAXAJD_VENDACCOUNT, String tempAXAJD_PurchID,
            String tempAXAJD_LINENUM, String tempAXAJD_ITEMID, String tempAXAJD_GOD_PARTCODE,
            String tempAXAJD_QTY, String tempAXAJD_TRANSDATE, String tempAXAJD_INVENTTRANSID,
            String tempAXAJD_INVENTDIMID, String tempAXAJD_INVENTLOCATIONID, String tempAXAJD_INVENTSITEID,
            String tempAXAJD_WMSLOCATIONID, String tempAXAJD_CREATEDDATETIME, String tempAXAJD_CREATEDBY)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXAJD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXAJD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_JOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_JOURNALID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_VENDACCOUNT", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)(tempAXAJD_VENDACCOUNT)},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_PURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_PurchID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_LINENUMBER", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXAJD_LINENUM)},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXAJD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_GOD_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXAJD_GOD_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_QTY", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXAJD_QTY)},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_TRANSDATE", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXAJD_TRANSDATE},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_INVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_INVENTTRANSID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_INVENTDIMID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_INVENTDIMID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXAJD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXAJD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_CREATEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXAJD_CREATEDDATETIME},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_CREATEDBY", ParameterType=SqlDbType.NVarChar, ParameterLength=8, keyData = (Object)tempAXAJD_CREATEDBY},
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXAJDetailByAXLineNum_JournalId_PurchId_ItemId_PartCode", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }

        private int insertAXAJDetail(database curDb, String axajId, String tempAXAJD_DATAAREAID,
            String tempAXAJD_JOURNALID, String tempAXAJD_VENDACCOUNT, String tempAXAJD_PurchID,
            String tempAXAJD_LINENUM, String tempAXAJD_ITEMID, String tempAXAJD_GOD_PARTCODE,
            String tempAXAJD_QTY, String tempAXAJD_TRANSDATE, String tempAXAJD_INVENTTRANSID,
            String tempAXAJD_INVENTDIMID, String tempAXAJD_INVENTLOCATIONID, String tempAXAJD_INVENTSITEID,
            String tempAXAJD_WMSLOCATIONID, String tempAXAJD_CREATEDDATETIME, String tempAXAJD_CREATEDBY,
            String tempAXAJD_BOX_NO, String tempAXAJD_CASE, String tempAXAJD_PRODUCT_NAME,
            String tempAXAJD_SO_NUMBER, String tempAXAJD_CUSTOMER_NAME)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };

                DataTable curDt = curDb.getDataTable("[selectNextAXAJDetailId]", curQueryParameter);

                List<Int64> list = new List<Int64>();
                for (int k = 0; k < curDt.Rows.Count; k++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[k][0]));
                }
                Int64 axajdId = list[0];

                curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXAJD_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axajdId)},
                                    new sqlQueryParameter {strParameter="@keyAXAJ_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axajId)},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXAJD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_JOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_JOURNALID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_VENDACCOUNT", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)(tempAXAJD_VENDACCOUNT)},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_PURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_PurchID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_LINENUMBER", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXAJD_LINENUM)},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXAJD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_GOD_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXAJD_GOD_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_QTY", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXAJD_QTY)},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_TRANSDATE", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXAJD_TRANSDATE},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_INVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_INVENTTRANSID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_INVENTDIMID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_INVENTDIMID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXAJD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXAJD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_CREATEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXAJD_CREATEDDATETIME},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_CREATEDBY", ParameterType=SqlDbType.NVarChar, ParameterLength=8, keyData = (Object)tempAXAJD_CREATEDBY},
                                    new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Int, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                    new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_BOX_NO", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_BOX_NO},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_CASE", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_CASE},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_PRODUCT_NAME", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXAJD_PRODUCT_NAME},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_SO_NUMBER", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXAJD_SO_NUMBER},
                                    new sqlQueryParameter {strParameter="@keyAXAJD_CUSTOMER_NAME", ParameterType=SqlDbType.NVarChar, ParameterLength=100, keyData = (Object)tempAXAJD_CUSTOMER_NAME},
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXAJDetail", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }


        //AX Arrival Journal
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public axAJObject[] getAllAXAJ()
        {
            axAJObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
                //new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argLocationId)},
                //new sqlQueryParameter {strParameter="@keyDocType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argDocType)}
                //new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(Session["curSessionWarehouseId"])}
            };

            try
            {
                //if (!(retrieveAllAXArrivalJournal() == "true"))
                //    throw new Exception("Failed to retrieve AX Arrival Journal");

                DataTable curDt = curDb.getDataTable("[selectAllAXAJ]", curQueryParameter);

                List<axAJObject> list = new List<axAJObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axAJObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        , curDt.Rows[i][4].ToString()
                        , curDt.Rows[i][5].ToString()
                        , curDt.Rows[i][6].ToString()
                        , curDt.Rows[i][7].ToString()
                        , curDt.Rows[i][8].ToString()
                        , curDt.Rows[i][9].ToString()
                        , curDt.Rows[i][10].ToString()
                        , curDt.Rows[i][11].ToString()
                        , curDt.Rows[i][12].ToString()
                        , curDt.Rows[i][13].ToString()
                        , curDt.Rows[i][14].ToString()
                        , curDt.Rows[i][15].ToString()
                        , curDt.Rows[i][16].ToString()
                        , curDt.Rows[i][17].ToString()
                        , curDt.Rows[i][18].ToString()
                        ));
                }

                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axAJObject[] getAllPendingAXAJ()
        {

            axAJObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
                //new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argLocationId)},
                //new sqlQueryParameter {strParameter="@keyDocType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argDocType)}
                //new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(Session["curSessionWarehouseId"])}
            };

            try
            {
                if (!(retrieveAllAXArrivalJournal() == "true"))
                    throw new Exception("Failed to retrieve AX Arrival Journal");

                DataTable curDt = curDb.getDataTable("[selectAllPendingAXAJ]", curQueryParameter);

                List<axAJObject> list = new List<axAJObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axAJObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        , curDt.Rows[i][4].ToString()
                        , curDt.Rows[i][5].ToString()
                        , curDt.Rows[i][6].ToString()
                        , curDt.Rows[i][7].ToString()
                        , curDt.Rows[i][8].ToString()
                        , curDt.Rows[i][9].ToString()
                        , curDt.Rows[i][10].ToString()
                        , curDt.Rows[i][11].ToString()
                        , curDt.Rows[i][12].ToString()
                        , curDt.Rows[i][13].ToString()
                        , curDt.Rows[i][14].ToString()
                        , curDt.Rows[i][15].ToString()
                        , curDt.Rows[i][16].ToString()
                        , curDt.Rows[i][17].ToString()
                        , curDt.Rows[i][18].ToString()
                        ));
                }

                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axAJDetailObject[] getAllPendingAXAJDetail()
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectAllPendingAXAJDetail]", curQueryParameter);

            List<axAJDetailObject> list = new List<axAJDetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axAJDetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    , curDt.Rows[i][21].ToString()
                    , curDt.Rows[i][22].ToString()
                    , curDt.Rows[i][23].ToString()
                    , curDt.Rows[i][24].ToString()
                    , curDt.Rows[i][25].ToString()
                    , curDt.Rows[i][26].ToString()
                    , curDt.Rows[i][27].ToString()
                    , curDt.Rows[i][28].ToString()
                    , curDt.Rows[i][29].ToString()
                    ));
            }

            axAJDetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }


        [WebMethod]
        public axAJPOAllocationObject[] getAllAXAJPOAllocation()
        {
            axAJPOAllocationObject[] curList = null;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    axConn.Open();
                    //Get all AX PO from AX DB View
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT [PURCHID] " +
                          ",[INVENTTRANSID] " +
                          ",[SO_NUMBER] " +
                          ",[CUSTOMERNAME] " +
                          ",[QTYALLOCATED] " +
                          ",[QTYONSTOCK] " +
                          "FROM [AX_WMS_PURCHORDERALLOCATION] " +
                          "WHERE [SO_NUMBER] IS NOT NULL " +
                          "AND [CUSTOMERNAME] IS NOT NULL " +
                          "AND [QTYALLOCATED] IS NOT NULL " +
                          "AND [QTYONSTOCK]  IS NOT NULL", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX AJ from AX DB View
                    DataTable axAJPOAllocationDt = new DataTable();

                    int noOfAXAJPOAllocation = sda.Fill(axAJPOAllocationDt);

                    List<axAJPOAllocationObject> list = new List<axAJPOAllocationObject>();
                    for (int i = 0; i < noOfAXAJPOAllocation; i++)
                    {
                        list.Add(new axAJPOAllocationObject(axAJPOAllocationDt.Rows[i][0].ToString()
                            , axAJPOAllocationDt.Rows[i][1].ToString()
                            , axAJPOAllocationDt.Rows[i][2].ToString()
                            , axAJPOAllocationDt.Rows[i][3].ToString()
                            , axAJPOAllocationDt.Rows[i][4].ToString()
                            , axAJPOAllocationDt.Rows[i][5].ToString()
                            ));
                    }

                    curList = list.ToArray();
                }
                catch (Exception e)
                {
                    //curDb.rollbackNCloseConnection();
                }
            }

            return curList;
        }

        [WebMethod]
        public axAJObject[] getAXAJByAXJournalId(String argAXJOURNALID)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength = 20, keyData = (Object)argAXJOURNALID},
            };

            DataTable curDt = curDb.getDataTable("[selectAXAJByAXJournalId]", curQueryParameter);

            List<axAJObject> list = new List<axAJObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axAJObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    ));
            }

            axAJObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axAJDetailObject[] getAXAJDetailByAXJournalId(String argAXJOURNALID)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength = 20, keyData = (Object)argAXJOURNALID},
            };

            DataTable curDt = curDb.getDataTable("[selectAXAJDetailByAXJournalId]", curQueryParameter);

            List<axAJDetailObject> list = new List<axAJDetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axAJDetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    , curDt.Rows[i][21].ToString()
                    , curDt.Rows[i][22].ToString()
                    , curDt.Rows[i][23].ToString()
                    , curDt.Rows[i][24].ToString()
                    , curDt.Rows[i][25].ToString()
                    , curDt.Rows[i][26].ToString()
                    , curDt.Rows[i][27].ToString()
                    , curDt.Rows[i][28].ToString()
                    , curDt.Rows[i][29].ToString()
                    ));
            }

            axAJDetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public String setAXAJDetailScanInput(List<AXAJDetailScanInput> aXAJDetailScanInputList)
        {
            String isSuccess = "AX ajD Update failed";
            int queryResult = -99;
            database curDb = new database();
            curDb.openConnection();
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            try
            {


                foreach (var aXAJDetailScanInput in aXAJDetailScanInputList)
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(aXAJDetailScanInput.argAXLINENUMBER)},
                        new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXAJDetailScanInput.argAXJOURNALID},
                        new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXAJDetailScanInput.argAXPURCHID},
                        new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXAJDetailScanInput.argAXITEMID},
                        new sqlQueryParameter {strParameter="@keyAXGOD_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXAJDetailScanInput.argAXGOD_PARTCODE},
                    };

                    curDt = curDb.getDataTable("[selectAXAJDetailByAXLineNum_JournalId_PurchId_ItemId_PartCode]", curQueryParameter);

                    if (curDt.Rows.Count > 0)
                    {
                        String tempAXAJD_DATAAREAID = curDt.Rows[0][2].ToString();
                        String tempAXAJD_JOURNALID = curDt.Rows[0][3].ToString();
                        String tempAXAJD_VENDACCOUNT = curDt.Rows[0][4].ToString();
                        String tempAXAJD_PurchID = curDt.Rows[0][5].ToString();
                        String tempAXAJD_LINENUM = curDt.Rows[0][6].ToString();
                        String tempAXAJD_ITEMID = curDt.Rows[0][7].ToString();
                        String tempAXAJD_GOD_PARTCODE = curDt.Rows[0][8].ToString();
                        String tempAXAJD_QTY = curDt.Rows[0][9].ToString();
                        String tempAXAJD_TRANSDATE = curDt.Rows[0][10].ToString();
                        String tempAXAJD_INVENTTRANSID = curDt.Rows[0][11].ToString();
                        String tempAXAJD_INVENTDIMID = curDt.Rows[0][12].ToString();
                        String tempAXAJD_INVENTLOCATIONID = curDt.Rows[0][13].ToString();
                        String tempAXAJD_INVENTSITEID = curDt.Rows[0][14].ToString();
                        String tempAXAJD_WMSLOCATIONID = curDt.Rows[0][15].ToString();
                        String tempAXAJD_CREATEDDATETIME = curDt.Rows[0][16].ToString();
                        String tempAXAJD_CREATEDBY = curDt.Rows[0][17].ToString();

                        if (Convert.ToDecimal(tempAXAJD_QTY) < Convert.ToDecimal(aXAJDetailScanInput.argScanQuantity))
                        {
                            throw new Exception("Update Failed - " + aXAJDetailScanInput.argAXITEMID +
                                " - Scan Quantity[" + aXAJDetailScanInput.argScanQuantity + "] more then Order Quantity [" + tempAXAJD_QTY + "]");
                        }

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(aXAJDetailScanInput.argAXLINENUMBER)},
                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXAJDetailScanInput.argAXJOURNALID},
                            new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXAJDetailScanInput.argAXPURCHID},
                            new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXAJDetailScanInput.argAXITEMID},
                            new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXAJDetailScanInput.argAXGOD_PARTCODE},
                            new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Decimal, keyData =  Convert.ToDecimal(aXAJDetailScanInput.argScanQuantity)},
                            new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData =  (Object)(aXAJDetailScanInput.argScanDatetime)},
                            new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData =  (Object)aXAJDetailScanInput.argRemarks},
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(aXAJDetailScanInput.argUserId) },
                            new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)aXAJDetailScanInput.argDeviceSN},
                            new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(9) },
                        };

                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXAJDetailScanInputByAXLineNum_JournalId_PurchId_ItemId_PartCode", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Update AX AJD Failed");
                        }

                        queryResult = insertMovementHistory(curDb, tempAXAJD_DATAAREAID, tempAXAJD_JOURNALID,
                            tempAXAJD_PurchID, tempAXAJD_ITEMID, tempAXAJD_GOD_PARTCODE,
                            tempAXAJD_INVENTLOCATIONID, tempAXAJD_INVENTSITEID, tempAXAJD_WMSLOCATIONID,
                            "", "", "", aXAJDetailScanInput.argScanQuantity, aXAJDetailScanInput.argUserId, aXAJDetailScanInput.argDeviceSN,
                            sharedRes.LGC2AX_AJ_DETAIL_UPDATE, aXAJDetailScanInput.argAXLINENUMBER);
                        if (queryResult == -99)
                        {
                            throw new Exception("insert movement history failed");
                        }

                    }
                    else
                    {
                        //Temporarily skip for those line is not existed in WMS DB
                        //continue;
                        throw new Exception("Update Failed - " + aXAJDetailScanInput.argAXITEMID + " does not found in AJ - " + aXAJDetailScanInput.argAXPURCHID);
                    }
                }

                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Comment this as Arrival Journal Only allow one submission per document
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //curQueryParameter = new List<sqlQueryParameter>() {
                //            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData =  (Object)argAXPURCHID },
                //        };

                //curDt = curDb.getDataTable("[checkExistenceAXAJDetailWRemainingQuantityByAXJournalId]", curQueryParameter);
                ////No more pending line for this AX AJ
                //if (curDt.Rows.Count <= 0)
                //{
                curQueryParameter = new List<sqlQueryParameter>()
                            {
                                new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXAJDetailScanInputList.First().argAXJOURNALID},
                                new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(9)},
                            };
                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXAJCompletedByAXJournalId", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("Update AX Arrival Journal Detail Status Failed");
                }
                //}
                //else
                //{
                //    throw new Exception("Update AX Arrival Journal Detail Status Failed");
                //}

                String postResult = postAXAJDetail2AX(curDb, aXAJDetailScanInputList[0].argAXJOURNALID,
                    aXAJDetailScanInputList[0].argUserId, aXAJDetailScanInputList[0].argDeviceSN);
                if (postResult != "true")
                    throw new Exception(postResult);

                isSuccess = "true";

            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        private String postAXAJDetail2AX(database curDb, String argAXJOURNALID, String argUserId, String argDeviceSN)
        {
            String isSuccess = "Failed to post data to AX";
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            String xmlMessage = "";
            String messageId = getAXMessageId();
            String isDevelopmentMode = "0";
            try
            {
                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)argAXJOURNALID}
                        };
                curDt = curDb.getDataTable("selectAXAJByAXJournalId", curQueryParameter);
                String argAXPACKINGSLIP = curDt.Rows[0][6].ToString();

                xmlMessage = "<?xml version =\"1.0\" encoding=\"UTF-8\"?>\r\n" +
                    "<ARRIVALJRN>\r\n";
                int counter = 0;

                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength = 20, keyData = (Object)argAXJOURNALID},
                };

                curDt = curDb.getDataTable("[selectAXAJDetailByAXJournalId]", curQueryParameter);

                List<axTODetailObject> list = new List<axTODetailObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    String tempAXAJD_DATAAREAID = curDt.Rows[i][2].ToString();
                    String tempAXAJD_JOURNALID = curDt.Rows[i][3].ToString();
                    String tempAXAJD_VENDACCOUNT = curDt.Rows[i][4].ToString();
                    String tempAXAJD_PurchID = curDt.Rows[i][5].ToString();
                    String tempAXAJD_LINENUM = curDt.Rows[i][6].ToString();
                    String tempAXAJD_ITEMID = curDt.Rows[i][7].ToString();
                    String tempAXAJD_GOD_PARTCODE = curDt.Rows[i][8].ToString();
                    String tempAXAJD_QTY = curDt.Rows[i][9].ToString();
                    String tempAXAJD_TRANSDATE = curDt.Rows[i][10].ToString();
                    String tempAXAJD_INVENTTRANSID = curDt.Rows[i][11].ToString();
                    String tempAXAJD_INVENTDIMID = curDt.Rows[i][12].ToString();
                    String tempAXAJD_INVENTLOCATIONID = curDt.Rows[i][13].ToString();
                    String tempAXAJD_INVENTSITEID = curDt.Rows[i][14].ToString();
                    String tempAXAJD_WMSLOCATIONID = curDt.Rows[i][15].ToString();
                    String tempAXAJD_CREATEDDATETIME = curDt.Rows[i][16].ToString();
                    String tempAXAJD_CREATEDBY = curDt.Rows[i][17].ToString();
                    String tempAXAJD_scanQuantity = curDt.Rows[i][18].ToString();
                    String tempAXAJD_BOX_NO = curDt.Rows[i][25].ToString();
                    String tempAXAJD_CASE = curDt.Rows[i][26].ToString();
                    String tempAXAJD_PRODUCT_NAME = curDt.Rows[i][27].ToString();

                    if (counter == 0)
                    {
                        xmlMessage += "<JRN>\r\n" +
                                         "<Company_Code>" + tempAXAJD_DATAAREAID + "</Company_Code>\r\n" +
                                         "<Arrival_Journal_ID>" + argAXJOURNALID + "</Arrival_Journal_ID>\r\n" +
                                         "<PO_GRN_NO>" + argAXPACKINGSLIP + "</PO_GRN_NO>\r\n" +
                                         "</JRN>\r\n";
                    }
                    counter++;
                    if (!(String.IsNullOrEmpty(tempAXAJD_scanQuantity)))
                    {

                        xmlMessage += "<JRN_DETAIL>\r\n" +
                            "<Company_Code>" + tempAXAJD_DATAAREAID + "</Company_Code>\r\n" +
                            "<LineNumber>" + tempAXAJD_LINENUM + "</LineNumber>\r\n" +
                            "<PO_NO>" + tempAXAJD_PurchID + "</PO_NO>\r\n" +
                            "<Account_Number>" + tempAXAJD_VENDACCOUNT + "</Account_Number>\r\n" +
                            "<ITEM_CODE>" + tempAXAJD_ITEMID + "</ITEM_CODE>\r\n" +
                            "<PART_CODE>" + tempAXAJD_GOD_PARTCODE + "</PART_CODE>\r\n" +
                            "<QTY>" + (String.IsNullOrEmpty(tempAXAJD_scanQuantity) ? "0" : tempAXAJD_scanQuantity) + "</QTY>\r\n" +
                            "</JRN_DETAIL>\r\n";

                    }

                }
                xmlMessage += "</ARRIVALJRN>";


                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"DEVELOPMENT MODE"},
                };

                curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                isDevelopmentMode = curDt.Rows[0][2].ToString();

                if (isDevelopmentMode == "0")
                {
                    AJPostService.WMS_ArrivalJournalPostServiceClient grnObject = new AJPostService.WMS_ArrivalJournalPostServiceClient();

                    AJPostService.CallContext context = new AJPostService.CallContext();
                    context.MessageId = messageId; // "{33000000-0000-0000-0000-000000000012}";

                    String result = "";
                    try
                    {
                        result = grnObject.ReadXMLFile(context, xmlMessage);
                    }
                    catch (Exception axEx)
                    {
                        throw new Exception("Failed to post data to AX - " + axEx.Message + " " + messageId);
                    }

                    Boolean isAXPostSuccess = false;
                    if (!String.IsNullOrEmpty(result))
                    {
                        string[] words = result.Split(' ');
                        if (words[0].Equals("Successfully", StringComparison.OrdinalIgnoreCase))
                        {
                            isAXPostSuccess = true;
                        }
                    }

                    if (!isAXPostSuccess)
                    {
                        throw new Exception("Failed to post data to AX - " + result + " " + messageId);
                        //String axExceptionMsg = retrieveAXExceptionMsg(messageId);
                        //if (axExceptionMsg == "")
                        //{
                        //    throw new Exception("Failed to post data to AX - " + result);
                        //}
                        //else
                        //{
                        //    throw new Exception("Failed to post data to AX - " + axExceptionMsg);
                        //}
                    }
                }
                //else
                //{
                //    ////Testing Purpose
                //    string pathString = XML_FOLDER_PATH;

                //    bool exists = System.IO.Directory.Exists(pathString);

                //    if (!exists)
                //        System.IO.Directory.CreateDirectory(pathString);

                //    string fileName = "AJ_" + getAXMessageId() + ".xml";
                //    pathString = System.IO.Path.Combine(pathString, fileName);

                //    if (!System.IO.File.Exists(pathString))
                //    {
                //        using (System.IO.StreamWriter file =
                //            new System.IO.StreamWriter(pathString, true))
                //        {
                //            file.WriteLine(xmlMessage);
                //        }
                //    }
                //    throw new Exception("XML Generated");
                //}

                isSuccess = "true";

            }
            catch (Exception e)
            {
                insertExceptionMsg(argAXJOURNALID, argUserId, argDeviceSN, "AJ", e.Message, messageId);
                isSuccess = e.Message;
                try
                {
                    ////Testing Purpose
                    string pathString = XML_FOLDER_PATH;

                    pathString = System.IO.Path.Combine(pathString, "AJ");
                    bool exists = System.IO.Directory.Exists(pathString);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(pathString);

                    string fileName = "AJ_" + messageId + ".xml";
                    pathString = System.IO.Path.Combine(pathString, fileName);

                    if (!System.IO.File.Exists(pathString))
                    {
                        using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(pathString, true))
                        {
                            file.WriteLine(xmlMessage);
                        }
                    }
                }
                catch (Exception e2) { }
            }
            return isSuccess;
        }

        [WebMethod]
        public void getAllAJGoodsReceiveInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];

            String searchHeader = "";
            String searchContent = "";
            String searchContent2 = "";
            String searchType = HttpContext.Current.Request.QueryString["filterType"];
            String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');
            String warehouseCode = HttpContext.Current.Request.QueryString["warehouseCode"].ToString();

            searchHeader = searchArray[0];
            if (searchArray.Length == 3)
            {
                searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
                searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
            }
            else
            {
                searchContent = searchArray[1] == "" ? "%" : searchArray[1];
            }

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchType},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent},
                new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent2},
                new sqlQueryParameter {strParameter="@keySearchWarehouseCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)warehouseCode}
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllAXAJGoodsReceiveInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");

        }

        //Integration - AX SO
        //+++++++++++++++++++++++++++++++++++++

        //Get All Pending AX SO From AX DB View
        [WebMethod]
        public String retrieveAllAXSalesOrder()
        {
            String isSuccess = "Failed Retrieve AX Sales Order";

            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            DataTable curDt;
            int queryResult = -99;
            String lastSyncDatetime = "";
            String currentSyncDatetime = getLastSyncDatetime();

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST SO SYNC"},
                    };


                    curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                    lastSyncDatetime = curDt.Rows[0][2].ToString();
                    if (String.IsNullOrEmpty(lastSyncDatetime))
                        lastSyncDatetime = "1999-01-01 00:00:00";

                    axConn.Open();
                    //++++++++++++++++++++++++++++++
                    //Select PO with modified data
                    //++++++++++++++++++++++++++++++
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT DISTINCT [AX_WMS_OPN_SALESORDERPICKLIST].[DATAAREAID] " +
                           ",[AX_WMS_OPN_SALESORDERPICKLIST].[PICKINGROUTEID] " +
                           ",[AX_WMS_OPN_SALESORDERPICKLIST].[SALESId] " +
                           ",[AX_WMS_OPN_SALESORDERPICKLIST].[confirmationDate] " +
                           ",[AX_WMS_OPN_SALESORDERPICKLIST].[CREATEDDATETIME] " +
                           ",[AX_WMS_OPN_SALESORDERPICKLIST].[SALESTYPE] " +
                           ",[AX_WMS_OPN_SALESORDERPICKLIST].[CustomerAccount] " +
                           ",[AX_WMS_OPN_SALESORDERPICKLIST].[CustomerName] " +
                           "FROM [AX_WMS_OPN_SALESORDERPICKLIST] " +
                           "JOIN AX_WMS_OPN_SALESLINEPICKLIST ON AX_WMS_OPN_SALESLINEPICKLIST.ROUTEID = AX_WMS_OPN_SALESORDERPICKLIST.PICKINGROUTEID " +
                        "WHERE AX_WMS_OPN_SALESLINEPICKLIST.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX SO from AX DB View
                    DataTable axSODt = new DataTable();

                    int noOfAXSO = sda.Fill(axSODt);

                    Int64 axsoId = -1;
                    //Loop through all AXSO from AX DB View
                    for (int i = 0; i < noOfAXSO; i++)
                    {
                        String tempAXSO_DATAAREAID = axSODt.Rows[i][0].ToString();
                        String tempAXSO_PICKINGROUTEID = axSODt.Rows[i][1].ToString();
                        String tempAXSO_SALESId = axSODt.Rows[i][2].ToString();
                        String tempAXSO_CONFIRMATIONDATE = axSODt.Rows[i][3].ToString();
                        String tempAXSO_CREATEDDATETIME = axSODt.Rows[i][4].ToString();
                        String tempAXSO_SALESTYPE = axSODt.Rows[i][5].ToString();
                        String tempAXSO_CUSTOMERACCOUNT = axSODt.Rows[i][6].ToString();
                        String tempAXSO_CUSTOMERNAME = axSODt.Rows[i][7].ToString();

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXROUTEId", ParameterType=SqlDbType.NVarChar,ParameterLength=10, keyData = (Object)tempAXSO_PICKINGROUTEID}
                        };
                        curDt = curDb.getDataTable("selectAXSOByAXRouteId", curQueryParameter);

                        //++++++++++++++++++++++++++++++
                        //If AX SO Existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        if (curDt.Rows.Count > 0)
                        {
                            axsoId = Convert.ToInt64(curDt.Rows[0][0].ToString());

                            axCommand = new SqlCommand(
                                "SELECT DISTINCT [DATAAREAID] " +
                                  ",[LINENUM] " +
                                  ",[ROUTEID] " +
                                  ",[INVENTTRANSID] " +
                                  ",[ITEMID] " +
                                  ",[DOT_PARTCODE] " +
                                  ",[NAMEALIAS] " +
                                  ",[SALESUNIT] " +
                                  ",[INVENTLOCATIONID] " +
                                  ",[INVENTSITEID] " +
                                  ",[WMSLOCATIONID] " +
                                  ",[QTY] " +
                                  "FROM [AX_WMS_OPN_SALESLINEPICKLIST] " +
                                  "WHERE [ROUTEID] = @keyAXSO_ROUTEID " +
                                  "AND AX_WMS_OPN_SALESLINEPICKLIST.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXSO_ROUTEID", tempAXSO_PICKINGROUTEID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axSODetailDt = new DataTable();
                            //AX SO Detail in AX DB View
                            sda.Fill(axSODetailDt);

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXSO_PICKINGROUTEID},
                            };

                            DataTable curAXSODetailDt = curDb.getDataTable("[selectAXSODetailByAXRouteId]", curQueryParameter);

                            //Loop through each of the AXSO Detail
                            for (int j = 0; j < axSODetailDt.Rows.Count; j++)
                            {
                                String tempAXSOD_DATAAREAID = axSODetailDt.Rows[j][0].ToString();
                                String tempAXSOD_LINENUMBER = axSODetailDt.Rows[j][1].ToString();
                                String tempAXSOD_ROUTEID = axSODetailDt.Rows[j][2].ToString();
                                String tempAXSOD_INVENTTRANSID = axSODetailDt.Rows[j][3].ToString();
                                String tempAXSOD_ITEMID = axSODetailDt.Rows[j][4].ToString();
                                String tempAXSOD_DOT_PARTCODE = axSODetailDt.Rows[j][5].ToString();
                                String tempAXSOD_NAME = axSODetailDt.Rows[j][6].ToString();
                                String tempAXSOD_SALESUNIT = axSODetailDt.Rows[j][7].ToString();
                                String tempAXSOD_INVENTLOCATIONID = axSODetailDt.Rows[j][8].ToString();
                                String tempAXSOD_INVENTSITEID = axSODetailDt.Rows[j][9].ToString();
                                String tempAXSOD_WMSLOCATIONID = axSODetailDt.Rows[j][10].ToString();
                                String tempAXSOD_QTY = axSODetailDt.Rows[j][11].ToString();

                                Boolean soDetailExisted = false;
                                for (int k = 0; k < curAXSODetailDt.Rows.Count; k++)
                                {
                                    String tempWMSSOD_id = curAXSODetailDt.Rows[k][0].ToString();
                                    String tempWMSSOD_LINENUMBER = curAXSODetailDt.Rows[k][3].ToString();
                                    String tempWMSSOD_ROUTEID = curAXSODetailDt.Rows[k][4].ToString();
                                    String tempWMSSOD_INVENTTRANSID = curAXSODetailDt.Rows[k][5].ToString();
                                    String tempWMSSOD_ITEMID = curAXSODetailDt.Rows[k][6].ToString();
                                    String tempWMSSOD_DOT_PARTCODE = curAXSODetailDt.Rows[k][7].ToString();
                                    String tempWMSSOD_scanQuantity = String.IsNullOrEmpty(curAXSODetailDt.Rows[k][14].ToString()) ? "0" : curAXSODetailDt.Rows[k][14].ToString(); 

                                    //If AX SO Detail Existed in WMS DB
                                    if (tempAXSOD_ITEMID.Trim() == tempWMSSOD_ITEMID.Trim() &&
                                        tempAXSOD_DOT_PARTCODE.Trim() == tempWMSSOD_DOT_PARTCODE.Trim() &&
                                        (Convert.ToDecimal(tempAXSOD_LINENUMBER) == Convert.ToDecimal(tempWMSSOD_LINENUMBER)))
                                    {
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\
                                        //Update quantity changes
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                        queryResult = setAXSODetail(curDb, tempAXSOD_DATAAREAID, tempAXSOD_LINENUMBER, tempAXSOD_ROUTEID,
                                            tempAXSOD_INVENTTRANSID, tempAXSOD_ITEMID, tempAXSOD_DOT_PARTCODE, tempAXSOD_NAME,
                                            tempAXSOD_SALESUNIT, tempAXSOD_INVENTLOCATIONID,
                                            tempAXSOD_INVENTSITEID, tempAXSOD_WMSLOCATIONID, tempAXSOD_QTY);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("update ax so detail failed");
                                        }

                                        Int64 curStatusId = 9;
                                        if (Convert.ToDecimal(tempWMSSOD_scanQuantity) == 0)
                                        {
                                            curStatusId = 1;
                                        }
                                        else if (Convert.ToDecimal(tempAXSOD_QTY) > Convert.ToDecimal(tempWMSSOD_scanQuantity))
                                        {
                                            curStatusId = 7;
                                        }

                                        curQueryParameter = new List<sqlQueryParameter>()
                                        {
                                                new sqlQueryParameter {strParameter="@keyTableName", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)"tbl_ax_soDetail"},
                                                new sqlQueryParameter {strParameter="@keyFieldName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"tbl_status_Id"},
                                                new sqlQueryParameter {strParameter="@keyFieldValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)curStatusId.ToString()},
                                                new sqlQueryParameter {strParameter="@keyIDName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"id"},
                                                new sqlQueryParameter {strParameter="@keyIDValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)tempWMSSOD_id}
                                            };
                                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateTableSingleField", curQueryParameter);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("Update AX Sales Order Detail Status Failed");
                                        }

                                        queryResult = insertMovementHistory(curDb, tempAXSO_DATAAREAID, tempAXSO_PICKINGROUTEID, tempAXSO_SALESId, tempAXSOD_ITEMID, tempAXSOD_DOT_PARTCODE,
                                            tempAXSOD_INVENTLOCATIONID, tempAXSOD_INVENTSITEID, tempAXSOD_WMSLOCATIONID, "", "", "", tempAXSOD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_SO_DETAIL_UPDATE, tempAXSOD_LINENUMBER);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("insert movement history failed");
                                        }

                                        soDetailExisted = true;
                                        break;
                                    }
                                }

                                //if AX SO Detail no existed in WMS DB
                                if (!soDetailExisted)
                                {
                                    queryResult = insertAXSODetail(curDb, axsoId.ToString(), tempAXSOD_DATAAREAID, tempAXSOD_LINENUMBER, tempAXSOD_ROUTEID,
                                    tempAXSOD_INVENTTRANSID, tempAXSOD_ITEMID, tempAXSOD_DOT_PARTCODE, tempAXSOD_NAME,
                                    tempAXSOD_SALESUNIT, tempAXSOD_INVENTLOCATIONID,
                                    tempAXSOD_INVENTSITEID, tempAXSOD_WMSLOCATIONID, tempAXSOD_QTY);
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert ax po detail failed");
                                    }

                                    queryResult = insertMovementHistory(curDb, tempAXSO_DATAAREAID, tempAXSO_PICKINGROUTEID, tempAXSO_SALESId, tempAXSOD_ITEMID, tempAXSOD_DOT_PARTCODE,
                                        tempAXSOD_INVENTLOCATIONID, tempAXSOD_INVENTSITEID, tempAXSOD_WMSLOCATIONID, "", "", "", tempAXSOD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_SO_DETAIL_INSERT, tempAXSOD_LINENUMBER);
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert movement history failed");
                                    }

                                }
                            }
                        }
                        //++++++++++++++++++++++++++++++
                        //if AX SO no existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        else
                        {
                            axCommand = new SqlCommand(
                                "SELECT DISTINCT [DATAAREAID] " +
                                  ",[LINENUM] " +
                                  ",[ROUTEID] " +
                                  ",[INVENTTRANSID] " +
                                  ",[ITEMID] " +
                                  ",[DOT_PARTCODE] " +
                                  ",[NAMEALIAS] " +
                                  ",[SALESUNIT] " +
                                  ",[INVENTLOCATIONID] " +
                                  ",[INVENTSITEID] " +
                                  ",[WMSLOCATIONID] " +
                                  ",[QTY] " +
                                  "FROM [AX_WMS_OPN_SALESLINEPICKLIST] " +
                                  "WHERE [ROUTEID] = @keyAXSO_ROUTEID", axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXSO_ROUTEID", tempAXSO_PICKINGROUTEID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axSODetailDt = new DataTable();
                            //AX SO Detail in AX DB View
                            sda.Fill(axSODetailDt);


                            curQueryParameter = new List<sqlQueryParameter>()
                            {
                            };

                            curDt = curDb.getDataTable("[selectNextAXSOId]", curQueryParameter);

                            List<Int64> list = new List<Int64>();
                            for (int j = 0; j < curDt.Rows.Count; j++)
                            {
                                list.Add(Convert.ToInt64(curDt.Rows[j][0]));
                            }
                            axsoId = list[0];

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXSO_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axsoId)},
                                new sqlQueryParameter {strParameter="@keyAXSO_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXSO_DATAAREAID},
                                new sqlQueryParameter {strParameter="@keyAXSO_ROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXSO_PICKINGROUTEID},
                                new sqlQueryParameter {strParameter="@keyAXSO_SALESId", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXSO_SALESId},
                                new sqlQueryParameter {strParameter="@keyAXSO_ConfirmationDate", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXSO_CONFIRMATIONDATE},
                                new sqlQueryParameter {strParameter="@keyAXSO_CREATEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXSO_CREATEDDATETIME},
                                new sqlQueryParameter {strParameter="@keyAXSO_SALESTYPE", ParameterType=SqlDbType.Int, keyData = Convert.ToInt32(tempAXSO_SALESTYPE)},
                                new sqlQueryParameter {strParameter="@keyReferenceNo", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)""},
                                new sqlQueryParameter {strParameter="@keyCreationDateTime", ParameterType=SqlDbType.DateTime, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyDocType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"so"},
                                new sqlQueryParameter {strParameter="@keyReferenceVersion", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyAXSO_CustomerAccount", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXSO_CUSTOMERACCOUNT},
                                new sqlQueryParameter {strParameter="@keyAXSO_CustomerName", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXSO_CUSTOMERNAME},
                            };
                            queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXSO", curQueryParameter);
                            if (queryResult == -99)
                            {
                                throw new Exception("insert ax sales order failed");
                            }

                            queryResult = insertMovementHistory(curDb, tempAXSO_DATAAREAID, tempAXSO_PICKINGROUTEID, tempAXSO_SALESId, "", "", "", "", "", "", "", "", "", "-1", "SYSTEM", sharedRes.AX2LGC_SO_INSERT, "");
                            if (queryResult == -99)
                            {
                                throw new Exception("insert movement history failed");
                            }

                            //Loop through each of the AXSO Detail
                            for (int j = 0; j < axSODetailDt.Rows.Count; j++)
                            {
                                String tempAXSOD_DATAAREAID = axSODetailDt.Rows[j][0].ToString();
                                String tempAXSOD_LINENUMBER = axSODetailDt.Rows[j][1].ToString();
                                String tempAXSOD_ROUTEID = axSODetailDt.Rows[j][2].ToString();
                                String tempAXSOD_INVENTTRANSID = axSODetailDt.Rows[j][3].ToString();
                                String tempAXSOD_ITEMID = axSODetailDt.Rows[j][4].ToString();
                                String tempAXSOD_DOT_PARTCODE = axSODetailDt.Rows[j][5].ToString();
                                String tempAXSOD_NAME = axSODetailDt.Rows[j][6].ToString();
                                String tempAXSOD_SALESUNIT = axSODetailDt.Rows[j][7].ToString();
                                String tempAXSOD_INVENTLOCATIONID = axSODetailDt.Rows[j][8].ToString();
                                String tempAXSOD_INVENTSITEID = axSODetailDt.Rows[j][9].ToString();
                                String tempAXSOD_WMSLOCATIONID = axSODetailDt.Rows[j][10].ToString();
                                String tempAXSOD_QTY = axSODetailDt.Rows[j][11].ToString();

                                queryResult = insertAXSODetail(curDb, axsoId.ToString(), tempAXSOD_DATAAREAID, tempAXSOD_LINENUMBER, tempAXSOD_ROUTEID,
                                    tempAXSOD_INVENTTRANSID, tempAXSOD_ITEMID, tempAXSOD_DOT_PARTCODE, tempAXSOD_NAME,
                                    tempAXSOD_SALESUNIT, tempAXSOD_INVENTLOCATIONID,
                                    tempAXSOD_INVENTSITEID, tempAXSOD_WMSLOCATIONID, tempAXSOD_QTY);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert ax po detail failed");
                                }

                                queryResult = insertMovementHistory(curDb, tempAXSO_DATAAREAID, tempAXSO_PICKINGROUTEID, tempAXSO_SALESId, tempAXSOD_ITEMID, tempAXSOD_DOT_PARTCODE,
                                    tempAXSOD_INVENTLOCATIONID, tempAXSOD_INVENTSITEID, tempAXSOD_WMSLOCATIONID, "", "", "", tempAXSOD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_SO_DETAIL_INSERT, tempAXSOD_LINENUMBER);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert movement history failed");
                                }
                            }
                        }
                    }
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST SO SYNC"},
                        new sqlQueryParameter {strParameter="@keyDataValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)currentSyncDatetime}
                    };

                    queryResult = curDb.insertUpdateDeleteDataIntoTable("updateSystemData", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert/update/delete failed");
                    }
                    
                    isSuccess = "true";
                }
                catch (Exception e)
                {
                    curDb.rollbackNCloseConnection();
                    isSuccess = e.Message;
                    return isSuccess;
                }
                if (!curDb.checkIsConnectionClosed())
                    curDb.closeConnection();
                return isSuccess;
            }
        }

        private int insertAXSODetail(database curDb, String axsoId, String tempAXSOD_DATAAREAID,
            String tempAXSOD_LINENUMBER, String tempAXSOD_ROUTEID, String tempAXSOD_INVENTTRANSID,
            String tempAXSOD_ITEMID, String tempAXSOD_DOT_PARTCODE, String tempAXSOD_NAME,
            String tempAXSOD_SALESUNIT, String tempAXSOD_INVENTLOCATIONID, String tempAXSOD_INVENTSITEID,
            String tempAXSOD_WMSLOCATIONID, String tempAXSOD_QTY)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };

                DataTable curDt = curDb.getDataTable("[selectNextAXSODetailId]", curQueryParameter);

                List<Int64> list = new List<Int64>();
                for (int k = 0; k < curDt.Rows.Count; k++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[k][0]));
                }
                Int64 axsodId = list[0];

                curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXSOD_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axsodId)},
                                    new sqlQueryParameter {strParameter="@keyAXPO_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axsoId)},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXSOD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_LINENUMBER", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXSOD_LINENUMBER)},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_ROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXSOD_ROUTEID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_INVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXSOD_INVENTTRANSID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXSOD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXSOD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_NAME", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXSOD_NAME},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_SALESUNIT", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXSOD_SALESUNIT},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXSOD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXSOD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXSOD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_QTY", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXSOD_QTY)},
                                    new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Int, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                    new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXSODetail", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }


        private int setAXSODetail(database curDb, String tempAXSOD_DATAAREAID,
            String tempAXSOD_LINENUMBER, String tempAXSOD_ROUTEID, String tempAXSOD_INVENTTRANSID,
            String tempAXSOD_ITEMID, String tempAXSOD_DOT_PARTCODE, String tempAXSOD_NAME,
            String tempAXSOD_SALESUNIT, String tempAXSOD_INVENTLOCATIONID, String tempAXSOD_INVENTSITEID,
            String tempAXSOD_WMSLOCATIONID, String tempAXSOD_QTY)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXSOD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXSOD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_LINENUMBER", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXSOD_LINENUMBER)},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_ROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXSOD_ROUTEID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_INVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXSOD_INVENTTRANSID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXSOD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXSOD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_NAME", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXSOD_NAME},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_SALESUNIT", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXSOD_SALESUNIT},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXSOD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXSOD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXSOD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXSOD_QTY", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXSOD_QTY)},
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXSODetailByAXLineNum_RouteId_ItemId_PartCode", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }


        //AX Sales Order
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public void getAllAXSOInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];

            String searchHeader = "";
            String searchContent = "";
            String searchContent2 = "";
            String searchType = HttpContext.Current.Request.QueryString["filterType"];
            String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');
            String warehouseCode = HttpContext.Current.Request.QueryString["warehouseCode"].ToString();

            searchHeader = searchArray[0];
            if (searchArray.Length == 3)
            {
                searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
                searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
            }
            else
            {
                searchContent = searchArray[1] == "" ? "%" : searchArray[1];
            }

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchType},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent},
                new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent2},
                new sqlQueryParameter {strParameter="@keySearchWarehouseCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)warehouseCode}
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllAXSOInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");

        }

        [WebMethod]
        public axSOObject[] getAllPendingAXSO()
        {

            axSOObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            try
            {
                if (!(retrieveAllAXSalesOrder() == "true"))
                    throw new Exception("Failed to retrieve AX Sales Order");

                DataTable curDt = curDb.getDataTable("[selectAllPendingAXSO]", curQueryParameter);

                List<axSOObject> list = new List<axSOObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axSOObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        , curDt.Rows[i][4].ToString()
                        , curDt.Rows[i][5].ToString()
                        , curDt.Rows[i][6].ToString()
                        , curDt.Rows[i][7].ToString()
                        , curDt.Rows[i][8].ToString()
                        , curDt.Rows[i][9].ToString()
                        , curDt.Rows[i][10].ToString()
                        , curDt.Rows[i][11].ToString()
                        , curDt.Rows[i][12].ToString()
                        , curDt.Rows[i][13].ToString()
                        , curDt.Rows[i][14].ToString()
                        , curDt.Rows[i][15].ToString()
                        , curDt.Rows[i][16].ToString()
                        , curDt.Rows[i][17].ToString()
                        , curDt.Rows[i][18].ToString()
                        ));
                }

                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axSOObject[] getAXSOByAXRouteId(String argAXROUTEId)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXROUTEId", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)argAXROUTEId},
            };

            DataTable curDt = curDb.getDataTable("[selectAXSOByAXRouteId]", curQueryParameter);

            List<axSOObject> list = new List<axSOObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axSOObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    ));
            }

            axSOObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axSODetailObject[] getAllPendingAXSODetail()
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectAllPendingAXSODetail]", curQueryParameter);

            List<axSODetailObject> list = new List<axSODetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axSODetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    ));
            }

            axSODetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axSODetailObject[] getAXSODetailByAXRouteId(String argAXROUTEId)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)argAXROUTEId},
            };

            DataTable curDt = curDb.getDataTable("[selectAXSODetailByAXRouteId]", curQueryParameter);

            List<axSODetailObject> list = new List<axSODetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axSODetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    ));
            }

            axSODetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public String setAXSODetailScanInput(List<AXSODetailScanInput> aXSODetailScanInputList)
        {
            String isSuccess = "AX SOD Update failed";
            int queryResult = -99;
            database curDb = new database();
            curDb.openConnection();
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            String tempAXSO_ROUTEID = "";
            String tempAXSO_SALESID = "";
            try
            {
                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXROUTEId", ParameterType=SqlDbType.NVarChar,ParameterLength=10, keyData = (Object)aXSODetailScanInputList[0].argAXROUTEID}
                        };
                curDt = curDb.getDataTable("selectAXSOByAXRouteId", curQueryParameter);

                tempAXSO_ROUTEID = aXSODetailScanInputList[0].argAXROUTEID;
                tempAXSO_SALESID = curDt.Rows[0][3].ToString();

                foreach (var aXSODetailScanInput in aXSODetailScanInputList)
                {

                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(aXSODetailScanInput.argAXLINENUMBER)},
                        new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)aXSODetailScanInput.argAXROUTEID},
                        new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXSODetailScanInput.argAXITEMID},
                        new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXSODetailScanInput.argAXDOT_PARTCODE},
                    };

                    curDt = curDb.getDataTable("[selectAXSODetailByAXLineNum_RouteId_ItemId_PartCode]", curQueryParameter);

                    if (curDt.Rows.Count > 0)
                    {
                        String tempAXSOD_DATAAREAID = curDt.Rows[0][2].ToString();
                        String tempAXSOD_LINENUMBER = curDt.Rows[0][3].ToString();
                        String tempAXSOD_ROUTEID = curDt.Rows[0][4].ToString();
                        String tempAXSOD_INVENTTRANSID = curDt.Rows[0][5].ToString();
                        String tempAXSOD_ITEMID = curDt.Rows[0][6].ToString();
                        String tempAXSOD_DOT_PARTCODE = curDt.Rows[0][7].ToString();
                        String tempAXSOD_NAME = curDt.Rows[0][8].ToString();
                        String tempAXSOD_SALESUNIT = curDt.Rows[0][9].ToString();
                        String tempAXSOD_INVENTLOCATIONID = curDt.Rows[0][10].ToString();
                        String tempAXSOD_INVENTSITEID = curDt.Rows[0][11].ToString();
                        String tempAXSOD_WMSLOCATIONID = curDt.Rows[0][12].ToString();
                        String tempAXSOD_QTY = curDt.Rows[0][13].ToString();

                        if (Convert.ToDecimal(tempAXSOD_QTY) < Convert.ToDecimal(aXSODetailScanInput.argScanQuantity))
                        {
                            throw new Exception("Update Failed - " + aXSODetailScanInput.argAXITEMID +
                                " - Scan Quantity[" + aXSODetailScanInput.argScanQuantity + "] more then Quantity [" + tempAXSOD_QTY + "]");
                        }

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(aXSODetailScanInput.argAXLINENUMBER)},
                            new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)aXSODetailScanInput.argAXROUTEID},
                            new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXSODetailScanInput.argAXITEMID},
                            new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXSODetailScanInput.argAXDOT_PARTCODE},
                            new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Decimal, keyData =  Convert.ToDecimal(aXSODetailScanInput.argScanQuantity)},
                            new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData =  (Object)(aXSODetailScanInput.argScanDatetime)},
                            new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData =  (Object)aXSODetailScanInput.argRemarks},
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(aXSODetailScanInput.argUserId) },
                            new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)aXSODetailScanInput.argDeviceSN},
                            new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(9) },
                        };

                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXSODetailScanInputByAXLineNum_RouteId_ItemId_PartCode", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Update AX SOD Failed");
                        }

                        queryResult = insertMovementHistory(curDb, tempAXSOD_DATAAREAID, tempAXSOD_ROUTEID,
                            tempAXSO_SALESID, tempAXSOD_ITEMID, tempAXSOD_DOT_PARTCODE,
                            tempAXSOD_INVENTLOCATIONID, tempAXSOD_INVENTSITEID, tempAXSOD_WMSLOCATIONID,
                            "", "", "", aXSODetailScanInput.argScanQuantity, aXSODetailScanInput.argUserId, aXSODetailScanInput.argDeviceSN, sharedRes.LGC2AX_SO_DETAIL_UPDATE,
                            aXSODetailScanInput.argAXLINENUMBER);
                        if (queryResult == -99)
                        {
                            throw new Exception("insert movement history failed");
                        }


                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        //Comment this as Arrival Journal Only allow one submission per document
                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        //curQueryParameter = new List<sqlQueryParameter>() {
                        //    new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData =  (Object)argAXPURCHID },
                        //};

                        //curDt = curDb.getDataTable("[checkExistenceAXSODetailWRemainingQuantityByAXPurchId]", curQueryParameter);
                        ////No more pending line for this AX PO
                        //if (curDt.Rows.Count <= 0)
                        //{
                        curQueryParameter = new List<sqlQueryParameter>()
                            {
                                new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)aXSODetailScanInput.argAXROUTEID},
                                new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(9)},
                            };
                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXSOCompletedByAXRouteId", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Update AX Sales Order Detail Status Failed");
                        }
                        //}
                    }
                    else
                    {
                        //Temporarily skip for those line is not existed in WMS DB
                        //continue;
                        throw new Exception("Update Failed - " + aXSODetailScanInput.argAXITEMID + " does not found in SO - " + aXSODetailScanInput.argAXROUTEID);
                    }

                }

                String postResult = postAXSODetail2AX(curDb, tempAXSO_ROUTEID, tempAXSO_SALESID, aXSODetailScanInputList[0].argUserId, aXSODetailScanInputList[0].argDeviceSN);
                if (postResult != "true")
                    throw new Exception(postResult);

                isSuccess = "true";

            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        private String postAXSODetail2AX(database curDb, String argAXROUTEId, String argAXSALESID, String argUserId, String argDeviceSN)
        {
            String isSuccess = "Failed to post data to AX";
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            String xmlMessage = "";
            String messageId = getAXMessageId();
            String isDevelopmentMode = "0";
            try
            {
                xmlMessage = "<?xml version =\"1.0\" encoding=\"UTF-8\"?>\r\n" +
                    "<SOPick>\r\n";
                int counter = 0;

                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)argAXROUTEId},
                };

                curDt = curDb.getDataTable("[selectAXSODetailByAXRouteId]", curQueryParameter);

                List<axTODetailObject> list = new List<axTODetailObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    String tempAXSOD_DATAAREAID = curDt.Rows[i][2].ToString();
                    String tempAXSOD_LINENUMBER = curDt.Rows[i][3].ToString();
                    String tempAXSOD_ROUTEID = curDt.Rows[i][4].ToString();
                    String tempAXSOD_INVENTTRANSID = curDt.Rows[i][5].ToString();
                    String tempAXSOD_ITEMID = curDt.Rows[i][6].ToString();
                    String tempAXSOD_DOT_PARTCODE = curDt.Rows[i][7].ToString();
                    String tempAXSOD_NAME = curDt.Rows[i][8].ToString();
                    String tempAXSOD_SALESUNIT = curDt.Rows[i][9].ToString();
                    String tempAXSOD_INVENTLOCATIONID = curDt.Rows[i][10].ToString();
                    String tempAXSOD_INVENTSITEID = curDt.Rows[i][11].ToString();
                    String tempAXSOD_WMSLOCATIONID = curDt.Rows[i][12].ToString();
                    String tempAXSOD_QTY = curDt.Rows[i][13].ToString();
                    String tempAXSOD_scanQuantity = curDt.Rows[i][14].ToString();

                    if (counter == 0)
                    {
                        xmlMessage += "<SOPickHeader>\r\n" +
                                         "<Company_Code>" + tempAXSOD_DATAAREAID + "</Company_Code>\r\n" +
                                         "<Sales_ID>" + argAXSALESID + "</Sales_ID>\r\n" +
                                         "<Picking_NO>" + argAXROUTEId + "</Picking_NO>\r\n" +
                                         "<Cust_Code></Cust_Code>\r\n" +
                                         "</SOPickHeader>\r\n";
                    }
                    counter++;

                    if (!(String.IsNullOrEmpty(tempAXSOD_scanQuantity)))
                    {
                        xmlMessage += "<SOPick_DETAIL>\r\n" +
                        "<Company_Code>" + tempAXSOD_DATAAREAID + "</Company_Code>\r\n" +
                        "<LineNumber>" + tempAXSOD_LINENUMBER + "</LineNumber>\r\n" +
                        "<ITEM_CODE>" + tempAXSOD_ITEMID + "</ITEM_CODE>\r\n" +
                        "<PART_CODE>" + tempAXSOD_DOT_PARTCODE + "</PART_CODE>\r\n" +
                        "<QTY>" + (String.IsNullOrEmpty(tempAXSOD_scanQuantity) ? "0" : tempAXSOD_scanQuantity) + "</QTY>\r\n" +
                        "<Sales_UOM>" + tempAXSOD_SALESUNIT + "</Sales_UOM>\r\n" +
                        "<Warehouse_Code>" + tempAXSOD_INVENTLOCATIONID + "</Warehouse_Code>\r\n" +
                        "<Location_Code>" + tempAXSOD_WMSLOCATIONID + "</Location_Code>\r\n" +
                        "<Invent_Trans_Id>" + tempAXSOD_INVENTTRANSID + "</Invent_Trans_Id>\r\n" +
                        "</SOPick_DETAIL>\r\n";
                    }

                }
                xmlMessage += "</SOPick>";


                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"DEVELOPMENT MODE"},
                };

                curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                isDevelopmentMode = curDt.Rows[0][2].ToString();

                if (isDevelopmentMode == "0")
                {
                    SOPostService.WMS_PickingListServiceClient soPostObject = new SOPostService.WMS_PickingListServiceClient();

                    SOPostService.CallContext context = new SOPostService.CallContext();
                    context.MessageId = messageId; // "{33000000-0000-0000-0000-000000000012}";

                    String result = "";
                    try
                    {
                        result = soPostObject.ReadXMLFile(context, xmlMessage);
                    }
                    catch (Exception axEx)
                    {
                        throw new Exception("Failed to post data to AX - " + axEx.Message + " " + messageId);
                    }

                    Boolean isAXPostSuccess = false;
                    if (!String.IsNullOrEmpty(result))
                    {
                        string[] words = result.Split(' ');
                        if (words[0].Equals("Successfully", StringComparison.OrdinalIgnoreCase))
                        {
                            isAXPostSuccess = true;
                        }
                    }

                    if (!isAXPostSuccess)
                    {
                        throw new Exception("Failed to post data to AX - " + result + " " + messageId);
                        //String axExceptionMsg = retrieveAXExceptionMsg(messageId);
                        //if (axExceptionMsg == "")
                        //{
                        //    throw new Exception("Failed to post data to AX - " + result);
                        //}
                        //else
                        //{
                        //    throw new Exception("Failed to post data to AX - " + axExceptionMsg);
                        //}
                    }
                }
                //else
                //{
                //    ////Testing Purpose
                //    string pathString = XML_FOLDER_PATH;

                //    bool exists = System.IO.Directory.Exists(pathString);

                //    if (!exists)
                //        System.IO.Directory.CreateDirectory(pathString);

                //    string fileName = "SO_" + getAXMessageId() + ".xml";
                //    pathString = System.IO.Path.Combine(pathString, fileName);

                //    if (!System.IO.File.Exists(pathString))
                //    {
                //        using (System.IO.StreamWriter file =
                //            new System.IO.StreamWriter(pathString, true))
                //        {
                //            file.WriteLine(xmlMessage);
                //        }
                //    }
                //    throw new Exception("XML Generated");
                //}


                isSuccess = "true";

            }
            catch (Exception e)
            {
                insertExceptionMsg(argAXROUTEId, argUserId, argDeviceSN, "SO", e.Message, messageId);
                isSuccess = e.Message;
                try
                {
                    ////Testing Purpose
                    string pathString = XML_FOLDER_PATH;

                    pathString = System.IO.Path.Combine(pathString, "SO");
                    bool exists = System.IO.Directory.Exists(pathString);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(pathString);

                    string fileName = "SO_" + messageId + ".xml";
                    pathString = System.IO.Path.Combine(pathString, fileName);

                    if (!System.IO.File.Exists(pathString))
                    {
                        using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(pathString, true))
                        {
                            file.WriteLine(xmlMessage);
                        }
                    }
                }
                catch (Exception e2) { }
            }
            return isSuccess;
        }

        //Integration - AX TO
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public String retrieveAllAXTransferOrder()
        {
            String isSuccess = "Failed Retrieve AX Transfer Order";

            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            DataTable curDt;
            int queryResult = -99;
            String lastSyncDatetime = "";
            String currentSyncDatetime = getLastSyncDatetime();

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST TO SYNC"},
                    };


                    curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                    lastSyncDatetime = curDt.Rows[0][2].ToString();
                    if (String.IsNullOrEmpty(lastSyncDatetime))
                        lastSyncDatetime = "1999-01-01 00:00:00";

                    axConn.Open();

                    //++++++++++++++++++++++++++++++
                    //Select PO with modified data
                    //++++++++++++++++++++++++++++++
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT DISTINCT [AX_WMS_INVENTTRANSFERTABLE].[DATAAREAID] " +
                          ",[AX_WMS_INVENTTRANSFERTABLE].[MODIFIEDDATETIME] " +
                          ",[AX_WMS_INVENTTRANSFERTABLE].[PICKINGROUTEID] " +
                          ",[AX_WMS_INVENTTRANSFERTABLE].[TRANSFERID] " +
                          ",[AX_WMS_INVENTTRANSFERTABLE].[DocumentType] " +
                          ",[AX_WMS_INVENTTRANSFERTABLE].[INVENTLOCATIONIDFROM] " +
                          ",[AX_WMS_INVENTTRANSFERTABLE].[INVENTLOCATIONIDTO] " +
                          ",[AX_WMS_INVENTTRANSFERTABLE].[TRANSFERSTATUS] " +
                          "FROM[AX_WMS_INVENTTRANSFERTABLE] " +
                          "JOIN AX_WMS_INVENTTRANSFERLINE ON AX_WMS_INVENTTRANSFERLINE.ROUTEID = AX_WMS_INVENTTRANSFERTABLE.PICKINGROUTEID " +
                        "WHERE AX_WMS_INVENTTRANSFERLINE.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX TO from AX DB View
                    DataTable axTODt = new DataTable();

                    int noOfAXTO = sda.Fill(axTODt);

                    Int64 axtoId = -1;
                    //Loop through all AXTO from AX DB View
                    for (int i = 0; i < noOfAXTO; i++)
                    {
                        String tempAXTO_DATAAREAID = axTODt.Rows[i][0].ToString();
                        String tempAXTO_MODIFIEDDATETIME = axTODt.Rows[i][1].ToString();
                        String tempAXTO_PICKINGROUTEID = axTODt.Rows[i][2].ToString();
                        String tempAXTO_TRANSFERID = axTODt.Rows[i][3].ToString();
                        String tempAXTO_DOCUMENTTYPE = axTODt.Rows[i][4].ToString();
                        String tempAXTO_INVENTLOCATIONIDFROM = axTODt.Rows[i][5].ToString();
                        String tempAXTO_INVENTLOCATIONIDTO = axTODt.Rows[i][6].ToString();
                        String tempAXTO_TRANSFERSTATUS = axTODt.Rows[i][7].ToString();

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar,ParameterLength=10, keyData = (Object)tempAXTO_PICKINGROUTEID}
                        };
                        curDt = curDb.getDataTable("selectAXTOByAXRouteId", curQueryParameter);

                        //++++++++++++++++++++++++++++++
                        //If AX TO Existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        if (curDt.Rows.Count > 0)
                        {
                            axtoId = Convert.ToInt64(curDt.Rows[0][0].ToString());

                            //Get all AX TO Detail By Route_ID from AX DB View
                            axCommand = new SqlCommand(
                            "SELECT DISTINCT [DATAAREAID] " +
                              ",[ROUTEID] " +
                              ",[TRANSFERID] " +
                              ",[INVENTTRANSID] " +
                              ",[ITEMID] " +
                              ",[PRODUCT_NAME] " +
                              ",[DOT_PARTCODE] " +
                              ",[UNITID] " +
                              ",[INVENTLOCATIONID] " +
                              ",[INVENTSITEID] " +
                              ",[QTY] " +
                              ",[docType] " +
                              ",[WMSLOCATIONID] " +
                              ",[NAMEALIAS] " +
                              "FROM [AX_WMS_INVENTTRANSFERLINE] " +
                              "WHERE [ROUTEID] = @keyAXROUTEID " +
                              "AND AX_WMS_INVENTTRANSFERLINE.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXROUTEID", tempAXTO_PICKINGROUTEID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axTODetailDt = new DataTable();
                            //AX tO Detail in AX DB View
                            sda.Fill(axTODetailDt);

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTO_PICKINGROUTEID},
                            };

                            DataTable curAXTODetailDt = curDb.getDataTable("[selectAXTODetailByAXRouteId]", curQueryParameter);

                            List<axTODetailObject> list = new List<axTODetailObject>();
                            for (int j = 0; j < axTODetailDt.Rows.Count; j++)
                            {
                                String tempAXTOD_DATAAREAID = axTODetailDt.Rows[j][0].ToString();
                                String tempAXTOD_ROUTEID = axTODetailDt.Rows[j][1].ToString();
                                String tempAXTOD_TRANSFERID = axTODetailDt.Rows[j][2].ToString();
                                String tempAXTOD_INVENTTRANSID = axTODetailDt.Rows[j][3].ToString();
                                String tempAXTOD_ITEMID = axTODetailDt.Rows[j][4].ToString();
                                String tempAXTOD_NAME = axTODetailDt.Rows[j][5].ToString();
                                String tempAXTOD_DOT_PARTCODE = axTODetailDt.Rows[j][6].ToString();
                                String tempAXTOD_UNITID = axTODetailDt.Rows[j][7].ToString();
                                String tempAXTOD_INVENTLOCATIONID = axTODetailDt.Rows[j][8].ToString();
                                String tempAXTOD_INVENTSITEID = axTODetailDt.Rows[j][9].ToString();
                                String tempAXTOD_QTY = axTODetailDt.Rows[j][10].ToString();
                                String tempAXTOD_DOCTYPE = axTODetailDt.Rows[j][11].ToString();
                                String tempAXTOD_WMSLOCATIONID = axTODetailDt.Rows[j][12].ToString();
                                String tempAXTOD_NAMEALIAS = axTODetailDt.Rows[j][13].ToString();

                                Boolean toDetailExisted = false;
                                for (int k = 0; k < curAXTODetailDt.Rows.Count; k++)
                                {

                                    String tempWMSTOD_id = curAXTODetailDt.Rows[k][0].ToString();
                                    String tempWMSTOD_INVENTTRANSID = curAXTODetailDt.Rows[k][5].ToString();
                                    String tempWMSTOD_ITEMID = curAXTODetailDt.Rows[k][6].ToString();
                                    String tempWMSTOD_DOT_PARTCODE = curAXTODetailDt.Rows[k][7].ToString();
                                    String tempWMSTOD_scanQuantity = String.IsNullOrEmpty(curAXTODetailDt.Rows[k][14].ToString()) ? "0" : curAXTODetailDt.Rows[k][14].ToString();

                                    //If AX TO Detail Existed in WMS DB
                                    if (tempAXTOD_ITEMID.Trim() == tempWMSTOD_ITEMID.Trim() &&
                                        tempAXTOD_DOT_PARTCODE.Trim() == tempWMSTOD_DOT_PARTCODE.Trim() &&
                                        tempAXTOD_INVENTTRANSID.Trim() == tempWMSTOD_INVENTTRANSID.Trim())
                                    {
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\
                                        //Update quantity changes
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                        queryResult = setAXTODetail(curDb, tempAXTOD_DATAAREAID, tempAXTOD_ROUTEID, tempAXTOD_TRANSFERID,
                                        tempAXTOD_INVENTTRANSID, tempAXTOD_ITEMID, tempAXTOD_DOT_PARTCODE, tempAXTOD_NAME,
                                        tempAXTOD_UNITID, tempAXTOD_INVENTLOCATIONID,
                                        tempAXTOD_INVENTSITEID, tempAXTOD_QTY, tempAXTOD_DOCTYPE, tempAXTOD_WMSLOCATIONID,
                                        tempAXTOD_NAMEALIAS);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("update ax to detail failed");
                                        }

                                        Int64 curStatusId = 9;
                                        if (Convert.ToDecimal(tempWMSTOD_scanQuantity) == 0)
                                        {
                                            curStatusId = 1;
                                        }
                                        else if (Convert.ToDecimal(tempAXTOD_QTY) > Convert.ToDecimal(tempWMSTOD_scanQuantity))
                                        {
                                            curStatusId = 7;
                                        }

                                        curQueryParameter = new List<sqlQueryParameter>()
                                        {
                                                new sqlQueryParameter {strParameter="@keyTableName", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)"tbl_ax_toDetail"},
                                                new sqlQueryParameter {strParameter="@keyFieldName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"tbl_status_Id"},
                                                new sqlQueryParameter {strParameter="@keyFieldValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)curStatusId.ToString()},
                                                new sqlQueryParameter {strParameter="@keyIDName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"id"},
                                                new sqlQueryParameter {strParameter="@keyIDValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)tempWMSTOD_id}
                                            };
                                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateTableSingleField", curQueryParameter);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("Update AX Transfer Order Detail Status Failed");
                                        }

                                        queryResult = insertMovementHistory(curDb, tempAXTO_DATAAREAID, tempAXTO_PICKINGROUTEID, tempAXTO_TRANSFERID, tempAXTOD_ITEMID, tempAXTOD_DOT_PARTCODE,
                                            tempAXTO_INVENTLOCATIONIDFROM, tempAXTOD_INVENTSITEID, "", tempAXTO_INVENTLOCATIONIDTO, "", "", tempAXTOD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_TO_DETAIL_UPDATE, "");
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("insert movement history failed");
                                        }

                                        toDetailExisted = true;
                                        break;
                                    }
                                }

                                //if AX TO Detail no existed in WMS DB
                                if (!toDetailExisted)
                                {
                                    queryResult = insertAXTODetail(curDb, axtoId.ToString(), tempAXTOD_DATAAREAID, tempAXTOD_ROUTEID, tempAXTOD_TRANSFERID,
                                    tempAXTOD_INVENTTRANSID, tempAXTOD_ITEMID, tempAXTOD_DOT_PARTCODE, tempAXTOD_NAME,
                                    tempAXTOD_UNITID, tempAXTOD_INVENTLOCATIONID,
                                    tempAXTOD_INVENTSITEID, tempAXTOD_QTY, tempAXTOD_DOCTYPE, tempAXTOD_WMSLOCATIONID,
                                    tempAXTOD_NAMEALIAS);
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert ax to detail failed");
                                    }

                                    queryResult = insertMovementHistory(curDb, tempAXTO_DATAAREAID, tempAXTO_PICKINGROUTEID, tempAXTO_TRANSFERID, tempAXTOD_ITEMID, tempAXTOD_DOT_PARTCODE,
                                        tempAXTO_INVENTLOCATIONIDFROM, tempAXTOD_INVENTSITEID, "", tempAXTO_INVENTLOCATIONIDTO, "", "", tempAXTOD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_TO_DETAIL_INSERT, "");
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert movement history failed");
                                    }

                                }

                            }
                        }
                        //++++++++++++++++++++++++++++++
                        //if AX TO no existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        else
                        {
                            //Get all AX TO Detail By Route_ID from AX DB View
                            axCommand = new SqlCommand(
                            "SELECT DISTINCT [DATAAREAID] " +
                              ",[ROUTEID] " +
                              ",[TRANSFERID] " +
                              ",[INVENTTRANSID] " +
                              ",[ITEMID] " +
                              ",[PRODUCT_NAME] " +
                              ",[DOT_PARTCODE] " +
                              ",[UNITID] " +
                              ",[INVENTLOCATIONID] " +
                              ",[INVENTSITEID] " +
                              ",[QTY] " +
                              ",[docType] " +
                              ",[WMSLOCATIONID] " +
                              ",[NAMEALIAS] " +
                              "FROM [AX_WMS_INVENTTRANSFERLINE] " +
                              "WHERE [ROUTEID] = @keyAXROUTEID", axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXROUTEID", tempAXTO_PICKINGROUTEID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axTODetailDt = new DataTable();
                            //AX tO Detail in AX DB View
                            sda.Fill(axTODetailDt);

                            curQueryParameter = new List<sqlQueryParameter>()
                            {
                            };

                            curDt = curDb.getDataTable("[selectNextAXTOId]", curQueryParameter);

                            List<Int64> list = new List<Int64>();
                            for (int j = 0; j < curDt.Rows.Count; j++)
                            {
                                list.Add(Convert.ToInt64(curDt.Rows[j][0]));
                            }
                            axtoId = list[0];

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXTO_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axtoId)},
                                new sqlQueryParameter {strParameter="@keyAXTO_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXTO_DATAAREAID},
                                new sqlQueryParameter {strParameter="@keyAXTO_MODIFIEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXTO_MODIFIEDDATETIME},
                                new sqlQueryParameter {strParameter="@keyAXTO_ROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTO_PICKINGROUTEID},
                                new sqlQueryParameter {strParameter="@keyAXTO_TRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTO_TRANSFERID},
                                new sqlQueryParameter {strParameter="@keyAXTO_DOCUMENTTYPE", ParameterType=SqlDbType.VarChar, ParameterLength=2, keyData = (Object)tempAXTO_DOCUMENTTYPE},
                                new sqlQueryParameter {strParameter="@keyAXTO_INVENTLOCATIONIDFROM", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTO_INVENTLOCATIONIDFROM},
                                new sqlQueryParameter {strParameter="@keyAXTO_INVENTLOCATIONIDTO", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTO_INVENTLOCATIONIDTO},
                                new sqlQueryParameter {strParameter="@keyAXTO_TRANSFERSTATUS", ParameterType=SqlDbType.Int, keyData = (Object)tempAXTO_TRANSFERSTATUS},
                                new sqlQueryParameter {strParameter="@keyReferenceNo", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)""},
                                new sqlQueryParameter {strParameter="@keyCreationDateTime", ParameterType=SqlDbType.DateTime, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyDocType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"tp"},
                                new sqlQueryParameter {strParameter="@keyReferenceVersion", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                            };
                            queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXTO", curQueryParameter);
                            if (queryResult == -99)
                            {
                                throw new Exception("insert ax transfer order failed");
                            }

                            queryResult = insertMovementHistory(curDb, tempAXTO_DATAAREAID, tempAXTO_PICKINGROUTEID, tempAXTO_TRANSFERID, "", "", tempAXTO_INVENTLOCATIONIDFROM, "", "", tempAXTO_INVENTLOCATIONIDTO, "", "", "", "-1", "SYSTEM", sharedRes.AX2LGC_TO_INSERT, "");
                            if (queryResult == -99)
                            {
                                throw new Exception("insert movement history failed");
                            }

                            //Loop through each of the AXTO Detail
                            for (int j = 0; j < axTODetailDt.Rows.Count; j++)
                            {
                                String tempAXTOD_DATAAREAID = axTODetailDt.Rows[j][0].ToString();
                                String tempAXTOD_ROUTEID = axTODetailDt.Rows[j][1].ToString();
                                String tempAXTOD_TRANSFERID = axTODetailDt.Rows[j][2].ToString();
                                String tempAXTOD_INVENTTRANSID = axTODetailDt.Rows[j][3].ToString();
                                String tempAXTOD_ITEMID = axTODetailDt.Rows[j][4].ToString();
                                String tempAXTOD_NAME = axTODetailDt.Rows[j][5].ToString();
                                String tempAXTOD_DOT_PARTCODE = axTODetailDt.Rows[j][6].ToString();
                                String tempAXTOD_UNITID = axTODetailDt.Rows[j][7].ToString();
                                String tempAXTOD_INVENTLOCATIONID = axTODetailDt.Rows[j][8].ToString();
                                String tempAXTOD_INVENTSITEID = axTODetailDt.Rows[j][9].ToString();
                                String tempAXTOD_QTY = axTODetailDt.Rows[j][10].ToString();
                                String tempAXTOD_DOCTYPE = axTODetailDt.Rows[j][11].ToString();
                                String tempAXTOD_WMSLOCATIONID = axTODetailDt.Rows[j][12].ToString();
                                String tempAXTOD_NAMEALIAS = axTODetailDt.Rows[j][13].ToString();

                                queryResult = insertAXTODetail(curDb, axtoId.ToString(), tempAXTOD_DATAAREAID, tempAXTOD_ROUTEID, tempAXTOD_TRANSFERID,
                                    tempAXTOD_INVENTTRANSID, tempAXTOD_ITEMID, tempAXTOD_DOT_PARTCODE, tempAXTOD_NAME,
                                    tempAXTOD_UNITID, tempAXTOD_INVENTLOCATIONID,
                                    tempAXTOD_INVENTSITEID, tempAXTOD_QTY, tempAXTOD_DOCTYPE, tempAXTOD_WMSLOCATIONID,
                                    tempAXTOD_NAMEALIAS);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert ax to detail failed");
                                }

                                queryResult = insertMovementHistory(curDb, tempAXTO_DATAAREAID, tempAXTO_PICKINGROUTEID, tempAXTO_TRANSFERID, tempAXTOD_ITEMID, tempAXTOD_DOT_PARTCODE,
                                    tempAXTO_INVENTLOCATIONIDFROM, tempAXTOD_INVENTSITEID, "", tempAXTO_INVENTLOCATIONIDTO, "", "", tempAXTOD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_TO_DETAIL_INSERT, "");
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert movement history failed");
                                }
                            }
                        }
                    }


                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST TO SYNC"},
                        new sqlQueryParameter {strParameter="@keyDataValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)currentSyncDatetime}
                    };

                    queryResult = curDb.insertUpdateDeleteDataIntoTable("updateSystemData", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert/update/delete failed");
                    }
                    
                    isSuccess = "true";
                }
                catch (Exception e)
                {
                    curDb.rollbackNCloseConnection();
                    isSuccess = e.Message;
                    return isSuccess;
                }
                if (!curDb.checkIsConnectionClosed())
                    curDb.closeConnection();
                return isSuccess;
            }
        }

        private int insertAXTODetail(database curDb, String axtoId, String tempAXTOD_DATAAREAID,
            String tempAXTOD_ROUTEID, String tempAXTOD_TRANSFERID, String tempAXTOD_INVENTTRANSID,
            String tempAXTOD_ITEMID, String tempAXTOD_DOT_PARTCODE, String tempAXTOD_NAME,
            String tempAXTOD_UNITID, String tempAXTOD_INVENTLOCATIONID, String tempAXTOD_INVENTSITEID,
            String tempAXTOD_QTY, String tempAXTOD_DOCTYPE, String tempAXTOD_WMSLOCATIONID,
            String tempAXTOD_NAMEALIAS)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };

                DataTable curDt = curDb.getDataTable("[selectNextAXTODetailId]", curQueryParameter);

                List<Int64> list = new List<Int64>();
                for (int k = 0; k < curDt.Rows.Count; k++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[k][0]));
                }
                Int64 axtodId = list[0];

                curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXTOD_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axtodId)},
                                    new sqlQueryParameter {strParameter="@keyAXTO_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axtoId)},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXTOD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_ROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTOD_ROUTEID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_TRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTOD_TRANSFERID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_INVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTOD_INVENTTRANSID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXTOD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXTOD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_NAME", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXTOD_NAME},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_UNITID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTOD_UNITID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTOD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTOD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_QTY", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXTOD_QTY)},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_DOCTYPE", ParameterType=SqlDbType.NVarChar, ParameterLength=6, keyData = (Object)tempAXTOD_DOCTYPE},
                                    new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Int, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                    new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXTOD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_NAMEALIAS", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTOD_NAMEALIAS},
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXTODetail", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }

        private int setAXTODetail(database curDb, String tempAXTOD_DATAAREAID,
            String tempAXTOD_ROUTEID, String tempAXTOD_TRANSFERID, String tempAXTOD_INVENTTRANSID,
            String tempAXTOD_ITEMID, String tempAXTOD_DOT_PARTCODE, String tempAXTOD_NAME,
            String tempAXTOD_UNITID, String tempAXTOD_INVENTLOCATIONID, String tempAXTOD_INVENTSITEID,
            String tempAXTOD_QTY, String tempAXTOD_DOCTYPE, String tempAXTOD_WMSLOCATIONID,
            String tempAXTOD_NAMEALIAS)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXTOD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXTOD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_ROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTOD_ROUTEID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_TRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTOD_TRANSFERID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_INVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTOD_INVENTTRANSID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXTOD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXTOD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_NAME", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXTOD_NAME},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_UNITID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTOD_UNITID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTOD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTOD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_QTY", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXTOD_QTY)},
                                    new sqlQueryParameter {strParameter="@keyAXTOD_DOCTYPE", ParameterType=SqlDbType.NVarChar, ParameterLength=6, keyData = (Object)tempAXTOD_DOCTYPE}
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXTODetailByAXRouteId_TransferId_InventTransId_ItemId_PartCode", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }

        //AX Transfer Order
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public void getAllAXTPInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];

            String searchHeader = "";
            String searchContent = "";
            String searchContent2 = "";
            String searchType = HttpContext.Current.Request.QueryString["filterType"];
            String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');
            String warehouseCode = HttpContext.Current.Request.QueryString["warehouseCode"].ToString();

            searchHeader = searchArray[0];
            if (searchArray.Length == 3)
            {
                searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
                searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
            }
            else
            {
                searchContent = searchArray[1] == "" ? "%" : searchArray[1];
            }

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchType},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent},
                new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent2},
                new sqlQueryParameter {strParameter="@keySearchWarehouseCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)warehouseCode}
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllAXTPInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");

        }

        //[WebMethod]
        //public axTOObject[] getAllAvailableAXTO(String argToken, String argLocationId, String argDocType)
        //{

        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
        //    {
        //        new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argLocationId)}
        //        //new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(Session["curSessionWarehouseId"])}
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectAllAvailableAXTO]", curQueryParameter);

        //    List<axTOObject> list = new List<axTOObject>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new axTOObject(curDt.Rows[i][0].ToString()
        //            , curDt.Rows[i][1].ToString()
        //            , curDt.Rows[i][2].ToString()
        //            , curDt.Rows[i][3].ToString()
        //            , curDt.Rows[i][4].ToString()
        //            , curDt.Rows[i][5].ToString()
        //            , curDt.Rows[i][6].ToString()
        //            , curDt.Rows[i][7].ToString()
        //            , curDt.Rows[i][8].ToString()
        //            , curDt.Rows[i][9].ToString()
        //            ));
        //    }

        //    axTOObject[] curList = list.ToArray();
        //    if (!curDb.checkIsConnectionClosed())
        //        curDb.closeConnection();

        //    return curList;
        //}

        //[WebMethod]
        //public axTOObject[] getAllAvailableAXTOWServiceStore(String argToken, String argLocationId, String argDocType)
        //{

        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
        //    {
        //        new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argLocationId)}
        //        //new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(Session["curSessionWarehouseId"])}
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectAllAvailableAXTOWServiceStore]", curQueryParameter);

        //    List<axTOObject> list = new List<axTOObject>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new axTOObject(curDt.Rows[i][0].ToString()
        //            , curDt.Rows[i][1].ToString()
        //            , curDt.Rows[i][2].ToString()
        //            , curDt.Rows[i][3].ToString()
        //            , curDt.Rows[i][4].ToString()
        //            , curDt.Rows[i][5].ToString()
        //            , curDt.Rows[i][6].ToString()
        //            , curDt.Rows[i][7].ToString()
        //            , curDt.Rows[i][8].ToString()
        //            , curDt.Rows[i][9].ToString()
        //            ));
        //    }

        //    axTOObject[] curList = list.ToArray();
        //    if (!curDb.checkIsConnectionClosed())
        //        curDb.closeConnection();

        //    return curList;
        //}

        //[WebMethod(EnableSession = true)]
        //public axTOObject[] getAllPickableAXTO(String argToken, String argLocationId, String argDocType)
        //{

        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
        //    {
        //        new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argLocationId)}
        //        //new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(Session["curSessionWarehouseId"])}
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectAllPickableAXTO]", curQueryParameter);

        //    List<axTOObject> list = new List<axTOObject>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new axTOObject(curDt.Rows[i][0].ToString()
        //            , curDt.Rows[i][1].ToString()
        //            , curDt.Rows[i][2].ToString()
        //            , curDt.Rows[i][3].ToString()
        //            , curDt.Rows[i][4].ToString()
        //            , curDt.Rows[i][5].ToString()
        //            , curDt.Rows[i][6].ToString()
        //            , curDt.Rows[i][7].ToString()
        //            , curDt.Rows[i][8].ToString()
        //            , curDt.Rows[i][9].ToString()
        //            ));
        //    }

        //    axTOObject[] curList = list.ToArray();
        //    if (!curDb.checkIsConnectionClosed())
        //        curDb.closeConnection();

        //    return curList;
        //}

        [WebMethod]
        public axTOObject[] getAllPendingAXTO()
        {

            axTOObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            try
            {
                if (!(retrieveAllAXTransferOrder() == "true"))
                    throw new Exception("Failed to retrieve AX Transfer Order");

                DataTable curDt = curDb.getDataTable("[selectAllPendingAXTO]", curQueryParameter);

                List<axTOObject> list = new List<axTOObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axTOObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        , curDt.Rows[i][4].ToString()
                        , curDt.Rows[i][5].ToString()
                        , curDt.Rows[i][6].ToString()
                        , curDt.Rows[i][7].ToString()
                        , curDt.Rows[i][8].ToString()
                        , curDt.Rows[i][9].ToString()
                        , curDt.Rows[i][10].ToString()
                        , curDt.Rows[i][11].ToString()
                        , curDt.Rows[i][12].ToString()
                        , curDt.Rows[i][13].ToString()
                        , curDt.Rows[i][14].ToString()
                        , curDt.Rows[i][15].ToString()
                        , curDt.Rows[i][16].ToString()
                        , curDt.Rows[i][17].ToString()
                        , curDt.Rows[i][18].ToString()
                        ));
                }

                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axTOObject[] getAXTOByAXRouteId(String argAXROUTEId)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXROUTEId", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)argAXROUTEId},
            };

            DataTable curDt = curDb.getDataTable("[selectAXTOByAXRouteId]", curQueryParameter);

            List<axTOObject> list = new List<axTOObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axTOObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    ));
            }

            axTOObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axTODetailObject[] getAllPendingAXTODetail()
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectAllPendingAXTODetail]", curQueryParameter);

            List<axTODetailObject> list = new List<axTODetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axTODetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    , curDt.Rows[i][21].ToString()
                    , curDt.Rows[i][22].ToString()
                    ));
            }

            axTODetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axTODetailObject[] getAXTODetailByAXRouteId(String argAXROUTEId)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)argAXROUTEId},
            };

            DataTable curDt = curDb.getDataTable("[selectAXTODetailByAXRouteId]", curQueryParameter);

            List<axTODetailObject> list = new List<axTODetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axTODetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    , curDt.Rows[i][21].ToString()
                    , curDt.Rows[i][22].ToString()
                    ));
            }

            axTODetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public String setAXTODetailScanInput(List<AXTODetailScanInput> aXTODetailScanInputList)
        {
            String isSuccess = "AX TOD Update failed";
            int queryResult = -99;
            database curDb = new database();
            curDb.openConnection();
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            try
            {

                String tempAXTO_ROUTEID = "";
                String tempAXTO_INVENTLOCATIONIDFROM = "";
                String tempAXTO_INVENTLOCATIONIDTO = "";

                foreach (var aXTODetailScanInput in aXTODetailScanInputList)
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)aXTODetailScanInput.argAXROUTEID},
                        new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXTODetailScanInput.argAXTRANSFERID},
                        new sqlQueryParameter {strParameter="@keyAXINVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXTODetailScanInput.argAXINVENTTRANSID},
                        new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXTODetailScanInput.argAXITEMID},
                        new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXTODetailScanInput.argAXDOT_PARTCODE},
                    };

                    curDt = curDb.getDataTable("[selectAXTODetailByAXRouteId_TransferId_InventTransId_ItemId_PartCode]", curQueryParameter);

                    if (curDt.Rows.Count > 0)
                    {
                        String tempAXTOD_DATAAREAID = curDt.Rows[0][2].ToString();
                        String tempAXTOD_ROUTEID = curDt.Rows[0][3].ToString();
                        String tempAXTOD_TRANSFERID = curDt.Rows[0][4].ToString();
                        String tempAXTOD_INVENTTRANSID = curDt.Rows[0][5].ToString();
                        String tempAXTOD_ITEMID = curDt.Rows[0][6].ToString();
                        String tempAXTOD_DOT_PARTCODE = curDt.Rows[0][7].ToString();
                        String tempAXTOD_NAME = curDt.Rows[0][8].ToString();
                        String tempAXTOD_UNITID = curDt.Rows[0][9].ToString();
                        String tempAXTOD_INVENTLOCATIONID = curDt.Rows[0][10].ToString();
                        String tempAXTOD_INVENTSITEID = curDt.Rows[0][11].ToString();
                        String tempAXTOD_QTY = curDt.Rows[0][12].ToString();
                        String tempAXTOD_DOCTYPE = curDt.Rows[0][13].ToString();

                        if (Convert.ToDecimal(tempAXTOD_QTY) < Convert.ToDecimal(aXTODetailScanInput.argScanQuantity))
                        {
                            throw new Exception("Update Failed - " + aXTODetailScanInput.argAXITEMID +
                                " - Scan Quantity[" + aXTODetailScanInput.argScanQuantity + "] more then Quantity [" + tempAXTOD_QTY + "]");
                        }

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)aXTODetailScanInput.argAXROUTEID},
                            new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXTODetailScanInput.argAXTRANSFERID},
                            new sqlQueryParameter {strParameter="@keyAXINVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXTODetailScanInput.argAXINVENTTRANSID},
                            new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXTODetailScanInput.argAXITEMID},
                            new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXTODetailScanInput.argAXDOT_PARTCODE},
                            new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Decimal, keyData =  Convert.ToDecimal(aXTODetailScanInput.argScanQuantity)},
                            new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData =  (Object)(aXTODetailScanInput.argScanDatetime)},
                            new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData =  (Object)aXTODetailScanInput.argRemarks},
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(aXTODetailScanInput.argUserId) },
                            new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)aXTODetailScanInput.argDeviceSN},
                            new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(9) },
                        };

                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXTODetailScanInputByAXRouteId_TransferId_InventTransId_ItemId_PartCode", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Update AX TOD Failed");
                        }

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar,ParameterLength=10, keyData = (Object)aXTODetailScanInput.argAXROUTEID}
                        };
                        curDt = curDb.getDataTable("selectAXTOByAXRouteId", curQueryParameter);
                        if (curDt.Rows.Count > 0)
                        {
                            tempAXTO_ROUTEID = curDt.Rows[0][3].ToString();
                            tempAXTO_INVENTLOCATIONIDFROM = curDt.Rows[0][6].ToString();
                            tempAXTO_INVENTLOCATIONIDTO = curDt.Rows[0][7].ToString();
                        }

                        queryResult = insertMovementHistory(curDb, tempAXTOD_DATAAREAID, tempAXTOD_ROUTEID,
                            aXTODetailScanInput.argAXTRANSFERID, tempAXTOD_ITEMID, tempAXTOD_DOT_PARTCODE,
                            tempAXTO_INVENTLOCATIONIDFROM, tempAXTOD_INVENTSITEID, "",
                            tempAXTO_INVENTLOCATIONIDTO, "", "", aXTODetailScanInput.argScanQuantity,
                            aXTODetailScanInput.argUserId,
                            aXTODetailScanInput.argDeviceSN, sharedRes.LGC2AX_TO_DETAIL_UPDATE, "");
                        if (queryResult == -99)
                        {
                            throw new Exception("insert movement history failed");
                        }


                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        //Comment this as Arrival Journal Only allow one submission per document
                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        //curQueryParameter = new List<sqlQueryParameter>() {
                        //    new sqlQueryParameter {strParameter="@keyAXPURCHID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData =  (Object)argAXPURCHID },
                        //};

                        //curDt = curDb.getDataTable("[checkExistenceAXTODetailWRemainingQuantityByAXPurchId]", curQueryParameter);
                        ////No more pending line for this AX PO
                        //if (curDt.Rows.Count <= 0)
                        //{
                        curQueryParameter = new List<sqlQueryParameter>()
                            {
                                new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)aXTODetailScanInput.argAXROUTEID},
                                new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(9)},
                            };
                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXTOCompletedByAXRouteId", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Update AX Transfer Order Detail Status Failed");
                        }
                        //}
                    }
                    else
                    {
                        //Temporarily skip for those line is not existed in WMS DB
                        //continue;
                        throw new Exception("Update Failed - " + aXTODetailScanInput.argAXITEMID + " does not found in TO - " + aXTODetailScanInput.argAXROUTEID);
                    }
                }

                String postResult = postAXTODetail2AX(curDb, tempAXTO_ROUTEID, tempAXTO_INVENTLOCATIONIDFROM, tempAXTO_INVENTLOCATIONIDTO, aXTODetailScanInputList[0].argUserId, aXTODetailScanInputList[0].argDeviceSN);
                if (postResult != "true")
                    throw new Exception(postResult);

                isSuccess = "true";

            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        private String postAXTODetail2AX(database curDb, String argAXROUTEId, String argTempAXTO_INVENTLOCATIONIDFROM, String argTempAXTO_INVENTLOCATIONIDTO,
            String argUserId, String argDeviceSN)
        {
            String isSuccess = "Failed to post data to AX";
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            String xmlMessage = "";
            String messageId = getAXMessageId();
            String isDevelopmentMode = "0";
            try
            {
                xmlMessage = "<?xml version =\"1.0\" encoding=\"UTF-8\"?>\r\n" +
                    "<TRANSFERPICK>\r\n";
                int counter = 0;

                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyAXROUTEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)argAXROUTEId},
                };

                curDt = curDb.getDataTable("[selectAXTODetailByAXRouteId]", curQueryParameter);

                List<axTODetailObject> list = new List<axTODetailObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    String tempAXTOD_DATAAREAID = curDt.Rows[i][2].ToString();
                    String tempAXTOD_ROUTEID = curDt.Rows[i][3].ToString();
                    String tempAXTOD_TRANSFERID = curDt.Rows[i][4].ToString();
                    String tempAXTOD_INVENTTRANSID = curDt.Rows[i][5].ToString();
                    String tempAXTOD_ITEMID = curDt.Rows[i][6].ToString();
                    String tempAXTOD_DOT_PARTCODE = curDt.Rows[i][7].ToString();
                    String tempAXTOD_NAME = curDt.Rows[i][8].ToString();
                    String tempAXTOD_UNITID = curDt.Rows[i][9].ToString();
                    String tempAXTOD_INVENTLOCATIONID = curDt.Rows[i][10].ToString();
                    String tempAXTOD_INVENTSITEID = curDt.Rows[i][11].ToString();
                    String tempAXTOD_QTY = curDt.Rows[i][12].ToString();
                    String tempAXTOD_DOCTYPE = curDt.Rows[i][13].ToString();
                    String tempAXTOD_scanQuantity = curDt.Rows[i][14].ToString();
                    String tempAXTOD_WMSLOCATIONID = curDt.Rows[i][21].ToString();
                    String tempAXTOD_NAMEALIAS = curDt.Rows[i][22].ToString();


                    if (counter == 0)
                    {
                        xmlMessage += "<TOPICKHeader>\r\n" +
                                         "<Company_Code>" + tempAXTOD_DATAAREAID + "</Company_Code>\r\n" +
                                         "<Transfer_ID>" + tempAXTOD_TRANSFERID + "</Transfer_ID>\r\n" +
                                         "<Picking_NO>" + tempAXTOD_ROUTEID + "</Picking_NO>\r\n" +
                                         "<Location_From>" + argTempAXTO_INVENTLOCATIONIDFROM + "</Location_From>\r\n" +
                                         "<Location_To>" + argTempAXTO_INVENTLOCATIONIDTO + "</Location_To>\r\n" +
                                         "</TOPICKHeader>\r\n";
                    }
                    counter++;
                    if (!(String.IsNullOrEmpty(tempAXTOD_scanQuantity)))
                    {
                        xmlMessage += "<TOPICK_DETAIL>\r\n" +
                        "<Company_Code>" + tempAXTOD_DATAAREAID + "</Company_Code>\r\n" +
                        "<Invent_Trans_Id>" + tempAXTOD_INVENTTRANSID + "</Invent_Trans_Id>\r\n" +
                        "<ITEM_CODE>" + tempAXTOD_ITEMID + "</ITEM_CODE>\r\n" +
                        "<PART_CODE>" + tempAXTOD_DOT_PARTCODE + "</PART_CODE>\r\n" +
                        "<QTY>" + (String.IsNullOrEmpty(tempAXTOD_scanQuantity) ? "0" : tempAXTOD_scanQuantity) + "</QTY>\r\n" +
                        "<UOM_Id>" + tempAXTOD_UNITID + "</UOM_Id>\r\n" +
                        "</TOPICK_DETAIL>\r\n";
                    }

                }
                xmlMessage += "</TRANSFERPICK>";


                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"DEVELOPMENT MODE"},
                };

                curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                isDevelopmentMode = curDt.Rows[0][2].ToString();

                if (isDevelopmentMode == "0")
                {
                    TOPostService.TransferOrderPickingListserviceClient toPostObject = new TOPostService.TransferOrderPickingListserviceClient();

                    TOPostService.CallContext context = new TOPostService.CallContext();
                    context.MessageId = messageId; // "{33000000-0000-0000-0000-000000000012}";

                    String result = "";
                    try
                    {
                        result = toPostObject.ReadXMLFile(context, xmlMessage);
                    }
                    catch (Exception axEx)
                    {
                        throw new Exception("Failed to post data to AX - " + axEx.Message + " " + messageId);
                    }

                    Boolean isAXPostSuccess = false;
                    if (!String.IsNullOrEmpty(result))
                    {
                        string[] words = result.Split(' ');
                        if (words[0].Equals("Successfully", StringComparison.OrdinalIgnoreCase))
                        {
                            isAXPostSuccess = true;
                        }
                    }

                    if (!isAXPostSuccess)
                    {
                        throw new Exception("Failed to post data to AX - " + result + " " + messageId);
                        //String axExceptionMsg = retrieveAXExceptionMsg(messageId);
                        //if (axExceptionMsg == "")
                        //{
                        //    throw new Exception("Failed to post data to AX - " + result);
                        //}
                        //else
                        //{
                        //    throw new Exception("Failed to post data to AX - " + axExceptionMsg);
                        //}
                    }
                }
                //else
                //{
                //    ////Testing Purpose
                //    string pathString = XML_FOLDER_PATH;

                //    bool exists = System.IO.Directory.Exists(pathString);

                //    if (!exists)
                //        System.IO.Directory.CreateDirectory(pathString);

                //    string fileName = "TO_" + getAXMessageId() + ".xml";
                //    pathString = System.IO.Path.Combine(pathString, fileName);

                //    if (!System.IO.File.Exists(pathString))
                //    {
                //        using (System.IO.StreamWriter file =
                //            new System.IO.StreamWriter(pathString, true))
                //        {
                //            file.WriteLine(xmlMessage);
                //        }
                //    }
                //    throw new Exception("XML Generated");
                //}


                isSuccess = "true";

            }
            catch (Exception e)
            {
                insertExceptionMsg(argAXROUTEId, argUserId, argDeviceSN, "TO", e.Message, messageId);
                isSuccess = e.Message;
                try
                {
                    ////Testing Purpose
                    string pathString = XML_FOLDER_PATH;

                    pathString = System.IO.Path.Combine(pathString, "TO");
                    bool exists = System.IO.Directory.Exists(pathString);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(pathString);

                    string fileName = "TO_" + messageId + ".xml";
                    pathString = System.IO.Path.Combine(pathString, fileName);

                    if (!System.IO.File.Exists(pathString))
                    {
                        using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(pathString, true))
                        {
                            file.WriteLine(xmlMessage);
                        }
                    }
                }
                catch (Exception e2)
                {
                }
            }
            return isSuccess;
        }

        //Integration - AX TR
        //+++++++++++++++++++++++++++++++++++++

        //Get All Pending AX TR From AX DB View
        [WebMethod]
        public String retrieveAllAXTransferReceive()
        {
            String isSuccess = "Failed Retrieve AX Transfer Receive";

            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            DataTable curDt;
            int queryResult = -99;
            String lastSyncDatetime = "";
            String currentSyncDatetime = getLastSyncDatetime();

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST TR SYNC"},
                    };

                    curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                    lastSyncDatetime = curDt.Rows[0][2].ToString();
                    if (String.IsNullOrEmpty(lastSyncDatetime))
                        lastSyncDatetime = "1999-01-01 00:00:00";

                    axConn.Open();
                    //Get all AX CJ from AX DB View
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT DISTINCT [AX_WMS_TRANSFERORDERSHIP].[DATAAREAID]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[MODIFIEDDATETIME]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[VOUCHERID]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[TRANSFERID]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[DocumentType]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[INVENTLOCATIONIDFROM]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[INVENTLOCATIONIDTO]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[TRANSFERSTATUS] " +
                        "FROM [AX_WMS_TRANSFERORDERSHIP] " +
                        "JOIN AX_WMS_TRANSFERORDERSHIPLINE ON AX_WMS_TRANSFERORDERSHIPLINE.TRANSFERID = [AX_WMS_TRANSFERORDERSHIP].TRANSFERID", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX TR from AX DB View
                    DataTable axTRDt = new DataTable();

                    int noOfAXTR = sda.Fill(axTRDt);
                    //no AX TR is found
                    //if (noOfAXTR <= 0) return "true";
                    Int64 axtrId = -1;
                    //Loop through all AXTR from AX DB View
                    for (int i = 0; i < noOfAXTR; i++)
                    {
                        String tempAXTR_DATAAREAID = axTRDt.Rows[i][0].ToString();
                        String tempAXTR_MODIFIEDDATETIME = axTRDt.Rows[i][1].ToString();
                        String tempAXTR_VOUCHERID = axTRDt.Rows[i][2].ToString();
                        String tempAXTR_TRANSFERID = axTRDt.Rows[i][3].ToString();

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXTR_TRANSFERID}
                        };
                        curDt = curDb.getDataTable("selectAXTRByAXTransferId", curQueryParameter);

                        //++++++++++++++++++++++++++++++
                        //If AX TR Existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        if (curDt.Rows.Count > 0)
                        {
                            axtrId = Convert.ToInt64(curDt.Rows[0][0].ToString());
                            //Update TR Status to Pending if TR still showing in Table View
                            curQueryParameter = new List<sqlQueryParameter>()
                                        {
                                            new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTR_TRANSFERID},
                                            new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                                            new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                        };
                            queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXTRCompletedByAXTransferId", curQueryParameter);
                            if (queryResult == -99)
                            {
                                throw new Exception("Update AX Transfer Receive Status Failed");
                            }
                        }
                    }


                    //++++++++++++++++++++++++++++++
                    //Select TR with modified data
                    //++++++++++++++++++++++++++++++
                    axCommand = new SqlCommand(
                        "SELECT DISTINCT [AX_WMS_TRANSFERORDERSHIP].[DATAAREAID]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[MODIFIEDDATETIME]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[VOUCHERID]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[TRANSFERID]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[DocumentType]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[INVENTLOCATIONIDFROM]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[INVENTLOCATIONIDTO]" +
                          ",[AX_WMS_TRANSFERORDERSHIP].[TRANSFERSTATUS] " +
                        "FROM [AX_WMS_TRANSFERORDERSHIP] " +
                        "JOIN AX_WMS_TRANSFERORDERSHIPLINE ON AX_WMS_TRANSFERORDERSHIPLINE.TRANSFERID = [AX_WMS_TRANSFERORDERSHIP].TRANSFERID " +
                        "WHERE AX_WMS_TRANSFERORDERSHIPLINE.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX TR from AX DB View
                    axTRDt = new DataTable();

                    noOfAXTR = sda.Fill(axTRDt);

                    //Loop through all AXTR from AX DB View
                    for (int i = 0; i < noOfAXTR; i++)
                    {
                        String tempAXTR_DATAAREAID = axTRDt.Rows[i][0].ToString();
                        String tempAXTR_MODIFIEDDATETIME = axTRDt.Rows[i][1].ToString();
                        String tempAXTR_VOUCHERID = axTRDt.Rows[i][2].ToString();
                        String tempAXTR_TRANSFERID = axTRDt.Rows[i][3].ToString();
                        String tempAXTR_DocumentType = axTRDt.Rows[i][4].ToString();
                        String tempAXTR_INVENTLOCATIONIDFROM = axTRDt.Rows[i][5].ToString();
                        String tempAXTR_INVENTLOCATIONIDTO = axTRDt.Rows[i][6].ToString();
                        String tempAXTR_TRANSFERSTATUS = axTRDt.Rows[i][7].ToString();

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXTR_TRANSFERID}
                        };
                        curDt = curDb.getDataTable("selectAXTRByAXTransferId", curQueryParameter);

                        //++++++++++++++++++++++++++++++
                        //If AX TR Existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        if (curDt.Rows.Count > 0)
                        {
                            axtrId = Convert.ToInt64(curDt.Rows[0][0].ToString());
                            //Get all AX TR Detail By Transfer_ID from AX DB View
                            axCommand = new SqlCommand(
                            "SELECT DISTINCT [DATAAREAID]" +
                              ",[LINENUM]" +
                              ",[TRANSFERID]" +
                              ",[TRANSFERSTATUS]" +
                              ",[INVENTTRANSID]" +
                              ",[ITEMID]" +
                              ",[DOT_PARTCODE]" +
                              ",[NAMEALIAS]" +
                              ",[UNITID]" +
                              ",[INVENTLOCATIONID]" +
                              ",[INVENTSITEID]" +
                              ",[WMSLOCATIONID]" +
                              ",[QTYSHIPPED]" +
                              ",[docType] " +
                            "FROM [AX_WMS_TRANSFERORDERSHIPLINE] " +
                            "WHERE [TRANSFERID] = @keyAXTR_TRANSFERID " +
                              "AND AX_WMS_TRANSFERORDERSHIPLINE.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXTR_TRANSFERID", tempAXTR_TRANSFERID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axTRDetailDt = new DataTable();
                            //AX TR Detail in AX DB View
                            sda.Fill(axTRDetailDt);

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXTR_TRANSFERID}
                            };
                            //AX TR Detail in WMS DB
                            DataTable curAXTRDetailDt = curDb.getDataTable("selectAXTRDetailByAXTransferId", curQueryParameter);

                            Boolean isTRStatusUpdated = false; //to make TR status change to pending if there is at least one new line added
                            //Loop through each of the AXTR Detail
                            for (int j = 0; j < axTRDetailDt.Rows.Count; j++)
                            {

                                String tempAXTRD_DATAAREAID = axTRDetailDt.Rows[j][0].ToString();
                                String tempAXTRD_LINENUM = axTRDetailDt.Rows[j][1].ToString();
                                String tempAXTRD_TRANSFERID = axTRDetailDt.Rows[j][2].ToString();
                                String tempAXTRD_TRANSFERSTATUS = axTRDetailDt.Rows[j][3].ToString();
                                String tempAXTRD_INVENTTRANSID = axTRDetailDt.Rows[j][4].ToString();
                                String tempAXTRD_ITEMID = axTRDetailDt.Rows[j][5].ToString();
                                String tempAXTRD_DOT_PARTCODE = axTRDetailDt.Rows[j][6].ToString();
                                String tempAXTRD_NAMEALIAS = axTRDetailDt.Rows[j][7].ToString();
                                String tempAXTRD_UNITID = axTRDetailDt.Rows[j][8].ToString();
                                String tempAXTRD_INVENTLOCATIONID = axTRDetailDt.Rows[j][9].ToString();
                                String tempAXTRD_INVENTSITEID = axTRDetailDt.Rows[j][10].ToString();
                                String tempAXTRD_WMSLOCATIONID = axTRDetailDt.Rows[j][11].ToString();
                                String tempAXTRD_QTYSHIPPED = axTRDetailDt.Rows[j][12].ToString();
                                String tempAXTRD_docType = axTRDetailDt.Rows[j][13].ToString();

                                Boolean trDetailExisted = false;
                                for (int k = 0; k < curAXTRDetailDt.Rows.Count; k++)
                                {
                                    String tempWMSTRD_id = curAXTRDetailDt.Rows[k][0].ToString();
                                    String tempWMSTRD_DATAAREAID = curAXTRDetailDt.Rows[k][2].ToString();
                                    String tempWMSTRD_LINENUM = curAXTRDetailDt.Rows[k][3].ToString();
                                    String tempWMSTRD_TRANSFERID = curAXTRDetailDt.Rows[k][4].ToString();
                                    String tempWMSTRD_TRANSFERSTATUS = curAXTRDetailDt.Rows[k][5].ToString();
                                    String tempWMSTRD_INVENTTRANSID = curAXTRDetailDt.Rows[k][6].ToString();
                                    String tempWMSTRD_ITEMID = curAXTRDetailDt.Rows[k][7].ToString();
                                    String tempWMSTRD_DOT_PARTCODE = curAXTRDetailDt.Rows[k][8].ToString();
                                    String tempWMSTRD_NAMEALIAS = curAXTRDetailDt.Rows[k][9].ToString();
                                    String tempWMSTRD_UNITID = curAXTRDetailDt.Rows[k][10].ToString();
                                    String tempWMSTRD_INVENTLOCATIONID = curAXTRDetailDt.Rows[k][11].ToString();
                                    String tempWMSTRD_INVENTSITEID = curAXTRDetailDt.Rows[k][12].ToString();
                                    String tempWMSTRD_QTYSHIPPED = curAXTRDetailDt.Rows[k][13].ToString();
                                    String tempWMSTRD_DocType = curAXTRDetailDt.Rows[k][14].ToString();
                                    String tempWMSTRD_scanQuantity = String.IsNullOrEmpty(curAXTRDetailDt.Rows[k][15].ToString()) ? "0" : curAXTRDetailDt.Rows[k][15].ToString();
                                    String tempWMSTRD_WMSLOCATIONID = curAXTRDetailDt.Rows[k][22].ToString();

                                    //If AX TR Detail Existed in WMS DB
                                    if (tempAXTRD_TRANSFERID.Trim() == tempWMSTRD_TRANSFERID.Trim() &&
                                        tempAXTRD_INVENTTRANSID.Trim() == tempWMSTRD_INVENTTRANSID.Trim() &&
                                        tempAXTRD_ITEMID.Trim() == tempWMSTRD_ITEMID.Trim() &&
                                        tempAXTRD_DOT_PARTCODE.Trim() == tempWMSTRD_DOT_PARTCODE.Trim() &&
                                        (Convert.ToDecimal(tempAXTRD_LINENUM) == Convert.ToDecimal(tempWMSTRD_LINENUM)))
                                    {
                                        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ +\
                                        //Update quantity changes
                                        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ +
                                        queryResult = setAXTRDetail(curDb, tempAXTRD_DATAAREAID, tempAXTRD_LINENUM,
                                                tempAXTRD_TRANSFERID, tempAXTRD_TRANSFERSTATUS, tempAXTRD_INVENTTRANSID, tempAXTRD_ITEMID,
                                                tempAXTRD_DOT_PARTCODE, tempAXTRD_NAMEALIAS, tempAXTRD_UNITID, tempAXTRD_INVENTLOCATIONID,
                                                tempAXTRD_INVENTSITEID, tempAXTRD_WMSLOCATIONID, tempAXTRD_QTYSHIPPED, tempAXTRD_docType);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("update ax tr detail failed");
                                        }

                                        Int64 curStatusId = 9;
                                        if (Convert.ToDecimal(tempWMSTRD_scanQuantity) == 0)
                                        {
                                            curStatusId = 1;
                                        }
                                        else if (Convert.ToDecimal(tempWMSTRD_QTYSHIPPED) > Convert.ToDecimal(tempWMSTRD_scanQuantity))
                                        {
                                            curStatusId = 7;
                                        }

                                        curQueryParameter = new List<sqlQueryParameter>()
                                        {
                                                new sqlQueryParameter {strParameter="@keyTableName", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)"tbl_ax_trDetail"},
                                                new sqlQueryParameter {strParameter="@keyFieldName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"tbl_status_Id"},
                                                new sqlQueryParameter {strParameter="@keyFieldValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)curStatusId.ToString()},
                                                new sqlQueryParameter {strParameter="@keyIDName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"id"},
                                                new sqlQueryParameter {strParameter="@keyIDValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)tempWMSTRD_id}
                                            };
                                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateTableSingleField", curQueryParameter);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("Update AX Purchase Order Detail Status Failed");
                                        }


                                        queryResult = insertMovementHistory(curDb, tempAXTR_DATAAREAID, tempAXTRD_TRANSFERID, "", tempAXTRD_ITEMID, tempAXTRD_DOT_PARTCODE,
                                                tempAXTRD_INVENTLOCATIONID, tempAXTRD_INVENTSITEID, "", "", "", "", tempAXTRD_QTYSHIPPED, "-1", "SYSTEM", sharedRes.AX2LGC_TR_DETAIL_UPDATE, tempAXTRD_LINENUM);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("insert movement history failed");
                                        }

                                        trDetailExisted = true;
                                        break;
                                    }
                                }

                                ////if AX TR Detail no existed in WMS DB
                                if (!trDetailExisted)
                                {

                                    queryResult = insertAXTRDetail(curDb, axtrId.ToString(), tempAXTRD_DATAAREAID, tempAXTRD_LINENUM,
                                    tempAXTRD_TRANSFERID, tempAXTRD_TRANSFERSTATUS, tempAXTRD_INVENTTRANSID, tempAXTRD_ITEMID,
                                    tempAXTRD_DOT_PARTCODE, tempAXTRD_NAMEALIAS, tempAXTRD_UNITID, tempAXTRD_INVENTLOCATIONID,
                                    tempAXTRD_INVENTSITEID, tempAXTRD_WMSLOCATIONID, tempAXTRD_QTYSHIPPED, tempAXTRD_docType);
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert ax tr detail failed");
                                    }

                                    queryResult = insertMovementHistory(curDb, tempAXTR_DATAAREAID, tempAXTRD_TRANSFERID, "", tempAXTRD_ITEMID, tempAXTRD_DOT_PARTCODE,
                                            tempAXTRD_INVENTLOCATIONID, tempAXTRD_INVENTSITEID, "", "", "", "", tempAXTRD_QTYSHIPPED, "-1", "SYSTEM", sharedRes.AX2LGC_TR_DETAIL_INSERT, tempAXTRD_LINENUM);
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert movement history failed");
                                    }

                                }
                            }

                        }
                        //++++++++++++++++++++++++++++++
                        //if AX TR no existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        else
                        {
                            //Get all AX TR Detail By Transfer_ID from AX DB View
                            axCommand = new SqlCommand(
                            "SELECT DISTINCT [DATAAREAID]" +
                              ",[LINENUM]" +
                              ",[TRANSFERID]" +
                              ",[TRANSFERSTATUS]" +
                              ",[INVENTTRANSID]" +
                              ",[ITEMID]" +
                              ",[DOT_PARTCODE]" +
                              ",[NAMEALIAS]" +
                              ",[UNITID]" +
                              ",[INVENTLOCATIONID]" +
                              ",[INVENTSITEID]" +
                              ",[WMSLOCATIONID]" +
                              ",[QTYSHIPPED]" +
                              ",[docType] " +
                            "FROM [AX_WMS_TRANSFERORDERSHIPLINE] " +
                            "WHERE [TRANSFERID] = @keyAXTR_TRANSFERID", axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXTR_TRANSFERID", tempAXTR_TRANSFERID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axTRDetailDt = new DataTable();
                            //AX TR Detail in AX DB View
                            sda.Fill(axTRDetailDt);

                            curQueryParameter = new List<sqlQueryParameter>()
                            {
                            };

                            curDt = curDb.getDataTable("[selectNextAXTRId]", curQueryParameter);

                            List<Int64> list = new List<Int64>();
                            for (int j = 0; j < curDt.Rows.Count; j++)
                            {
                                list.Add(Convert.ToInt64(curDt.Rows[j][0]));
                            }
                            axtrId = list[0];

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXTR_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axtrId)},
                                new sqlQueryParameter {strParameter="@keyAXTR_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXTR_DATAAREAID},
                                new sqlQueryParameter {strParameter="@keyAXTR_MODIFIEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXTR_MODIFIEDDATETIME},
                                new sqlQueryParameter {strParameter="@keyAXTR_VOUCHERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTR_VOUCHERID},
                                new sqlQueryParameter {strParameter="@keyAXTR_TRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTR_TRANSFERID},
                                new sqlQueryParameter {strParameter="@keyAXTR_DOCUMENTTYPE", ParameterType=SqlDbType.VarChar, ParameterLength=2, keyData = (Object)tempAXTR_DocumentType},
                                new sqlQueryParameter {strParameter="@keyAXTR_INVENTLOCATIONIDFROM", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTR_INVENTLOCATIONIDFROM},
                                new sqlQueryParameter {strParameter="@keyAXTR_INVENTLOCATIONIDTO", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTR_INVENTLOCATIONIDTO},
                                new sqlQueryParameter {strParameter="@keyAXTR_TRANSFERSTATUS", ParameterType=SqlDbType.Int, keyData = (Object)tempAXTR_TRANSFERSTATUS},
                                new sqlQueryParameter {strParameter="@keyReferenceNo", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)""},
                                new sqlQueryParameter {strParameter="@keyCreationDateTime", ParameterType=SqlDbType.DateTime, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyDocType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"tr"},
                                new sqlQueryParameter {strParameter="@keyReferenceVersion", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                            };
                            queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXTR", curQueryParameter);
                            if (queryResult == -99)
                            {
                                throw new Exception("insert ax transfer receive failed");
                            }

                            queryResult = insertMovementHistory(curDb, tempAXTR_DATAAREAID, tempAXTR_TRANSFERID, "", "", "", "", "", "", "", "", "", "", "-1", "SYSTEM", sharedRes.AX2LGC_TR_INSERT, "");
                            if (queryResult == -99)
                            {
                                throw new Exception("insert movement history failed");
                            }

                            //Loop through each of the AXTR Detail
                            for (int j = 0; j < axTRDetailDt.Rows.Count; j++)
                            {
                                String tempAXTRD_DATAAREAID = axTRDetailDt.Rows[j][0].ToString();
                                String tempAXTRD_LINENUM = axTRDetailDt.Rows[j][1].ToString();
                                String tempAXTRD_TRANSFERID = axTRDetailDt.Rows[j][2].ToString();
                                String tempAXTRD_TRANSFERSTATUS = axTRDetailDt.Rows[j][3].ToString();
                                String tempAXTRD_INVENTTRANSID = axTRDetailDt.Rows[j][4].ToString();
                                String tempAXTRD_ITEMID = axTRDetailDt.Rows[j][5].ToString();
                                String tempAXTRD_DOT_PARTCODE = axTRDetailDt.Rows[j][6].ToString();
                                String tempAXTRD_NAMEALIAS = axTRDetailDt.Rows[j][7].ToString();
                                String tempAXTRD_UNITID = axTRDetailDt.Rows[j][8].ToString();
                                String tempAXTRD_INVENTLOCATIONID = axTRDetailDt.Rows[j][9].ToString();
                                String tempAXTRD_INVENTSITEID = axTRDetailDt.Rows[j][10].ToString();
                                String tempAXTRD_WMSLOCATIONID = axTRDetailDt.Rows[j][11].ToString();
                                String tempAXTRD_QTYSHIPPED = axTRDetailDt.Rows[j][12].ToString();
                                String tempAXTRD_docType = axTRDetailDt.Rows[j][13].ToString();


                                queryResult = insertAXTRDetail(curDb, axtrId.ToString(), tempAXTRD_DATAAREAID, tempAXTRD_LINENUM,
                                    tempAXTRD_TRANSFERID, tempAXTRD_TRANSFERSTATUS, tempAXTRD_INVENTTRANSID, tempAXTRD_ITEMID,
                                    tempAXTRD_DOT_PARTCODE, tempAXTRD_NAMEALIAS, tempAXTRD_UNITID, tempAXTRD_INVENTLOCATIONID,
                                    tempAXTRD_INVENTSITEID, tempAXTRD_WMSLOCATIONID, tempAXTRD_QTYSHIPPED, tempAXTRD_docType);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert ax tr detail failed");
                                }

                                queryResult = insertMovementHistory(curDb, tempAXTR_DATAAREAID, tempAXTRD_TRANSFERID, "", tempAXTRD_ITEMID, tempAXTRD_DOT_PARTCODE,
                                        tempAXTRD_INVENTLOCATIONID, tempAXTRD_INVENTSITEID, "", "", "", "", tempAXTRD_QTYSHIPPED, "-1", "SYSTEM", sharedRes.AX2LGC_TR_DETAIL_INSERT, tempAXTRD_LINENUM);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert movement history failed");
                                }

                            }
                        }
                    }

                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST TR SYNC"},
                        new sqlQueryParameter {strParameter="@keyDataValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)currentSyncDatetime}
                    };

                    queryResult = curDb.insertUpdateDeleteDataIntoTable("updateSystemData", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert/update/delete failed");
                    }

                    isSuccess = "true";
                }
                catch (Exception e)
                {
                    curDb.rollbackNCloseConnection();
                    isSuccess = e.Message;
                    return isSuccess;
                }
                if (!curDb.checkIsConnectionClosed())
                    curDb.closeConnection();
                return isSuccess;
            }
        }

        private int insertAXTRDetail(database curDb, String axtrId, String tempAXTRD_DATAAREAID,
            String tempAXTRD_LINENUM, String tempAXTRD_TRANSFERID, String tempAXTRD_TRANSFERSTATUS,
            String tempAXTRD_INVENTTRANSID, String tempAXTRD_ITEMID, String tempAXTRD_DOT_PARTCODE,
            String tempAXTRD_NAMEALIAS, String tempAXTRD_UNITID, String tempAXTRD_INVENTLOCATIONID,
            String tempAXTRD_INVENTSITEID, String tempAXTRD_WMSLOCATIONID,
            String tempAXTRD_QTYSHIPPED, String tempAXTRD_docType)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };

                DataTable curDt = curDb.getDataTable("[selectNextAXTRDetailId]", curQueryParameter);

                List<Int64> list = new List<Int64>();
                for (int k = 0; k < curDt.Rows.Count; k++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[k][0]));
                }
                Int64 axtrdId = list[0];

                curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXTRD_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axtrdId)},
                                    new sqlQueryParameter {strParameter="@keyAXTR_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axtrId)},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXTRD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_LINENUMBER", ParameterType=SqlDbType.Decimal, keyData = tempAXTRD_LINENUM == "" ? (Object)DBNull.Value : Convert.ToDecimal(tempAXTRD_LINENUM)},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_TRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTRD_TRANSFERID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_TRANSFERSTATUS", ParameterType=SqlDbType.Int, keyData = Convert.ToInt64(tempAXTRD_TRANSFERSTATUS)},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_INVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTRD_INVENTTRANSID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXTRD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXTRD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_NAMEALIAS", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTRD_NAMEALIAS},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_UNITID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTRD_UNITID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTRD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTRD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXTRD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_QTYSHIIPPED", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXTRD_QTYSHIPPED)},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_docType", ParameterType=SqlDbType.VarChar, ParameterLength=6, keyData = (Object)tempAXTRD_docType},
                                    new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Int, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                    new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value}
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXTRDetail", curQueryParameter);
            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }

        private int setAXTRDetail(database curDb, String tempAXTRD_DATAAREAID,
            String tempAXTRD_LINENUM, String tempAXTRD_TRANSFERID, String tempAXTRD_TRANSFERSTATUS,
            String tempAXTRD_INVENTTRANSID, String tempAXTRD_ITEMID, String tempAXTRD_DOT_PARTCODE,
            String tempAXTRD_NAMEALIAS, String tempAXTRD_UNITID, String tempAXTRD_INVENTLOCATIONID,
            String tempAXTRD_INVENTSITEID, String tempAXTRD_WMSLOCATIONID,
            String tempAXTRD_QTYSHIPPED, String tempAXTRD_docType)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                                     new sqlQueryParameter {strParameter="@keyAXTRD_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXTRD_DATAAREAID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_LINENUMBER", ParameterType=SqlDbType.Decimal, keyData = tempAXTRD_LINENUM == "" ? (Object)DBNull.Value : Convert.ToDecimal(tempAXTRD_LINENUM)},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_TRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTRD_TRANSFERID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_TRANSFERSTATUS", ParameterType=SqlDbType.Int, keyData = Convert.ToInt64(tempAXTRD_TRANSFERSTATUS)},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_INVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTRD_INVENTTRANSID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXTRD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXTRD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_NAMEALIAS", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTRD_NAMEALIAS},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_UNITID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTRD_UNITID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXTRD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXTRD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXTRD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_QTYSHIIPPED", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXTRD_QTYSHIPPED)},
                                    new sqlQueryParameter {strParameter="@keyAXTRD_docType", ParameterType=SqlDbType.VarChar, ParameterLength=6, keyData = (Object)tempAXTRD_docType}
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXTRDetailByAXLineNum_TransferId_InventTransId_ItemId_PartCode", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }

        //AX Transfer Receive
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public void getAllAXTRInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];

            String searchHeader = "";
            String searchContent = "";
            String searchContent2 = "";
            String searchType = HttpContext.Current.Request.QueryString["filterType"];
            String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');
            String warehouseCode = HttpContext.Current.Request.QueryString["warehouseCode"].ToString();

            searchHeader = searchArray[0];
            if (searchArray.Length == 3)
            {
                searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
                searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
            }
            else
            {
                searchContent = searchArray[1] == "" ? "%" : searchArray[1];
            }

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchType},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent},
                new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent2},
                new sqlQueryParameter {strParameter="@keySearchWarehouseCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)warehouseCode}
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllAXTRInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");

        }

        [WebMethod]
        public axTRObject[] getAllPendingAXTR()
        {

            axTRObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            try
            {
                if (!(retrieveAllAXTransferReceive() == "true"))
                    throw new Exception("Failed to retrieve AX Transfer Receive");

                DataTable curDt = curDb.getDataTable("[selectAllPendingAXTR]", curQueryParameter);

                List<axTRObject> list = new List<axTRObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axTRObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        , curDt.Rows[i][4].ToString()
                        , curDt.Rows[i][5].ToString()
                        , curDt.Rows[i][6].ToString()
                        , curDt.Rows[i][7].ToString()
                        , curDt.Rows[i][8].ToString()
                        , curDt.Rows[i][9].ToString()
                        , curDt.Rows[i][10].ToString()
                        , curDt.Rows[i][11].ToString()
                        , curDt.Rows[i][12].ToString()
                        , curDt.Rows[i][13].ToString()
                        , curDt.Rows[i][14].ToString()
                        , curDt.Rows[i][15].ToString()
                        , curDt.Rows[i][16].ToString()
                        , curDt.Rows[i][17].ToString()
                        , curDt.Rows[i][18].ToString()
                        ));
                }

                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axTRObject[] getAXTRByAXTransferId(String argAXTRANSFERID)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)argAXTRANSFERID},
            };

            DataTable curDt = curDb.getDataTable("[selectAXTRByAXTransferId]", curQueryParameter);

            List<axTRObject> list = new List<axTRObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axTRObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    ));
            }

            axTRObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axTRDetailObject[] getAllPendingAXTRDetail()
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectAllPendingAXTRDetail]", curQueryParameter);

            List<axTRDetailObject> list = new List<axTRDetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axTRDetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    , curDt.Rows[i][21].ToString()
                    , curDt.Rows[i][22].ToString()
                    ));
            }

            axTRDetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axTRDetailObject[] getAXTRDetailByAXTransferId(String argAXTRANSFERID)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)argAXTRANSFERID},
            };

            DataTable curDt = curDb.getDataTable("[selectAXTRDetailByAXTransferId]", curQueryParameter);

            List<axTRDetailObject> list = new List<axTRDetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axTRDetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    , curDt.Rows[i][19].ToString()
                    , curDt.Rows[i][20].ToString()
                    , curDt.Rows[i][21].ToString()
                    , curDt.Rows[i][22].ToString()
                    ));
            }

            axTRDetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public String setAXTRDetailScanInput(List<AXTRDetailScanInput> aXTRDetailScanInputList)
        {
            String isSuccess = "AX trD Update failed";
            int queryResult = -99;
            database curDb = new database();
            curDb.openConnection();
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            String xmlMessage = "";
            String messageId = getAXMessageId();
            String isDevelopmentMode = "0";

            try
            {
                xmlMessage = "<?xml version =\"1.0\" encoding=\"UTF-8\"?>\r\n" +
                    "<TRANSFERRECEIVE>\r\n";
                int counter = 0;

                foreach (var aXTRDetailScanInput in aXTRDetailScanInputList)
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(aXTRDetailScanInput.argAXLINENUMBER)},
                        new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXTRDetailScanInput.argAXTRANSFERID},
                        new sqlQueryParameter {strParameter="@keyAXINVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXTRDetailScanInput.argAXINVENTTRANSID},
                        new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXTRDetailScanInput.argAXITEMID},
                        new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXTRDetailScanInput.argAXDOT_PARTCODE},
                    };

                    curDt = curDb.getDataTable("[selectAXTRDetailByAXLineNum_TransferId_InventTransId_ItemId_PartCode]", curQueryParameter);

                    if (curDt.Rows.Count > 0)
                    {

                        String tempWMSTRD_DATAAREAID = curDt.Rows[0][2].ToString();
                        String tempWMSTRD_LINENUM = curDt.Rows[0][3].ToString();
                        String tempWMSTRD_TRANSFERID = curDt.Rows[0][4].ToString();
                        String tempWMSTRD_TRANSFERSTATUS = curDt.Rows[0][5].ToString();
                        String tempWMSTRD_INVENTTRANSID = curDt.Rows[0][6].ToString();
                        String tempWMSTRD_ITEMID = curDt.Rows[0][7].ToString();
                        String tempWMSTRD_DOT_PARTCODE = curDt.Rows[0][8].ToString();
                        String tempWMSTRD_NAMEALIAS = curDt.Rows[0][9].ToString();
                        String tempWMSTRD_UNITID = curDt.Rows[0][10].ToString();
                        String tempWMSTRD_INVENTLOCATIONID = curDt.Rows[0][11].ToString();
                        String tempWMSTRD_INVENTSITEID = curDt.Rows[0][12].ToString();
                        String tempWMSTRD_QTYSHIPPED = curDt.Rows[0][13].ToString();
                        String tempWMSTRD_DocType = curDt.Rows[0][14].ToString();

                        //if (Convert.ToDecimal(tempAXCJD_QTY) < Convert.ToDecimal(aXCJDetailScanInput.argScanQuantity)) {
                        //    throw new Exception("Update Failed - " + aXCJDetailScanInput.argAXITEMID +
                        //        " - Scan Quantity[" + aXCJDetailScanInput.argScanQuantity + "] more then Quantity [" + tempAXCJD_QTY + "]");
                        //}
                        Int64 curStatusId = 9;
                        if (Convert.ToDecimal(tempWMSTRD_QTYSHIPPED) > Convert.ToDecimal(aXTRDetailScanInput.argScanQuantity))
                        {
                            curStatusId = 7;
                        }

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(aXTRDetailScanInput.argAXLINENUMBER)},
                            new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXTRDetailScanInput.argAXTRANSFERID},
                            new sqlQueryParameter {strParameter="@keyAXINVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXTRDetailScanInput.argAXINVENTTRANSID},
                            new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXTRDetailScanInput.argAXITEMID},
                            new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXTRDetailScanInput.argAXDOT_PARTCODE},
                            new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Decimal, keyData =  Convert.ToDecimal(aXTRDetailScanInput.argScanQuantity)},
                            new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData =  (Object)(aXTRDetailScanInput.argScanDatetime)},
                            new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData =  (Object)aXTRDetailScanInput.argRemarks},
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(aXTRDetailScanInput.argUserId) },
                            new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)aXTRDetailScanInput.argDeviceSN},
                            new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(curStatusId) },
                        };

                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXTRDetailScanInputByAXLineNum_TransferId_InventTransId_ItemId_PartCode", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Update AX TRD Failed");
                        }

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)aXTRDetailScanInput.argAXTRANSFERID}
                        };
                        curDt = curDb.getDataTable("selectAXTRByAXTransferId", curQueryParameter);

                        String tempAXTR_DATAAREAID = "";
                        if (curDt.Rows.Count > 0)
                        {
                            tempAXTR_DATAAREAID = curDt.Rows[0][1].ToString();
                        }

                        if (counter == 0)
                        {
                            xmlMessage += "<TORECEIVEHEADER>\r\n" +
                                             "<Company_Code>" + tempAXTR_DATAAREAID + "</Company_Code>\r\n" +
                                             "<Transfer_ID>" + tempWMSTRD_TRANSFERID + "</Transfer_ID>\r\n" +
                                             "</TORECEIVEHEADER>\r\n";
                        }
                        counter++;
                        xmlMessage += "<TORECEIVE_DETAIL>\r\n" +
                            "<InventTransId>" + tempWMSTRD_INVENTTRANSID + "</InventTransId>\r\n" +
                            "<ITEM_CODE>" + tempWMSTRD_ITEMID + "</ITEM_CODE>\r\n" +
                            "<PART_CODE>" + tempWMSTRD_DOT_PARTCODE + "</PART_CODE>\r\n" +
                            "<QTY>" + aXTRDetailScanInput.argScanQuantity + "</QTY>\r\n" +
                            "</TORECEIVE_DETAIL>\r\n";

                        queryResult = insertMovementHistory(curDb, tempAXTR_DATAAREAID, tempWMSTRD_TRANSFERID,
                            tempWMSTRD_INVENTTRANSID, tempWMSTRD_ITEMID, tempWMSTRD_DOT_PARTCODE,
                            tempWMSTRD_INVENTLOCATIONID, tempWMSTRD_INVENTSITEID, "",
                            "", "", "", aXTRDetailScanInput.argScanQuantity, aXTRDetailScanInput.argUserId, aXTRDetailScanInput.argDeviceSN, sharedRes.LGC2AX_TR_DETAIL_UPDATE,
                            aXTRDetailScanInput.argAXLINENUMBER);
                        if (queryResult == -99)
                        {
                            throw new Exception("insert movement history failed");
                        }

                    }
                    else
                    {
                        //Temporarily skip for those line is not existed in WMS DB
                        //continue;
                        throw new Exception("Update Failed - " + aXTRDetailScanInput.argAXITEMID + " does not found in TR - " + aXTRDetailScanInput.argAXTRANSFERID);
                    }
                }

                xmlMessage += "</TRANSFERRECEIVE>";


                //curQueryParameter = new List<sqlQueryParameter>() {
                //            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData =  (Object)aXCJDetailScanInputList.First().argAXJOURNALID },
                //        };

                //curDt = curDb.getDataTable("[checkExistenceAXCJDetailWRemainingQuantityByAXJournalId]", curQueryParameter);
                ////No more pending line for this AX CJ
                //if (curDt.Rows.Count <= 0)
                //{
                curQueryParameter = new List<sqlQueryParameter>()
                            {
                                new sqlQueryParameter {strParameter="@keyAXTRANSFERID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXTRDetailScanInputList[0].argAXTRANSFERID},
                                new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(9)},
                            };
                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXTRCompletedByAXTransferId", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("Update AX Transfer Receive Detail Status Failed");
                }
                //}


                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"DEVELOPMENT MODE"},
                };


                curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                isDevelopmentMode = curDt.Rows[0][2].ToString();

                if (isDevelopmentMode == "0")
                {
                    TRPostService.WMS_TransferOrderReceiveServiceClient trPostObject = new TRPostService.WMS_TransferOrderReceiveServiceClient();

                    TRPostService.CallContext context = new TRPostService.CallContext();
                    //context.LogonAsUser = @"GOLDBELL\itadmin2";

                    context.MessageId = messageId; // "{33000000-0000-0000-0000-000000000012}";

                    String result = "";
                    try
                    {
                        result = trPostObject.ReadXMLFile(context, xmlMessage);
                    }
                    catch (Exception axEx)
                    {
                        throw new Exception("Failed to post data to AX - " + axEx.Message + " " + messageId);
                    }

                    Boolean isAXPostSuccess = false;
                    if (!String.IsNullOrEmpty(result))
                    {
                        string[] words = result.Split(' ');
                        if (words[0].Equals("Successfully", StringComparison.OrdinalIgnoreCase))
                        {
                            isAXPostSuccess = true;
                        }
                    }

                    if (!isAXPostSuccess)
                    {
                        throw new Exception("Failed to post data to AX - " + result + " " + messageId);
                        //String axExceptionMsg = retrieveAXExceptionMsg(messageId);
                        //if (axExceptionMsg == "")
                        //{
                        //    throw new Exception("Failed to post data to AX - " + result);
                        //}
                        //else
                        //{
                        //    throw new Exception("Failed to post data to AX - " + axExceptionMsg);
                        //}
                    }
                }
                //else
                //{
                //    throw new Exception("XML Generated");
                //}

                isSuccess = "true";

            }
            catch (Exception e)
            {
                insertExceptionMsg(aXTRDetailScanInputList[0].argAXTRANSFERID, aXTRDetailScanInputList[0].argUserId,
                    aXTRDetailScanInputList[0].argDeviceSN, "TR", e.Message, messageId);
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
                try
                {
                    string pathString = XML_FOLDER_PATH;

                    pathString = System.IO.Path.Combine(pathString, "TR");
                    bool exists = System.IO.Directory.Exists(pathString);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(pathString);

                    string fileName = "TR_" + messageId + ".xml";
                    pathString = System.IO.Path.Combine(pathString, fileName);

                    if (!System.IO.File.Exists(pathString))
                    {
                        using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(pathString, true))
                        {
                            file.WriteLine(xmlMessage);
                        }
                    }


                }
                catch (Exception e2)
                {
                }
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        //Integration - AX CJ
        //+++++++++++++++++++++++++++++++++++++

        //Get All Pending AX CJ From AX DB View
        [WebMethod]
        public String retrieveAllAXCountingJournal()
        {
            String isSuccess = "Failed Retrieve AX Counting Journal";

            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            DataTable curDt;
            int queryResult = -99;
            String lastSyncDatetime = "";
            String currentSyncDatetime = getLastSyncDatetime();

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST CJ SYNC"},
                    };


                    curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                    lastSyncDatetime = curDt.Rows[0][2].ToString();
                    if (String.IsNullOrEmpty(lastSyncDatetime))
                        lastSyncDatetime = "1999-01-01 00:00:00";

                    axConn.Open();
                    //Get all AX CJ from AX DB View
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT DISTINCT [AX_WMS_COUNTINGJOURNAL].[DATAAREAID] " +
                          ",[AX_WMS_COUNTINGJOURNAL].[POSTEDDATETIME] " +
                          ",[AX_WMS_COUNTINGJOURNAL].[JOURNALID] " +
                          ",[AX_WMS_COUNTINGJOURNAL].[DESCRIPTION] " +
                          "FROM [AX_WMS_COUNTINGJOURNAL] " +
                          "JOIN AX_WMS_COUNTINGJOURNALLINE ON AX_WMS_COUNTINGJOURNALLINE.JOURNALID = [AX_WMS_COUNTINGJOURNAL].JOURNALID", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX CJ from AX DB View
                    DataTable axCJDt = new DataTable();

                    int noOfAXCJ = sda.Fill(axCJDt);
                    //no AX CJ is found
                    //if (noOfAXCJ <= 0) return "true";

                    Int64 axcjId = -1;
                    for (int i = 0; i < noOfAXCJ; i++)
                    {
                        String tempAXCJ_DATAAREAID = axCJDt.Rows[i][0].ToString();
                        String tempAXCJ_POSTEDDATETIME = axCJDt.Rows[i][1].ToString();
                        String tempAXCJ_JOURNALID = axCJDt.Rows[i][2].ToString();
                        String tempAXCJ_DESCRIPTION = axCJDt.Rows[i][3].ToString();

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXCJ_JOURNALID}
                        };
                        curDt = curDb.getDataTable("selectAXCJByAXJournalId", curQueryParameter);

                        //++++++++++++++++++++++++++++++
                        //If AX CJ Existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        if (curDt.Rows.Count > 0)
                        {
                            axcjId = Convert.ToInt64(curDt.Rows[0][0].ToString());

                            //Update CJ Status to Pending if CJ still showing in Table View
                            curQueryParameter = new List<sqlQueryParameter>()
                                        {
                                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXCJ_JOURNALID},
                                            new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                                            new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                        };
                            queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXCJCompletedByAXJournalId", curQueryParameter);
                            if (queryResult == -99)
                            {
                                throw new Exception("Update AX Counting Journal Detail Status Failed");
                            }
                        }

                    }


                    //++++++++++++++++++++++++++++++
                    //Select CJ with modified data
                    //++++++++++++++++++++++++++++++
                    axCommand = new SqlCommand(
                        "SELECT DISTINCT [AX_WMS_COUNTINGJOURNAL].[DATAAREAID] " +
                          ",[AX_WMS_COUNTINGJOURNAL].[POSTEDDATETIME] " +
                          ",[AX_WMS_COUNTINGJOURNAL].[JOURNALID] " +
                          ",[AX_WMS_COUNTINGJOURNAL].[DESCRIPTION] " +
                          "FROM [AX_WMS_COUNTINGJOURNAL] " +
                          "JOIN AX_WMS_COUNTINGJOURNALLINE ON AX_WMS_COUNTINGJOURNALLINE.JOURNALID = [AX_WMS_COUNTINGJOURNAL].JOURNALID " +
                        "WHERE AX_WMS_COUNTINGJOURNALLINE.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX CJ from AX DB View
                    axCJDt = new DataTable();

                    noOfAXCJ = sda.Fill(axCJDt);

                    axcjId = -1;
                    for (int i = 0; i < noOfAXCJ; i++)
                    {
                        String tempAXCJ_DATAAREAID = axCJDt.Rows[i][0].ToString();
                        String tempAXCJ_POSTEDDATETIME = axCJDt.Rows[i][1].ToString();
                        String tempAXCJ_JOURNALID = axCJDt.Rows[i][2].ToString();
                        String tempAXCJ_DESCRIPTION = axCJDt.Rows[i][3].ToString();

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXCJ_JOURNALID}
                        };
                        curDt = curDb.getDataTable("selectAXCJByAXJournalId", curQueryParameter);

                        //++++++++++++++++++++++++++++++
                        //If AX CJ Existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        if (curDt.Rows.Count > 0)
                        {
                            axcjId = Convert.ToInt64(curDt.Rows[0][0].ToString());

                            axCommand = new SqlCommand(
                            "SELECT DISTINCT [JOURNALID]" +
                              ",[LINENUM]" +
                              ",[ITEMID]" +
                              ",[DOT_PARTCODE]" +
                              ",[QTY]" +
                              ",[InventUnitID]" +
                              ",[Warehouse]" +
                              ",[INVENTSITEID]" +
                              ",[LocationID]" +
                              ",[INVENTTRANSID]" +
                              ",[PRODUCT_NAME]" +
                                "FROM [AX_WMS_COUNTINGJOURNALLINE] " +
                            "WHERE [JOURNALID] = @keyAXCJ_JOURNALID " +
                              "AND AX_WMS_COUNTINGJOURNALLINE.MODIFIEDDATETIME > '" + lastSyncDatetime + "'", axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXCJ_JOURNALID", tempAXCJ_JOURNALID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axCJDetailDt = new DataTable();
                            //AX CJ Detail in AX DB View
                            sda.Fill(axCJDetailDt);

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)tempAXCJ_JOURNALID}
                            };
                            //AX CJ Detail in WMS DB
                            DataTable curAXCJDetailDt = curDb.getDataTable("selectAXCJDetailByAXJournalId", curQueryParameter);

                            Boolean isCJStatusUpdated = false; //to make CJ status change to pending if there is at least one new line added
                            //Loop through each of the AXCJ Detail
                            for (int j = 0; j < axCJDetailDt.Rows.Count; j++)
                            {
                                String tempAXCJD_JOURNALID = axCJDetailDt.Rows[j][0].ToString();
                                String tempAXCJD_LINENUM = axCJDetailDt.Rows[j][1].ToString();
                                String tempAXCJD_ITEMID = axCJDetailDt.Rows[j][2].ToString();
                                String tempAXCJD_DOT_PARTCODE = axCJDetailDt.Rows[j][3].ToString();
                                String tempAXCJD_QTY = axCJDetailDt.Rows[j][4].ToString();
                                String tempAXCJD_INVENTUNITID = axCJDetailDt.Rows[j][5].ToString();
                                String tempAXCJD_INVENTLOCATIONID = axCJDetailDt.Rows[j][6].ToString();
                                String tempAXCJD_INVENTSITEID = axCJDetailDt.Rows[j][7].ToString();
                                String tempAXCJD_WMSLOCATIONID = axCJDetailDt.Rows[j][8].ToString();
                                String tempAXCJD_INVENTTRANSID = axCJDetailDt.Rows[j][9].ToString();
                                String tempAXCJD_PRODUCT_NAME = axCJDetailDt.Rows[j][10].ToString();

                                Boolean cjDetailExisted = false;
                                for (int k = 0; k < curAXCJDetailDt.Rows.Count; k++)
                                {
                                    String tempWMSCJD_id = curAXCJDetailDt.Rows[k][0].ToString();
                                    String tempWMSCJD_JOURNALID = curAXCJDetailDt.Rows[k][2].ToString();
                                    String tempWMSCJD_LINENUM = curAXCJDetailDt.Rows[k][3].ToString();
                                    String tempWMSCJD_ITEMID = curAXCJDetailDt.Rows[k][4].ToString();
                                    String tempWMSCJD_DOT_PARTCODE = curAXCJDetailDt.Rows[k][5].ToString();
                                    String tempWMSCJD_scanQuantity = String.IsNullOrEmpty(curAXCJDetailDt.Rows[k][11].ToString()) ? "0" : curAXCJDetailDt.Rows[k][11].ToString();

                                    //    //If AX CJ Detail Existed in WMS DB
                                    if (tempAXCJD_JOURNALID.Trim() == tempWMSCJD_JOURNALID.Trim() &&
                                        tempAXCJD_ITEMID.Trim() == tempWMSCJD_ITEMID.Trim() &&
                                        tempAXCJD_DOT_PARTCODE.Trim() == tempWMSCJD_DOT_PARTCODE.Trim() &&
                                        (Convert.ToDecimal(tempAXCJD_LINENUM) == Convert.ToDecimal(tempWMSCJD_LINENUM)))
                                    {
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\
                                        //Update quantity changes
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                                        queryResult = setAXCJDetail(curDb, tempAXCJD_JOURNALID, tempAXCJD_LINENUM,
                                        tempAXCJD_ITEMID, tempAXCJD_DOT_PARTCODE, tempAXCJD_QTY, tempAXCJD_INVENTUNITID,
                                        tempAXCJD_INVENTLOCATIONID, tempAXCJD_INVENTSITEID, tempAXCJD_WMSLOCATIONID, tempAXCJD_INVENTTRANSID,
                                        tempAXCJD_PRODUCT_NAME);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("update ax aj detail failed");
                                        }

                                        Int64 curStatusId = 7;
                                        if (Convert.ToDecimal(tempWMSCJD_scanQuantity) == 0)
                                        {
                                            curStatusId = 1;
                                        }
                                        else if (Convert.ToDecimal(tempAXCJD_QTY) > Convert.ToDecimal(tempWMSCJD_scanQuantity))
                                        {
                                            curStatusId = 7;
                                        }

                                        curQueryParameter = new List<sqlQueryParameter>()
                                        {
                                                new sqlQueryParameter {strParameter="@keyTableName", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)"tbl_ax_poDetail"},
                                                new sqlQueryParameter {strParameter="@keyFieldName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"tbl_status_Id"},
                                                new sqlQueryParameter {strParameter="@keyFieldValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)curStatusId.ToString()},
                                                new sqlQueryParameter {strParameter="@keyIDName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"id"},
                                                new sqlQueryParameter {strParameter="@keyIDValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)tempWMSCJD_id}
                                            };
                                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateTableSingleField", curQueryParameter);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("Update AX Purchase Order Detail Status Failed");
                                        }

                                        queryResult = insertMovementHistory(curDb, tempAXCJ_DATAAREAID, tempAXCJD_JOURNALID, "", tempAXCJD_ITEMID, tempAXCJD_DOT_PARTCODE,
                                            tempAXCJD_INVENTLOCATIONID, tempAXCJD_INVENTSITEID, tempAXCJD_WMSLOCATIONID, "", "", "", tempAXCJD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_CJ_DETAIL_UPDATE, tempAXCJD_LINENUM);
                                        if (queryResult == -99)
                                        {
                                            throw new Exception("insert movement history failed");
                                        }

                                        cjDetailExisted = true;
                                        break;
                                    }
                                }

                                ////if AX CJ Detail no existed in WMS DB
                                if (!cjDetailExisted)
                                {
                                    queryResult = insertAXCJDetail(curDb, axcjId.ToString(), tempAXCJD_JOURNALID, tempAXCJD_LINENUM,
                                    tempAXCJD_ITEMID, tempAXCJD_DOT_PARTCODE, tempAXCJD_QTY, tempAXCJD_INVENTUNITID,
                                    tempAXCJD_INVENTLOCATIONID, tempAXCJD_INVENTSITEID, tempAXCJD_WMSLOCATIONID, tempAXCJD_INVENTTRANSID,
                                    tempAXCJD_PRODUCT_NAME);
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert ax cj detail failed");
                                    }

                                    queryResult = insertMovementHistory(curDb, tempAXCJ_DATAAREAID, tempAXCJD_JOURNALID, "", tempAXCJD_ITEMID, tempAXCJD_DOT_PARTCODE,
                                            tempAXCJD_INVENTLOCATIONID, tempAXCJD_INVENTSITEID, tempAXCJD_WMSLOCATIONID, "", "", "", tempAXCJD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_CJ_DETAIL_INSERT, tempAXCJD_LINENUM);
                                    if (queryResult == -99)
                                    {
                                        throw new Exception("insert movement history failed");
                                    }
                                    
                                }
                            }

                        }
                        //++++++++++++++++++++++++++++++
                        //if AX CJ no existed in WMS DB
                        //++++++++++++++++++++++++++++++
                        else
                        {
                            axCommand = new SqlCommand(
                            "SELECT DISTINCT [JOURNALID]" +
                              ",[LINENUM]" +
                              ",[ITEMID]" +
                              ",[DOT_PARTCODE]" +
                              ",[QTY]" +
                              ",[InventUnitID]" +
                              ",[Warehouse]" +
                              ",[INVENTSITEID]" +
                              ",[LocationID]" +
                              ",[INVENTTRANSID]" +
                              ",[PRODUCT_NAME]" +
                                "FROM [AX_WMS_COUNTINGJOURNALLINE] " +
                            "WHERE [JOURNALID] = @keyAXCJ_JOURNALID " , axConn);
                            axCommand.Parameters.Add(new SqlParameter("@keyAXCJ_JOURNALID", tempAXCJ_JOURNALID));
                            axCommand.CommandTimeout = 1800;

                            sda.SelectCommand = axCommand;
                            DataTable axCJDetailDt = new DataTable();
                            //AX CJ Detail in AX DB View
                            sda.Fill(axCJDetailDt);

                            curQueryParameter = new List<sqlQueryParameter>()
                            {
                            };

                            curDt = curDb.getDataTable("[selectNextAXCJId]", curQueryParameter);

                            List<Int64> list = new List<Int64>();
                            for (int j = 0; j < curDt.Rows.Count; j++)
                            {
                                list.Add(Convert.ToInt64(curDt.Rows[j][0]));
                            }
                            axcjId = list[0];

                            curQueryParameter = new List<sqlQueryParameter>() {
                                new sqlQueryParameter {strParameter="@keyAXCJ_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axcjId)},
                                new sqlQueryParameter {strParameter="@keyAXCJ_DATAAREAID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)tempAXCJ_DATAAREAID},
                                new sqlQueryParameter {strParameter="@keyAXCJ_POSTEDDATETIME", ParameterType=SqlDbType.DateTime, keyData = (Object)tempAXCJ_POSTEDDATETIME},
                                new sqlQueryParameter {strParameter="@keyAXCJ_JOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXCJ_JOURNALID},
                                new sqlQueryParameter {strParameter="@keyAXCJ_DESCRIPTION", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXCJ_DESCRIPTION},
                                new sqlQueryParameter {strParameter="@keyReferenceNo", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)""},
                                new sqlQueryParameter {strParameter="@keyCreationDateTime", ParameterType=SqlDbType.DateTime, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyDocType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"cj"},
                                new sqlQueryParameter {strParameter="@keyReferenceVersion", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                            };
                            queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXCJ", curQueryParameter);
                            if (queryResult == -99)
                            {
                                throw new Exception("insert ax counting journal failed");
                            }

                            queryResult = insertMovementHistory(curDb, tempAXCJ_DATAAREAID, tempAXCJ_JOURNALID, "", "", "", "", "", "", "", "", "", "", "-1", "SYSTEM", sharedRes.AX2LGC_CJ_INSERT, "");
                            if (queryResult == -99)
                            {
                                throw new Exception("insert movement history failed");
                            }

                            //Loop through each of the AXCJ Detail
                            for (int j = 0; j < axCJDetailDt.Rows.Count; j++)
                            {
                                String tempAXCJD_JOURNALID = axCJDetailDt.Rows[j][0].ToString();
                                String tempAXCJD_LINENUM = axCJDetailDt.Rows[j][1].ToString();
                                String tempAXCJD_ITEMID = axCJDetailDt.Rows[j][2].ToString();
                                String tempAXCJD_DOT_PARTCODE = axCJDetailDt.Rows[j][3].ToString();
                                String tempAXCJD_QTY = axCJDetailDt.Rows[j][4].ToString();
                                String tempAXCJD_INVENTUNITID = axCJDetailDt.Rows[j][5].ToString();
                                String tempAXCJD_INVENTLOCATIONID = axCJDetailDt.Rows[j][6].ToString();
                                String tempAXCJD_INVENTSITEID = axCJDetailDt.Rows[j][7].ToString();
                                String tempAXCJD_WMSLOCATIONID = axCJDetailDt.Rows[j][8].ToString();
                                String tempAXCJD_INVENTTRANSID = axCJDetailDt.Rows[j][9].ToString();
                                String tempAXCJD_PRODUCT_NAME = axCJDetailDt.Rows[j][10].ToString();

                                queryResult = insertAXCJDetail(curDb, axcjId.ToString(), tempAXCJD_JOURNALID,
                                        tempAXCJD_LINENUM, tempAXCJD_ITEMID, tempAXCJD_DOT_PARTCODE, tempAXCJD_QTY,
                                            tempAXCJD_INVENTUNITID, tempAXCJD_INVENTLOCATIONID,
                                            tempAXCJD_INVENTSITEID, tempAXCJD_WMSLOCATIONID, tempAXCJD_INVENTTRANSID,
                                            tempAXCJD_PRODUCT_NAME);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert ax cj detail failed");
                                }

                                queryResult = insertMovementHistory(curDb, tempAXCJ_DATAAREAID, tempAXCJD_JOURNALID, "", tempAXCJD_ITEMID, tempAXCJD_DOT_PARTCODE,
                                        tempAXCJD_INVENTLOCATIONID, tempAXCJD_INVENTSITEID, tempAXCJD_WMSLOCATIONID, "", "", "", tempAXCJD_QTY, "-1", "SYSTEM", sharedRes.AX2LGC_CJ_DETAIL_INSERT, tempAXCJD_LINENUM);
                                if (queryResult == -99)
                                {
                                    throw new Exception("insert movement history failed");
                                }
                            }
                        }
                    }
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"LAST CJ SYNC"},
                        new sqlQueryParameter {strParameter="@keyDataValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)currentSyncDatetime}
                    };

                    queryResult = curDb.insertUpdateDeleteDataIntoTable("updateSystemData", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert/update/delete failed");
                    }
                    
                    isSuccess = "true";
                }
                catch (Exception e)
                {
                    curDb.rollbackNCloseConnection();
                    isSuccess = e.Message;
                    return isSuccess;
                }
                if (!curDb.checkIsConnectionClosed())
                    curDb.closeConnection();
                return isSuccess;
            }
        }

        private int insertAXCJDetail(database curDb, String axcjId, String tempAXCJD_JOURNALID,
            String tempAXCJD_LINENUM, String tempAXCJD_ITEMID, String tempAXCJD_DOT_PARTCODE,
            String tempAXCJD_QTY, String tempAXCJD_INVENTUNITID, String tempAXCJD_INVENTLOCATIONID,
            String tempAXCJD_INVENTSITEID, String tempAXCJD_WMSLOCATIONID, String tempAXCJD_INVENTTRANSID,
            String tempAXCJD_PRODUCT_NAME)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };

                DataTable curDt = curDb.getDataTable("[selectNextAXCJDetailId]", curQueryParameter);

                List<Int64> list = new List<Int64>();
                for (int k = 0; k < curDt.Rows.Count; k++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[k][0]));
                }
                Int64 axcjdId = list[0];

                curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXCJD_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axcjdId)},
                                    new sqlQueryParameter {strParameter="@keyAXCJ_Id", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(axcjId)},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_JOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXCJD_JOURNALID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_LINENUMBER", ParameterType=SqlDbType.Decimal, keyData = tempAXCJD_LINENUM == "" ? (Object)DBNull.Value : Convert.ToDecimal(tempAXCJD_LINENUM)},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXCJD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXCJD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_QTY", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXCJD_QTY)},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_INVENTUNITID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXCJD_INVENTUNITID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXCJD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXCJD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXCJD_WMSLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Int, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(1)},
                                    new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DBNull.Value},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_INVENTTRANSID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXCJD_INVENTTRANSID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_PRODUCT_NAME", ParameterType=SqlDbType.NVarChar, ParameterLength=60, keyData = (Object)tempAXCJD_PRODUCT_NAME}
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewAXCJDetail", curQueryParameter);
            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }


        private int setAXCJDetail(database curDb, String tempAXCJD_JOURNALID,
            String tempAXCJD_LINENUM, String tempAXCJD_ITEMID, String tempAXCJD_DOT_PARTCODE,
            String tempAXCJD_QTY, String tempAXCJD_INVENTUNITID, String tempAXCJD_INVENTLOCATIONID,
            String tempAXCJD_INVENTSITEID, String tempAXCJD_WMSLOCATIONID, String tempAXCJD_INVENTTRANSID,
            String tempAXCJD_PRODUCT_NAME)
        {
            int queryResult = -99;
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                                    new sqlQueryParameter {strParameter="@keyAXCJD_JOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXCJD_JOURNALID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_LINENUMBER", ParameterType=SqlDbType.Decimal, keyData = tempAXCJD_LINENUM == "" ? (Object)DBNull.Value : Convert.ToDecimal(tempAXCJD_LINENUM)},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_ITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)tempAXCJD_ITEMID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_DOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)tempAXCJD_DOT_PARTCODE},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_QTY", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(tempAXCJD_QTY)},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_INVENTUNITID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXCJD_INVENTUNITID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_INVENTLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)tempAXCJD_INVENTLOCATIONID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_INVENTSITEID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)tempAXCJD_INVENTSITEID},
                                    new sqlQueryParameter {strParameter="@keyAXCJD_WMSLOCATIONID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)tempAXCJD_WMSLOCATIONID},
                                };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXCJDetailByAXLineNum_JournalId_ItemId_PartCode", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }


        //AX Counting Journal
        //+++++++++++++++++++++++++++++++++++++

        //[WebMethod(EnableSession = true)]
        //public void getAllAXPRInfo(String page, String per_page)
        //{
        //    Int32 total_number_page = 1;
        //    Int32 cur_start_item = 1;
        //    Int32 cur_end_item = 1;
        //    DataTable splitDt = new DataTable();

        //    String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
        //    String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];

        //    String searchHeader = "";
        //    String searchContent = "";
        //    String searchContent2 = "";
        //    String searchType = HttpContext.Current.Request.QueryString["filterType"];
        //    String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');
        //    searchHeader = searchArray[0];
        //    if (searchArray.Length == 3)
        //    {
        //        searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
        //        searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
        //    }
        //    else
        //    {
        //        searchContent = searchArray[1] == "" ? "%" : searchArray[1];
        //    }

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
        //        new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
        //        new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
        //        new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchType},
        //        new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
        //        new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent},
        //        new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent2},
        //        new sqlQueryParameter {strParameter="@keySearchLocationId", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)Session["curSessionWarehouseId"]}
        //    };

        //    database curDb = new database();
        //    curDb.openConnection();
        //    DataTable curDt = curDb.getDataTable("selectAllAXPRInfo", curQueryParameter);
        //    curDb.closeConnection();

        //    if (curDt.Rows.Count != 0)
        //    {
        //        splitDt = splitDataTable(curDt, page, per_page);
        //        total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
        //        cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
        //        cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
        //    }

        //    Context.Response.ContentType = "text/plain";
        //    //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
        //    Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
        //    Context.Response.Write(DataTableToJPRNWithStringBuilder(splitDt));
        //    Context.Response.Write("}");

        //}

        [WebMethod(EnableSession = true)]
        public void getAllAXCJInfo(String page, String per_page)
        {

            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];

            String searchHeader = "";
            String searchContent = "";
            String searchContent2 = "";
            String searchType = HttpContext.Current.Request.QueryString["filterType"];
            String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');
            String warehouseCode = HttpContext.Current.Request.QueryString["warehouseCode"].ToString();

            searchHeader = searchArray[0];
            if (searchArray.Length == 3)
            {
                searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
                searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
            }
            else
            {
                searchContent = searchArray[1] == "" ? "%" : searchArray[1];
            }

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchType},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent},
                new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent2},
                new sqlQueryParameter {strParameter="@keySearchWarehouseCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)warehouseCode}
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllAXCJInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");

        }

        [WebMethod]
        public axCJObject[] getAllPendingAXCJ()
        {

            axCJObject[] curList = null;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            try
            {
                if (!(retrieveAllAXCountingJournal() == "true"))
                    throw new Exception("Failed to retrieve AX Counting Journal");

                DataTable curDt = curDb.getDataTable("[selectAllPendingAXCJ]", curQueryParameter);

                List<axCJObject> list = new List<axCJObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new axCJObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        , curDt.Rows[i][4].ToString()
                        , curDt.Rows[i][5].ToString()
                        , curDt.Rows[i][6].ToString()
                        , curDt.Rows[i][7].ToString()
                        , curDt.Rows[i][8].ToString()
                        , curDt.Rows[i][9].ToString()
                        , curDt.Rows[i][10].ToString()
                        , curDt.Rows[i][11].ToString()
                        , curDt.Rows[i][12].ToString()
                        , curDt.Rows[i][13].ToString()
                        , curDt.Rows[i][14].ToString()
                        ));
                }

                curList = list.ToArray();
            }
            catch (Exception e)
            {
                //curDb.rollbackNCloseConnection();
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axCJObject[] getAXCJByAXJournalId(String argAXJOURNALId)
        {
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXJOURNALId", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)argAXJOURNALId},
            };

            DataTable curDt = curDb.getDataTable("[selectAXCJByAXJournalId]", curQueryParameter);

            List<axCJObject> list = new List<axCJObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axCJObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    ));
            }

            axCJObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axCJDetailObject[] getAllPendingAXCJDetail()
        {
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectAllPendingAXCJDetail]", curQueryParameter);

            List<axCJDetailObject> list = new List<axCJDetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axCJDetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    ));
            }

            axCJDetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public axCJDetailObject[] getAXCJDetailByAXJournalId(String argAXJOURNALId)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyAXJOURNALId", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)argAXJOURNALId},
            };

            DataTable curDt = curDb.getDataTable("[selectAXCJDetailByAXJournalId]", curQueryParameter);

            List<axCJDetailObject> list = new List<axCJDetailObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new axCJDetailObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    , curDt.Rows[i][16].ToString()
                    , curDt.Rows[i][17].ToString()
                    , curDt.Rows[i][18].ToString()
                    ));
            }

            axCJDetailObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public String setAXCJDetailScanInput(List<AXCJDetailScanInput> aXCJDetailScanInputList)
        {
            String isSuccess = "AX cjD Update failed";
            int queryResult = -99;
            database curDb = new database();
            curDb.openConnection();
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            String xmlMessage = "";
            String messageId = getAXMessageId();
            String isDevelopmentMode = "0";

            try
            {
                xmlMessage = "<?xml version =\"1.0\" encoding=\"UTF-8\"?>\r\n" +
                    "<CJRNo>\r\n";
                int counter = 0;

                foreach (var aXCJDetailScanInput in aXCJDetailScanInputList)
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                        new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(aXCJDetailScanInput.argAXLINENUMBER)},
                        new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXCJDetailScanInput.argAXJOURNALID},
                        new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXCJDetailScanInput.argAXITEMID},
                        new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXCJDetailScanInput.argAXDOT_PARTCODE},
                    };

                    curDt = curDb.getDataTable("[selectAXCJDetailByAXLineNum_JournalId_ItemId_PartCode]", curQueryParameter);

                    if (curDt.Rows.Count > 0)
                    {
                        String tempAXCJD_JOURNALID = curDt.Rows[0][2].ToString();
                        String tempAXCJD_LINENUM = curDt.Rows[0][3].ToString();
                        String tempAXCJD_ITEMID = curDt.Rows[0][4].ToString();
                        String tempAXCJD_DOT_PARTCODE = curDt.Rows[0][5].ToString();
                        String tempAXCJD_QTY = curDt.Rows[0][6].ToString();
                        String tempAXCJD_INVENTUNITID = curDt.Rows[0][7].ToString();
                        String tempAXCJD_INVENTLOCATIONID = curDt.Rows[0][8].ToString();
                        String tempAXCJD_INVENTSITEID = curDt.Rows[0][9].ToString();
                        String tempAXCJD_WMSLOCATIONID = curDt.Rows[0][10].ToString();
                        String tempAXCJD_INVENTTRANSID = curDt.Rows[0][18].ToString();

                        //if (Convert.ToDecimal(tempAXCJD_QTY) < Convert.ToDecimal(aXCJDetailScanInput.argScanQuantity)) {
                        //    throw new Exception("Update Failed - " + aXCJDetailScanInput.argAXITEMID +
                        //        " - Scan Quantity[" + aXCJDetailScanInput.argScanQuantity + "] more then Quantity [" + tempAXCJD_QTY + "]");
                        //}
                        Int64 curStatusId = 7;
                        if (Convert.ToDecimal(tempAXCJD_QTY) == Convert.ToDecimal(aXCJDetailScanInput.argScanQuantity))
                        {
                            curStatusId = 8;
                        }


                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXLINENUM", ParameterType=SqlDbType.Decimal, keyData = Convert.ToDecimal(aXCJDetailScanInput.argAXLINENUMBER)},
                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXCJDetailScanInput.argAXJOURNALID},
                            new sqlQueryParameter {strParameter="@keyAXITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)aXCJDetailScanInput.argAXITEMID},
                            new sqlQueryParameter {strParameter="@keyAXDOT_PARTCODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)aXCJDetailScanInput.argAXDOT_PARTCODE},
                            new sqlQueryParameter {strParameter="@keyScanQuantity", ParameterType=SqlDbType.Decimal, keyData =  Convert.ToDecimal(aXCJDetailScanInput.argScanQuantity)},
                            new sqlQueryParameter {strParameter="@keyScanDatetime", ParameterType=SqlDbType.DateTime, keyData =  (Object)(aXCJDetailScanInput.argScanDatetime)},
                            new sqlQueryParameter {strParameter="@keyRemarks", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData =  (Object)aXCJDetailScanInput.argRemarks},
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(aXCJDetailScanInput.argUserId) },
                            new sqlQueryParameter {strParameter="@keyDeviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)aXCJDetailScanInput.argDeviceSN},
                            new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData =  Convert.ToInt64(curStatusId) },
                        };

                        queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXCJDetailScanInputByAXLineNum_JournalId_ItemId_PartCode", curQueryParameter);
                        if (queryResult == -99)
                        {
                            throw new Exception("Update AX CJD Failed");
                        }

                        curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar,ParameterLength=20, keyData = (Object)aXCJDetailScanInput.argAXJOURNALID}
                        };
                        curDt = curDb.getDataTable("selectAXCJByAXJournalId", curQueryParameter);

                        String tempAXCJ_DATAAREAID = "";
                        if (curDt.Rows.Count > 0)
                        {
                            tempAXCJ_DATAAREAID = curDt.Rows[0][1].ToString();
                        }

                        if (counter == 0)
                        {
                            xmlMessage += "<CJR>\r\n" +
                                             "<Company_Code>" + tempAXCJ_DATAAREAID + "</Company_Code>\r\n" +
                                             "<Journal_Id>" + tempAXCJD_JOURNALID + "</Journal_Id>\r\n" +
                                             "</CJR>\r\n";
                        }
                        counter++;
                        xmlMessage += "<CJR_DETAIL>\r\n" +
                            "<ITEM_CODE>" + tempAXCJD_ITEMID + "</ITEM_CODE>\r\n" +
                            "<InventTrans_Id>" + tempAXCJD_INVENTTRANSID + "</InventTrans_Id>\r\n" +
                            "<Invent_Site>" + tempAXCJD_INVENTSITEID + "</Invent_Site>\r\n" +
                            "<Invent_Warehouse>" + tempAXCJD_INVENTLOCATIONID + "</Invent_Warehouse>\r\n" +
                            "<Invent_Location>" + tempAXCJD_WMSLOCATIONID + "</Invent_Location>\r\n" +
                            "<PART_CODE>" + tempAXCJD_DOT_PARTCODE + "</PART_CODE>\r\n" +
                            "<Counted_QTY>" + aXCJDetailScanInput.argScanQuantity + "</Counted_QTY>\r\n" +
                            "</CJR_DETAIL>\r\n";

                        queryResult = insertMovementHistory(curDb, tempAXCJ_DATAAREAID, tempAXCJD_JOURNALID,
                            tempAXCJD_INVENTTRANSID, tempAXCJD_ITEMID, tempAXCJD_DOT_PARTCODE,
                            tempAXCJD_INVENTLOCATIONID, tempAXCJD_INVENTSITEID, tempAXCJD_WMSLOCATIONID,
                            "", "", "", aXCJDetailScanInput.argScanQuantity, aXCJDetailScanInput.argUserId, aXCJDetailScanInput.argDeviceSN, sharedRes.LGC2AX_CJ_DETAIL_UPDATE,
                            aXCJDetailScanInput.argAXLINENUMBER);
                        if (queryResult == -99)
                        {
                            throw new Exception("insert movement history failed");
                        }

                    }
                    else
                    {
                        //Temporarily skip for those line is not existed in WMS DB
                        //continue;
                        throw new Exception("Update Failed - " + aXCJDetailScanInput.argAXITEMID + " does not found in CJ - " + aXCJDetailScanInput.argAXJOURNALID);
                    }
                }

                xmlMessage += "</CJRNo>";


                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData =  (Object)aXCJDetailScanInputList.First().argAXJOURNALID },
                        };

                curDt = curDb.getDataTable("[checkExistenceAXCJDetailWRemainingQuantityByAXJournalId]", curQueryParameter);
                ////No more pending line for this AX CJ
                //if (curDt.Rows.Count <= 0)
                //{
                curQueryParameter = new List<sqlQueryParameter>()
                            {
                                new sqlQueryParameter {strParameter="@keyAXJOURNALID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)aXCJDetailScanInputList.First().argAXJOURNALID},
                                new sqlQueryParameter {strParameter="@keyCompletionDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                                new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(9)},
                            };
                queryResult = curDb.insertUpdateDeleteDataIntoTable("updateAXAJCompletedByAXJournalId", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("Update AX Counting Journal Detail Status Failed");
                }
                //}


                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)"DEVELOPMENT MODE"},
                };


                curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);
                isDevelopmentMode = curDt.Rows[0][2].ToString();

                if (isDevelopmentMode == "0")
                {
                    CJPostService.WMS_CountingJournalServiceClient cjPostObject = new CJPostService.WMS_CountingJournalServiceClient();

                    //cjPostObject.ClientCredentials.UserName.UserName = @"AWSBC01V_UAT\BCAdmin";
                    //cjPostObject.ClientCredentials.UserName.Password = "SKtd78)s";

                    CJPostService.CallContext context = new CJPostService.CallContext();
                    //context.LogonAsUser = @"GOLDBELL\itadmin2";

                    context.MessageId = messageId; // "{33000000-0000-0000-0000-000000000012}";

                    String result = "";
                    try
                    {
                        result = cjPostObject.ReadXMLFile(context, xmlMessage);
                    }
                    catch (Exception axEx)
                    {
                        throw new Exception("Failed to post data to AX - " + axEx.Message + " " + messageId);
                    }

                    Boolean isAXPostSuccess = false;
                    if (!String.IsNullOrEmpty(result))
                    {
                        string[] words = result.Split(' ');
                        if (words[0].Equals("Successfully", StringComparison.OrdinalIgnoreCase))
                        {
                            isAXPostSuccess = true;
                        }
                    }

                    if (!isAXPostSuccess)
                    {
                        throw new Exception("Failed to post data to AX - " + result + " " + messageId);

                        //String axExceptionMsg = retrieveAXExceptionMsg(messageId);
                        //if (axExceptionMsg == "")
                        //{
                        //    throw new Exception("Failed to post data to AX - " + result);
                        //}
                        //else
                        //{
                        //    throw new Exception("Failed to post data to AX - " + axExceptionMsg);
                        //}
                    }

                }
                //else
                //{
                //    throw new Exception("XML Generated");
                //}

                isSuccess = "true";

            }
            catch (Exception e)
            {
                insertExceptionMsg(aXCJDetailScanInputList[0].argAXJOURNALID, aXCJDetailScanInputList[0].argUserId,
                    aXCJDetailScanInputList[0].argDeviceSN, "CJ", e.Message, messageId);
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
                try
                {
                    string pathString = XML_FOLDER_PATH;

                    pathString = System.IO.Path.Combine(pathString, "CJ");
                    bool exists = System.IO.Directory.Exists(pathString);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(pathString);

                    string fileName = "CJ_" + messageId + ".xml";
                    pathString = System.IO.Path.Combine(pathString, fileName);

                    if (!System.IO.File.Exists(pathString))
                    {
                        using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(pathString, true))
                        {
                            file.WriteLine(xmlMessage);
                        }
                    }


                }
                catch (Exception e2)
                {
                }
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        //AX Common shared
        //+++++++++++++++++++++++++++++++++++++


        private int insertMovementHistory(database curDb, String argAXDATAREADID, String argAXDOCUMENTID,
            String argAXSubDOCUMENTID, String argAXITEMID, String argAXPART_CODE, String argAXINVENTLOCATION_FROMID,
            String argAXINVENTSITE_FROMID, String argAXWMSLOCATION_FROMID, String argAXINVENTLOCATION_TOID,
            String argAXINVENTSITE_TOID, String argAXWMSLOCATION_TOID, String argQty,
            String argUser_Id, String argDeviceSN, String argMovementType, String argAXLINENUM
            )
        {
            int queryResult = -99;
            try
            {
                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@axDATAREADID", ParameterType=SqlDbType.NVarChar, ParameterLength=4, keyData = (Object)argAXDATAREADID},
                            new sqlQueryParameter {strParameter="@axDOCUMENTID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)argAXDOCUMENTID},
                            new sqlQueryParameter {strParameter="@axSubDOCUMENTID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)argAXSubDOCUMENTID},
                            new sqlQueryParameter {strParameter="@axITEMID", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)argAXITEMID},
                            new sqlQueryParameter {strParameter="@axPART_CODE", ParameterType=SqlDbType.NVarChar, ParameterLength=80, keyData = (Object)argAXPART_CODE},
                            new sqlQueryParameter {strParameter="@axINVENTLOCATION_FROMID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)argAXINVENTLOCATION_FROMID},
                            new sqlQueryParameter {strParameter="@axINVENTSITE_FROMID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)argAXINVENTSITE_FROMID},
                            new sqlQueryParameter {strParameter="@axWMSLOCATION_FROMID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)argAXWMSLOCATION_FROMID},
                            new sqlQueryParameter {strParameter="@axINVENTLOCATION_TOID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)argAXINVENTLOCATION_TOID},
                            new sqlQueryParameter {strParameter="@axINVENTSITE_TOID", ParameterType=SqlDbType.NVarChar, ParameterLength=10, keyData = (Object)argAXINVENTSITE_TOID },
                            new sqlQueryParameter {strParameter="@axWMSLOCATION_TOID", ParameterType=SqlDbType.NVarChar, ParameterLength=25, keyData = (Object)argAXWMSLOCATION_TOID},
                            new sqlQueryParameter {strParameter="@qty", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = String.IsNullOrEmpty(argQty) ? '0':(Object)(argQty)},
                            new sqlQueryParameter {strParameter="@movementDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                            new sqlQueryParameter {strParameter="@tbl_user_Id", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argUser_Id)},
                            new sqlQueryParameter {strParameter="@deviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argDeviceSN},
                            new sqlQueryParameter {strParameter="@movementType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argMovementType},
                            new sqlQueryParameter {strParameter="@axLINENUM", ParameterType=SqlDbType.Decimal, keyData = String.IsNullOrEmpty(argAXLINENUM) ? (Object)DBNull.Value: Convert.ToDecimal(argAXLINENUM)},
                        };

                queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewMovementHistory", curQueryParameter);

            }
            catch (Exception e)
            {
                queryResult = -99;
            }

            return queryResult;
        }

        private String insertExceptionMsg(String argAXDOCUMENTID, String argUser_Id, String argDeviceSN,
            String argMovementType, String argExceptionMsg, String strMessageId)
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "Exception Message insert failed";

            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@axDOCUMENTID", ParameterType=SqlDbType.NVarChar, ParameterLength=20, keyData = (Object)argAXDOCUMENTID},
                            new sqlQueryParameter {strParameter="@creationDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                            new sqlQueryParameter {strParameter="@tbl_user_Id", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argUser_Id)},
                            new sqlQueryParameter {strParameter="@deviceSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argDeviceSN},
                            new sqlQueryParameter {strParameter="@docType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argMovementType},
                            new sqlQueryParameter {strParameter="@exceptionMsg", ParameterType=SqlDbType.NVarChar, ParameterLength=255, keyData = (Object)argExceptionMsg},
                            new sqlQueryParameter {strParameter="@messageId", ParameterType=SqlDbType.NVarChar, ParameterLength=50, keyData = (Object)strMessageId},
                        };

                int queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewExceptionMsg", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;

        }

        [WebMethod]
        public String retrieveAXExceptionMsg(String strMessageId)
        {
            strMessageId = strMessageId.Substring(1, strMessageId.Length - 2);
            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                try
                {
                    axConn.Open();

                    //Get AX Exception from AX DB View
                    SqlCommand axCommand = new SqlCommand(
                        "SELECT [MESSAGEID] " +
                          ",[EXCEPTIONID] " +
                          ",[PORTNAME] " +
                          ",[DESCRIPTION] " +
                          ",[MODULE] " +
                          ",[SUBSYSTEM] " +
                          ",[ACTION] " +
                          ",[CREATEDDATETIME] " +
                          ",[CREATEDBY] " +
                            "FROM  [AX_WMS_AIFSERVICEEXCEPTIONLOG] " +
                        "WHERE [MESSAGEID] = @keyAX_MESSAGEID", axConn);
                    axCommand.Parameters.Add(new SqlParameter("@keyAX_MESSAGEID", strMessageId));
                    axCommand.CommandTimeout = 1800;

                    //command.ExecuteNonQuery();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = axCommand;
                    //AX CJ from AX DB View
                    DataTable axExceptionMsgDt = new DataTable();

                    int noOfAXExceptionMsg = sda.Fill(axExceptionMsgDt);
                    String tempMsg = "";
                    if (noOfAXExceptionMsg == 3)
                    {
                        //get the 2nd msg
                        tempMsg = axExceptionMsgDt.Rows[1][3].ToString();
                    }
                    else if (noOfAXExceptionMsg == 2 || noOfAXExceptionMsg == 1)
                    {
                        //get the 1st msg
                        tempMsg = axExceptionMsgDt.Rows[0][3].ToString();
                    }
                    else if (noOfAXExceptionMsg > 3)
                    {
                        for (int i = 0; i < axExceptionMsgDt.Rows.Count; i++)
                        {
                            String tempAXEXCEPTION_DESC = axExceptionMsgDt.Rows[i][3].ToString();
                            if (i > 0) tempMsg += " ";
                            tempMsg += tempAXEXCEPTION_DESC;
                        }
                    }

                    return tempMsg;

                }
                catch (Exception ex)
                {
                    return "Failed to retrieve AX Exception - " + ex.Message;
                }
            }
        }

        [WebMethod]
        public void getAllAXExceptionMsgInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];

            String searchHeader = "";
            String searchContent = "";
            String searchContent2 = "";
            String searchType = HttpContext.Current.Request.QueryString["filterType"];
            String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');

            searchHeader = searchArray[0];
            if (searchArray.Length == 3)
            {
                searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
                searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
            }
            else
            {
                searchContent = searchArray[1] == "" ? "%" : searchArray[1];
            }

            //List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
            //    new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
            //    new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
            //    new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchType},
            //    new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
            //    new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent},
            //    new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent2}
            //};

            String sqlCond3 = "";
            if (searchType == "DATE")
            {
                sqlCond3 = "WHERE " + searchHeader + " BETWEEN '" + searchContent + "' AND '" + searchContent2 + "' ";
            }
            else
            {
                sqlCond3 = "WHERE " + searchHeader + " LIKE '%" + searchContent + "%' ";
            }

            DataTable curDt = new DataTable();

            using (SqlConnection axConn = new SqlConnection(axConnectionString))
            {
                axConn.Open();

                //Get AX Exception from AX DB View
                SqlCommand axCommand = new SqlCommand(
                    "SELECT [MESSAGEID] " +
                      ",[EXCEPTIONID] " +
                      ",[PORTNAME] " +
                      ",[DESCRIPTION] " +
                      ",[MODULE] " +
                      ",[SUBSYSTEM] " +
                      ",[ACTION] " +
                      ",[CREATEDDATETIME] " +
                      ",[CREATEDBY] " +
                        "FROM  [AX_WMS_AIFSERVICEEXCEPTIONLOG] " +
                        sqlCond3 +
                    "ORDER BY " + header + " " + content, axConn);
                //axCommand.Parameters.Add(new SqlParameter("@keyAX_MESSAGEID", strMessageId));
                axCommand.CommandTimeout = 1800;

                //command.ExecuteNonQuery();
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = axCommand;
                //AX CJ from AX DB View

                sda.Fill(curDt);
            }

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");

        }

        //Dashboard
        //+++++++++++++++++++++++++++++++++++++
        //[WebMethod]
        //public String getTotalSales()
        //{
        //    String tempTotalSales = "-1";
        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
        //    {
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectTotalSales]", curQueryParameter);

        //    List<String> list = new List<String>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add((curDt.Rows[i][0]).ToString());
        //    }

        //    tempTotalSales = list[0];

        //    return tempTotalSales;
        //}

        //[WebMethod]
        //public String getTotalInventoryCost()
        //{
        //    String tempTotalCost = "-1";
        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
        //    {
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectTotalInventoryCost]", curQueryParameter);

        //    List<String> list = new List<String>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add((curDt.Rows[i][0]).ToString());
        //    }

        //    tempTotalCost = list[0];

        //    return tempTotalCost;
        //}

        //[WebMethod]
        //public String getTotalProfit()
        //{
        //    String tempTotalProfit = "-1";
        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
        //    {
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectTotalProfit]", curQueryParameter);

        //    List<String> list = new List<String>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add((curDt.Rows[i][0]).ToString());
        //    }

        //    tempTotalProfit = list[0];

        //    return tempTotalProfit;
        //}

        //[WebMethod]
        //public String[][] getSalesByPeriod(String argDateFrom, String argDateTo)
        //{
        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
        //    {
        //        new sqlQueryParameter {strParameter="@keyDateFrom", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.ParseExact(argDateFrom, "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0")},
        //        new sqlQueryParameter {strParameter="@keyDateTo", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.ParseExact(argDateTo, "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59")}
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectSalesByPeriod]", curQueryParameter);

        //    List<String[]> list = new List<String[]>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new String[] { (curDt.Rows[i][0]).ToString(), (curDt.Rows[i][1]).ToString() });
        //    }

        //    String[][] curList = list.ToArray();

        //    return curList;
        //}

        //[WebMethod]
        //public String[][] getTopSalesItemByPeriod(String argDateFrom, String argDateTo)
        //{
        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
        //    {
        //        new sqlQueryParameter {strParameter="@keyDateFrom", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.ParseExact(argDateFrom, "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0")},
        //        new sqlQueryParameter {strParameter="@keyDateTo", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.ParseExact(argDateTo, "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59")}
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectTopSalesItemByPeriod]", curQueryParameter);

        //    List<String[]> list = new List<String[]>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new String[] { (curDt.Rows[i][0]).ToString(), (curDt.Rows[i][1]).ToString() });
        //    }

        //    String[][] curList = list.ToArray();

        //    return curList;
        //}



        //Module Company
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public string saveCompany(String argCompanyName, String argCompanyAddress1,
            String argCompanyAddress2, String argCompanyAddress3, String argCompanyPhoneNumber, String argCompanyFaxNumber,
            String argCompanyEmail, String argCompanyWebsite, String argCompanyRegNumber, String argCompanyGSTRegNumber
            )
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "Company Saving failed";
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
                {
                };
                DataTable curDt = curDb.getDataTable("checkExistenceOfCompanyRow", curQueryParameter);


                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyCompanyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argCompanyName},
                            new sqlQueryParameter {strParameter="@keyCompanyAddress1", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argCompanyAddress1},
                            new sqlQueryParameter {strParameter="@keyCompanyAddress2", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argCompanyAddress2},
                            new sqlQueryParameter {strParameter="@keyCompanyAddress3", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argCompanyAddress3},
                            new sqlQueryParameter {strParameter="@keyCompanyPhoneNumber", ParameterType=SqlDbType.VarChar, ParameterLength=20, keyData = (Object)argCompanyPhoneNumber},
                            new sqlQueryParameter {strParameter="@keyCompanyFaxNumber", ParameterType=SqlDbType.VarChar, ParameterLength=20, keyData = (Object)argCompanyFaxNumber},
                            new sqlQueryParameter {strParameter="@keyCompanyEmail", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argCompanyEmail},
                            new sqlQueryParameter {strParameter="@keyCompanyWebsite", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argCompanyWebsite},
                            new sqlQueryParameter {strParameter="@keyCompanyRegNumber", ParameterType=SqlDbType.VarChar, ParameterLength=20, keyData = (Object)argCompanyRegNumber},
                            new sqlQueryParameter {strParameter="@keyCompanyGSTRegNumber", ParameterType=SqlDbType.VarChar, ParameterLength=20, keyData = (Object)argCompanyGSTRegNumber}
                        };
                int queryResult = -99;
                if (curDt.Rows.Count > 0)
                {
                    queryResult = curDb.insertUpdateDeleteDataIntoTable("updateCompany", curQueryParameter);
                }
                else
                {
                    queryResult = curDb.insertUpdateDeleteDataIntoTable("insertCompany", curQueryParameter);
                }
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod]
        public companyObject[] getCompany()
        {
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectCompany]", curQueryParameter);

            List<companyObject> list = new List<companyObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new companyObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    ));
            }

            companyObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public dashboardInfoObject[] getDashboardInfo(String argLocationId)
        {
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
                new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argLocationId)},
            };

            DataTable curDt = curDb.getDataTable("[selectDashboardInfo]", curQueryParameter);

            List<dashboardInfoObject> list = new List<dashboardInfoObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new dashboardInfoObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][8].ToString()
                    , curDt.Rows[i][9].ToString()
                    , curDt.Rows[i][10].ToString()
                    , curDt.Rows[i][11].ToString()
                    , curDt.Rows[i][12].ToString()
                    , curDt.Rows[i][13].ToString()
                    , curDt.Rows[i][14].ToString()
                    , curDt.Rows[i][15].ToString()
                    ));
            }

            dashboardInfoObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        //Module Document Number
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public docNumberObject[] getDocNumber()
        {
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectDocNumber]", curQueryParameter);

            List<docNumberObject> list = new List<docNumberObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new docNumberObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    ));
            }

            docNumberObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public String saveDocNumber(String[] argDocNumberId, String[] argDocNumberPrefix, String[] argDocNumberLength, String[] argDocNumberNextNumber)
        {
            String isSuccess = "Document Number update failed";
            database curDb = new database();
            curDb.openConnection();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            try
            {



                for (int i = 0; i < argDocNumberId.Length; i++)
                {

                    curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyDocNumberId", ParameterType=SqlDbType.SmallInt, keyData = Convert.ToInt32(argDocNumberId[i])},
                            new sqlQueryParameter {strParameter="@keyDocNumberPrefix", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argDocNumberPrefix[i] },
                            new sqlQueryParameter {strParameter="@keyDocNumberLength", ParameterType = SqlDbType.SmallInt, keyData = Convert.ToInt32(argDocNumberLength[i]) },
                            new sqlQueryParameter {strParameter="@keyDocNumberNextNumber", ParameterType = SqlDbType.BigInt, keyData = Convert.ToInt64(argDocNumberNextNumber[i])}
                        };
                    int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateDocNumber", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("update Document Number failed");
                    }
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
                return isSuccess;
            }
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        //Printer
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public void getAllPrinterInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];
            String searchHeader = HttpContext.Current.Request.QueryString["filter"].Split('|')[0];
            String searchContent = HttpContext.Current.Request.QueryString["filter"].Split('|')[1];
            String showAll = HttpContext.Current.Request.QueryString["showAll"];

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keyShowAll", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)showAll},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent}
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllPrinterInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");

        }

        [WebMethod]
        public String createPrinter(String argPrinterName, Boolean argPrinterActive)
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "Printer creation failed";
            try
            {
                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyPrinterName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argPrinterName}
                };
                DataTable curDt = curDb.getDataTable("checkExistenceOfPrinterName", curQueryParameter);

                if (curDt.Rows.Count > 0)
                {
                    throw new Exception("Duplicated printer name is found.");
                }

                //if (!RawPrinterHelper.isPrinterReady(argPrinterName))
                //{
                //    throw new Exception("Invalid Printer");
                //}

                curQueryParameter = new List<sqlQueryParameter>()
                {
                };

                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyPrinterName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argPrinterName},
                            new sqlQueryParameter {strParameter="@keyPrinterActive", ParameterType=SqlDbType.Bit, keyData = (Object)argPrinterActive}
                        };

                int queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewPrinter", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod]
        public printerObject[] getPrinterByPrinterId(string argPrinterId)
        {
            Int64 bigintPrinterId = Int64.Parse(argPrinterId);

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyPrinterId", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintPrinterId},
            };

            DataTable curDt = curDb.getDataTable("[selectPrinterByPrinterId]", curQueryParameter);

            List<printerObject> list = new List<printerObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new printerObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , (Boolean)curDt.Rows[i][2]
                    , (Boolean)curDt.Rows[i][3]
                    ));
            }

            printerObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public String setPrinter(String argPrinterId, String argPrinterName, Boolean argPrinterActive)
        {
            database curDb = new database();
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            curDb.openConnection();
            int queryResult = -99;
            String isSuccess = "Printer update failed";
            try
            {
                if (!RawPrinterHelper.isPrinterReady(argPrinterName))
                {
                    throw new Exception("Invalid Printer");
                }

                Int64 bigintPrinterId = Int64.Parse(argPrinterId);

                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyPrinterId", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintPrinterId},
                            new sqlQueryParameter {strParameter="@keyPrinterName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argPrinterName},
                            new sqlQueryParameter {strParameter="@keyPrinterActive", ParameterType=SqlDbType.Bit, keyData = (Object)argPrinterActive}
                        };
                queryResult = curDb.insertUpdateDeleteDataIntoTable("updatePrinter", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod]
        public printerObject[] getAllActivePrinter()
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectAllPrinterActive]", curQueryParameter);

            List<printerObject> list = new List<printerObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new printerObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , (Boolean)curDt.Rows[i][2]
                    , (Boolean)curDt.Rows[i][3]
                    ));
            }

            printerObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return curList;
        }

        //Module User
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public String createUser(String argUsername, String argUsercode, String argUserPass, String argUserLevelId, Boolean argUserActive, String[] argUserLocation)
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "User creation failed";
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyUserCode", ParameterType=SqlDbType.VarChar,ParameterLength=50, keyData = (Object)argUsercode}
                };
                DataTable curDt = curDb.getDataTable("checkExistenceOfUserCode", curQueryParameter);

                if (curDt.Rows.Count > 0)
                {
                    throw new Exception("Duplicated User Code is found.");
                }

                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyUsername", ParameterType=SqlDbType.VarChar,ParameterLength=50, keyData = (Object)argUsername}
                };
                curDt = curDb.getDataTable("checkExistenceOfUsername", curQueryParameter);

                if (curDt.Rows.Count > 0)
                {
                    throw new Exception("Duplicated Username is found.");
                }

                curQueryParameter = new List<sqlQueryParameter>()
                {
                };
                curDt = curDb.getDataTable("[selectNextUserId]", curQueryParameter);

                List<Int64> list = new List<Int64>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[i][0]));
                }

                Int64 userId = list[0];

                Int64 bigintUserLevelId = Int64.Parse(argUserLevelId);
                //Boolean boolUserActive = Boolean.Parse(argUserActive);
                SHA256Managed h = new SHA256Managed();

                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)userId},
                            new sqlQueryParameter {strParameter="@keyUserCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argUsername},
                            new sqlQueryParameter {strParameter="@keyUserName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argUsercode},
                            new sqlQueryParameter {strParameter="@keyUserPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object) Encoding.Unicode.GetString(h.ComputeHash(Encoding.Unicode.GetBytes(argUserPass)))},
                            //new sqlQueryParameter {strParameter="@keyUserPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object) argUserPass},
                            new sqlQueryParameter {strParameter="@keyUserLevelId", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintUserLevelId},
                            new sqlQueryParameter {strParameter="@keyUserActive", ParameterType=SqlDbType.Bit, keyData = (Object)argUserActive}
                        };
                int queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewUser", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                for (int i = 0; i < argUserLocation.Length; i++)
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(userId)},
                            new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argUserLocation[i])},
                        };
                    queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewUserXLocation", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert/update/delete User X Location failed");
                    }
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;

        }

        [WebMethod]
        public sysUserS[] getUserByUserId(string argUserId)
        {
            Int64 bigintUserId = Int64.Parse(argUserId);

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintUserId},
            };

            DataTable curDt = curDb.getDataTable("[selectUserXLocationByUserId]", curQueryParameter);
            List<userLocationObject> userLocationList = new List<userLocationObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                userLocationList.Add(new userLocationObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()));
            }

            curDt = curDb.getDataTable("[selectUserByUserId]", curQueryParameter);

            List<sysUserS> list = new List<sysUserS>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new sysUserS(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][9].ToString()
                    , (Boolean)curDt.Rows[i][5]
                    , curDt.Rows[i][6].ToString()
                    , userLocationList.ToArray()
                    ));
            }

            sysUserS[] curItem = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curItem;
        }

        [WebMethod]
        public userLocationObject[] getUserXLocationByUserId(string argUserId)
        {
            //Int64 bigintUserId = Int64.Parse(argUserId);

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argUserId},
            };

            DataTable curDt = curDb.getDataTable("[selectUserXLocationByUserId]", curQueryParameter);

            List<userLocationObject> list = new List<userLocationObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new userLocationObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()

                    ));
            }

            userLocationObject[] curItem = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curItem;
        }

        [WebMethod]
        public String setUser(String argUserId, String argUserCode, String argUserPass, String argUserName, String argUserLevelId, Boolean argUserActive, String[] argUserLocation)
        {
            String isSuccess = "User update failed";
            database curDb = new database();
            curDb.openConnection();
            try
            {
                Int64 bigintUserId = Int64.Parse(argUserId);
                Int64 bigintUserLevelId = Int64.Parse(argUserLevelId);
                SHA256Managed h = new SHA256Managed();


                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserId)},
                    new sqlQueryParameter {strParameter="@keyUserCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argUserCode},
                    //new sqlQueryParameter {strParameter="@keyUserPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object) argUserPass},
                    new sqlQueryParameter {strParameter="@keyUserPass", ParameterType=SqlDbType.NVarChar, ParameterLength=64, keyData = (Object) Encoding.Unicode.GetString(h.ComputeHash(Encoding.Unicode.GetBytes(argUserPass)))},
                    new sqlQueryParameter {strParameter="@keyUserName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argUserName},
                    new sqlQueryParameter {strParameter="@keyUserLevelId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserLevelId)},
                    new sqlQueryParameter {strParameter="@keyUserActive", ParameterType=SqlDbType.Bit, keyData = (Object)argUserActive}
                        };
                int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateUser", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserId)}
                        };
                queryResult = curDb.insertUpdateDeleteDataIntoTable("deleteUserXLocationByUserId", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("delete User X Location failed");
                }

                for (int i = 0; i < argUserLocation.Length; i++)
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserId)},
                            new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argUserLocation[i])},
                        };
                    queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewUserXLocation", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert User X Location failed");
                    }
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod]
        public void getAllUserInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];
            String showAll = HttpContext.Current.Request.QueryString["showAll"];
            //String bigintUserId = HttpContext.Current.Request.QueryString["userId"];
            String searchHeader = HttpContext.Current.Request.QueryString["filter"].Split('|')[0];
            String searchContent = HttpContext.Current.Request.QueryString["filter"].Split('|')[1];

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                //new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)bigintUserId},
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keyShowAll", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)showAll},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent}
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllUserInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");
        }

        [WebMethod]
        public sysUserLevelS[] getAllUserLevel(String argUserId)
        {
            Int64 bigintUserId = Int64.Parse(argUserId);
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
                //new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserId)}
            };

            DataTable curDt = curDb.getDataTable("selectAllUserLevel", curQueryParameter);
            curDb.closeConnection();


            List<sysUserLevelS> list = new List<sysUserLevelS>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new sysUserLevelS(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , (Boolean)curDt.Rows[i][3]
                    ));
            }

            sysUserLevelS[] curUserLevelS = list.ToArray();

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curUserLevelS;
        }

        [WebMethod]
        public sysUserPassword[] getAllActiveUser()
        {

            sysUserPassword[] curItem = null;
            database curDb = new database();
            curDb.openConnection();
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            try
            {

                curDt = curDb.getDataTable("[selectAllUserActive]", curQueryParameter);

                List<sysUserPassword> list = new List<sysUserPassword>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new sysUserPassword(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][7].ToString()
                    , curDt.Rows[i][9].ToString()
                    , (Boolean)curDt.Rows[i][5]
                    , curDt.Rows[i][6].ToString()
                    ));
                }

                curItem = list.ToArray();
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                return curItem;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return curItem;
        }

        [WebMethod]
        public userLocationObject[] getAllUserXLocation()
        {
            userLocationObject[] curItem = null;
            database curDb = new database();
            curDb.openConnection();
            DataTable curDt;
            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
            try
            {


                curDt = curDb.getDataTable("[selectAllUserXLocation]", curQueryParameter);

                List<userLocationObject> list = new List<userLocationObject>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(new userLocationObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()

                        ));
                }

                curItem = list.ToArray();
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                return curItem;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return curItem;
        }

        //Module User Level
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public String createUserLevel(String argUserLevelDescription, Boolean argUserLevelActive, Int16[] argActionIdList,
            Boolean[] argCanReadList, Boolean[] argCanAddList, Boolean[] argCanEditList, Boolean[] argCanDeleteList, Boolean[] argCanPrintList)
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "User Level creation failed";
            try
            {

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyUserLevelDescription", ParameterType=SqlDbType.VarChar,ParameterLength=50, keyData = (Object)argUserLevelDescription},
                     new sqlQueryParameter {strParameter="@keyExcludedUserLevelId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(-1)}

                };
                DataTable curDt = curDb.getDataTable("checkExistenceOfUserLevel", curQueryParameter);

                if (curDt.Rows.Count > 0)
                {
                    throw new Exception("Duplicated User Level is found.");
                }

                curQueryParameter = new List<sqlQueryParameter>()
                {
                };

                curDt = curDb.getDataTable("[selectNextUserLevelId]", curQueryParameter);

                List<Int64> list = new List<Int64>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[i][0]));
                }

                Int64 userLevelId = list[0];

                curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyUserLevelId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(userLevelId) },
                            new sqlQueryParameter {strParameter="@keyUserLevelDescription", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argUserLevelDescription},
                            new sqlQueryParameter {strParameter="@keyUserLevelActive", ParameterType=SqlDbType.Bit, keyData = (Object)argUserLevelActive}
                        };
                int queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewUserLevel", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete User Level failed");
                }

                for (int i = 0; i < argActionIdList.Length; i++)
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyUserLevelId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(userLevelId)},
                            new sqlQueryParameter {strParameter="@keyAccessId", ParameterType=SqlDbType.TinyInt, keyData = Convert.ToInt16(argActionIdList[i])},
                            new sqlQueryParameter {strParameter="@keyCanRead", ParameterType=SqlDbType.Bit, keyData = (Object)argCanReadList[i]},
                            new sqlQueryParameter {strParameter="@keyCanAdd", ParameterType=SqlDbType.Bit, keyData = (Object)argCanAddList[i]},
                            new sqlQueryParameter {strParameter="@keyCanEdit", ParameterType=SqlDbType.Bit, keyData = (Object)argCanEditList[i]},
                            new sqlQueryParameter {strParameter="@keyCanDelete", ParameterType=SqlDbType.Bit, keyData = (Object)argCanDeleteList[i]},
                            new sqlQueryParameter {strParameter="@keyCanPrint", ParameterType=SqlDbType.Bit, keyData = (Object)argCanPrintList[i]}
                        };
                    queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewUserLevelXAccess", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert/update/delete User Level X Access failed");
                    }
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;

        }

        [WebMethod]
        public sysUserLevelS[] getUserLevelByUserLevelId(string argUserLevelId)
        {
            Int64 bigintUserLevelId = Int64.Parse(argUserLevelId);

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyUserLevelId", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintUserLevelId},
            };

            DataTable curDt = curDb.getDataTable("[selectUserLevelByUserLevelId]", curQueryParameter);

            List<sysUserLevelS> list = new List<sysUserLevelS>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new sysUserLevelS(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , (Boolean)curDt.Rows[i][3]
                    ));
            }

            sysUserLevelS[] curItem = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curItem;
        }

        [WebMethod]
        public String setUserLevel(String argUserLevelId, String argUserLevelDescription, Boolean argUserLevelActive, Int16[] argActionIdList,
            Boolean[] argCanReadList, Boolean[] argCanAddList, Boolean[] argCanEditList, Boolean[] argCanDeleteList, Boolean[] argCanPrintList)
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "User Level update failed";
            try
            {
                Int64 bigintUserLevelId = Int64.Parse(argUserLevelId);

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyUserLevelDescription", ParameterType=SqlDbType.VarChar,ParameterLength=50, keyData = (Object)argUserLevelDescription},
                     new sqlQueryParameter {strParameter="@keyExcludedUserLevelId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserLevelId)}

                };
                DataTable curDt = curDb.getDataTable("checkExistenceOfUserLevel", curQueryParameter);

                if (curDt.Rows.Count > 0)
                {
                    throw new Exception("Duplicated User Level is found.");
                }

                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyUserLevelId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserLevelId)},
                    new sqlQueryParameter {strParameter="@keyUserLevelDescription", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argUserLevelDescription},
                    new sqlQueryParameter {strParameter="@keyUserLevelActive", ParameterType=SqlDbType.Bit, keyData = (Object)argUserLevelActive}
                        };
                int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateUserLevel", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyUserLevelId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserLevelId)},
                };
                queryResult = curDb.insertUpdateDeleteDataIntoTable("deleteUserLevelXAccessByUserLevelId", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                for (int i = 0; i < argActionIdList.Length; i++)
                {
                    curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyUserLevelId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(bigintUserLevelId)},
                            new sqlQueryParameter {strParameter="@keyAccessId", ParameterType=SqlDbType.TinyInt, keyData = Convert.ToInt16(argActionIdList[i])},
                            new sqlQueryParameter {strParameter="@keyCanRead", ParameterType=SqlDbType.Bit, keyData = (Object)argCanReadList[i]},
                            new sqlQueryParameter {strParameter="@keyCanAdd", ParameterType=SqlDbType.Bit, keyData = (Object)argCanAddList[i]},
                            new sqlQueryParameter {strParameter="@keyCanEdit", ParameterType=SqlDbType.Bit, keyData = (Object)argCanEditList[i]},
                            new sqlQueryParameter {strParameter="@keyCanDelete", ParameterType=SqlDbType.Bit, keyData = (Object)argCanDeleteList[i]},
                            new sqlQueryParameter {strParameter="@keyCanPrint", ParameterType=SqlDbType.Bit, keyData = (Object)argCanPrintList[i]}
                        };
                    queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewUserLevelXAccess", curQueryParameter);
                    if (queryResult == -99)
                    {
                        throw new Exception("insert/update/delete User Level X Access failed");
                    }
                }
                isSuccess = "true";

            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod]
        public void getAllUserLevelInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];
            String showAll = HttpContext.Current.Request.QueryString["showAll"];
            //String bigintUserId = HttpContext.Current.Request.QueryString["userId"];
            String searchHeader = HttpContext.Current.Request.QueryString["filter"].Split('|')[0];
            String searchContent = HttpContext.Current.Request.QueryString["filter"].Split('|')[1];

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keyShowAll", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)showAll},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent}
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllUserLevelInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");
        }

        [WebMethod]
        public userAccessObject[] getUserLevelAccess()
        {
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectUserLevelAccess]", curQueryParameter);

            List<userAccessObject> list = new List<userAccessObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new userAccessObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    ));
            }

            userAccessObject[] curList = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public xUserLevelXAccessObject[] getUserLevelXAccessByUserLevelId(string argUserLevelId)
        {
            Int64 bigintUserLevelId = Int64.Parse(argUserLevelId);

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyUserLevelId", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintUserLevelId},
            };

            DataTable curDt = curDb.getDataTable("[selectUserLevelXAccessByUserLevelId]", curQueryParameter);

            List<xUserLevelXAccessObject> list = new List<xUserLevelXAccessObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new xUserLevelXAccessObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , curDt.Rows[i][5].ToString()
                    , curDt.Rows[i][6].ToString()

                    ));
            }

            xUserLevelXAccessObject[] curItem = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curItem;
        }

        //Module Location
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public void getAllLevelOneLocation(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];
            String showAll = HttpContext.Current.Request.QueryString["showAll"];
            String searchHeader = HttpContext.Current.Request.QueryString["filter"].Split('|')[0];
            String searchContent = HttpContext.Current.Request.QueryString["filter"].Split('|')[1];

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keyShowAll", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)showAll},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)searchContent}
            };

            DataTable curDt = curDb.getDataTable("selectAllLevelOneLocation", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");
        }

        [WebMethod]
        public string createLocation(String argLocCode, String argLocDesc, String argLocLevel, String argLocParentId, Boolean argLocActive)
        {
            database curDb = new database();
            curDb.openConnection();

            Int64 bigintLocLevel = Int64.Parse(argLocLevel);
            Int64 bigintLocParentId = Int64.Parse(argLocParentId);

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyLocCode", ParameterType=SqlDbType.VarChar,ParameterLength=50, keyData = (Object)argLocCode}
            };
            DataTable curDt = curDb.getDataTable("checkExistenceOfLocation", curQueryParameter);

            if (curDt.Rows.Count == 0)
            {
                curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyLocCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argLocCode},
                    new sqlQueryParameter {strParameter="@keyLocDesc", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argLocDesc},
                    new sqlQueryParameter {strParameter="@keyLocLevel", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintLocLevel},
                    new sqlQueryParameter {strParameter="@keyLocParentId", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintLocParentId},
                    new sqlQueryParameter {strParameter="@keyLocActive", ParameterType=SqlDbType.Bit, keyData = (Object)argLocActive}
                    };
                int queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewLocation", curQueryParameter);
                if (queryResult == -99)
                {
                    curDb.closeConnection();
                    throw new Exception("insert/update/delete failed");
                }
            }
            else
            {
                return "false";
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return "true";
        }

        [WebMethod]
        public void getAllLocationByLocLvlAndParentLocCode(String argLocLevel, String argParentLocCode, String page, String per_page)
        {
            //int intLocLevel = Int32.Parse(argLocLevel);
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];
            String showAll = HttpContext.Current.Request.QueryString["showAll"];

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyLocLv", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argLocLevel},
                new sqlQueryParameter {strParameter="@keyParentLocCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argParentLocCode},
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keyShowAll", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)showAll}
            };

            DataTable curDt = curDb.getDataTable("selectLocByLocLvAndLocCode", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }

            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");
        }

        [WebMethod]
        public locationObject[] getLocationByLocationId(string argLocationId)
        {
            Int64 bigintLocationId = Int64.Parse(argLocationId);

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintLocationId},
            };

            DataTable curDt = curDb.getDataTable("[selectLocationByLocationId]", curQueryParameter);

            List<locationObject> list = new List<locationObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new locationObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , (Boolean)curDt.Rows[i][5]
                    ));
            }

            locationObject[] curLocation = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curLocation;
        }

        [WebMethod]
        public locationObject[] getAllLocationTwo()
        {
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectAllLocation]", curQueryParameter);

            List<locationObject> list = new List<locationObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                if (Convert.ToInt64(curDt.Rows[i][3]) == 2)
                {
                    list.Add(new locationObject(curDt.Rows[i][0].ToString()
                        , curDt.Rows[i][1].ToString()
                        , curDt.Rows[i][2].ToString()
                        , curDt.Rows[i][3].ToString()
                        , curDt.Rows[i][4].ToString()
                        , (Boolean)curDt.Rows[i][5]
                        ));
                }
            }

            locationObject[] curLocation = list.ToArray();
            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();

            return curLocation;
        }

        [WebMethod]
        public Boolean setLocation(String argLocationId, String argLocationCode, String argLocationDesc, Boolean argLocationActive)
        {
            database curDb = new database();
            curDb.openConnection();
            try
            {
                Int64 bigintLocationId = Int64.Parse(argLocationId);

                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = (Object)bigintLocationId},
                            new sqlQueryParameter {strParameter="@keyLocationCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argLocationCode},
                            new sqlQueryParameter {strParameter="@keyLocationDesc", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argLocationDesc},
                            new sqlQueryParameter {strParameter="@keyLocationActive", ParameterType=SqlDbType.Bit, keyData = (Object)argLocationActive}
                        };
                int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateLocation", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                return false;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return true;
        }

        //Module Movement History
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod(EnableSession = true)]
        public void getAllMovementHistoryInfo(String page, String per_page)
        {
            Int32 total_number_page = 1;
            Int32 cur_start_item = 1;
            Int32 cur_end_item = 1;
            DataTable splitDt = new DataTable();

            String header = HttpContext.Current.Request.QueryString["sort"].Split('|')[0];
            String content = HttpContext.Current.Request.QueryString["sort"].Split('|')[1];
            String searchHeader = "";
            String searchContent = "";
            String searchContent2 = "";
            String searchType = "";
            String[] searchArray = HttpContext.Current.Request.QueryString["filter"].Split('|');
            String warehouseCode = HttpContext.Current.Request.QueryString["warehouseCode"].ToString();
            String showEntries = HttpContext.Current.Request.QueryString["showEntries"].ToString();

            searchHeader = searchArray[0];
            searchContent = searchArray[1];
            searchContent2 = searchArray[2];

            String[] searchHeaderArray = searchHeader.Split(',');
            String[] searchContentArray = searchContent.Split(',');
            String[] searchContent2Array = searchContent2.Split(',');

            searchContent = "";
            searchContent2 = "";
            for (int i = 0; i < searchHeaderArray.Length; i++)
            {
                if (i > 0)
                {
                    searchContent += ",";
                    searchContent2 += ",";
                    searchType += ",";
                }
                if (searchContent2Array[i].Length > 0)
                {
                    searchType += "DATE";
                    searchContent += searchContentArray[i] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchContentArray[i], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
                    searchContent2 += searchContent2Array[i] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchContent2Array[i], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
                }
                else
                {
                    searchType += "-";
                    searchContent += searchContentArray[i] == "" ? "%" : searchContentArray[i];
                    searchContent2 += "-";
                }
            }

            //if (searchArray.Length == 3)
            //{
            //    searchType = "DATE";
            //    searchContent = searchArray[1] == "" ? "1753-01-01 0:0" : DateTime.ParseExact(searchArray[1], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 0:0");
            //    searchContent2 = searchArray[2] == "" ? "9999-12-31 23:59:59" : DateTime.ParseExact(searchArray[2], "dd/MM/yyyy", null).ToString("yyyy-MM-dd 23:59:59");
            //}
            //else
            //{
            //    searchContent = searchArray[1] == "" ? "%" : searchArray[1];
            //}

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keySortHeader", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)header},
                new sqlQueryParameter {strParameter="@keySortContent", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)content},
                new sqlQueryParameter {strParameter="@keySearchType", ParameterType=SqlDbType.NVarChar, ParameterLength=1000, keyData = (Object)searchType},
                new sqlQueryParameter {strParameter="@keySearchHeader", ParameterType=SqlDbType.NVarChar, ParameterLength=1000, keyData = (Object)searchHeader},
                new sqlQueryParameter {strParameter="@keySearchContent", ParameterType=SqlDbType.NVarChar, ParameterLength=1000, keyData = (Object)searchContent},
                new sqlQueryParameter {strParameter="@keySearchContent2", ParameterType=SqlDbType.NVarChar, ParameterLength=1000, keyData = (Object)searchContent2},
                new sqlQueryParameter {strParameter="@keySearchWarehouseCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)warehouseCode},
                new sqlQueryParameter {strParameter="@keySearchShowEntries", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)showEntries},
            };

            database curDb = new database();
            curDb.openConnection();
            DataTable curDt = curDb.getDataTable("selectAllMovementHistoryInfo", curQueryParameter);
            curDb.closeConnection();

            if (curDt.Rows.Count != 0)
            {
                splitDt = splitDataTable(curDt, page, per_page);
                total_number_page = (Int32)Math.Ceiling(((Double)curDt.Rows.Count / Double.Parse(per_page)));
                cur_start_item = (Int32.Parse(per_page) * (Int32.Parse(page) - 1)) + 1;
                cur_end_item = (Int32.Parse(per_page) * Int32.Parse(page));
            }


            Context.Response.ContentType = "text/plain";
            //Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + curDt.Rows.Count.ToString() + @",""current_page"":1,""last_page"":1,""next_page_url"":null,""prev_page_url"":null,""from"":1,""to"":" + curDt.Rows.Count.ToString() + @",""data"":");
            Context.Response.Write(@"{""total"":" + curDt.Rows.Count.ToString() + @",""per_page"":" + per_page + @",""current_page"":" + page.ToString() + @",""last_page"":" + total_number_page.ToString() + @",""next_page_url"":null,""prev_page_url"":null,""from"":" + cur_start_item.ToString() + @",""to"":" + cur_end_item.ToString() + @",""data"":");
            Context.Response.Write(DataTableToJSONWithStringBuilder(splitDt));
            Context.Response.Write("}");

        }

        public string insertNewMovementHistory(database curDb, String argItemId, String argSN, String argMovementType, String argLocationId,
             String argQuantity, String argUserId, String argDocId)
        {
            try
            {
                if (Convert.ToInt32(argQuantity) != 0)
                {
                    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                            new sqlQueryParameter {strParameter="@keyItemId", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argItemId)},
                            new sqlQueryParameter {strParameter="@keySN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argSN},
                            new sqlQueryParameter {strParameter="@keyMovementType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argMovementType },
                            new sqlQueryParameter {strParameter="@keyMovementDatetime", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss")},
                            new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argLocationId)},
                            new sqlQueryParameter {strParameter="@keyQuantity",ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argQuantity) },
                            new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argUserId)},
                            new sqlQueryParameter {strParameter="@keyDocId",ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)(argDocId) }
                        };
                    int queryResult = curDb.insertUpdateDeleteDataIntoTable("insertNewMovementHistory", curQueryParameter);
                    if (queryResult == -99)
                    {
                        return "insert movement history failed";
                    }
                }
                return "success";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        //+++++++++++++++++++++++++++++++++++++
        //Common Use
        //+++++++++++++++++++++++++++++++++++++

        [WebMethod]
        public Int64 getSumActiveItem()
        {
            Int64 tempSum = -1;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
            };

            DataTable curDt = curDb.getDataTable("[selectSumActiveItem]", curQueryParameter);

            List<Int64> list = new List<Int64>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(Convert.ToInt64(curDt.Rows[i][0]));
            }

            tempSum = list[0];

            return tempSum;
        }

        [WebMethod]
        public String getDocReferenceNumberByType(String argType)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argType},
            };

            DataTable curDt = curDb.getDataTable("[selectDocReferenceNumberByType]", curQueryParameter);

            List<docReferenceNumber> list = new List<docReferenceNumber>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new docReferenceNumber(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    ));
            }

            docReferenceNumber[] curList = list.ToArray();
            curDb.closeConnection();

            return curList[0].nextGeneratedReferenceNumber;
        }

        [WebMethod]
        public String getDocReferenceNumberByTypeWithPrebookedNumber(String argType, String prebookedNumber)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argType},
            };

            DataTable curDt = curDb.getDataTable("[selectDocReferenceNumberByType]", curQueryParameter);

            List<docReferenceNumber> list = new List<docReferenceNumber>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new docReferenceNumber(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    , curDt.Rows[i][3].ToString()
                    , curDt.Rows[i][4].ToString()
                    , prebookedNumber
                    ));
            }

            docReferenceNumber[] curList = list.ToArray();
            curDb.closeConnection();

            return curList[0].referenceNumberWithPrebookedNumber;
        }

        [WebMethod]
        public systemDataObject[] getSystemData(String argName)
        {

            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argName},
            };


            DataTable curDt = curDb.getDataTable("[selectSystemData]", curQueryParameter);

            List<systemDataObject> list = new List<systemDataObject>();
            for (int i = 0; i < curDt.Rows.Count; i++)
            {
                list.Add(new systemDataObject(curDt.Rows[i][0].ToString()
                    , curDt.Rows[i][1].ToString()
                    , curDt.Rows[i][2].ToString()
                    ));
            }

            systemDataObject[] curList = list.ToArray();
            curDb.closeConnection();

            return curList;
        }

        [WebMethod]
        public String setSystemData(String argName, String argDataValue)
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "System Data update failed";
            try
            {
                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argName},
                    new sqlQueryParameter {strParameter="@keyDataValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argDataValue}
                };

                int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateSystemData", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod]
        public String setTableSingleField(String argTableName, String argFieldName, String argFieldValue,
             String argIDName, String argIDValue)
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "Table Field update failed";
            try
            {
                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyTableName", ParameterType=SqlDbType.VarChar, ParameterLength=100, keyData = (Object)argTableName},
                        new sqlQueryParameter {strParameter="@keyFieldName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argFieldName},
                        new sqlQueryParameter {strParameter="@keyFieldValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argFieldValue},
                        new sqlQueryParameter {strParameter="@keyIDName", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argIDName},
                        new sqlQueryParameter {strParameter="@keyIDValue", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argIDValue}
                    };

                int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateTableSingleField", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod(EnableSession = true)]
        public String setSession(String argSessionName, String argSessionValue)
        {

            String isSuccess = "Session update failed";
            try
            {
                Session[argSessionName] = argSessionValue;
                isSuccess = "true";
            }
            catch (Exception e)
            {
                isSuccess = e.Message;
            }

            return isSuccess;
        }

        [WebMethod]
        public String setUserSelectedLocationByUserId(String argSelectedLocationId, String argUserdId)
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "User Selected Location update failed";
            try
            {
                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keySelectedLocationId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argSelectedLocationId)},
                    new sqlQueryParameter {strParameter="@keyUserId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argUserdId)}
                };

                int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateUserSelectedLocationByUserId", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod]
        public String setReferenceVersion(String argType, String argID)
        {
            database curDb = new database();
            curDb.openConnection();
            String isSuccess = "Reference Version update failed";
            try
            {
                List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
                    new sqlQueryParameter {strParameter="@keyType", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argType},
                    new sqlQueryParameter {strParameter="@keyID", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argID}
                };

                int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateReferenceVersion", curQueryParameter);
                if (queryResult == -99)
                {
                    throw new Exception("insert/update/delete failed");
                }

                isSuccess = "true";
            }
            catch (Exception e)
            {
                curDb.rollbackNCloseConnection();
                isSuccess = e.Message;
            }

            if (!curDb.checkIsConnectionClosed())
                curDb.closeConnection();
            return isSuccess;
        }

        [WebMethod]
        public Int64 getItemIdByPartNumber(String argPartNumber)
        {
            Int64 tempResult = -1;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
                    new sqlQueryParameter {strParameter="@keyPartNumber", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argPartNumber}
            };

            DataTable curDt = curDb.getDataTable("[selectItemIdByPartNumber]", curQueryParameter);

            if (curDt.Rows.Count > 0)
            {
                List<Int64> list = new List<Int64>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[i][0]));
                }

                tempResult = list[0];
            }

            return tempResult;
        }

        [WebMethod]
        public Int64 getTotalNumberOfPendingAXPO()
        {
            return getTotalNumberOfAXPOByStatusId("1");
        }

        [WebMethod]
        public Int64 getTotalNumberOfCompletedAXPO()
        {
            return getTotalNumberOfAXPOByStatusId("4");
        }

        [WebMethod]
        public Int64 getTotalNumberOfSyncedAXPO()
        {
            return getTotalNumberOfAXPOByStatusId("5");
        }

        [WebMethod]
        public Int64 getTotalNumberOfAXPOByStatusId(String argStatusId)
        {
            Int64 tempResult = -1;
            database curDb = new database();
            curDb.openConnection();

            List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
            {
                    new sqlQueryParameter {strParameter="@keyStatusId", ParameterType=SqlDbType.BigInt, keyData = Convert.ToInt64(argStatusId)}
            };

            DataTable curDt = curDb.getDataTable("[selectTotalNumberOfAXPOByStatusId]", curQueryParameter);

            if (curDt.Rows.Count > 0)
            {
                List<Int64> list = new List<Int64>();
                for (int i = 0; i < curDt.Rows.Count; i++)
                {
                    list.Add(Convert.ToInt64(curDt.Rows[i][0]));
                }

                tempResult = list[0];
            }

            return tempResult;
        }

        //[WebMethod]
        //public itemSNObject[] getItemSNByItemIdItemSN(String argItemId, String argItemSN)
        //{
        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
        //        new sqlQueryParameter {strParameter="@keyItemId", ParameterType=SqlDbType.BigInt, keyData = (Object)Convert.ToInt64(argItemId)},
        //        new sqlQueryParameter {strParameter="@keyItemSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argItemSN}
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectItemSNByItemIdItemSN]", curQueryParameter);

        //    List<itemSNObject> list = new List<itemSNObject>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new itemSNObject(curDt.Rows[i][0].ToString()
        //            , curDt.Rows[i][1].ToString()
        //            , curDt.Rows[i][2].ToString()
        //            , curDt.Rows[i][3].ToString()
        //            , curDt.Rows[i][4].ToString()
        //            , curDt.Rows[i][5].ToString()
        //            ));
        //    }

        //    itemSNObject[] curList = list.ToArray();
        //    if (!curDb.checkIsConnectionClosed())
        //        curDb.closeConnection();

        //    return curList;
        //}

        //[WebMethod]
        //public itemSNObject[] getItemEntriesSNByItemIdItemSN(String argItemId, String argItemSN)
        //{
        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
        //        new sqlQueryParameter {strParameter="@keyItemId", ParameterType=SqlDbType.BigInt, keyData = (Object)Convert.ToInt64(argItemId)},
        //        new sqlQueryParameter {strParameter="@keyItemSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argItemSN}
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectItemEntriesSNByItemIdItemSN]", curQueryParameter);

        //    List<itemSNObject> list = new List<itemSNObject>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new itemSNObject(curDt.Rows[i][0].ToString()
        //            , curDt.Rows[i][1].ToString()
        //            , curDt.Rows[i][2].ToString()
        //            , curDt.Rows[i][3].ToString()
        //            , curDt.Rows[i][4].ToString()
        //            , curDt.Rows[i][5].ToString()
        //            ));
        //    }

        //    itemSNObject[] curList = list.ToArray();
        //    if (!curDb.checkIsConnectionClosed())
        //        curDb.closeConnection();

        //    return curList;
        //}

        //[WebMethod]
        //public itemSNObject[] getItemEntriesSNByItemIdItemSNLocationId(String argItemId, String argItemSN, String argLocationId)
        //{
        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
        //        new sqlQueryParameter {strParameter="@keyItemId", ParameterType=SqlDbType.BigInt, keyData = (Object)Convert.ToInt64(argItemId)},
        //        new sqlQueryParameter {strParameter="@keyItemSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argItemSN},
        //        new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = (Object)Convert.ToInt64(argLocationId)}
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectItemEntriesSNByItemIdItemSNLocationId]", curQueryParameter);

        //    List<itemSNObject> list = new List<itemSNObject>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new itemSNObject(curDt.Rows[i][0].ToString()
        //            , curDt.Rows[i][1].ToString()
        //            , curDt.Rows[i][2].ToString()
        //            , curDt.Rows[i][3].ToString()
        //            , curDt.Rows[i][4].ToString()
        //            , curDt.Rows[i][5].ToString()
        //            ));
        //    }

        //    itemSNObject[] curList = list.ToArray();
        //    if (!curDb.checkIsConnectionClosed())
        //        curDb.closeConnection();

        //    return curList;
        //}

        //[WebMethod]
        //public item[] getItemByItemCode(String argItemCode)
        //{

        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
        //        new sqlQueryParameter {strParameter="@keyItemCode", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argItemCode},
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectItemByItemCode]", curQueryParameter);

        //    List<item> list = new List<item>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new item(curDt.Rows[i][0].ToString()
        //            , curDt.Rows[i][1].ToString()
        //            , curDt.Rows[i][2].ToString()
        //            , curDt.Rows[i][3].ToString()
        //            , curDt.Rows[i][4].ToString()
        //            , curDt.Rows[i][5].ToString()
        //            , curDt.Rows[i][6].ToString()
        //            , curDt.Rows[i][7].ToString()
        //            , curDt.Rows[i][8].ToString()
        //            , (Boolean)curDt.Rows[i][9]
        //            ));
        //    }

        //    item[] curItem = list.ToArray();
        //    if (!curDb.checkIsConnectionClosed())
        //        curDb.closeConnection();

        //    return curItem;
        //}

        //[WebMethod]
        //public itemSNwSOSNStatusObject[] getItemSNByItemIdItemSNLocationIdSOId(String argItemId, String argItemSN, String argLocationId, String argSOId)
        //{
        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() {
        //        new sqlQueryParameter {strParameter="@keyItemId", ParameterType=SqlDbType.BigInt, keyData = (Object)Convert.ToInt64(argItemId)},
        //        new sqlQueryParameter {strParameter="@keyItemSN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argItemSN},
        //        new sqlQueryParameter {strParameter="@keyLocationId", ParameterType=SqlDbType.BigInt, keyData = (Object)Convert.ToInt64(argLocationId)},
        //        new sqlQueryParameter {strParameter="@keySOId", ParameterType=SqlDbType.BigInt, keyData = (Object)Convert.ToInt64(argSOId)}
        //    };

        //    DataTable curDt = curDb.getDataTable("[selectItemSNByItemIdItemSNLocationIdSOId]", curQueryParameter);

        //    List<itemSNwSOSNStatusObject> list = new List<itemSNwSOSNStatusObject>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new itemSNwSOSNStatusObject(curDt.Rows[i][0].ToString()
        //            , curDt.Rows[i][1].ToString()
        //            , curDt.Rows[i][2].ToString()
        //            , curDt.Rows[i][3].ToString()
        //            , curDt.Rows[i][4].ToString()
        //            , curDt.Rows[i][5].ToString()
        //            , curDt.Rows[i][6].ToString()
        //            ));
        //    }

        //    itemSNwSOSNStatusObject[] curList = list.ToArray();
        //    if (!curDb.checkIsConnectionClosed())
        //        curDb.closeConnection();

        //    return curList;
        //}

        //[WebMethod]
        //public String getSerialNumberWithPrebookedNumber(String prebookedNumber)
        //{
        //    return getDocReferenceNumberByTypeWithPrebookedNumber("ITEM", prebookedNumber);
        //}

        //[WebMethod]
        //public string printVemacLabel(string[] argBarcode)
        //{
        //    String isSuccess = "Print Label failed";
        //    database curDb = new database();
        //    curDb.openConnection();
        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>() { };
        //    try
        //    {
        //        for (int i = 0; i < argBarcode.Length; i++)
        //        {
        //            curQueryParameter = new List<sqlQueryParameter>() {
        //                new sqlQueryParameter {strParameter="@keyItemEntrySN", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argBarcode[i]},
        //            };

        //            DataTable curDt = curDb.getDataTable("[selectItemByItemEntrySN]", curQueryParameter);

        //            List<item> list = new List<item>();
        //            for (int j = 0; j < curDt.Rows.Count; j++)
        //            {
        //                list.Add(new item(curDt.Rows[j][0].ToString()
        //                    , curDt.Rows[j][1].ToString()
        //                    , curDt.Rows[j][2].ToString()
        //                    , curDt.Rows[j][3].ToString()
        //                    , curDt.Rows[j][4].ToString()
        //                    , curDt.Rows[j][5].ToString()
        //                    , curDt.Rows[j][6].ToString()
        //                    , curDt.Rows[j][7].ToString()
        //                    , curDt.Rows[j][8].ToString()
        //                    , (Boolean)curDt.Rows[j][9]
        //                    ));
        //            }

        //            item[] curItem = list.ToArray();
        //            if (curItem.Length <= 0)
        //            {
        //                throw new Exception("Failed to get item information");
        //            }

        //            string text = File.ReadAllText(Server.MapPath("/_barcode_template/vemac_label.prn"));
        //            text = text.Replace("ITEMNAME", curItem[0].itemCode);
        //            text = text.Replace("ITEMDESC", curItem[0].itemDescription);
        //            text = text.Replace("ABC1234567", argBarcode[i]);

        //            //PrinterList();
        //            RawPrinterHelper.SendStringToPrinter(WebConfigurationManager.AppSettings["FirstPrinterName"], text);

        //            Boolean tempBit = true;
        //            curQueryParameter = new List<sqlQueryParameter>() {
        //                    new sqlQueryParameter {strParameter="@keyItemSerialNumber", ParameterType=SqlDbType.VarChar, ParameterLength=50, keyData = (Object)argBarcode[i]},
        //                    new sqlQueryParameter {strParameter="@keyItemEntryIsPrinted", ParameterType=SqlDbType.Bit, keyData = (Object)tempBit},
        //                };
        //            int queryResult = curDb.insertUpdateDeleteDataIntoTable("updateItemEntryIsPrinted", curQueryParameter);
        //            if (queryResult == -99)
        //            {
        //                throw new Exception("insert/update/delete failed");
        //            }
        //        }
        //        isSuccess = "true";
        //    }
        //    catch (Exception e)
        //    {
        //        curDb.rollbackNCloseConnection();
        //        isSuccess = e.Message;
        //        return isSuccess;
        //    }
        //    if (!curDb.checkIsConnectionClosed())
        //        curDb.closeConnection();
        //    return isSuccess;
        //}

        public string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("" + JsonConvert.ToString(table.Columns[j].ColumnName.ToString()) + ":" + "" + JsonConvert.ToString(table.Rows[i][j].ToString()) + ",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("" + JsonConvert.ToString(table.Columns[j].ColumnName.ToString()) + ":" + "" + JsonConvert.ToString(table.Rows[i][j].ToString()) + "");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        public DataTable splitDataTable(DataTable argDt, String page, String per_page)
        {
            Int32 bigintPage = Int32.Parse(page);
            Int32 bigintPerPage = Int32.Parse(per_page);

            DataTable dtPage = argDt.Rows.Cast<System.Data.DataRow>().Skip((bigintPage - 1) * bigintPerPage).Take(bigintPerPage).CopyToDataTable();

            return dtPage;
        }

        //[WebMethod]
        //public systemDataObject[] getGST()
        //{

        //    database curDb = new database();
        //    curDb.openConnection();

        //    List<sqlQueryParameter> curQueryParameter = new List<sqlQueryParameter>()
        //    {

        //    };

        //    DataTable curDt = curDb.getDataTable("[selectGST]", curQueryParameter);

        //    List<systemDataObject> list = new List<systemDataObject>();
        //    for (int i = 0; i < curDt.Rows.Count; i++)
        //    {
        //        list.Add(new systemDataObject(curDt.Rows[i][0].ToString()
        //            , curDt.Rows[i][1].ToString()
        //            , curDt.Rows[i][2].ToString()
        //            ));
        //    }

        //    systemDataObject[] curList = list.ToArray();
        //    curDb.closeConnection();

        //    return curList;
        //}


    }
}
