﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="wmsGoldbell.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WMS</title>
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="Style/login.css"/>
    <!-- SweetAlert CSS -->
    <link rel="stylesheet" type="text/css" href="ajnRes/sweetalert2/sweetalert2.css" />
    <script src="ajnRes/sweetalert2/sweetalert2.js"></script>
    <script type="text/javascript">
        function tbUsername_click() {
            if (window.event.keyCode == 13) {
                document.getElementById('tbUserPassword').focus();
            }
        }

        function tbUserPassword_click() {
            if (window.event.keyCode == 13) {
                document.getElementById('btnUserLogin').click();
            }
        }
    </script>
</head>
<body" >

    <form id="form1" runat="server">
        <video  style="position: fixed;
                top: 50%;
                left: 50%;
                min-width: 100%;
                min-height: 100%;
                width: auto;
                height: auto;
                z-index: -100;
                transform: translateX(-50%) translateY(-50%);
                background-size: cover;
                transition: 1s opacity;"
                poster="images/cloud-login-poster.png" autoplay="" id="video_background" src="images/cloud_background.webm" loop=""></video>
        <%--<div style="height: 100%; width: 100%; position: absolute; color: white; 
            background-color: rgba(255,255,255,0.5); padding: 0; text-align: center; margin: 0;">
            <img style="margin:auto;margin-top:6%" height="100" width="400" src="images/imsTitle.png">
        </div>--%>

        <div id="fs-login">
            <div id="fs-login-glass"></div>
            <div id="fs-login-content">
                <img style="margin:auto;margin-top:1%" height="104" width="332" src="images/Goldbell-Logo2.png" />
                <div class="fs-login-input fs-login-input-username">
                    <asp:TextBox ID="tbUsername" runat="server" tabindex="1" class="fs-login-username" MaxLength="25" placeholder="Username" ></asp:TextBox>
                </div>
                <div class="fs-login-input fs-login-input-password">
                    <asp:TextBox ID="tbUserPassword" runat="server" tabindex="2" class="fs-login-password" MaxLength="25" TextMode="Password" placeholder="Password"></asp:TextBox>
                </div>
                <asp:Button ID="btnUserLogin" runat="server" Text="Login" type="button" OnClick="btnUserLogin_Click" CssClass="fs-login-input" style="cursor:pointer; top: 0px; left: 0px;" UseSubmitBehavior="False"/>
                <div class="pull-right"><asp:Label  ID="lblVersion" runat="server" Text="Label" style="display:block; float:right;">v1.0.18</asp:Label></div>
            </div>
        </div>
    </form>
</body>
</html>
