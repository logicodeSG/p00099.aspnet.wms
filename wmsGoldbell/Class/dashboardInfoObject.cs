﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class dashboardInfoObject
    {
        public String diTotalPOPending;
        public String diTotalPOCompleted;
        public String diTotalAJPending;
        public String diTotalAJCompleted;
        public String diTotalNTOPending;
        public String diTotalNTOCompleted;
        public String diTotalSTOPending;
        public String diTotalSTOCompleted;
        public String diTotalSOPending;
        public String diTotalSOCompleted;
        public String diTotalSRPending;
        public String diTotalSRCompleted;
        public String diTotalPRPending;
        public String diTotalPRCompleted;
        public String diTotalCJPending;
        public String diTotalCJCompleted;

        public dashboardInfoObject() { }

        public dashboardInfoObject(string diTotalPOPending, string diTotalPOCompleted, string diTotalAJPending, string diTotalAJCompleted, string diTotalNTOPending, string diTotalNTOCompleted, string diTotalSTOPending, string diTotalSTOCompleted, string diTotalSOPending, string diTotalSOCompleted, string diTotalSRPending, string diTotalSRCompleted, string diTotalPRPending, string diTotalPRCompleted, string diTotalCJPending, string diTotalCJCompleted)
        {
            this.diTotalPOPending = diTotalPOPending;
            this.diTotalPOCompleted = diTotalPOCompleted;
            this.diTotalAJPending = diTotalAJPending;
            this.diTotalAJCompleted = diTotalAJCompleted;
            this.diTotalNTOPending = diTotalNTOPending;
            this.diTotalNTOCompleted = diTotalNTOCompleted;
            this.diTotalSTOPending = diTotalSTOPending;
            this.diTotalSTOCompleted = diTotalSTOCompleted;
            this.diTotalSOPending = diTotalSOPending;
            this.diTotalSOCompleted = diTotalSOCompleted;
            this.diTotalSRPending = diTotalSRPending;
            this.diTotalSRCompleted = diTotalSRCompleted;
            this.diTotalPRPending = diTotalPRPending;
            this.diTotalPRCompleted = diTotalPRCompleted;
            this.diTotalCJPending = diTotalCJPending;
            this.diTotalCJCompleted = diTotalCJCompleted;
        }
    }
}