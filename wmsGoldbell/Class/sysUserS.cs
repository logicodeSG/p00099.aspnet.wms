﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class {
    public class sysUserS {
        public String userId;
        public String userCode;
        public String username;
        public String userLevelId;
        public String userLevelDesc;
        public Boolean userActive;
        public String userLastSelectedLocationId;
        public userLocationObject[] uloList;

        public sysUserS() { }

        public sysUserS(string userId, string userCode, string username, string userLevelId, 
            string userLevelDesc, Boolean userActive,
            string userLastSelectedLocationId, userLocationObject[] uloList = null) {
            this.userId = userId;
            this.userCode = userCode;
            this.username = username;
            this.userLevelId = userLevelId;
            this.userLevelDesc = userLevelDesc;
            this.userActive = userActive;
            this.userLastSelectedLocationId = userLastSelectedLocationId;
            this.uloList = uloList;
        }
    }
}