﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axTODetailObject
    {
        public String todId;
        public String todTOId;
        public String todAX_DataAreaId;
        public String todAX_RouteId;
        public String todAX_TransferId;
        public String todAX_InventTransId;
        public String todAX_ItemId;
        public String todAX_Dot_PartCode;
        public String todAX_Name;
        public String todAX_UnitId;
        public String todAX_InventLocationId;
        public String todAX_InventSiteId;
        public String todAX_Qty;
        public String todAX_DocType;
        public String todScanQuantity;
        public String todScanDatetime;
        public String todRemarks;
        public String todStatusId;
        public String todUserId;
        public String todDeviceSN;
        public String todUsername;
        public String todAX_WMSLocationId;
        public String todAX_NameAlias;


        public axTODetailObject() { }
        public axTODetailObject(string todId, string todTOId, string todAX_DataAreaId, string todAX_RouteId, string todAX_TransferId, string todAX_InventTransId, string todAX_ItemId, string todAX_Dot_PartCode, string todAX_Name, string todAX_UnitId, string todAX_InventLocationId, string todAX_InventSiteId, string todAX_Qty, string todAX_DocType, string todScanQuantity, string todScanDatetime, string todRemarks, string todStatusId, string todUserId, string todDeviceSN, string todUsername, string todAX_WMSLocationId, string todAX_NameAlias)
        {
            this.todId = todId;
            this.todTOId = todTOId;
            this.todAX_DataAreaId = todAX_DataAreaId;
            this.todAX_RouteId = todAX_RouteId;
            this.todAX_TransferId = todAX_TransferId;
            this.todAX_InventTransId = todAX_InventTransId;
            this.todAX_ItemId = todAX_ItemId;
            this.todAX_Dot_PartCode = todAX_Dot_PartCode;
            this.todAX_Name = todAX_Name;
            this.todAX_UnitId = todAX_UnitId;
            this.todAX_InventLocationId = todAX_InventLocationId;
            this.todAX_InventSiteId = todAX_InventSiteId;
            this.todAX_Qty = sharedRes.trimOffTrailingZero(todAX_Qty); ;
            this.todAX_DocType = todAX_DocType;
            this.todScanQuantity = sharedRes.trimOffTrailingZero(todScanQuantity);
            this.todScanDatetime = todScanDatetime;
            this.todRemarks = todRemarks;
            this.todStatusId = todStatusId;
            this.todUserId = todUserId;
            this.todDeviceSN = todDeviceSN;
            this.todUsername = todUsername;
            this.todAX_WMSLocationId = todAX_WMSLocationId;
            this.todAX_NameAlias = todAX_NameAlias;
        }
    }
}