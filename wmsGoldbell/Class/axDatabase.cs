﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Web.Configuration;
using wmsGoldbell.Class;
namespace wmsGoldbell.Class
{
    public class axDatabase : IDisposable
    {

        String connectionString = WebConfigurationManager.AppSettings["AX_DB_CONNECTION_STRING"];

        private SqlConnection conn = new SqlConnection();
        private SqlTransaction sqlTran;

        public axDatabase()
        {

        }

        public void openConnection()
        {
            conn.ConnectionString = connectionString;
            conn.Open();
            sqlTran = conn.BeginTransaction();
        }

        public void closeConnection()
        {
            sqlTran.Commit();
            conn.Close();
            conn.Dispose();
        }

        public void rollbackNCloseConnection()
        {
            sqlTran.Rollback();
            conn.Close();
            conn.Dispose();
        }

        public void checkIsConnectionUp()
        {
            if (conn.State == System.Data.ConnectionState.Open)
            {
                Debug.WriteLine(conn.Database + " is Up!");
            }
        }

        public Boolean checkIsConnectionClosed()
        {
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                return true;
                Debug.WriteLine("Database is Down!");
            }
            return false;
        }

        public DataTable getDataTable(string dbStoredProdName, List<sqlQueryParameter> dbArgParameter)
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Transaction = sqlTran;
                    string strParameter = "";
                    var arr = dbArgParameter.ToArray();

                    for (int i = 0; i < arr.Length; i++)
                    {
                        strParameter += arr[i].strParameter;
                        if ((i + 1) != arr.Length)
                            strParameter += ",";
                    }

                    cmd.CommandText = "Execute " + dbStoredProdName + " " + strParameter;

                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i].ParameterType == SqlDbType.VarChar)
                        {
                            cmd.Parameters.Add(arr[i].strParameter
                                                , arr[i].ParameterType
                                                , arr[i].ParameterLength).Value = (String)arr[i].keyData;
                        }
                        else if (arr[i].ParameterType == SqlDbType.BigInt)
                        {
                            cmd.Parameters.Add(arr[i].strParameter
                                                , arr[i].ParameterType).Value = (Int64)arr[i].keyData;
                        }
                        else if (arr[i].ParameterType == SqlDbType.SmallInt)
                        {
                            cmd.Parameters.Add(arr[i].strParameter
                                                , arr[i].ParameterType).Value = (Int32)arr[i].keyData;
                        }
                        else if (arr[i].ParameterType == SqlDbType.Int)
                        {
                            cmd.Parameters.Add(arr[i].strParameter
                                                , arr[i].ParameterType).Value = (Int32)arr[i].keyData;
                        }
                        else if (arr[i].ParameterType == SqlDbType.DateTime)
                        {
                            cmd.Parameters.Add(arr[i].strParameter
                                                , arr[i].ParameterType).Value = DateTime.Parse((String)arr[i].keyData);
                        }
                        else if (arr[i].ParameterType == SqlDbType.Bit)
                        {
                            cmd.Parameters.Add(arr[i].strParameter
                                                , arr[i].ParameterType).Value = (Boolean)arr[i].keyData;
                        }
                        else if (arr[i].ParameterType == SqlDbType.Decimal)
                        {
                            cmd.Parameters.Add(arr[i].strParameter
                                                , arr[i].ParameterType).Value = (Decimal)arr[i].keyData;
                        }
                        else if (arr[i].ParameterType == SqlDbType.NVarChar)
                        {
                            cmd.Parameters.Add(arr[i].strParameter
                                                , arr[i].ParameterType).Value = (String)arr[i].keyData;
                        }
                    }

                    sda.SelectCommand = cmd;
                    cmd.ExecuteNonQuery();

                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);

                        return dt;
                    }
                }
            }
        }

        public int insertUpdateDeleteDataIntoTable(string dbStoredProdName, List<sqlQueryParameter> dbArgParameter)
        {
            try
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Transaction = sqlTran;
                    string strParameter = "";
                    var arr = dbArgParameter.ToArray();

                    for (int i = 0; i < arr.Length; i++)
                    {
                        strParameter += arr[i].strParameter;
                        if ((i + 1) != arr.Length)
                            strParameter += ",";
                    }

                    cmd.CommandText = "Execute " + dbStoredProdName + " " + strParameter;

                    for (int i = 0; i < arr.Length; i++)
                    {
                        //if (arr[i].ParameterType == SqlDbType.VarChar)
                        //{
                        cmd.Parameters.Add(arr[i].strParameter
                                            , arr[i].ParameterType
                                            , arr[i].ParameterLength).Value = (Object)arr[i].keyData ?? DBNull.Value;
                        //}
                        //else if (arr[i].ParameterType == SqlDbType.Int)
                        //{
                        //    cmd.Parameters.Add(arr[i].strParameter
                        //                        , arr[i].ParameterType).Value = (Int32)arr[i].keyData;
                        //}
                        //else if (arr[i].ParameterType == SqlDbType.BigInt)
                        //{
                        //    cmd.Parameters.Add(arr[i].strParameter
                        //                        , arr[i].ParameterType).Value = (Int64)arr[i].keyData;
                        //}
                        //else if (arr[i].ParameterType == SqlDbType.SmallInt)
                        //{
                        //    cmd.Parameters.Add(arr[i].strParameter
                        //                        , arr[i].ParameterType).Value = (Int32)arr[i].keyData;
                        //}
                        //else if (arr[i].ParameterType == SqlDbType.TinyInt)
                        //{
                        //    cmd.Parameters.Add(arr[i].strParameter
                        //                        , arr[i].ParameterType).Value = (Int16)arr[i].keyData;
                        //}
                        //else if (arr[i].ParameterType == SqlDbType.DateTime)
                        //{
                        //    cmd.Parameters.Add(arr[i].strParameter
                        //                        , arr[i].ParameterType).Value = DateTime.Parse((String)arr[i].keyData);
                        //}
                        //else if (arr[i].ParameterType == SqlDbType.Bit)
                        //{
                        //    cmd.Parameters.Add(arr[i].strParameter
                        //                        , arr[i].ParameterType).Value = (Boolean)arr[i].keyData;
                        //}
                        //else if (arr[i].ParameterType == SqlDbType.Decimal)
                        //{
                        //    cmd.Parameters.Add(arr[i].strParameter
                        //                        , arr[i].ParameterType).Value = (Decimal)arr[i].keyData;
                        //}
                        //else if (arr[i].ParameterType == SqlDbType.NVarChar)
                        //{
                        //    cmd.Parameters.Add(arr[i].strParameter
                        //                        , arr[i].ParameterType).Value = (String)arr[i].keyData;
                        //}
                    }

                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception e)
            {
                return -99;
            }
        }
        

        void IDisposable.Dispose()
        {

        }
    }
}