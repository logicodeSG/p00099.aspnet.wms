﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class systemDataObject
    {
        public String systemDataId;
        public String systemDataName;
        public String systemDataValue;

        public systemDataObject() { }

        public systemDataObject(string systemDataId, string systemDataName, string systemDataValue)
        {
            this.systemDataId = systemDataId;
            this.systemDataName = systemDataName;
            this.systemDataValue = systemDataValue;

        }
    }
}