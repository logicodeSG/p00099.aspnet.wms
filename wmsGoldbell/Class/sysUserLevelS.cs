﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class {
    public class sysUserLevelS
    {
        public String userLevelId;
        public String userLevelOrder;
        public String userLevelDesc;
        public Boolean userLevelActive;

        public sysUserLevelS() { }

        public sysUserLevelS(string userLevelId, string userLevelOrder, string userLevelDesc, Boolean userLevelActive)
        {
            this.userLevelId = userLevelId;
            this.userLevelOrder = userLevelOrder;
            this.userLevelDesc = userLevelDesc;
            this.userLevelActive = userLevelActive;
        }
    }
}