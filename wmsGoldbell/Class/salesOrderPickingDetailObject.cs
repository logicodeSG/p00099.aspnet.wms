﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell
{
    public class salesOrderPickingDetailObject
    {
        public String sopDetailId;
        public String sopDetailTPId;
        public String sopDetailItemId;
        public String sopDetailItemCode;
        public String sopDetailItemDescription;
        public String sopDetailItemUOM;
        public String sopDetailItemPickedQuantity;
        public String sopDetailItemGroupId;
        public String sopDetailItemBinId;

        public salesOrderPickingDetailObject() { }

        public salesOrderPickingDetailObject(string sopDetailId, string sopDetailTPId, string sopDetailItemId,
            string sopDetailItemCode, string sopDetailItemDescription, string sopDetailItemUOM,
            string sopDetailItemPickedQuantity, string sopDetailItemGroupId, string sopDetailItemBinId)
        {
            this.sopDetailId = sopDetailId;
            this.sopDetailTPId = sopDetailTPId;
            this.sopDetailItemId = sopDetailItemId;
            this.sopDetailItemCode = sopDetailItemCode;
            this.sopDetailItemDescription = sopDetailItemDescription;
            this.sopDetailItemUOM = sopDetailItemUOM;
            this.sopDetailItemPickedQuantity = sopDetailItemPickedQuantity;
            this.sopDetailItemGroupId = sopDetailItemGroupId;
            this.sopDetailItemBinId = sopDetailItemBinId;
        }
    }
}