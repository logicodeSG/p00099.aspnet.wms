﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axInventoryObject
    {
      public String axId;
      public String axDATAAREAID;
      public String axITEMID;
      public String axDOT_PARTCODE;
      public String axNAME;
      public String axDESCRIPTION;
      public String axITEMTYPE;
      public String axInventUnitID;
      public String axPurhUnitID;
      public String axSalesUnitID;
      public String axIsSerialized;
      public String axActive;
      public String axStartDate;
      public String axPRODUCT;
      public String axCREATEDDATETIME;
      public String axMODIFIEDDATETIME;
      public String axITEMGROUPID;

    public axInventoryObject() { }

        public axInventoryObject(string axId, string axDATAAREAID, string axITEMID, string axDOT_PARTCODE, string axNAME, string axDESCRIPTION, string axITEMTYPE, string axInventUnitID, string axPurhUnitID, string axSalesUnitID, string axIsSerialized, string axActive, string axStartDate, string axPRODUCT, string axCREATEDDATETIME, string axMODIFIEDDATETIME, string axITEMGROUPID)
        {
            this.axId = axId;
            this.axDATAAREAID = axDATAAREAID;
            this.axITEMID = axITEMID;
            this.axDOT_PARTCODE = axDOT_PARTCODE;
            this.axNAME = axNAME;
            this.axDESCRIPTION = axDESCRIPTION;
            this.axITEMTYPE = axITEMTYPE;
            this.axInventUnitID = axInventUnitID;
            this.axPurhUnitID = axPurhUnitID;
            this.axSalesUnitID = axSalesUnitID;
            this.axIsSerialized = axIsSerialized;
            this.axActive = axActive;
            this.axStartDate = axStartDate;
            this.axPRODUCT = axPRODUCT;
            this.axCREATEDDATETIME = axCREATEDDATETIME;
            this.axMODIFIEDDATETIME = axMODIFIEDDATETIME;
            this.axITEMGROUPID = axITEMGROUPID;
        }

        public axInventoryObject(string axId, string axITEMID, string axDOT_PARTCODE)
        {
            this.axId = axId;
            this.axITEMID = axITEMID;
            this.axDOT_PARTCODE = axDOT_PARTCODE;
        }


        public axInventoryObject(string axId, string axDATAAREAID, string axITEMID, string axDOT_PARTCODE, string axNAME)
        {
            this.axId = axId;
            this.axDATAAREAID = axDATAAREAID;
            this.axITEMID = axITEMID;
            this.axDOT_PARTCODE = axDOT_PARTCODE;
            this.axNAME = axNAME;
        }
    }
}