﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class uomObject
    {
        public String uomId;
        public String uomName;
        public Boolean uomActive;

        public uomObject() { }

        public uomObject(string uomId, string uomName, Boolean uomActive)
        {
            this.uomId = uomId;
            this.uomName = uomName;
            this.uomActive = uomActive;
        }
    }
}