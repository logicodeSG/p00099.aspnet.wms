﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axSODetailObject
    {
        public String sodId;
        public String sodSOId;
        public String sodAX_DataAreaId;
        public String sodAX_LineNumber;
        public String sodAX_RouteId;
        public String sodAX_InventTransId;
        public String sodAX_ItemId;
        public String sodAX_Dot_PartCode;
        public String sodAX_Name;
        public String sodAX_SalesUnit;
        public String sodAX_InventLocationId;
        public String sodAX_InventSiteId;
        public String sodAX_WMSLocationId;
        public String sodAX_Qty;
        public String sodScanQuantity;
        public String sodScanDatetime;
        public String sodRemarks;
        public String sodStatusId;
        public String sodUserId;
        public String sodDeviceSN;
        public String sodUsername;

        public axSODetailObject() { }

        public axSODetailObject(string sodId, string sodSOId, string sodAX_DataAreaId, string sodAX_LineNumber, string sodAX_RouteId, string sodAX_InventTransId, string sodAX_ItemId, string sodAX_Dot_PartCode, string sodAX_Name, string sodAX_SalesUnit, string sodAX_InventLocationId, string sodAX_InventSiteId, string sodAX_WMSLocationId, string sodAX_Qty, string sodScanQuantity, string sodScanDatetime, string sodRemarks, string sodStatusId, string sodUserId, string sodDeviceSN, string sodUsername)
        {
            this.sodId = sodId;
            this.sodSOId = sodSOId;
            this.sodAX_DataAreaId = sodAX_DataAreaId;
            this.sodAX_LineNumber = sharedRes.trimOffTrailingZero(sodAX_LineNumber);
            this.sodAX_RouteId = sodAX_RouteId;
            this.sodAX_InventTransId = sodAX_InventTransId;
            this.sodAX_ItemId = sodAX_ItemId;
            this.sodAX_Dot_PartCode = sodAX_Dot_PartCode;
            this.sodAX_Name = sodAX_Name;
            this.sodAX_SalesUnit = sodAX_SalesUnit;
            this.sodAX_InventLocationId = sodAX_InventLocationId;
            this.sodAX_InventSiteId = sodAX_InventSiteId;
            this.sodAX_WMSLocationId = sodAX_WMSLocationId;
            this.sodAX_Qty = sharedRes.trimOffTrailingZero(sodAX_Qty);
            this.sodScanQuantity = sharedRes.trimOffTrailingZero(sodScanQuantity);
            this.sodScanDatetime = sodScanDatetime;
            this.sodRemarks = sodRemarks;
            this.sodStatusId = sodStatusId;
            this.sodUserId = sodUserId;
            this.sodDeviceSN = sodDeviceSN;
            this.sodUsername = sodUsername;
        }
    }
}