﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class userLocationObject
    {
        public String userId;
        public String locationId;
        public String locationCode;
        public String locationDescription;

        public userLocationObject() { }

        public userLocationObject(string userId, string locationId, string locationCode, string locationDescription)
        {
            this.userId = userId;
            this.locationId = locationId;
            this.locationCode = locationCode;
            this.locationDescription = locationDescription;
        }
    }
}