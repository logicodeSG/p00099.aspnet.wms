﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axCJObject
    {
        public String cjId;
        public String cjAX_DataAreaId;
        public String cjAX_PostedDatetime;
        public String cjAX_JournalId;
        public String cjAX_Description;
        public String cjReferenceNo;
        public String cjCreationDatetime;
        public String cjCompletionDatetime;
        public String cjScanDatetime;
        public String cjStatusId;
        public String cjDocType;
        public String cjReferenceVersion;
        public String cjUserId;
        public String cjDeviceSN;
        public String cjUsername;

        public axCJObject() { }

        public axCJObject(string cjId, string cjAX_DataAreaId, string cjAX_PostedDatetime, string cjAX_JournalId, string cjAX_Description, string cjReferenceNo, string cjCreationDatetime, string cjCompletionDatetime, string cjScanDatetime, string cjStatusId, string cjDocType, string cjReferenceVersion, string cjUserId, string cjDeviceSN, string cjUsername)
        {
            this.cjId = cjId;
            this.cjAX_DataAreaId = cjAX_DataAreaId;
            this.cjAX_PostedDatetime = cjAX_PostedDatetime;
            this.cjAX_JournalId = cjAX_JournalId;
            this.cjAX_Description = cjAX_Description;
            this.cjReferenceNo = cjReferenceNo;
            this.cjCreationDatetime = cjCreationDatetime;
            this.cjCompletionDatetime = cjCompletionDatetime;
            this.cjScanDatetime = cjScanDatetime;
            this.cjStatusId = cjStatusId;
            this.cjDocType = cjDocType;
            this.cjReferenceVersion = cjReferenceVersion;
            this.cjUserId = cjUserId;
            this.cjDeviceSN = cjDeviceSN;
            this.cjUsername = cjUsername;
        }
    }
}