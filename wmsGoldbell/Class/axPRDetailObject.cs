﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axPRDetailObject
    {
        public String prDetailId;
        public String prDetailPRId;
        public String prDetailItemId;
        public String prDetailItemCode;
        public String prDetailItemDescription;
        public String prDetailItemUOM;
        public String prDetailItemQuantity;
        public String prDetailItemPickedQuantity;
        public String prDetailItemGroupId;
        public String prDetailItemBinId;


        public axPRDetailObject() { }

        public axPRDetailObject(string prDetailId, string prDetailPRId, string prDetailItemId,
            string prDetailItemCode, string prDetailItemDescription, string prDetailItemUOM, string prDetailItemQuantity,
            string prDetailItemPickedQuantity, string prDetailItemGroupId, string prDetailItemBinId)
        {
            this.prDetailId = prDetailId;
            this.prDetailPRId = prDetailPRId;
            this.prDetailItemCode = prDetailItemCode;
            this.prDetailItemDescription = prDetailItemDescription;
            this.prDetailItemId = prDetailItemId;
            this.prDetailItemUOM = prDetailItemUOM;
            this.prDetailItemQuantity = prDetailItemQuantity;
            this.prDetailItemPickedQuantity = prDetailItemPickedQuantity;
            this.prDetailItemGroupId = prDetailItemGroupId;
            this.prDetailItemBinId = prDetailItemBinId;
        }
    }
}