﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class printerObject
    {
        public String printerId;
        public String printerName;
        public Boolean printerActive;
        public Boolean printerIsDefault;

        public printerObject() { }

        public printerObject(string printerId, string printerName, bool printerActive, bool printerIsDefault)
        {
            this.printerId = printerId;
            this.printerName = printerName;
            this.printerActive = printerActive;
            this.printerIsDefault = printerIsDefault;
        }
    }
}