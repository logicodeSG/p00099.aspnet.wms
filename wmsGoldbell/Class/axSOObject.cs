﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axSOObject
    {
        public String soId;
        public String soAX_DataAreaId;
        public String soAX_PickingRouteId;
        public String soAX_SalesId;
        public String soAX_ConfirmationDatetime;
        public String soAX_CreationDatetime;
        public String soAX_SalesType;
        public String soReferenceNo;
        public String soCreationDatetime;
        public String soCompletionDatetime;
        public String soScanDatetime;
        public String soStatusId;
        public String soDocType;
        public String soReferenceVersion;
        public String soUserId;
        public String soDeviceSN;
        public String soUsername;
        public String soAX_CustomerAccount;
        public String soAX_CustomerName;

        public axSOObject() { }

        public axSOObject(string soId, string soAX_DataAreaId, string soAX_PickingRouteId, 
            string soAX_SalesId, string soAX_ConfirmationDatetime, string soAX_CreationDatetime, 
            string soAX_SalesType, string soReferenceNo, string soCreationDatetime, string soCompletionDatetime, 
            string soScanDatetime, string soStatusId, string soDocType, string soReferenceVersion, 
            string soUserId, string soDeviceSN, string soUsername,
            string soAX_CustomerAccount, string soAX_CustomerName)
        {
            this.soId = soId;
            this.soAX_DataAreaId = soAX_DataAreaId;
            this.soAX_PickingRouteId = soAX_PickingRouteId;
            this.soAX_SalesId = soAX_SalesId;
            this.soAX_ConfirmationDatetime = soAX_ConfirmationDatetime;
            this.soAX_CreationDatetime = soAX_CreationDatetime;
            this.soAX_SalesType = soAX_SalesType;
            this.soReferenceNo = soReferenceNo;
            this.soCreationDatetime = soCreationDatetime;
            this.soCompletionDatetime = soCompletionDatetime;
            this.soScanDatetime = soScanDatetime;
            this.soStatusId = soStatusId;
            this.soDocType = soDocType;
            this.soReferenceVersion = soReferenceVersion;
            this.soUserId = soUserId;
            this.soDeviceSN = soDeviceSN;
            this.soUsername = soUsername;
            this.soAX_CustomerAccount = soAX_CustomerAccount;
            this.soAX_CustomerName = soAX_CustomerName;
        }
    }
}