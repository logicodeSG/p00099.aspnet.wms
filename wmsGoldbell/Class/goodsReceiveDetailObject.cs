﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class goodsReceiveDetailObject
    {
        public String grDetailId;
        public String grDetailGRId;
        public String grDetailItemId;
        public String grDetailItemCode;
        public String grDetailItemDescription;
        public String grDetailItemUOM;
        public String grDetailItemReceiveQuantity;
        public String grDetailItemLocationName;
        public String grDetailItemLocationId;


        public goodsReceiveDetailObject() { }

        public goodsReceiveDetailObject(string grDetailId, string grDetailGRId, string grDetailItemId, 
            string grDetailItemCode, string grDetailItemDescription, string grDetailItemUOM, 
            string grDetailItemReceiveQuantity, string grDetailItemLocationName, string grDetailItemLocationId)
        {
            this.grDetailId = grDetailId;
            this.grDetailGRId = grDetailGRId;
            this.grDetailItemId = grDetailItemId;
            this.grDetailItemCode = grDetailItemCode;
            this.grDetailItemDescription = grDetailItemDescription;
            this.grDetailItemUOM = grDetailItemUOM;
            this.grDetailItemReceiveQuantity = grDetailItemReceiveQuantity;
            this.grDetailItemLocationName = grDetailItemLocationName;
            this.grDetailItemLocationId = grDetailItemLocationId;

        }
    }
}