﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class transferPickingObject
    {
        public String tpId;
        public String tpReferenceNo;
        public String tpTOReferenceNo;
        public String tpPickingDate;
        public String tpCreatedBy;
        public String tpStatusId;
        public String tpLocationId;
        public String tpReferenceVersion;

        public transferPickingObject() { }

        public transferPickingObject(string tpId, string tpReferenceNo, string tpTOReferenceNo, string tpPickingDate, string tpCreatedBy, string tpStatusId, string tpLocationId, string tpReferenceVersion)
        {
            this.tpId = tpId;
            this.tpReferenceNo = tpReferenceNo;
            this.tpTOReferenceNo = tpTOReferenceNo;
            this.tpPickingDate = tpPickingDate;
            this.tpCreatedBy = tpCreatedBy;
            this.tpStatusId = tpStatusId;
            this.tpLocationId = tpLocationId;
            this.tpReferenceVersion = tpReferenceVersion;
        }
    }
}