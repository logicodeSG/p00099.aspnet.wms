﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class userAccessObject
    {
        public String userAccessId;
        public String userAccessName;


        public userAccessObject() { }

        public userAccessObject(string userAccessId, string userAccessName)
        {
            this.userAccessId = userAccessId;
            this.userAccessName = userAccessName;
        }
    
    }
}