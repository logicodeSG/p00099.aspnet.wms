﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class docNumberObject
    {
        public String docNumberId;
        public String docNumberType;
        public String docNumberNextNumber;
        public String docNumberLength;
        public String docNumberPrefix;


        public docNumberObject() { }

        public docNumberObject(string docNumberId, string docNumberType,
             string docNumberNextNumber, string docNumberLength, string docNumberPrefix)
        {
            this.docNumberId = docNumberId;
            this.docNumberType = docNumberType;
            this.docNumberNextNumber = docNumberNextNumber;
            this.docNumberLength = docNumberLength;
            this.docNumberPrefix = docNumberPrefix;

        }
    }
}