﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class xUserLevelXAccessObject
    {
        public String userLevelId;
        public String userAccessId;
        public String canRead;
        public String canAdd;
        public String canEdit;
        public String canDelete;
        public String canPrint;


        public xUserLevelXAccessObject() { }

        public xUserLevelXAccessObject(string userLevelId, string userAccessId, string canRead, string canAdd
            , string canEdit, string canDelete, string canPrint)
        {
            this.userLevelId = userLevelId;
            this.userAccessId = userAccessId;
            this.canRead = canRead;
            this.canAdd = canAdd;
            this.canEdit = canEdit;
            this.canDelete = canDelete;
            this.canPrint = canPrint;
        }
    }
}