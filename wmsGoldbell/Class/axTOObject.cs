﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axTOObject
    {
        public String toId;
        public String toAX_DataAreaId;
        public String toAX_ModifiedDatetime;
        public String toAX_PickingRouteId;
        public String toAX_TransferId;
        public String toAX_DocumentType;
        public String toAX_InventLocationIdFrom;
        public String toAX_InventLocationIdTo;
        public String toAX_TransferStatus;
        public String toReferenceNo;
        public String toCreationDatetime;
        public String toCompletionDatetime;
        public String toScanDatetime;
        public String toStatusId;
        public String toDocType;
        public String toReferenceVersion;
        public String toUserId;
        public String toDeviceSN;
        public String toUsername;

        public axTOObject() { }

        public axTOObject(string toId, string toAX_DataAreaId, string toAX_ModifiedDatetime, string toAX_PickingRouteId, string toAX_TransferId, string toAX_DocumentType, string toAX_InventLocationIdFrom, string toAX_InventLocationIdTo, string toAX_TransferStatus, string toReferenceNo, string toCreationDatetime, string toCompletionDatetime, string toScanDatetime, string toStatusId, string toDocType, string toReferenceVersion, string toUserId, string toDeviceSN, string toUsername)
        {
            this.toId = toId;
            this.toAX_DataAreaId = toAX_DataAreaId;
            this.toAX_ModifiedDatetime = toAX_ModifiedDatetime;
            this.toAX_PickingRouteId = toAX_PickingRouteId;
            this.toAX_TransferId = toAX_TransferId;
            this.toAX_DocumentType = toAX_DocumentType;
            this.toAX_InventLocationIdFrom = toAX_InventLocationIdFrom;
            this.toAX_InventLocationIdTo = toAX_InventLocationIdTo;
            this.toAX_TransferStatus = toAX_TransferStatus;
            this.toReferenceNo = toReferenceNo;
            this.toCreationDatetime = toCreationDatetime;
            this.toCompletionDatetime = toCompletionDatetime;
            this.toScanDatetime = toScanDatetime;
            this.toStatusId = toStatusId;
            this.toDocType = toDocType;
            this.toReferenceVersion = toReferenceVersion;
            this.toUserId = toUserId;
            this.toDeviceSN = toDeviceSN;
            this.toUsername = toUsername;
        }
    }
}