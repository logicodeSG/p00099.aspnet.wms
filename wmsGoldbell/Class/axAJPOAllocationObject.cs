﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axAJPOAllocationObject
    {
        public String ajAX_PurchId;
        public String ajAX_InventTransId;
        public String ajAX_SO_Number;
        public String ajAX_Customer_Name;
        public String ajAX_QtyAllocated;
        public String ajAX_QtyOnStock;

        public axAJPOAllocationObject() { }

        public axAJPOAllocationObject(string ajAX_PurchId, string ajAX_InventTransId, string ajAX_SO_Number, string ajAX_Customer_Name, string ajAX_QtyAllocated, string ajAX_QtyOnStock)
        {
            this.ajAX_PurchId = ajAX_PurchId;
            this.ajAX_InventTransId = ajAX_InventTransId;
            this.ajAX_SO_Number = ajAX_SO_Number;
            this.ajAX_Customer_Name = ajAX_Customer_Name;
            this.ajAX_QtyAllocated = sharedRes.trimOffTrailingZero(ajAX_QtyAllocated);
            this.ajAX_QtyOnStock = sharedRes.trimOffTrailingZero(ajAX_QtyOnStock);
        }
    }
}