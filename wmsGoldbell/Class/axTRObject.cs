﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axTRObject
    {
        public String trId;
        public String trAX_DataAreaId;
        public String trAX_ModifiedDatetime;
        public String trAX_VoucherId;
        public String trAX_TransferId;
        public String trAX_DocumentType;
        public String trAX_InventLocationIdFrom;
        public String trAX_InventLocationIdTo;
        public String trAX_TransferStatus;
        public String trReferenceNo;
        public String trCreationDatetime;
        public String trCompletionDatetime;
        public String trScanDatetime;
        public String trStatusId;
        public String trDocType;
        public String trReferenceVersion;
        public String trUserId;
        public String trDeviceSN;
        public String trUsername;

        public axTRObject() { }

        public axTRObject(string trId, string trAX_DataAreaId, string trAX_ModifiedDatetime, string trAX_VoucherId, string trAX_TransferId, string trAX_DocumentType, string trAX_InventLocationIdFrom, string trAX_InventLocationIdTo, string trAX_TransferStatus, string trReferenceNo, string trCreationDatetime, string trCompletionDatetime, string trScanDatetime, string trStatusId, string trDocType, string trReferenceVersion, string trUserId, string trDeviceSN, string trUsername)
        {
            this.trId = trId;
            this.trAX_DataAreaId = trAX_DataAreaId;
            this.trAX_ModifiedDatetime = trAX_ModifiedDatetime;
            this.trAX_VoucherId = trAX_VoucherId;
            this.trAX_TransferId = trAX_TransferId;
            this.trAX_DocumentType = trAX_DocumentType;
            this.trAX_InventLocationIdFrom = trAX_InventLocationIdFrom;
            this.trAX_InventLocationIdTo = trAX_InventLocationIdTo;
            this.trAX_TransferStatus = trAX_TransferStatus;
            this.trReferenceNo = trReferenceNo;
            this.trCreationDatetime = trCreationDatetime;
            this.trCompletionDatetime = trCompletionDatetime;
            this.trScanDatetime = trScanDatetime;
            this.trStatusId = trStatusId;
            this.trDocType = trDocType;
            this.trReferenceVersion = trReferenceVersion;
            this.trUserId = trUserId;
            this.trDeviceSN = trDeviceSN;
            this.trUsername = trUsername;
        }
    }
}