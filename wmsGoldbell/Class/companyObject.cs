﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class companyObject
    {
        public String companyName;
        public String companyAddress1;
        public String companyAddress2;
        public String companyAddress3;
        public String companyPhoneNumber;
        public String companyFaxNumber;
        public String companyEmail;
        public String companyWebsite;
        public String companyRegNumber;
        public String companyGSTRegNumber;

        public companyObject() { }

        public companyObject(string companyName, string companyAddress1, string companyAddress2, string companyAddress3,
            string companyPhoneNumber, string companyFaxNumber, string companyEmail, string companyWebsite, string companyRegNumber, string companyGSTRegNumber)
        {
            this.companyName = companyName;
            this.companyAddress1 = companyAddress1;
            this.companyAddress2 = companyAddress2;
            this.companyAddress3 = companyAddress3;
            this.companyPhoneNumber = companyPhoneNumber;
            this.companyFaxNumber = companyFaxNumber;
            this.companyEmail = companyEmail;
            this.companyWebsite = companyWebsite;
            this.companyRegNumber = companyRegNumber;
            this.companyGSTRegNumber = companyGSTRegNumber;

        }
    }
}