﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axCJDetailObject
    {
        public String cjdId;
        public String cjdCJId;
        public String cjdAX_JournalId;
        public String cjdAX_LineNumber;
        public String cjdAX_ItemId;
        public String cjdAX_Dot_PartCode;
        public String cjdAX_Qty;
        public String cjdAX_InventUnitId;
        public String cjdAX_InventLocationId;
        public String cjdAX_InventSiteId;
        public String cjdAX_WMSLocationId;
        public String cjdScanQuantity;
        public String cjdScanDatetime;
        public String cjdRemarks;
        public String cjdStatusId;
        public String cjdUserId;
        public String cjdDeviceSN;
        public String cjdUsername;
        public String cjdAX_Product_Name;

        public axCJDetailObject() { }

        public axCJDetailObject(string cjdId, string cjdCJId, string cjdAX_JournalId, string cjdAX_LineNumber, string cjdAX_ItemId, string cjdAX_Dot_PartCode, string cjdAX_Qty, string cjdAX_InventUnitId, string cjdAX_InventLocationId, string cjdAX_InventSiteId, string cjdAX_WMSLocationId, string cjdScanQuantity, string cjdScanDatetime, string cjdRemarks, string cjdStatusId, string cjdUserId, string cjdDeviceSN, string cjdUsername, string cjdAX_Product_Name)
        {
            this.cjdId = cjdId;
            this.cjdCJId = cjdCJId;
            this.cjdAX_JournalId = cjdAX_JournalId;
            this.cjdAX_LineNumber = sharedRes.trimOffTrailingZero(cjdAX_LineNumber);
            this.cjdAX_ItemId = cjdAX_ItemId;
            this.cjdAX_Dot_PartCode = cjdAX_Dot_PartCode;
            this.cjdAX_Qty = sharedRes.trimOffTrailingZero(cjdAX_Qty);
            this.cjdAX_InventUnitId = cjdAX_InventUnitId;
            this.cjdAX_InventLocationId = cjdAX_InventLocationId;
            this.cjdAX_InventSiteId = cjdAX_InventSiteId;
            this.cjdAX_WMSLocationId = cjdAX_WMSLocationId;
            this.cjdScanQuantity = sharedRes.trimOffTrailingZero(cjdScanQuantity);
            this.cjdScanDatetime = cjdScanDatetime;
            this.cjdRemarks = cjdRemarks;
            this.cjdStatusId = cjdStatusId;
            this.cjdUserId = cjdUserId;
            this.cjdDeviceSN = cjdDeviceSN;
            this.cjdUsername = cjdUsername;
            this.cjdAX_Product_Name = cjdAX_Product_Name;
        }
    }
}