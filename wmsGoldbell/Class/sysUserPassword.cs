﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class sysUserPassword
    {
        public String userId;
        public String userCode;
        public String username;
        public String userPassword;
        public String userLevelId;
        public String userLevelDesc;
        public Boolean userActive;
        public String userLastSelectedLocationId;

        public sysUserPassword() { }

        public sysUserPassword(string userId, string userCode, string username, string userPassword,
            string userLevelId, string userLevelDesc, Boolean userActive,
            string userLastSelectedLocationId)
        {
            this.userId = userId;
            this.userCode = userCode;
            this.username = username;
            this.userPassword = userPassword;
            this.userLevelId = userLevelId;
            this.userLevelDesc = userLevelDesc;
            this.userActive = userActive;
            this.userLastSelectedLocationId = userLastSelectedLocationId;
        }

    }
}