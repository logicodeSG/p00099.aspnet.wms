﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axPOObject
    {
        public String poId;
        public String poAX_DataAreaId;
        public String poAX_PurchId;
        public String poAX_PurchName;
        public String poAX_OrderAccount;
        public String poAX_InvoiceAccount;
        public String poAX_LastConfirmationDatetime;
        public String poAX_DeliveryDatetime;
        public String poAX_CreationDatetime;
        public String poReferenceNo;
        public String poCreationDatetime;
        public String poCompletionDatetime;
        public String poScanDatetime;
        public String poStatusId;
        public String poDocType;
        public String poReferenceVersion;
        public String poUserId;
        public String poDeviceSN;
        public String poUsername;

        public axPOObject() { }

        public axPOObject(string poId, string poAX_DataAreaId, string poAX_PurchId, string poAX_PurchName, string poAX_OrderAccount, string poAX_InvoiceAccount, string poAX_LastConfirmationDatetime, string poAX_DeliveryDatetime, string poAX_CreationDatetime, string poReferenceNo, string poCreationDatetime, string poCompletionDatetime, string poScanDatetime, string poStatusId, string poDocType, string poReferenceVersion, string poUserId, string poDeviceSN, string poUsername)
        {
            this.poId = poId;
            this.poAX_DataAreaId = poAX_DataAreaId;
            this.poAX_PurchId = poAX_PurchId;
            this.poAX_PurchName = poAX_PurchName;
            this.poAX_OrderAccount = poAX_OrderAccount;
            this.poAX_InvoiceAccount = poAX_InvoiceAccount;
            this.poAX_LastConfirmationDatetime = poAX_LastConfirmationDatetime;
            this.poAX_DeliveryDatetime = poAX_DeliveryDatetime;
            this.poAX_CreationDatetime = poAX_CreationDatetime;
            this.poReferenceNo = poReferenceNo;
            this.poCreationDatetime = poCreationDatetime;
            this.poCompletionDatetime = poCompletionDatetime;
            this.poScanDatetime = poScanDatetime;
            this.poStatusId = poStatusId;
            this.poDocType = poDocType;
            this.poReferenceVersion = poReferenceVersion;
            this.poUserId = poUserId;
            this.poDeviceSN = poDeviceSN;
            this.poUsername = poUsername;
        }
    }
}