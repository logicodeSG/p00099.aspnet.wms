﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class stockTakeDetailObject
    {
        public String stDetailId;
        public String stDetailCJId;
        public String stDetailItemId;
        public String stDetailItemCode;
        public String stDetailItemDescription;
        public String stDetailItemUOM;
        public String stDetailItemQuantity;
        public String stDetailItemGroupId;
        public String stDetailItemBinId;

        public stockTakeDetailObject() { }

        public stockTakeDetailObject(string stDetailId, string stDetailCJId, string stDetailItemId, string stDetailItemCode, string stDetailItemDescription, string stDetailItemUOM, string stDetailItemQuantity, string stDetailItemGroupId, string stDetailItemBinId)
        {
            this.stDetailId = stDetailId;
            this.stDetailCJId = stDetailCJId;
            this.stDetailItemId = stDetailItemId;
            this.stDetailItemCode = stDetailItemCode;
            this.stDetailItemDescription = stDetailItemDescription;
            this.stDetailItemUOM = stDetailItemUOM;
            this.stDetailItemQuantity = stDetailItemQuantity;
            this.stDetailItemGroupId = stDetailItemGroupId;
            this.stDetailItemBinId = stDetailItemBinId;
        }
    }
}