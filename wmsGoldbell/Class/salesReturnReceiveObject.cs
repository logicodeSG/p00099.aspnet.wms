﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class salesReturnReceiveObject
    {
        public String srrId;
        public String srrReferenceNo;
        public String srrSRReferenceNo;
        public String srrPickingDate;
        public String srrCreatedBy;
        public String srrStatusId;
        public String srrLocationId;
        public String srrReferenceVersion;

        public salesReturnReceiveObject() { }

        public salesReturnReceiveObject(string srrId, string srrReferenceNo, string srrSRReferenceNo, string srrPickingDate, string srrCreatedBy, string srrStatusId, string srrLocationId, string srrReferenceVersion)
        {
            this.srrId = srrId;
            this.srrReferenceNo = srrReferenceNo;
            this.srrSRReferenceNo = srrSRReferenceNo;
            this.srrPickingDate = srrPickingDate;
            this.srrCreatedBy = srrCreatedBy;
            this.srrStatusId = srrStatusId;
            this.srrLocationId = srrLocationId;
            this.srrReferenceVersion = srrReferenceVersion;
        }
    }
}