﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class locationObject
    {
        public String locationId;
        public String locationCode;
        public String locationDescription;
        public String locationLevelId;
        public String locationParentId;
        public Boolean locationActive;


        public locationObject() { }

        public locationObject(string locationId, string locationCode, string locationDescription, string locationLevelId, string locationParentId, Boolean locationActive)
        {
            this.locationId = locationId;
            this.locationCode = locationCode;
            this.locationDescription = locationDescription;
            this.locationLevelId = locationLevelId;
            this.locationParentId = locationParentId;
            this.locationActive = locationActive;

        }
    }
}