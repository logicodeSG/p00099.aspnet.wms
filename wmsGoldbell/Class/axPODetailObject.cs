﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axPODetailObject
    {
        public String podId;
        public String podPOId;
        public String podAX_DataAreaId;
        public String podAX_PurchId;
        public String podAX_LineNumber;
        public String podAX_ItemId;
        public String podAX_Name;
        public String podAX_Dot_PartCode;
        public String podAX_OrderAccount;
        public String podAX_InvoiceAccount;
        public String podAX_PurchUnit;
        public String podAX_InventLocationId;
        public String podAX_InventSiteId;
        public String podAX_WMSLocationId;
        public String podAX_QtyOrdered;
        public String podAX_PurchReceivedNow;
        public String podScanQuantity;
        public String podScanDatetime;
        public String podRemarks;
        public String podStatusId;
        public String podUserId;
        public String podDeviceSN;
        public String podUsername;
        public String podGRNDocNo;

        public axPODetailObject() { }

        public axPODetailObject(string podId, string podPOId, string podAX_DataAreaId, string podAX_PurchId, string podAX_LineNumber, string podAX_ItemId, string podAX_Name, string podAX_Dot_PartCode, string podAX_OrderAccount, string podAX_InvoiceAccount, string podAX_PurchUnit, string podAX_InventLocationId, string podAX_InventSiteId, string podAX_WMSLocationId, string podAX_QtyOrdered, string podAX_PurchReceivedNow, string podScanQuantity, string podScanDatetime, string podRemarks, string podStatusId, string podUserId, string podDeviceSN, string podUsername, string podGRNDocNo)
        {
            this.podId = podId;
            this.podPOId = podPOId;
            this.podAX_DataAreaId = podAX_DataAreaId;
            this.podAX_PurchId = podAX_PurchId;
            this.podAX_LineNumber = sharedRes.trimOffTrailingZero(podAX_LineNumber);
            this.podAX_ItemId = podAX_ItemId;
            this.podAX_Name = podAX_Name;
            this.podAX_Dot_PartCode = podAX_Dot_PartCode;
            this.podAX_OrderAccount = podAX_OrderAccount;
            this.podAX_InvoiceAccount = podAX_InvoiceAccount;
            this.podAX_PurchUnit = podAX_PurchUnit;
            this.podAX_InventLocationId = podAX_InventLocationId;
            this.podAX_InventSiteId = podAX_InventSiteId;
            this.podAX_WMSLocationId = podAX_WMSLocationId;
            this.podAX_QtyOrdered = sharedRes.trimOffTrailingZero(podAX_QtyOrdered);
            this.podAX_PurchReceivedNow = sharedRes.trimOffTrailingZero(podAX_PurchReceivedNow);
            this.podScanQuantity = sharedRes.trimOffTrailingZero(podScanQuantity);
            this.podScanDatetime = podScanDatetime;
            this.podRemarks = podRemarks;
            this.podStatusId = podStatusId;
            this.podUserId = podUserId;
            this.podDeviceSN = podDeviceSN;
            this.podUsername = podUsername;
            this.podGRNDocNo = podGRNDocNo;
        }
    }
}