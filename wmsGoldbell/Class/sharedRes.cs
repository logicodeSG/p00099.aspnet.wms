﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class sharedRes
    {
        public static Boolean isTOSyncing = false;
        public static Boolean isTRSyncing = false;
        public static Boolean isCJSyncing = false;
        public static Boolean isSOSyncing = false;
        public static Boolean isAJSyncing = false;
        public static Boolean isPOSyncing = false;
        public static Boolean isInventorySyncing = false;

        //PO
        public const String AX2LGC_PO_DELETE_PENDING = "AX2LGC PO DELETE PENDING";
        public const String AX2LGC_PO_DETAIL_DELETE_PENDING = "AX2LGC PO DETAIL DELETE PENDING";
        public const String AX2LGC_PO_DETAIL_INSERT = "AX2LGC PO DETAIL INSERT";
        public const String AX2LGC_PO_INSERT = "AX2LGC PO INSERT";
        public const String AX2LGC_PO_DETAIL_UPDATE = "AX2LGC PO DETAIL UPDATE";
        public const String LGC2AX_PO_DETAIL_UPDATE = "LGC2AX PO DETAIL UPDATE";
        //AJ
        public const String AX2LGC_AJ_DELETE_PENDING = "AX2LGC AJ DELETE PENDING";
        public const String AX2LGC_AJ_INSERT = "AX2LGC AJ INSERT";
        public const String AX2LGC_AJ_DETAIL_INSERT = "AX2LGC AJ DETAIL INSERT";
        public const String LGC2AX_AJ_DETAIL_UPDATE = "LGC2AX AJ DETAIL UPDATE";
        //SO
        public const String AX2LGC_SO_DELETE_PENDING = "AX2LGC SO DELETE PENDING";
        public const String AX2LGC_SO_INSERT = "AX2LGC SO INSERT";
        public const String AX2LGC_SO_DETAIL_INSERT = "AX2LGC SO DETAIL INSERT";
        public const String AX2LGC_SO_DETAIL_UPDATE = "AX2LGC SO DETAIL UPDATE";
        public const String LGC2AX_SO_DETAIL_UPDATE = "LGC2AX SO DETAIL UPDATE";
        //TO
        public const String AX2LGC_TO_DELETE_PENDING = "AX2LGC TO DELETE PENDING";
        public const String AX2LGC_TO_INSERT = "AX2LGC TO INSERT";
        public const String AX2LGC_TO_DETAIL_INSERT = "AX2LGC TO DETAIL INSERT";
        public const String AX2LGC_TO_DETAIL_UPDATE = "AX2LGC TO DETAIL UPDATE";
        public const String LGC2AX_TO_DETAIL_UPDATE = "LGC2AX TO DETAIL UPDATE";
        //CJ
        public const String AX2LGC_CJ_DELETE_PENDING = "AX2LGC CJ DELETE PENDING";
        public const String AX2LGC_CJ_DETAIL_DELETE_PENDING = "AX2LGC CJ DETAIL DELETE PENDING";
        public const String AX2LGC_CJ_DETAIL_INSERT = "AX2LGC CJ DETAIL INSERT";
        public const String AX2LGC_CJ_INSERT = "AX2LGC CJ INSERT";
        public const String AX2LGC_CJ_DETAIL_UPDATE = "AX2LGC CJ DETAIL UPDATE";
        public const String LGC2AX_CJ_DETAIL_UPDATE = "LGC2AX CJ DETAIL UPDATE";
        //TR
        public const String AX2LGC_TR_DELETE_PENDING = "AX2LGC TR DELETE PENDING";
        public const String AX2LGC_TR_DETAIL_DELETE_PENDING = "AX2LGC TR DETAIL DELETE PENDING";
        public const String AX2LGC_TR_DETAIL_INSERT = "AX2LGC TR DETAIL INSERT";
        public const String AX2LGC_TR_INSERT = "AX2LGC TR INSERT";
        public const String AX2LGC_TR_DETAIL_UPDATE = "AX2LGC TR DETAIL UPDATE";
        public const String LGC2AX_TR_DETAIL_UPDATE = "LGC2AX TR DETAIL UPDATE";

        public static String trimOffTrailingZero(String s)
        {
            return String.IsNullOrEmpty(s) ? s : decimal.Parse(s).ToString("G29");
        }

    }
}