﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class docReferenceNumber
    {
        public String id;
        public String type;
        public String nextNumber;
        public String numberLength;
        public String prefix;
        public String nextGeneratedReferenceNumber;
        public String referenceNumberWithPrebookedNumber;

        public docReferenceNumber() { }

        public docReferenceNumber(string id, string type, string nextNumber, string numberLength, string prefix)
        {
            this.id = id;
            this.type = type;
            this.nextNumber = nextNumber;
            this.numberLength = numberLength;
            this.prefix = prefix;
            this.nextGeneratedReferenceNumber = getNextReferenceNumber();
        }

        public docReferenceNumber(string id, string type, string nextNumber, string numberLength, string prefix, string prebookedNumber)
        {
            this.id = id;
            this.type = type;
            this.nextNumber = nextNumber;
            this.numberLength = numberLength;
            this.prefix = prefix;
            this.nextGeneratedReferenceNumber = getNextReferenceNumber();
            this.referenceNumberWithPrebookedNumber = getReferenceNumberWithPrebookedNumber(Convert.ToInt64(prebookedNumber));
        }

        private String getNextReferenceNumber()
        {
            Int64 next = Convert.ToInt64(nextNumber);
            String temp = next.ToString("D" + numberLength);
            return prefix + temp;
        }

        private String getReferenceNumberWithPrebookedNumber(Int64 prebookedNumber)
        {
            Int64 next = Convert.ToInt64(nextNumber + prebookedNumber);
            String temp = next.ToString("D" + numberLength);
            return prefix + temp;
        }

    }
}