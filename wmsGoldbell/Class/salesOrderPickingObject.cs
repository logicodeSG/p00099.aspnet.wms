﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class salesOrderPickingObject
    {
        public String sopId;
        public String sopReferenceNo;
        public String sopSOReferenceNo;
        public String sopPickingDate;
        public String sopCreatedBy;
        public String sopStatusId;
        public String sopLocationId;
        public String sopReferenceVersion;

        public salesOrderPickingObject() { }

        public salesOrderPickingObject(string sopId, string sopReferenceNo, string sopSOReferenceNo, string sopPickingDate, string sopCreatedBy, string sopStatusId, string sopLocationId, string sopReferenceVersion)
        {
            this.sopId = sopId;
            this.sopReferenceNo = sopReferenceNo;
            this.sopSOReferenceNo = sopSOReferenceNo;
            this.sopPickingDate = sopPickingDate;
            this.sopCreatedBy = sopCreatedBy;
            this.sopStatusId = sopStatusId;
            this.sopLocationId = sopLocationId;
            this.sopReferenceVersion = sopReferenceVersion;
        }
    }
}