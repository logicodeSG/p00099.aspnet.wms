﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axSRObject
    {
        public String srId;
        public String srReferenceNo;
        public String srDate;
        public String srCreationDatetime;
        public String srStatusId;
        public String srDocType;
        public String srLocationId;
        public String srReferenceVersion;

        public axSRObject() { }

        public axSRObject(string srId, string srReferenceNo, string srDate, string srCreationDatetime,
             string srStatusId, string srDocType, string srLocationId, string srReferenceVersion)
        {
            this.srId = srId;
            this.srReferenceNo = srReferenceNo;
            this.srDate = srDate;
            this.srCreationDatetime = srCreationDatetime;
            this.srStatusId = srStatusId;
            this.srDocType = srDocType;
            this.srLocationId = srLocationId;
            this.srReferenceVersion = srReferenceVersion;
        }
    }
}