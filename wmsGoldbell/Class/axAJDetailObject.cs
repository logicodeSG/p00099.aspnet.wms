﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axAJDetailObject
    {
        public String ajdId;
        public String ajdAJId;
        public String ajdAX_DataAreaId;
        public String ajdAX_JournalId;
        public String ajdAX_VendAccount;
        public String ajdAX_PurchId;
        public String ajdAX_LineNumber;
        public String ajdAX_ItemId;
        public String ajdAX_God_PartCode;
        public String ajdAX_Qty;
        public String ajdAX_TransDate;
        public String ajdAX_InventTransId;
        public String ajdAX_InventDimId;
        public String ajdAX_InventLocationId;
        public String ajdAX_InventSiteId;
        public String ajdAX_WMSLocationId;
        public String ajdAX_CreatedDatetime;
        public String ajdAX_CreatedBy;
        public String ajdScanQuantity;
        public String ajdScanDatetime;
        public String ajdRemarks;
        public String ajdStatusId;
        public String ajdUserId;
        public String ajdDeviceSN;
        public String ajdUsername;
        public String ajdAX_Box_No;
        public String ajdAX_Case;
        public String ajdAX_Product_Name;
        public String ajdAX_SO_Number;
        public String ajdAX_Customer_Name;

        public axAJDetailObject() { }

        public axAJDetailObject(string ajdId, string ajdAJId, string ajdAX_DataAreaId, string ajdAX_JournalId, string ajdAX_VendAccount, string ajdAX_PurchId, string ajdAX_LineNumber, string ajdAX_ItemId, string ajdAX_God_PartCode, string ajdAX_Qty, string ajdAX_TransDate, string ajdAX_InventTransId, string ajdAX_InventDimId, string ajdAX_InventLocationId, string ajdAX_InventSiteId, string ajdAX_WMSLocationId, string ajdAX_CreatedDatetime, string ajdAX_CreatedBy, string ajdScanQuantity, string ajdScanDatetime, string ajdRemarks, string ajdStatusId, string ajdUserId, string ajdDeviceSN, string ajdUsername, string ajdAX_Box_No, string ajdAX_Case, string ajdAX_Product_Name, string ajdAX_SO_Number, string ajdAX_Customer_Name)
        {
            this.ajdId = ajdId;
            this.ajdAJId = ajdAJId;
            this.ajdAX_DataAreaId = ajdAX_DataAreaId;
            this.ajdAX_JournalId = ajdAX_JournalId;
            this.ajdAX_VendAccount = ajdAX_VendAccount;
            this.ajdAX_PurchId = ajdAX_PurchId;
            this.ajdAX_LineNumber = sharedRes.trimOffTrailingZero(ajdAX_LineNumber);
            this.ajdAX_ItemId = ajdAX_ItemId;
            this.ajdAX_God_PartCode = ajdAX_God_PartCode;
            this.ajdAX_Qty = sharedRes.trimOffTrailingZero(ajdAX_Qty);
            this.ajdAX_TransDate = ajdAX_TransDate;
            this.ajdAX_InventTransId = ajdAX_InventTransId;
            this.ajdAX_InventDimId = ajdAX_InventDimId;
            this.ajdAX_InventLocationId = ajdAX_InventLocationId;
            this.ajdAX_InventSiteId = ajdAX_InventSiteId;
            this.ajdAX_WMSLocationId = ajdAX_WMSLocationId;
            this.ajdAX_CreatedDatetime = ajdAX_CreatedDatetime;
            this.ajdAX_CreatedBy = ajdAX_CreatedBy;
            this.ajdScanQuantity = sharedRes.trimOffTrailingZero(ajdScanQuantity);
            this.ajdScanDatetime = ajdScanDatetime;
            this.ajdRemarks = ajdRemarks;
            this.ajdStatusId = ajdStatusId;
            this.ajdUserId = ajdUserId;
            this.ajdDeviceSN = ajdDeviceSN;
            this.ajdUsername = ajdUsername;
            this.ajdAX_Box_No = ajdAX_Box_No;
            this.ajdAX_Case = ajdAX_Case;
            this.ajdAX_Product_Name = ajdAX_Product_Name;
            this.ajdAX_SO_Number = ajdAX_SO_Number;
            this.ajdAX_Customer_Name = ajdAX_Customer_Name;
        }
    }
}