﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axPRObject
    {
        public String prId;
        public String prReferenceNo;
        public String prDate;
        public String prCreationDatetime;
        public String prStatusId;
        public String prDocType;
        public String prLocationId;
        public String prReferenceVersion;

        public axPRObject() { }

        public axPRObject(string prId, string prReferenceNo, string prDate, string prCreationDatetime,
             string prStatusId, string prDocType, string prLocationId, string prReferenceVersion)
        {
            this.prId = prId;
            this.prReferenceNo = prReferenceNo;
            this.prDate = prDate;
            this.prCreationDatetime = prCreationDatetime;
            this.prStatusId = prStatusId;
            this.prDocType = prDocType;
            this.prLocationId = prLocationId;
            this.prReferenceVersion = prReferenceVersion;
        }
    }
}