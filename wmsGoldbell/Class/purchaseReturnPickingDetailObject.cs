﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell
{
    public class purchaseReturnPickingDetailObject
    {
        public String prpDetailId;
        public String prpDetailPRPId;
        public String prpDetailItemId;
        public String prpDetailItemCode;
        public String prpDetailItemDescription;
        public String prpDetailItemUOM;
        public String prpDetailItemPickedQuantity;
        public String prpDetailItemGroupId;
        public String prpDetailItemBinId;

        public purchaseReturnPickingDetailObject() { }

        public purchaseReturnPickingDetailObject(string prpDetailId, string prpDetailPRPId, string prpDetailItemId,
            string prpDetailItemCode, string prpDetailItemDescription, string prpDetailItemUOM,
            string prpDetailItemPickedQuantity, string prpDetailItemGroupId, string prpDetailItemBinId)
        {
            this.prpDetailId = prpDetailId;
            this.prpDetailPRPId = prpDetailPRPId;
            this.prpDetailItemId = prpDetailItemId;
            this.prpDetailItemCode = prpDetailItemCode;
            this.prpDetailItemDescription = prpDetailItemDescription;
            this.prpDetailItemUOM = prpDetailItemUOM;
            this.prpDetailItemPickedQuantity = prpDetailItemPickedQuantity;
            this.prpDetailItemGroupId = prpDetailItemGroupId;
            this.prpDetailItemBinId = prpDetailItemBinId;
        }
    }
}