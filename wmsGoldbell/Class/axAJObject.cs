﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axAJObject
    {
        public String ajId;
        public String ajAX_JournalId;
        public String ajAX_Description;
        public String ajAX_posted;
        public String ajAX_JournalNameId;
        public String ajAX_InventDimId;
        public String ajAX_PackingSlip;
        public String ajAX_CreationDatetime;
        public String ajAX_DataAreaId;
        public String ajReferenceNo;
        public String ajCreationDatetime;
        public String ajCompletionDatetime;
        public String ajScanDatetime;
        public String ajStatusId;
        public String ajDocType;
        public String ajReferenceVersion;
        public String ajUserId;
        public String ajDeviceSN;
        public String ajUsername;

        public axAJObject() { }

        public axAJObject(string ajId, string ajAX_JournalId, string ajAX_Description, string ajAX_posted, string ajAX_JournalNameId, string ajAX_InventDimId, string ajAX_PackingSlip, string ajAX_CreationDatetime, string ajAX_DataAreaId, string ajReferenceNo, string ajCreationDatetime, string ajCompletionDatetime, string ajScanDatetime, string ajStatusId, string ajDocType, string ajReferenceVersion, string ajUserId, string ajDeviceSN, string ajUsername)
        {
            this.ajId = ajId;
            this.ajAX_JournalId = ajAX_JournalId;
            this.ajAX_Description = ajAX_Description;
            this.ajAX_posted = ajAX_posted;
            this.ajAX_JournalNameId = ajAX_JournalNameId;
            this.ajAX_InventDimId = ajAX_InventDimId;
            this.ajAX_PackingSlip = ajAX_PackingSlip;
            this.ajAX_CreationDatetime = ajAX_CreationDatetime;
            this.ajAX_DataAreaId = ajAX_DataAreaId;
            this.ajReferenceNo = ajReferenceNo;
            this.ajCreationDatetime = ajCreationDatetime;
            this.ajCompletionDatetime = ajCompletionDatetime;
            this.ajScanDatetime = ajScanDatetime;
            this.ajStatusId = ajStatusId;
            this.ajDocType = ajDocType;
            this.ajReferenceVersion = ajReferenceVersion;
            this.ajUserId = ajUserId;
            this.ajDeviceSN = ajDeviceSN;
            this.ajUsername = ajUsername;
        }
    }
}