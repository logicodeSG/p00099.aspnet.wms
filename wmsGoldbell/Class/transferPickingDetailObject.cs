﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class transferPickingDetailObject
    {
        public String tpDetailId;
        public String tpDetailTPId;
        public String tpDetailItemId;
        public String tpDetailItemCode;
        public String tpDetailItemDescription;
        public String tpDetailItemUOM;
        public String tpDetailItemPickedQuantity;
        public String tpDetailItemReceiveQuantity;
        public String tpDetailItemGroupId;
        public String tpDetailItemBinId;

        public transferPickingDetailObject() { }

        public transferPickingDetailObject(string tpDetailId, string tpDetailTPId, string tpDetailItemId,
            string tpDetailItemCode, string tpDetailItemDescription, string tpDetailItemUOM,
            string tpDetailItemPickedQuantity, string tpDetailItemReceiveQuantity, string tpDetailItemGroupId, string tpDetailItemBinId)
        {
            this.tpDetailId = tpDetailId;
            this.tpDetailTPId = tpDetailTPId;
            this.tpDetailItemId = tpDetailItemId;
            this.tpDetailItemCode = tpDetailItemCode;
            this.tpDetailItemDescription = tpDetailItemDescription;
            this.tpDetailItemUOM = tpDetailItemUOM;
            this.tpDetailItemPickedQuantity = tpDetailItemPickedQuantity;
            this.tpDetailItemReceiveQuantity = tpDetailItemReceiveQuantity;
            this.tpDetailItemGroupId = tpDetailItemGroupId;
            this.tpDetailItemBinId = tpDetailItemBinId;
        }
    }
}