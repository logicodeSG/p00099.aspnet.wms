﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class purchaseReturnPickingObject
    {
        public String prpId;
        public String prpReferenceNo;
        public String prpPRReferenceNo;
        public String prpPickingDate;
        public String prpCreatedBy;
        public String prpStatusId;
        public String prpLocationId;
        public String prpReferenceVersion;

        public purchaseReturnPickingObject() { }

        public purchaseReturnPickingObject(string prpId, string prpReferenceNo, string prpPRReferenceNo, string prpPickingDate, string prpCreatedBy, string prpStatusId, string prpLocationId, string prpReferenceVersion)
        {
            this.prpId = prpId;
            this.prpReferenceNo = prpReferenceNo;
            this.prpPRReferenceNo = prpPRReferenceNo;
            this.prpPickingDate = prpPickingDate;
            this.prpCreatedBy = prpCreatedBy;
            this.prpStatusId = prpStatusId;
            this.prpLocationId = prpLocationId;
            this.prpReferenceVersion = prpReferenceVersion;
        }
    }
}