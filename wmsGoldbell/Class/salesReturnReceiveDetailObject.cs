﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class salesReturnReceiveDetailObject
    {
        public String srrDetailId;
        public String srrDetailSRRId;
        public String srrDetailItemId;
        public String srrDetailItemCode;
        public String srrDetailItemDescription;
        public String srrDetailItemUOM;
        public String srrDetailItemReceivedQuantity;
        public String srrDetailItemGroupId;

        public salesReturnReceiveDetailObject() { }

        public salesReturnReceiveDetailObject(string srrDetailId, string srrDetailSRRId, 
            string srrDetailItemId, string srrDetailItemCode, string srrDetailItemDescription, 
            string srrDetailItemUOM, string srrDetailItemReceivedQuantity, string srrDetailItemGroupId)
        {
            this.srrDetailId = srrDetailId;
            this.srrDetailSRRId = srrDetailSRRId;
            this.srrDetailItemId = srrDetailItemId;
            this.srrDetailItemCode = srrDetailItemCode;
            this.srrDetailItemDescription = srrDetailItemDescription;
            this.srrDetailItemUOM = srrDetailItemUOM;
            this.srrDetailItemReceivedQuantity = srrDetailItemReceivedQuantity;
            this.srrDetailItemGroupId = srrDetailItemGroupId;
        }
    }
}