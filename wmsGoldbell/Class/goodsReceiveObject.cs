﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class goodsReceiveObject
    {
        public String grId;
        public String grReferenceNo;
        public String grDOReferenceNo;
        public String grPOReferenceNo;
        public String grReceiveDate;
        public String grCreatedBy;
        public String grStatusId;
        public String grLocationId;
        public String grReferenceVersion;

        public goodsReceiveObject() { }

        public goodsReceiveObject(string grId, string grReferenceNo, string grDOReferenceNo, string grPOReferenceNo, string grReceiveDate,
            string grCreatedBy, string grStatusId, string grLocationId, string grReferenceVersion)
        {
            this.grId = grId;
            this.grReferenceNo = grReferenceNo;
            this.grDOReferenceNo = grDOReferenceNo;
            this.grPOReferenceNo = grPOReferenceNo;
            this.grReceiveDate = grReceiveDate;
            this.grCreatedBy = grCreatedBy;
            this.grStatusId = grStatusId;
            this.grLocationId = grLocationId;
            this.grReferenceVersion = grReferenceVersion;
        }
    }
}