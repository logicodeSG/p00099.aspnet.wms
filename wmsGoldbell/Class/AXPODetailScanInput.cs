﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class {
    public class AXPODetailScanInput {
        public String argAXPURCHID { get; set; }
        public String argAXLINENUMBER { get; set; }
        public String argAXITEMID { get; set; }
        public String argAXDOT_PARTCODE { get; set; }
        public String argScanQuantity { get; set; }
        public String argScanDatetime { get; set; }
        public String argRemarks { get; set; }
        public String argUserId { get; set; }
        public String argGRNDocumentNo { get; set; }
        public String argDeviceSN { get; set; }
    }
}