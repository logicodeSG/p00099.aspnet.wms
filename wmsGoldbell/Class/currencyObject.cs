﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class currencyObject
    {
        public String currencyId;
        public String currencyIssuer;
        public String currencyCode;
        public String currencySymbol;
        public String currencyName;

        public currencyObject() { }

        public currencyObject(string currencyId, string currencyIssuer, string currencyCode, string currencySymbol, string currencyName)
        {
            this.currencyId = currencyId;
            this.currencyIssuer = currencyIssuer;
            this.currencyCode = currencyCode;
            this.currencySymbol = currencySymbol;
            this.currencyName = currencyName;
        }
    }
}