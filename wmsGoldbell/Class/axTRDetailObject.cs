﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell.Class
{
    public class axTRDetailObject
    {
        public String trdId;
        public String trdTRId;
        public String trdAX_DataAreaId;
        public String trdAX_LineNum;
        public String trdAX_TransferId;
        public String trdAX_TransferStatus;
        public String trdAX_InventTransId;
        public String trdAX_ItemId;
        public String trdAX_Dot_PartCode;
        public String trdAX_NameAlias;
        public String trdAX_UnitId;
        public String trdAX_InventLocationId;
        public String trdAX_InventSiteId;
        public String trdAX_QtyShipped;
        public String trdAX_DocType;
        public String trdScanQuantity;
        public String trdScanDatetime;
        public String trdRemarks;
        public String trdStatusId;
        public String trdUserId;
        public String trdDeviceSN;
        public String trdUsername;
        public String trdAX_WMSLocationId;

        public axTRDetailObject() { }

        public axTRDetailObject(string trdId, string trdTRId, string trdAX_DataAreaId, string trdAX_LineNum, string trdAX_TransferId, string trdAX_TransferStatus, string trdAX_InventTransId, string trdAX_ItemId, string trdAX_Dot_PartCode, string trdAX_NameAlias, string trdAX_UnitId, string trdAX_InventLocationId, string trdAX_InventSiteId, string trdAX_QtyShipped, string trdAX_DocType, string trdScanQuantity, string trdScanDatetime, string trdRemarks, string trdStatusId, string trdUserId, string trdDeviceSN, string trdUsername, string trdAX_WMSLocationId)
        {
            this.trdId = trdId;
            this.trdTRId = trdTRId;
            this.trdAX_DataAreaId = trdAX_DataAreaId;
            this.trdAX_LineNum = sharedRes.trimOffTrailingZero(trdAX_LineNum);
            this.trdAX_TransferId = trdAX_TransferId;
            this.trdAX_TransferStatus = trdAX_TransferStatus;
            this.trdAX_InventTransId = trdAX_InventTransId;
            this.trdAX_ItemId = trdAX_ItemId;
            this.trdAX_Dot_PartCode = trdAX_Dot_PartCode;
            this.trdAX_NameAlias = trdAX_NameAlias;
            this.trdAX_UnitId = trdAX_UnitId;
            this.trdAX_InventLocationId = trdAX_InventLocationId;
            this.trdAX_InventSiteId = trdAX_InventSiteId;
            this.trdAX_QtyShipped = sharedRes.trimOffTrailingZero(trdAX_QtyShipped);
            this.trdAX_DocType = trdAX_DocType;
            this.trdScanQuantity = sharedRes.trimOffTrailingZero(trdScanQuantity);
            this.trdScanDatetime = trdScanDatetime;
            this.trdRemarks = trdRemarks;
            this.trdStatusId = trdStatusId;
            this.trdUserId = trdUserId;
            this.trdDeviceSN = trdDeviceSN;
            this.trdUsername = trdUsername;
            this.trdAX_WMSLocationId = trdAX_WMSLocationId;
        }
    }
}