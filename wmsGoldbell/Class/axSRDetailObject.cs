﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wmsGoldbell
{
    public class axSRDetailObject
    {
        public String srDetailId;
        public String srDetailSRId;
        public String srDetailItemId;
        public String srDetailItemCode;
        public String srDetailItemDescription;
        public String srDetailItemUOM;
        public String srDetailItemQuantity;
        public String srDetailItemReceivedQuantity;
        public String srDetailItemGroupId;


        public axSRDetailObject() { }

        public axSRDetailObject(string srDetailId, string srDetailSRId, string srDetailItemId,
            string srDetailItemCode, string srDetailItemDescription, string srDetailItemUOM, string srDetailItemQuantity,
            string srDetailItemReceivedQuantity, string srDetailItemGroupId)
        {
            this.srDetailId = srDetailId;
            this.srDetailSRId = srDetailSRId;
            this.srDetailItemCode = srDetailItemCode;
            this.srDetailItemDescription = srDetailItemDescription;
            this.srDetailItemId = srDetailItemId;
            this.srDetailItemUOM = srDetailItemUOM;
            this.srDetailItemQuantity = srDetailItemQuantity;
            this.srDetailItemReceivedQuantity = srDetailItemReceivedQuantity;
            this.srDetailItemGroupId = srDetailItemGroupId;
        }
    }
}