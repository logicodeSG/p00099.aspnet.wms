﻿CREATE TABLE [dbo].[tbl_systemCurrency] (
    [id]                BIGINT          NOT NULL,
    [currencyCode]      NVARCHAR (5)    NULL,
    [currencySymbol]    NVARCHAR (5)    NULL,
    [currencyName]      NVARCHAR (50)   NULL,
    [rate]              DECIMAL (18, 2) NULL,
    [isDefaultCurrency] BIT             NULL,
    [active]            BIT             NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

