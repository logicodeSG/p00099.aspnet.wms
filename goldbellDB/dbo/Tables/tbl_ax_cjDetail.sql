﻿CREATE TABLE [dbo].[tbl_ax_cjDetail] (
    [id]                 BIGINT           NOT NULL,
    [tbl_ax_cj_Id]       BIGINT           NOT NULL,
    [axJOURNALID]        NVARCHAR (20)    NOT NULL,
    [axLINENUM]          NUMERIC (32, 16) NULL,
    [axITEMID]           NVARCHAR (50)    NOT NULL,
    [axDOT_PARTCODE]     NVARCHAR (80)    NOT NULL,
    [axQTY]              NUMERIC (32, 16) NOT NULL,
    [axInventUnitID]     NVARCHAR (10)    NOT NULL,
    [axINVENTLOCATIONID] NVARCHAR (20)    NOT NULL,
    [axINVENTSITEID]     NVARCHAR (10)    NOT NULL,
    [axWMSLOCATIONID]    NVARCHAR (25)    NOT NULL,
    [scanQuantity]       NUMERIC (32, 16) NULL,
    [scanDatetime]       DATETIME         NULL,
    [remarks]            NVARCHAR (255)   NULL,
    [tbl_status_Id]      BIGINT           CONSTRAINT [DF__tbl_ax_cj__tbl_s__5EDF0F2E] DEFAULT ((1)) NOT NULL,
    [tbl_user_Id]        BIGINT           NULL,
    [deviceSN]           VARCHAR (50)     NULL,
    [tbl_user_Username]  VARCHAR (50)     NULL,
    [axINVENTTRANSID]    NVARCHAR (20)    NULL,
    [axPRODUCT_NAME]     NVARCHAR (60)    NULL,
    CONSTRAINT [PK_tbl_ax_cjDetail] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_tbl_ax_cjDetail_tbl_ax_cj] FOREIGN KEY ([tbl_ax_cj_Id]) REFERENCES [dbo].[tbl_ax_cj] ([id]) ON DELETE CASCADE
);













