﻿CREATE TABLE [dbo].[XXXtbl_purchaseReturnPicking] (
    [id]               BIGINT       NOT NULL,
    [referenceNo]      VARCHAR (50) NULL,
    [tbl_ax_pr_Id]     BIGINT       NULL,
    [creationDatetime] DATETIME     NULL,
    [pickingDatetime]  DATETIME     NULL,
    [tbl_user_Id]      BIGINT       NULL,
    [tbl_status_Id]    BIGINT       NULL,
    [deviceSN]         VARCHAR (50) NULL,
    [lastEdit]         BIGINT       NULL,
    [tbl_location_Id]  BIGINT       NULL,
    [referenceVersion] BIGINT       NULL,
    CONSTRAINT [PK__tbl_purc__3213E83F4937DFC6] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [fk_tbl_ax_pr_tbl_purchaseReturnPicking] FOREIGN KEY ([tbl_ax_pr_Id]) REFERENCES [dbo].[XXXtbl_ax_pr] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_ax_pr_tbl_purchaseReturnPicking_idx]
    ON [dbo].[XXXtbl_purchaseReturnPicking]([tbl_ax_pr_Id] ASC);

