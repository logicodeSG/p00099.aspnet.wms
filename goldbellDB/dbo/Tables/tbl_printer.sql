﻿CREATE TABLE [dbo].[tbl_printer] (
    [id]        SMALLINT     NOT NULL,
    [name]      VARCHAR (50) NOT NULL,
    [active]    BIT          NULL,
    [isDefault] BIT          NULL,
    CONSTRAINT [PK_tbl_printer] PRIMARY KEY CLUSTERED ([id] ASC)
);

