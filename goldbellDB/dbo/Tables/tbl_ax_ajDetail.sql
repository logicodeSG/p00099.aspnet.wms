﻿CREATE TABLE [dbo].[tbl_ax_ajDetail] (
    [id]                 BIGINT           NOT NULL,
    [tbl_ax_aj_Id]       BIGINT           NOT NULL,
    [axDATAAREAID]       NVARCHAR (4)     NOT NULL,
    [axJOURNALID]        NVARCHAR (20)    NOT NULL,
    [axVENDACCOUNT]      NVARCHAR (20)    NOT NULL,
    [axPurchID]          NVARCHAR (20)    NOT NULL,
    [axLINENUM]          NUMERIC (32, 16) NOT NULL,
    [axITEMID]           NVARCHAR (50)    NOT NULL,
    [axGOD_PARTCODE]     NVARCHAR (80)    NOT NULL,
    [axQTY]              NUMERIC (32, 16) NOT NULL,
    [axTRANSDATE]        DATETIME         NOT NULL,
    [axINVENTTRANSID]    NVARCHAR (20)    NOT NULL,
    [axINVENTDIMID]      NVARCHAR (20)    NOT NULL,
    [axINVENTLOCATIONID] NVARCHAR (20)    NOT NULL,
    [axINVENTSITEID]     NVARCHAR (10)    NOT NULL,
    [axWMSLOCATIONID]    NVARCHAR (25)    NOT NULL,
    [axCREATEDDATETIME]  DATETIME         NOT NULL,
    [axCREATEDBY]        NVARCHAR (8)     NOT NULL,
    [scanQuantity]       NUMERIC (32, 16) NULL,
    [scanDatetime]       DATETIME         NULL,
    [remarks]            NVARCHAR (255)   NULL,
    [tbl_status_Id]      BIGINT           CONSTRAINT [DF__tbl_ax_aj__tbl_s__5DEAEAF5] DEFAULT ((1)) NOT NULL,
    [tbl_user_Id]        BIGINT           NULL,
    [deviceSN]           VARCHAR (50)     NULL,
    [tbl_user_Username]  VARCHAR (50)     NULL,
    [axBOX_NO]           NVARCHAR (20)    NULL,
    [axCASE]             NVARCHAR (20)    NULL,
    [axPRODUCT_NAME]     NVARCHAR (60)    NULL,
    [axSO_NUMBER]        NVARCHAR (20)    NULL,
    [axCUSTOMER_NAME]    NVARCHAR (100)   NULL,
    CONSTRAINT [PK_tbl_ax_cjDetail_1] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_tbl_ax_ajDetail_tbl_ax_aj] FOREIGN KEY ([tbl_ax_aj_Id]) REFERENCES [dbo].[tbl_ax_aj] ([id]) ON DELETE CASCADE
);

















