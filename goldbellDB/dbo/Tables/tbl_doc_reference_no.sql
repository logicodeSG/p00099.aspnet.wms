﻿CREATE TABLE [dbo].[tbl_doc_reference_no] (
    [id]           SMALLINT     NOT NULL,
    [type]         VARCHAR (50) NULL,
    [nextNumber]   BIGINT       NULL,
    [numberLength] SMALLINT     NULL,
    [prefix]       VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

