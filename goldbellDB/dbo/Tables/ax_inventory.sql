﻿CREATE TABLE [dbo].[ax_inventory] (
    [id]               BIGINT          NOT NULL,
    [DATAAREAID]       NVARCHAR (4)    NULL,
    [ITEMID]           NVARCHAR (50)   NULL,
    [DOT_PARTCODE]     NVARCHAR (80)   NULL,
    [NAME]             NVARCHAR (60)   NULL,
    [DESCRIPTION]      NVARCHAR (1000) NULL,
    [ITEMTYPE]         INT             NULL,
    [InventUnitID]     NVARCHAR (10)   NULL,
    [PurhUnitID]       NVARCHAR (10)   NULL,
    [SalesUnitID]      NVARCHAR (10)   NULL,
    [isSerialized]     INT             NULL,
    [active]           INT             NULL,
    [StartDate]        DATETIME        NULL,
    [PRODUCT]          BIGINT          NULL,
    [CREATEDDATETIME]  DATETIME        NULL,
    [MODIFIEDDATETIME] DATETIME        NULL,
    [ITEMGROUPID]      NVARCHAR (20)   NULL,
    CONSTRAINT [PK_ax_inventory] PRIMARY KEY CLUSTERED ([id] ASC)
);



