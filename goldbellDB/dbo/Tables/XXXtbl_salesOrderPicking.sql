﻿CREATE TABLE [dbo].[XXXtbl_salesOrderPicking] (
    [id]               BIGINT       NOT NULL,
    [referenceNo]      VARCHAR (50) NULL,
    [tbl_ax_so_Id]     BIGINT       NULL,
    [creationDatetime] DATETIME     NULL,
    [pickingDatetime]  DATETIME     NULL,
    [tbl_user_Id]      BIGINT       NULL,
    [tbl_status_Id]    BIGINT       NULL,
    [deviceSN]         VARCHAR (50) NULL,
    [lastEdit]         BIGINT       NULL,
    [tbl_location_Id]  BIGINT       NULL,
    [referenceVersion] BIGINT       NULL,
    CONSTRAINT [PK__tbl_sale__3213E83FBB2F8BCB] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_ax_so_tbl_salesOrderPicking_idx]
    ON [dbo].[XXXtbl_salesOrderPicking]([tbl_ax_so_Id] ASC);

