﻿CREATE TABLE [dbo].[tbl_status] (
    [id]   BIGINT       NOT NULL,
    [name] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

