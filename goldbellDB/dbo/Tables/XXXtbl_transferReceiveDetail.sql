﻿CREATE TABLE [dbo].[XXXtbl_transferReceiveDetail] (
    [id]                     SMALLINT      NOT NULL,
    [tbl_transferReceive_Id] BIGINT        NOT NULL,
    [tbl_item_Id]            BIGINT        NULL,
    [tbl_item_code]          VARCHAR (45)  NULL,
    [tbl_item_description]   VARCHAR (255) NULL,
    [uom]                    VARCHAR (50)  NULL,
    [quantity]               INT           NULL,
    PRIMARY KEY CLUSTERED ([id] ASC, [tbl_transferReceive_Id] ASC),
    CONSTRAINT [fk_tbl_transferReceive_tbl_transferReceiveDetail] FOREIGN KEY ([tbl_transferReceive_Id]) REFERENCES [dbo].[XXXtbl_transferReceive] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_transferReceive_tbl_transferReceiveDetail_idx]
    ON [dbo].[XXXtbl_transferReceiveDetail]([tbl_transferReceive_Id] ASC);

