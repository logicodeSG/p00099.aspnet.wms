﻿CREATE TABLE [dbo].[tbl_ax_toDetail] (
    [id]                 BIGINT           NOT NULL,
    [tbl_ax_to_Id]       BIGINT           NOT NULL,
    [axDATAAREAID]       NVARCHAR (4)     NOT NULL,
    [axROUTEID]          NVARCHAR (10)    NOT NULL,
    [axTRANSFERID]       NVARCHAR (20)    NOT NULL,
    [axINVENTTRANSID]    NVARCHAR (20)    NOT NULL,
    [axITEMID]           NVARCHAR (50)    NOT NULL,
    [axDOT_PARTCODE]     NVARCHAR (80)    NULL,
    [axNAME]             NVARCHAR (60)    NULL,
    [axUNITID]           NVARCHAR (10)    NOT NULL,
    [axINVENTLOCATIONID] NVARCHAR (20)    NOT NULL,
    [axINVENTSITEID]     NVARCHAR (10)    NOT NULL,
    [axQTY]              NUMERIC (32, 16) NOT NULL,
    [axDocType]          VARCHAR (6)      NOT NULL,
    [scanQuantity]       NUMERIC (32, 16) NULL,
    [scanDatetime]       DATETIME         NULL,
    [remarks]            NVARCHAR (255)   NULL,
    [tbl_status_Id]      BIGINT           CONSTRAINT [DF__tbl_ax_to__tbl_s__61BB7BD9] DEFAULT ((1)) NOT NULL,
    [tbl_user_Id]        BIGINT           NULL,
    [deviceSN]           VARCHAR (50)     NULL,
    [tbl_user_Username]  VARCHAR (50)     NULL,
    [axWMSLOCATIONID]    NVARCHAR (25)    NULL,
    [axNAMEALIAS]        NVARCHAR (20)    NULL,
    CONSTRAINT [PK_tbl_ax_toDetail] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_tbl_ax_toDetail_tbl_ax_to] FOREIGN KEY ([tbl_ax_to_Id]) REFERENCES [dbo].[tbl_ax_to] ([id]) ON DELETE CASCADE
);














GO


