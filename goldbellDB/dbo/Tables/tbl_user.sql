﻿CREATE TABLE [dbo].[tbl_user] (
    [id]                       BIGINT        NOT NULL,
    [code]                     VARCHAR (50)  NULL,
    [username]                 VARCHAR (50)  NULL,
    [password]                 NVARCHAR (64) NULL,
    [tbl_userLevel_Id]         BIGINT        NULL,
    [active]                   BIT           NULL,
    [lastSelected_location_Id] BIGINT        CONSTRAINT [DF_tbl_user_lastSelected_Location_Id] DEFAULT ((-1)) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [fk_tbl_user_tbl_userLevel] FOREIGN KEY ([tbl_userLevel_Id]) REFERENCES [dbo].[tbl_userLevel] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_user_tbl_userLevel_idx]
    ON [dbo].[tbl_user]([tbl_userLevel_Id] ASC);

