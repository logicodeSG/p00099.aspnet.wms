﻿CREATE TABLE [dbo].[tbl_ax_so] (
    [id]                 BIGINT        NOT NULL,
    [axDATAAREAID]       NVARCHAR (4)  NULL,
    [axPICKINGROUTEID]   NVARCHAR (10) NULL,
    [axSALESId]          NVARCHAR (20) NULL,
    [axConfirmationDate] DATETIME      NULL,
    [axCREATEDDATETIME]  DATETIME      NULL,
    [axSALESTYPE]        INT           NULL,
    [referenceNo]        VARCHAR (50)  NULL,
    [creationDatetime]   DATETIME      NULL,
    [completionDatetime] DATETIME      NULL,
    [scanDatetime]       DATETIME      NULL,
    [tbl_status_Id]      BIGINT        NULL,
    [docType]            VARCHAR (50)  NULL,
    [referenceVersion]   BIGINT        NULL,
    [tbl_user_Id]        BIGINT        NULL,
    [deviceSN]           VARCHAR (50)  NULL,
    [tbl_user_Username]  VARCHAR (50)  NULL,
    [axCustomerAccount]  NVARCHAR (20) NULL,
    [axCustomerName]     NVARCHAR (60) NULL,
    CONSTRAINT [PK__tbl_ax_s__3213E83F8C61F47F] PRIMARY KEY CLUSTERED ([id] ASC)
);









