﻿CREATE TABLE [dbo].[XXXtbl_ax_srDetail] (
    [id]                   SMALLINT      NOT NULL,
    [tbl_ax_sr_Id]         BIGINT        NOT NULL,
    [tbl_item_Id]          BIGINT        NULL,
    [tbl_item_code]        VARCHAR (50)  NULL,
    [tbl_item_description] VARCHAR (255) NULL,
    [uom]                  VARCHAR (50)  NULL,
    [quantity]             INT           NULL,
    [tbl_group_Id]         SMALLINT      NULL,
    PRIMARY KEY CLUSTERED ([id] ASC, [tbl_ax_sr_Id] ASC),
    CONSTRAINT [fk_tbl_ax_sr_tbl_ax_srDetail] FOREIGN KEY ([tbl_ax_sr_Id]) REFERENCES [dbo].[XXXtbl_ax_sr] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_ax_sr_tbl_ax_srDetail_idx]
    ON [dbo].[XXXtbl_ax_srDetail]([tbl_ax_sr_Id] ASC);

