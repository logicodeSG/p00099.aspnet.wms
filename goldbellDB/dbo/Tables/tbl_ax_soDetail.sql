﻿CREATE TABLE [dbo].[tbl_ax_soDetail] (
    [id]                 BIGINT           NOT NULL,
    [tbl_ax_so_Id]       BIGINT           NOT NULL,
    [axDATAAREAID]       NVARCHAR (4)     NOT NULL,
    [axLINENUM]          NUMERIC (32, 16) NOT NULL,
    [axROUTEID]          NVARCHAR (10)    NOT NULL,
    [axINVENTTRANSID]    NVARCHAR (20)    NOT NULL,
    [axITEMID]           NVARCHAR (50)    NOT NULL,
    [axDOT_PARTCODE]     NVARCHAR (80)    NULL,
    [axNAME]             NVARCHAR (60)    NULL,
    [axSALESUNIT]        NVARCHAR (10)    NOT NULL,
    [axINVENTLOCATIONID] NVARCHAR (20)    NOT NULL,
    [axINVENTSITEID]     NVARCHAR (10)    NOT NULL,
    [axWMSLOCATIONID]    NVARCHAR (25)    NOT NULL,
    [axQTY]              NUMERIC (32, 16) NOT NULL,
    [scanQuantity]       NUMERIC (32, 16) NULL,
    [scanDatetime]       DATETIME         NULL,
    [remarks]            NVARCHAR (255)   NULL,
    [tbl_status_Id]      BIGINT           CONSTRAINT [DF__tbl_ax_so__tbl_s__60C757A0] DEFAULT ((1)) NOT NULL,
    [tbl_user_Id]        BIGINT           NULL,
    [deviceSN]           VARCHAR (50)     NULL,
    [tbl_user_Username]  VARCHAR (50)     NULL,
    CONSTRAINT [PK_tbl_ax_soDetail] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_tbl_ax_soDetail_tbl_ax_so] FOREIGN KEY ([tbl_ax_so_Id]) REFERENCES [dbo].[tbl_ax_so] ([id]) ON DELETE CASCADE
);










GO


