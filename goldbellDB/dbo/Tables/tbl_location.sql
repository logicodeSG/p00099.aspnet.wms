﻿CREATE TABLE [dbo].[tbl_location] (
    [id]                    BIGINT       NOT NULL,
    [code]                  VARCHAR (50) NULL,
    [description]           VARCHAR (50) NULL,
    [tbl_locationLevel_Id]  BIGINT       NULL,
    [tbl_locationParent_Id] BIGINT       NULL,
    [active]                BIT          NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [fk_tbl_location_tbl_locationLevevl] FOREIGN KEY ([tbl_locationLevel_Id]) REFERENCES [dbo].[tbl_locationLevel] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_location_tbl_locationLevevl_idx]
    ON [dbo].[tbl_location]([tbl_locationLevel_Id] ASC);

