﻿CREATE TABLE [dbo].[x_tbl_userLevelXtbl_access] (
    [tbl_userLevel_Id] BIGINT  NOT NULL,
    [tbl_access_Id]    TINYINT NOT NULL,
    [canRead]          BIT     NULL,
    [canAdd]           BIT     NULL,
    [canEdit]          BIT     NULL,
    [canDelete]        BIT     NULL,
    [canPrint]         BIT     NULL,
    CONSTRAINT [PK__x_tbl_us__63FC4FD1B5E5BB8A] PRIMARY KEY CLUSTERED ([tbl_userLevel_Id] ASC, [tbl_access_Id] ASC),
    CONSTRAINT [fk_tbl_access_x_tbl_userLevelXtbl_access] FOREIGN KEY ([tbl_access_Id]) REFERENCES [dbo].[tbl_access] ([id]),
    CONSTRAINT [fk_tbl_userLevel_x_tbl_userLevelXtbl_access] FOREIGN KEY ([tbl_userLevel_Id]) REFERENCES [dbo].[tbl_userLevel] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_access_x_tbl_userLevelXtbl_access_idx]
    ON [dbo].[x_tbl_userLevelXtbl_access]([tbl_access_Id] ASC);

