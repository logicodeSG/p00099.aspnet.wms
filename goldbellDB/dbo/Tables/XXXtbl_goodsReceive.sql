﻿CREATE TABLE [dbo].[XXXtbl_goodsReceive] (
    [id]               BIGINT       NOT NULL,
    [referenceNo]      VARCHAR (50) NULL,
    [doReferenceNo]    VARCHAR (50) NULL,
    [tbl_ax_po_Id]     BIGINT       NULL,
    [creationDatetime] DATETIME     NULL,
    [receiveDatetime]  DATETIME     NULL,
    [tbl_user_Id]      BIGINT       NULL,
    [tbl_status_Id]    BIGINT       NULL,
    [deviceSN]         VARCHAR (50) NULL,
    [lastEdit]         BIGINT       NULL,
    [tbl_location_Id]  BIGINT       NULL,
    [referenceVersion] BIGINT       NULL,
    CONSTRAINT [PK_tbl_goodsReceive] PRIMARY KEY CLUSTERED ([id] ASC)
);

