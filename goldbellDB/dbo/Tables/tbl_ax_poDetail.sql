﻿CREATE TABLE [dbo].[tbl_ax_poDetail] (
    [id]                 BIGINT           NOT NULL,
    [tbl_ax_po_Id]       BIGINT           NOT NULL,
    [axDATAAREAID]       NVARCHAR (4)     NOT NULL,
    [axPURCHID]          NVARCHAR (20)    NOT NULL,
    [axLINENUMBER]       BIGINT           NOT NULL,
    [axITEMID]           NVARCHAR (50)    NOT NULL,
    [axNAME]             NVARCHAR (1000)  NOT NULL,
    [axDOT_PARTCODE]     NVARCHAR (80)    NOT NULL,
    [axORDERACCOUNT]     NVARCHAR (20)    NOT NULL,
    [axINVOICEACCOUNT]   NVARCHAR (20)    NOT NULL,
    [axPURCHUNIT]        NVARCHAR (10)    NOT NULL,
    [axINVENTLOCATIONID] NVARCHAR (20)    NOT NULL,
    [axINVENTSITEID]     NVARCHAR (10)    NOT NULL,
    [axWMSLOCATIONID]    NVARCHAR (25)    NOT NULL,
    [axQTYORDERED]       NUMERIC (32, 16) NOT NULL,
    [axPURCHRECEIVEDNOW] NUMERIC (32, 16) NOT NULL,
    [scanQuantity]       NUMERIC (32, 16) NULL,
    [scanDatetime]       DATETIME         NULL,
    [remarks]            NVARCHAR (255)   NULL,
    [tbl_status_Id]      BIGINT           DEFAULT ((1)) NOT NULL,
    [tbl_user_Id]        BIGINT           NULL,
    [deviceSN]           VARCHAR (50)     NULL,
    [tbl_user_Username]  VARCHAR (50)     NULL,
    [grnDocNo] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_tbl_ax_poDetail] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_tbl_ax_poDetail_tbl_ax_po] FOREIGN KEY ([tbl_ax_po_Id]) REFERENCES [dbo].[tbl_ax_po] ([id]) ON DELETE CASCADE
);








GO


