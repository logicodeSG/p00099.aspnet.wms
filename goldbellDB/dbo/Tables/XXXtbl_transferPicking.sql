﻿CREATE TABLE [dbo].[XXXtbl_transferPicking] (
    [id]               BIGINT       NOT NULL,
    [referenceNo]      VARCHAR (50) NULL,
    [tbl_ax_to_Id]     BIGINT       NULL,
    [creationDatetime] DATETIME     NULL,
    [pickingDatetime]  DATETIME     NULL,
    [tbl_user_Id]      BIGINT       NULL,
    [tbl_status_Id]    BIGINT       NULL,
    [deviceSN]         VARCHAR (50) NULL,
    [lastEdit]         BIGINT       NULL,
    [tbl_location_Id]  BIGINT       NULL,
    [referenceVersion] BIGINT       NULL,
    CONSTRAINT [PK__tbl_tran__3213E83FFC10A338] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [fk_tbl_ax_to_tbl_transferPicking] FOREIGN KEY ([tbl_ax_to_Id]) REFERENCES [dbo].[tbl_ax_to] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_ax_to_tbl_transferPicking_idx]
    ON [dbo].[XXXtbl_transferPicking]([tbl_ax_to_Id] ASC);

