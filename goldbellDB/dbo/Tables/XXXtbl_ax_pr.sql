﻿CREATE TABLE [dbo].[XXXtbl_ax_pr] (
    [id]               BIGINT       NOT NULL,
    [referenceNo]      VARCHAR (50) NULL,
    [prDatetime]       DATETIME     CONSTRAINT [DF_tbl_ax_pr_poDatetime] DEFAULT (((2018)-(5))-(8)) NULL,
    [creationDatetime] DATETIME     NULL,
    [tbl_status_Id]    BIGINT       NULL,
    [docType]          VARCHAR (50) NULL,
    [tbl_location_Id]  BIGINT       NULL,
    [referenceVersion] BIGINT       NULL,
    CONSTRAINT [PK__tbl_ax_p__3213E83FB7AC7285] PRIMARY KEY CLUSTERED ([id] ASC)
);

