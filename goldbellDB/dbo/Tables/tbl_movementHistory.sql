﻿CREATE TABLE [dbo].[tbl_movementHistory] (
    [id]                      BIGINT           NOT NULL,
    [axDATAREADID]            NVARCHAR (4)     NULL,
    [axDOCUMENTID]            NVARCHAR (20)    NULL,
    [axSubDOCUMENTID]         NVARCHAR (20)    NULL,
    [axITEMID]                NVARCHAR (50)    NULL,
    [axPART_CODE]             NVARCHAR (80)    NULL,
    [axINVENTLOCATION_FROMID] NVARCHAR (20)    NULL,
    [axINVENTSITE_FROMID]     NVARCHAR (10)    NULL,
    [axWMSLOCATION_FROMID]    NVARCHAR (25)    NULL,
    [axINVENTLOCATION_TOID]   NVARCHAR (20)    NULL,
    [axINVENTSITE_TOID]       NVARCHAR (10)    NULL,
    [axWMSLOCATION_TOID]      NVARCHAR (25)    NULL,
    [qty]                     NUMERIC (32, 16) NULL,
    [movementDatetime]        DATETIME         NULL,
    [tbl_user_Id]             BIGINT           NULL,
    [tbl_username]            VARCHAR (50)     NULL,
    [deviceSN]                VARCHAR (50)     NULL,
    [movementType]            VARCHAR (50)     NULL,
    [axLINENUM] NUMERIC(32, 16) NULL, 
    CONSTRAINT [PK__tbl_move__3213E83F2D8CF1A8] PRIMARY KEY CLUSTERED ([id] ASC)
);







