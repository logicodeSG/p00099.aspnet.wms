﻿CREATE TABLE [dbo].[tbl_exceptionMsg] (
    [id]               BIGINT         NOT NULL,
    [axDOCUMENTID]     NVARCHAR (20)  NULL,
    [creationDatetime] DATETIME       NULL,
    [tbl_user_Id]      BIGINT         NULL,
    [tbl_username]     VARCHAR (50)   NULL,
    [deviceSN]         VARCHAR (50)   NULL,
    [docType]          VARCHAR (50)   NULL,
    [exceptionMsg]     NVARCHAR (255) NULL,
    [messageId]        NVARCHAR (50)  NULL,
    CONSTRAINT [PK_tbl_exceptionMsg] PRIMARY KEY CLUSTERED ([id] ASC)
);

