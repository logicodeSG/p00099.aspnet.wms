﻿CREATE TABLE [dbo].[XXXtbl_itemSupersession] (
    [tbl_item_Id] BIGINT       NOT NULL,
    [partNumber]  VARCHAR (50) NOT NULL,
    CONSTRAINT [PK__tbl_item__316CC39754C3DEB2] PRIMARY KEY CLUSTERED ([tbl_item_Id] ASC, [partNumber] ASC)
);

