﻿CREATE TABLE [dbo].[XXXtbl_salesOrderPickingDetail] (
    [id]                       SMALLINT      NOT NULL,
    [tbl_salesOrderPicking_Id] BIGINT        NOT NULL,
    [tbl_item_Id]              BIGINT        NULL,
    [tbl_item_code]            VARCHAR (50)  NULL,
    [tbl_item_description]     VARCHAR (255) NULL,
    [uom]                      VARCHAR (50)  NULL,
    [quantity]                 INT           NULL,
    [tbl_group_Id]             SMALLINT      NULL,
    [tbl_bin_Id]               VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC, [tbl_salesOrderPicking_Id] ASC),
    CONSTRAINT [fk_tbl_salesOrderPicking_tbl_salesOrderPickingDetail] FOREIGN KEY ([tbl_salesOrderPicking_Id]) REFERENCES [dbo].[XXXtbl_salesOrderPicking] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_salesOrderPicking_tbl_salesOrderPickingDetail_idx]
    ON [dbo].[XXXtbl_salesOrderPickingDetail]([tbl_salesOrderPicking_Id] ASC);

