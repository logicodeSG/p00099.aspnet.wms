﻿CREATE TABLE [dbo].[tbl_ax_to] (
    [id]                     BIGINT        NOT NULL,
    [axDATAAREAID]           NVARCHAR (4)  NULL,
    [axMODIFIEDDATETIME]     DATETIME      NULL,
    [axPICKINGROUTEID]       NVARCHAR (10) NULL,
    [axTRANSFERID]           NVARCHAR (20) NULL,
    [axDocumentType]         VARCHAR (2)   NULL,
    [axINVENTLOCATIONIDFROM] NVARCHAR (20) NULL,
    [axINVENTLOCATIONIDTO]   NVARCHAR (20) NULL,
    [axTRANSFERSTATUS]       INT           NULL,
    [referenceNo]            VARCHAR (50)  NULL,
    [creationDatetime]       DATETIME      NULL,
    [completionDatetime]     DATETIME      NULL,
    [scanDatetime]           DATETIME      NULL,
    [tbl_status_Id]          BIGINT        NULL,
    [docType]                VARCHAR (50)  NULL,
    [referenceVersion]       BIGINT        NULL,
    [tbl_user_Id]            BIGINT        NULL,
    [deviceSN]               VARCHAR (50)  NULL,
    [tbl_user_Username]      VARCHAR (50)  NULL,
    CONSTRAINT [PK__tbl_ax_t__3213E83FC20EA093] PRIMARY KEY CLUSTERED ([id] ASC)
);







