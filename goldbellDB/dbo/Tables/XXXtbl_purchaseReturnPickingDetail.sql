﻿CREATE TABLE [dbo].[XXXtbl_purchaseReturnPickingDetail] (
    [id]                           SMALLINT      NOT NULL,
    [tbl_purchaseReturnPicking_Id] BIGINT        NOT NULL,
    [tbl_item_Id]                  BIGINT        NULL,
    [tbl_item_code]                VARCHAR (50)  NULL,
    [tbl_item_description]         VARCHAR (255) NULL,
    [uom]                          VARCHAR (50)  NULL,
    [quantity]                     INT           NULL,
    [tbl_group_Id]                 SMALLINT      NULL,
    [tbl_bin_Id]                   VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC, [tbl_purchaseReturnPicking_Id] ASC),
    CONSTRAINT [fk_tbl_purchasePicking_tbl_purchaseReturnPickingDetail] FOREIGN KEY ([tbl_purchaseReturnPicking_Id]) REFERENCES [dbo].[XXXtbl_purchaseReturnPicking] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_purchasePicking_tbl_purchaseReturnPickingDetail_idx]
    ON [dbo].[XXXtbl_purchaseReturnPickingDetail]([tbl_purchaseReturnPicking_Id] ASC);

