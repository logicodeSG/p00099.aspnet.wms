﻿CREATE TABLE [dbo].[XXXtbl_itemInventory] (
    [tbl_item_Id]     BIGINT NOT NULL,
    [tbl_location_Id] BIGINT NULL,
    [quantity]        INT    NULL,
    PRIMARY KEY CLUSTERED ([tbl_item_Id] ASC)
);

