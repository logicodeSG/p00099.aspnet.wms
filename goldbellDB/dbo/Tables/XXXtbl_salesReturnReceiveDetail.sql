﻿CREATE TABLE [dbo].[XXXtbl_salesReturnReceiveDetail] (
    [id]                        SMALLINT      NOT NULL,
    [tbl_salesReturnReceive_Id] BIGINT        NOT NULL,
    [tbl_item_Id]               BIGINT        NULL,
    [tbl_item_code]             VARCHAR (50)  NULL,
    [tbl_item_description]      VARCHAR (255) NULL,
    [uom]                       VARCHAR (50)  NULL,
    [quantity]                  INT           NULL,
    [tbl_group_Id]              SMALLINT      NULL,
    PRIMARY KEY CLUSTERED ([id] ASC, [tbl_salesReturnReceive_Id] ASC),
    CONSTRAINT [fk_tbl_salesReturnReceive_tbl_salesReturnReceiveDetail] FOREIGN KEY ([tbl_salesReturnReceive_Id]) REFERENCES [dbo].[XXXtbl_salesReturnReceive] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_salesReturnReceive_tbl_salesReturnReceiveDetail_idx]
    ON [dbo].[XXXtbl_salesReturnReceiveDetail]([tbl_salesReturnReceive_Id] ASC);

