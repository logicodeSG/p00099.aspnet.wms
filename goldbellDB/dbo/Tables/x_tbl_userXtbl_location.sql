﻿CREATE TABLE [dbo].[x_tbl_userXtbl_location] (
    [tbl_user_Id]     BIGINT NOT NULL,
    [tbl_location_Id] BIGINT NOT NULL,
    PRIMARY KEY CLUSTERED ([tbl_user_Id] ASC, [tbl_location_Id] ASC),
    CONSTRAINT [fk_Xtbl_location] FOREIGN KEY ([tbl_location_Id]) REFERENCES [dbo].[tbl_location] ([id]),
    CONSTRAINT [fk_Xtbl_user] FOREIGN KEY ([tbl_user_Id]) REFERENCES [dbo].[tbl_user] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_Xtbl_location_idx]
    ON [dbo].[x_tbl_userXtbl_location]([tbl_location_Id] ASC);

