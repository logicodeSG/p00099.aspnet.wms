﻿CREATE TABLE [dbo].[tbl_company] (
    [id]       BIGINT       NOT NULL,
    [name]     VARCHAR (50) NULL,
    [address1] VARCHAR (50) NULL,
    [address2] VARCHAR (50) NULL,
    [address3] VARCHAR (50) NULL,
    [phone]    VARCHAR (20) NULL,
    [fax]      VARCHAR (20) NULL,
    [email]    VARCHAR (50) NULL,
    [website]  VARCHAR (50) NULL,
    [regNo]    VARCHAR (20) NULL,
    [GSTRegNo] VARCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

