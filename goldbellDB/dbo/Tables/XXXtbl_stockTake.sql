﻿CREATE TABLE [dbo].[XXXtbl_stockTake] (
    [id]               BIGINT       NOT NULL,
    [referenceNo]      VARCHAR (50) NULL,
    [tbl_ax_cj_Id]     BIGINT       NULL,
    [creationDatetime] DATETIME     NULL,
    [stDatetime]       DATETIME     NULL,
    [tbl_user_Id]      BIGINT       NULL,
    [tbl_status_Id]    BIGINT       NULL,
    [deviceSN]         VARCHAR (50) NULL,
    [lastEdit]         BIGINT       NULL,
    [tbl_location_Id]  BIGINT       NULL,
    [referenceVersion] BIGINT       NULL,
    CONSTRAINT [PK__tbl_stoc__3213E83F336298CC] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_stockTake_tbl_stockTakeJob_idx]
    ON [dbo].[XXXtbl_stockTake]([tbl_ax_cj_Id] ASC);

