﻿CREATE TABLE [dbo].[XXXtbl_goodsReceiveDetail] (
    [id]                   SMALLINT      NOT NULL,
    [tbl_goodsReceive_Id]  BIGINT        NOT NULL,
    [tbl_item_Id]          BIGINT        NULL,
    [tbl_item_code]        VARCHAR (50)  NULL,
    [tbl_item_description] VARCHAR (255) NULL,
    [uom]                  VARCHAR (50)  NULL,
    [quantity]             INT           NULL,
    [tbl_location_Id]      BIGINT        NULL,
    [tbl_group_Id]         SMALLINT      NULL,
    PRIMARY KEY CLUSTERED ([id] ASC, [tbl_goodsReceive_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_goodsReceive_tbl_goodsReceiveDetail_idx]
    ON [dbo].[XXXtbl_goodsReceiveDetail]([tbl_goodsReceive_Id] ASC);

