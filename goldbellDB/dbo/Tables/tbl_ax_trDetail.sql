﻿CREATE TABLE [dbo].[tbl_ax_trDetail] (
    [id]                 BIGINT           NOT NULL,
    [tbl_ax_tr_Id]       BIGINT           NOT NULL,
    [axDATAAREAID]       NVARCHAR (4)     NULL,
    [axLINENUM]          NUMERIC (32, 16) NULL,
    [axTRANSFERID]       NVARCHAR (20)    NULL,
    [axTRANSFERSTATUS]   INT              NULL,
    [axINVENTTRANSID]    NVARCHAR (20)    NULL,
    [axITEMID]           NVARCHAR (50)    NULL,
    [axDOT_PARTCODE]     NVARCHAR (80)    NULL,
    [axNAMEALIAS]        NVARCHAR (20)    NULL,
    [axUNITID]           NVARCHAR (10)    NULL,
    [axINVENTLOCATIONID] NVARCHAR (20)    NULL,
    [axINVENTSITEID]     NVARCHAR (10)    NULL,
    [axQTYSHIPPED]       NUMERIC (32, 16) NULL,
    [axDocType]          VARCHAR (6)      NULL,
    [scanQuantity]       NUMERIC (32, 16) NULL,
    [scanDatetime]       DATETIME         NULL,
    [remarks]            NVARCHAR (255)   NULL,
    [tbl_status_Id]      BIGINT           CONSTRAINT [DF_tbl_ax_trDetail_tbl_status_Id] DEFAULT ((1)) NOT NULL,
    [tbl_user_Id]        BIGINT           NULL,
    [deviceSN]           VARCHAR (50)     NULL,
    [tbl_user_Username]  VARCHAR (50)     NULL,
    [axWMSLOCATIONID]    NVARCHAR (25)    NULL,
    CONSTRAINT [PK_tbl_ax_trDetail] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_tbl_ax_trDetail_tbl_ax_tr] FOREIGN KEY ([tbl_ax_tr_Id]) REFERENCES [dbo].[tbl_ax_tr] ([id]) ON DELETE CASCADE
);





