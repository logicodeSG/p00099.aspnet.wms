﻿CREATE TABLE [dbo].[tbl_ax_aj] (
    [id]                 BIGINT        NOT NULL,
    [axJOURNALID]        NVARCHAR (20) NULL,
    [axDESCRIPTION]      NVARCHAR (60) NULL,
    [axPOSTED]           INT           NULL,
    [axJOURNALNAMEID]    NVARCHAR (20) NULL,
    [axINVENTDIMID]      NVARCHAR (20) NULL,
    [axPACKINGSLIP]      NVARCHAR (20) NULL,
    [axCREATEDDATETIME]  DATETIME      NULL,
    [axDATAAREAID]       NVARCHAR (4)  NULL,
    [referenceNo]        VARCHAR (50)  NULL,
    [creationDatetime]   DATETIME      NULL,
    [completionDatetime] DATETIME      NULL,
    [scanDatetime]       DATETIME      NULL,
    [tbl_status_Id]      BIGINT        NULL,
    [docType]            VARCHAR (50)  NULL,
    [referenceVersion]   BIGINT        NULL,
    [tbl_user_Id]        BIGINT        NULL,
    [deviceSN]           VARCHAR (50)  NULL,
    [tbl_user_Username]  VARCHAR (50)  NULL,
    CONSTRAINT [PK__tbl_stoc__3213E83F61B2E463] PRIMARY KEY CLUSTERED ([id] ASC)
);





