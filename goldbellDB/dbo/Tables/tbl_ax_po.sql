﻿CREATE TABLE [dbo].[tbl_ax_po] (
    [id]                     BIGINT        NOT NULL,
    [axDATAAREAID]           NVARCHAR (4)  NOT NULL,
    [axPURCHID]              NVARCHAR (20) NOT NULL,
    [axPURCHNAME]            NVARCHAR (60) NOT NULL,
    [axORDERACCOUNT]         NVARCHAR (20) NOT NULL,
    [axINVOICEACCOUNT]       NVARCHAR (20) NOT NULL,
    [axLastConfirmationDate] DATETIME      NULL,
    [axDELIVERYDATE]         DATETIME      NOT NULL,
    [axCREATEDDATETIME]      DATETIME      NOT NULL,
    [referenceNo]            VARCHAR (50)  NULL,
    [creationDatetime]       DATETIME      NULL,
    [completionDatetime]     DATETIME      NULL,
    [scanDatetime]           DATETIME      NULL,
    [tbl_status_Id]          BIGINT        NULL,
    [docType]                VARCHAR (50)  NULL,
    [referenceVersion]       BIGINT        NULL,
    [tbl_user_Id]            BIGINT        NULL,
    [deviceSN]               VARCHAR (50)  NULL,
    [tbl_user_Username]      VARCHAR (50)  NULL,
    CONSTRAINT [PK__tbl_ax_p__3213E83FB06307CE] PRIMARY KEY CLUSTERED ([id] ASC)
);









