﻿CREATE TABLE [dbo].[tbl_systemData] (
    [id]        BIGINT        NOT NULL,
    [name]      VARCHAR (50)  NULL,
    [dataValue] VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

