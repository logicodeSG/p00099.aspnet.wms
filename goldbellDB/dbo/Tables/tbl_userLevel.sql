﻿CREATE TABLE [dbo].[tbl_userLevel] (
    [id]          BIGINT       NOT NULL,
    [order]       INT          NULL,
    [description] VARCHAR (50) NULL,
    [active]      BIT          NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

