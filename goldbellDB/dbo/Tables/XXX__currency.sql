﻿CREATE TABLE [dbo].[XXX__currency] (
    [id]     SMALLINT      NOT NULL,
    [issuer] NVARCHAR (50) NULL,
    [code]   NVARCHAR (5)  NULL,
    [symbol] NVARCHAR (5)  NULL,
    [name]   NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

