﻿CREATE TABLE [dbo].[XXXtbl_ax_sr] (
    [id]               BIGINT       NOT NULL,
    [referenceNo]      VARCHAR (50) NULL,
    [srDatetime]       DATETIME     CONSTRAINT [DF_tbl_ax_sr_soDatetime] DEFAULT (((2018)-(5))-(8)) NULL,
    [creationDatetime] DATETIME     NULL,
    [tbl_status_Id]    BIGINT       NULL,
    [docType]          VARCHAR (50) NULL,
    [tbl_location_Id]  BIGINT       NULL,
    [referenceVersion] BIGINT       NULL,
    CONSTRAINT [PK__tbl_ax_s__3213E83F49EBF22C] PRIMARY KEY CLUSTERED ([id] ASC)
);

