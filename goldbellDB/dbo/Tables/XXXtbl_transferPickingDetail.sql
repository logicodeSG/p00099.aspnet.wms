﻿CREATE TABLE [dbo].[XXXtbl_transferPickingDetail] (
    [id]                     SMALLINT      NOT NULL,
    [tbl_transferPicking_Id] BIGINT        NOT NULL,
    [tbl_item_Id]            BIGINT        NULL,
    [tbl_item_code]          VARCHAR (50)  NULL,
    [tbl_item_description]   VARCHAR (255) NULL,
    [uom]                    VARCHAR (50)  NULL,
    [quantity]               INT           NULL,
    [receive_quantity]       INT           NULL,
    [tbl_group_Id]           SMALLINT      NULL,
    [tbl_bin_Id]             VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC, [tbl_transferPicking_Id] ASC),
    CONSTRAINT [fk_tbl_transferPicking_tbl_transferPickingDetail] FOREIGN KEY ([tbl_transferPicking_Id]) REFERENCES [dbo].[XXXtbl_transferPicking] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_transferPicking_tbl_transferPickingDetail_idx]
    ON [dbo].[XXXtbl_transferPickingDetail]([tbl_transferPicking_Id] ASC);

