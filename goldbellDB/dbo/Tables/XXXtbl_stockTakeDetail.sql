﻿CREATE TABLE [dbo].[XXXtbl_stockTakeDetail] (
    [id]                   SMALLINT      NOT NULL,
    [tbl_stockTake_Id]     BIGINT        NOT NULL,
    [tbl_item_Id]          BIGINT        NULL,
    [tbl_item_code]        VARCHAR (50)  NULL,
    [tbl_item_description] VARCHAR (255) NULL,
    [uom]                  VARCHAR (50)  NULL,
    [quantity]             INT           NULL,
    [tbl_group_Id]         SMALLINT      NULL,
    [tbl_bin_Id]           VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC, [tbl_stockTake_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_stockTake_tbl_stockTakeDetail_idx]
    ON [dbo].[XXXtbl_stockTakeDetail]([tbl_stockTake_Id] ASC);

