﻿CREATE TABLE [dbo].[XXXtbl_transferReceive] (
    [id]                     BIGINT       NOT NULL,
    [referenceNo]            VARCHAR (50) NULL,
    [tbl_transferPicking_Id] BIGINT       NULL,
    [creationDatetime]       DATETIME     NULL,
    [receiveDatetime]        DATETIME     NULL,
    [tbl_user_Id]            BIGINT       NULL,
    [tbl_status_Id]          BIGINT       NULL,
    [deviceSN]               VARCHAR (50) NULL,
    [lastEdit]               BIGINT       NULL,
    [tbl_location_Id]        BIGINT       NULL,
    [referenceVersion]       BIGINT       NULL,
    CONSTRAINT [PK__tbl_tran__3213E83FDE79704B] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [fk_tbl_transferPicking_tbl_transferReceive] FOREIGN KEY ([tbl_transferPicking_Id]) REFERENCES [dbo].[XXXtbl_transferPicking] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_transferPicking_tbl_transferReceive_idx]
    ON [dbo].[XXXtbl_transferReceive]([tbl_transferPicking_Id] ASC);

