﻿CREATE TABLE [dbo].[tbl_ax_cj] (
    [id]                 BIGINT        NOT NULL,
    [axDATAAREAID]       NVARCHAR (4)  NOT NULL,
    [axPOSTEDDATETIME]   DATETIME      NOT NULL,
    [axJOURNALID]        NVARCHAR (20) NOT NULL,
    [axDESCRIPTION]      NVARCHAR (60) NOT NULL,
    [referenceNo]        VARCHAR (50)  NULL,
    [creationDatetime]   DATETIME      NULL,
    [completionDatetime] DATETIME      NULL,
    [scanDatetime]       DATETIME      NULL,
    [tbl_status_Id]      BIGINT        NULL,
    [docType]            VARCHAR (50)  NULL,
    [referenceVersion]   BIGINT        NULL,
    [tbl_user_Id]        BIGINT        NULL,
    [deviceSN]           VARCHAR (50)  NULL,
    [tbl_user_Username]  VARCHAR (50)  NULL,
    CONSTRAINT [PK_tbl_ax_cj] PRIMARY KEY CLUSTERED ([id] ASC)
);







