﻿CREATE TABLE [dbo].[tbl_locationLevel] (
    [id]          BIGINT       NOT NULL,
    [order]       INT          NULL,
    [description] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

