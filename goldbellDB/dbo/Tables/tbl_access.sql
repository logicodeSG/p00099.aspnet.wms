﻿CREATE TABLE [dbo].[tbl_access] (
    [id]   TINYINT      NOT NULL,
    [name] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

