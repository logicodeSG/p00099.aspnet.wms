﻿CREATE TABLE [dbo].[XXXtbl_priceList] (
    [id]                    SMALLINT      NOT NULL,
    [name]                  NVARCHAR (50) NULL,
    [tbl_systemCurrency_Id] BIGINT        NULL,
    [type]                  NVARCHAR (5)  NULL,
    [isDefaultPriceList]    BIT           NULL,
    [active]                BIT           NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [fk_tbl_priceList_tbl_systemCurrency] FOREIGN KEY ([tbl_systemCurrency_Id]) REFERENCES [dbo].[tbl_systemCurrency] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_priceList_tbl_systemCurrency_idx]
    ON [dbo].[XXXtbl_priceList]([tbl_systemCurrency_Id] ASC);

