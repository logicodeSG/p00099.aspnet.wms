﻿CREATE TABLE [dbo].[XXXtbl_salesReturnReceive] (
    [id]               BIGINT       NOT NULL,
    [referenceNo]      VARCHAR (50) NULL,
    [tbl_ax_sr_Id]     BIGINT       NULL,
    [creationDatetime] DATETIME     NULL,
    [receiveDatetime]  DATETIME     NULL,
    [tbl_user_Id]      BIGINT       NULL,
    [tbl_status_Id]    BIGINT       NULL,
    [deviceSN]         VARCHAR (50) NULL,
    [lastEdit]         BIGINT       NULL,
    [tbl_location_Id]  BIGINT       NULL,
    [referenceVersion] BIGINT       NULL,
    CONSTRAINT [PK__tbl_sale__3213E83F26000545] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [fk_tbl_ax_sr_tbl_salesReturnReceive] FOREIGN KEY ([tbl_ax_sr_Id]) REFERENCES [dbo].[XXXtbl_ax_sr] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_ax_sr_tbl_salesReturnReceive_idx]
    ON [dbo].[XXXtbl_salesReturnReceive]([tbl_ax_sr_Id] ASC);

