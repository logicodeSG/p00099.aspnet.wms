﻿CREATE TABLE [dbo].[XXXtbl_ax_prDetail] (
    [id]                   SMALLINT      NOT NULL,
    [tbl_ax_pr_Id]         BIGINT        NOT NULL,
    [tbl_item_Id]          BIGINT        NULL,
    [tbl_item_code]        VARCHAR (50)  NULL,
    [tbl_item_description] VARCHAR (255) NULL,
    [uom]                  VARCHAR (50)  NULL,
    [quantity]             INT           NULL,
    [tbl_group_Id]         SMALLINT      NULL,
    [tbl_bin_Id]           VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC, [tbl_ax_pr_Id] ASC),
    CONSTRAINT [fk_tbl_ax_pr_tbl_ax_prDetail] FOREIGN KEY ([tbl_ax_pr_Id]) REFERENCES [dbo].[XXXtbl_ax_pr] ([id])
);


GO
CREATE NONCLUSTERED INDEX [fk_tbl_ax_pr_tbl_ax_prDetail_idx]
    ON [dbo].[XXXtbl_ax_prDetail]([tbl_ax_pr_Id] ASC);

