﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXTRDetailByAXLineNum_TransferId_InventTransId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
	@keyAXTRD_DATAAREAID nvarchar(4),
	@keyAXTRD_LINENUMBER numeric(32,16),
	@keyAXTRD_TRANSFERID nvarchar(20),
	@keyAXTRD_TRANSFERSTATUS int,
	@keyAXTRD_INVENTTRANSID nvarchar(20),
	@keyAXTRD_ITEMID nvarchar(50),
	@keyAXTRD_DOT_PARTCODE nvarchar(80),
	@keyAXTRD_NAMEALIAS nvarchar(20),
	@keyAXTRD_UNITID nvarchar(10),
	@keyAXTRD_INVENTLOCATIONID nvarchar(20),
	@keyAXTRD_INVENTSITEID nvarchar(10),
	@keyAXTRD_WMSLOCATIONID nvarchar(25),
	@keyAXTRD_QTYSHIPPED numeric(32,16),
	@keyAXTRD_docType varchar(6)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_trDetail
	SET 
	axDATAAREAID = @keyAXTRD_DATAAREAID,
	axTRANSFERSTATUS = @keyAXTRD_TRANSFERSTATUS,
	axNAMEALIAS = @keyAXTRD_NAMEALIAS,
	axUNITID = @keyAXTRD_UNITID,
	axINVENTLOCATIONID = @keyAXTRD_INVENTLOCATIONID,
	axINVENTSITEID = @keyAXTRD_INVENTSITEID,
	axWMSLOCATIONID = @keyAXTRD_WMSLOCATIONID,
	axQTYSHIPPED = @keyAXTRD_QTYSHIPPED,
	axDocType = @keyAXTRD_docType
	WHERE axLINENUM = @keyAXTRD_LINENUMBER
	AND axTRANSFERID = @keyAXTRD_TRANSFERID
	AND axINVENTTRANSID = @keyAXTRD_INVENTTRANSID
	AND axITEMID = @keyAXTRD_ITEMID
	AND axDOT_PARTCODE = @keyAXTRD_DOT_PARTCODE
END