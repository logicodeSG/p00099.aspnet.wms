﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[checkExistenceAXCJDetailWRemainingQuantityByAXJournalId]
	-- Add the parameters for the stored procedure here
	@keyAXJOURNALID nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
        [id]
      ,[tbl_ax_cj_Id]
      ,[axJOURNALID]
      ,[axLINENUM]
      ,[axITEMID]
      ,[axDOT_PARTCODE]
      ,[axQTY]
      ,[axInventUnitID]
      ,[axINVENTLOCATIONID]
      ,[axINVENTSITEID]
      ,[axWMSLOCATIONID]
      ,[scanQuantity]
      ,[scanDatetime]
      ,[remarks]
      ,[tbl_status_Id]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
	FROM tbl_ax_cjDetail
	WHERE axJOURNALID = @keyAXJOURNALID
    AND tbl_status_Id = 1
END