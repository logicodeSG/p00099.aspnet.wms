﻿CREATE PROCEDURE [dbo].[selectNextSalesReturnReceiveDetailId]
	-- Add the parameters for the stored procedure here
	@keySRRId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CASE WHEN MAX(tbl_salesReturnReceiveDetail.id) IS NULL THEN 1 ELSE MAX(tbl_salesReturnReceiveDetail.id)+1 END 
	FROM tbl_salesReturnReceiveDetail WHERE tbl_salesReturnReceive_Id=@keySRRId
END