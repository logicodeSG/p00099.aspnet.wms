﻿CREATE PROCEDURE [dbo].[selectAXTRDetailByAXLineNum_TransferId_InventTransId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
    @keyAXLINENUM  numeric(32,16),
	@keyAXTRANSFERID nvarchar(20),
	@keyAXINVENTTRANSID nvarchar(20),
	@keyAXITEMID nvarchar(50),
	@keyAXDOT_PARTCODE nvarchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	    SELECT [id]
      ,[tbl_ax_tr_Id]
      ,[axDATAAREAID]
      ,[axLINENUM]
      ,[axTRANSFERID]
      ,[axTRANSFERSTATUS]
      ,[axINVENTTRANSID]
      ,[axITEMID]
      ,[axDOT_PARTCODE]
      ,[axNAMEALIAS]
      ,[axUNITID]
      ,[axINVENTLOCATIONID]
      ,[axINVENTSITEID]
      ,[axQTYSHIPPED]
      ,[axDocType]
      ,[scanQuantity]
      ,[scanDatetime]
      ,[remarks]
      ,[tbl_status_Id]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
      ,[axWMSLOCATIONID]
  FROM [tbl_ax_trDetail]
    WHERE axLINENUM = @keyAXLINENUM
    AND axTRANSFERID = @keyAXTRANSFERID
    AND axINVENTTRANSID = @keyAXINVENTTRANSID
    AND axITEMID = @keyAXITEMID
    AND axDOT_PARTCODE = @keyAXDOT_PARTCODE
END