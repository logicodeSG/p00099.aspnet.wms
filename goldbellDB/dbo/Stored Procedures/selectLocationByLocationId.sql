﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectLocationByLocationId]
	-- Add the parameters for the stored procedure here
	@keyLocationId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [id]
      ,[code]
      ,[description]
      ,[tbl_locationLevel_Id]
      ,[tbl_locationParent_Id]
      ,[active]
	FROM tbl_location
	WHERE id = @keyLocationId
END