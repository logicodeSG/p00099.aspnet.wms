﻿CREATE PROCEDURE [dbo].[selectSTDetailByCJIdItemIdQtyGroupId]
	-- Add the parameters for the stored procedure here
	@keyCJId bigInt,
	@keyItemId smallInt,
	@keySOPQuantity Int,
	@keyGroupId smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_stockTakeDetail.id, tbl_stockTakeDetail.tbl_stockTake_Id, 
	tbl_stockTakeDetail.tbl_item_Id, tbl_stockTakeDetail.tbl_item_code,
	tbl_stockTakeDetail.tbl_item_description, tbl_stockTakeDetail.uom,
	tbl_stockTakeDetail.quantity, 
	tbl_stockTakeDetail.tbl_group_Id, 
	tbl_stockTakeDetail.tbl_bin_Id
 	FROM tbl_stockTakeDetail
	WHERE tbl_stockTakeDetail.tbl_stockTake_Id = 
	(SELECT tbl_stockTake.id FROM tbl_stockTake 
	WHERE tbl_stockTake.tbl_ax_cj_Id=@keyCJId)
	AND tbl_stockTakeDetail.tbl_item_Id = @keyItemId
	AND tbl_stockTakeDetail.quantity = @keySOPQuantity
	AND tbl_stockTakeDetail.tbl_group_Id = @keyGroupId
END