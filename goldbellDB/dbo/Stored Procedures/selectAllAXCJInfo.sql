﻿CREATE PROCEDURE [dbo].[selectAllAXCJInfo]
	-- Add the parameters for the stored procedure here
	--@keyUserId AS varchar(50),
	 @keySortHeader AS varchar(100),
	 @keySortContent AS varchar(50),
	 @keySearchType AS varchar(50),
	 @keySearchHeader AS varchar(50),
	 @keySearchContent AS varchar(50),
	 @keySearchContent2 AS varchar(50),
	 @keySearchWarehouseCode AS varchar(50)
	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery1 AS NVARCHAR(MAX)
	DECLARE @SQLQuery2 AS NVARCHAR(MAX)
	DECLARE @SQLQuery3 AS NVARCHAR(MAX)
	DECLARE @SQLQuery NVARCHAR(MAX)

    -- Insert statements for procedure here
	-- Insert statements for procedure here

	SET @SQLQuery3 =
	CASE 
		WHEN @keySearchType IN ('DATE') 
			THEN 'WHERE '+@keySearchHeader+' BETWEEN '''+ @keySearchContent +''' AND ''' + @keySearchContent2 +''' '
		WHEN @keySearchType IN ('DROPDOWN') 
				THEN CASE 
					WHEN @keySearchContent IN ('COMPLETED')
						THEN 'WHERE '+@keySearchHeader+' = 9 '
					ELSE 'WHERE '+@keySearchHeader+' = 1 '
				END
		ELSE 'WHERE '+@keySearchHeader+' LIKE ''%'+ @keySearchContent +'%'' '
	END 

	SET @SQLQuery = ' 
		SELECT DISTINCT TOP(SELECT CAST(dataValue AS INT) FROM tbl_systemData WHERE name=''DISPLAY ROWS'') 
		 [tbl_ax_cj].[id]
      ,[tbl_ax_cj].[axDATAAREAID]
      ,[tbl_ax_cj].[axPOSTEDDATETIME]
      ,[tbl_ax_cj].[axJOURNALID]
      ,[tbl_ax_cj].[axDESCRIPTION]
      ,[tbl_ax_cj].[referenceNo]
      ,[tbl_ax_cj].[creationDatetime]
      ,[tbl_ax_cj].[completionDatetime]
      ,[tbl_ax_cj].[scanDatetime]
      ,[tbl_ax_cj].[tbl_status_Id]
      ,[tbl_ax_cj].[docType]
      ,[tbl_ax_cj].[referenceVersion]
      ,[tbl_ax_cj].[tbl_user_Id]
      ,[tbl_ax_cj].[deviceSN]
      ,[tbl_ax_cj].[tbl_user_Username]
	  FROM  [tbl_ax_cj] '+ 
		  @SQLQuery3 +
	  ' AND [tbl_ax_cj].[axDATAAREAID] = ''' + @keySearchWarehouseCode + '''
		   ORDER BY '+@keySortHeader+' '+@keySortContent+''
        

	EXECUTE(@SQLQuery)

END