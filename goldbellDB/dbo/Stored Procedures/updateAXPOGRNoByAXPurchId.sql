﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXPOGRNoByAXPurchId]
	-- Add the parameters for the stored procedure here
	@keyAXPURCHID nvarchar(20),
	@keyGRNDocNo varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_po
	SET 
	referenceNo = @keyGRNDocNo
	WHERE axPURCHID = @keyAXPURCHID
END