﻿CREATE PROCEDURE [dbo].[checkExistenceAXSRDetailWRemainingQuantityBySRId]
	-- Add the parameters for the ssrred srocedure here
	@keyAXSRId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added sr srevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for srocedure here
	SELECT tbl_ax_srDetail.id, tbl_ax_srDetail.tbl_ax_sr_Id, tbl_ax_srDetail.tbl_item_Id,
	tbl_ax_srDetail.tbl_item_code, tbl_ax_srDetail.tbl_item_description,
	tbl_ax_srDetail.uom, tbl_ax_srDetail.quantity, 
	SUM(ISNULL(tbl_salesReturnReceiveDetail.quantity,0)) AS [receivedQuantity],
	tbl_ax_srDetail.tbl_group_Id
 	FROM tbl_ax_srDetail
	LEFT JOIN tbl_salesReturnReceive ON tbl_salesReturnReceive.tbl_ax_sr_Id = tbl_ax_srDetail.tbl_ax_sr_Id
	AND tbl_salesReturnReceive.tbl_status_Id NOT IN (3)
	LEFT JOIN tbl_salesReturnReceiveDetail ON tbl_salesReturnReceiveDetail.tbl_salesReturnReceive_Id = tbl_salesReturnReceive.id
	AND tbl_salesReturnReceiveDetail.tbl_item_Id = tbl_ax_srDetail.tbl_item_Id
	AND tbl_salesReturnReceiveDetail.tbl_group_Id = tbl_ax_srDetail.tbl_group_Id
	WHERE tbl_ax_srDetail.tbl_ax_sr_Id = @keyAXSRId
	GROUP BY tbl_ax_srDetail.id, tbl_ax_srDetail.tbl_ax_sr_Id, tbl_ax_srDetail.tbl_item_Id,
	tbl_ax_srDetail.tbl_item_code, tbl_ax_srDetail.tbl_item_description,
	tbl_ax_srDetail.uom, tbl_ax_srDetail.quantity,tbl_ax_srDetail.tbl_group_Id
	HAVING tbl_ax_srDetail.quantity-SUM(ISNULL(tbl_salesReturnReceiveDetail.quantity,0)) > 0
END