﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXCJ]
	-- Add the parameters for the stored procedure here
	@keyAXCJ_Id bigint,
	@keyAXCJ_DATAAREAID nvarchar(4),
	@keyAXCJ_POSTEDDATETIME datetime,
	@keyAXCJ_JOURNALID nvarchar(20),
	@keyAXCJ_DESCRIPTION nvarchar(60),
	@keyReferenceNo varchar(50),
	@keyCreationDatetime datetime,
	@keyScanDatetime datetime,
	@keyStatusId bigint,
	@keyDocType varchar(50),
	@keyReferenceVersion bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_cj(
	[id]
      ,[axDATAAREAID]
      ,[axPOSTEDDATETIME]
      ,[axJOURNALID]
      ,[axDESCRIPTION]
      ,[referenceNo]
      ,[creationDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
	)
	values(
	@keyAXCJ_Id ,
	@keyAXCJ_DATAAREAID ,
	@keyAXCJ_POSTEDDATETIME ,
	@keyAXCJ_JOURNALID ,
	@keyAXCJ_DESCRIPTION ,
	@keyReferenceNo ,
	@keyCreationDatetime ,
	@keyScanDatetime ,
	@keyStatusId ,
	@keyDocType,
	@keyReferenceVersion ,
	@keyUserId,
	@keyDeviceSN)
END