﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllLevelOneLocation]
	-- Add the parameters for the stored procedure here
	@keySortHeader AS varchar(50),
	@keySortContent AS varchar(50),
	@keyShowAll AS varchar(50),
	 @keySearchHeader AS varchar(50),
	 @keySearchContent AS varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.R
	SET NOCOUNT ON;

	DECLARE @SQLQuery AS NVARCHAR(2000)
	DECLARE @SQLCond AS NVARCHAR(50)

    -- Insert statements for procedure here
	IF @keyShowAll = 'false'
	SET @SQLCond = 'AND tbl_location.active = 1 '
	ELSE
	SET @SQLCond = ''

	SET @SQLQuery = ' 
		SELECT tbl_location.id
			,tbl_location.code
			,tbl_location.[description] 
			,tbl_location.active
		FROM tbl_location
		INNER JOIN tbl_locationLevel
		ON tbl_location.tbl_locationLevel_Id = tbl_locationLevel.id
		WHERE tbl_locationLevel.[order] = 1 ' + @SQLCond +
		'AND '+@keySearchHeader+' LIKE ''%'+ @keySearchContent +'%'' ' +
		'ORDER BY '+@keySortHeader+' '+@keySortContent+'
		'
	EXECUTE(@SQLQuery)

	
END