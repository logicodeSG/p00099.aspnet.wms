﻿CREATE PROCEDURE [dbo].[selectSTDetailBySTIdSTDetailId]
	-- Add the parameters for the stored procedure here
	@keySTId bigInt,
	@keySTDetailId smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_stockTakeDetail.id, tbl_stockTakeDetail.tbl_stockTake_Id,
	tbl_stockTakeDetail.tbl_item_Id, tbl_stockTakeDetail.tbl_item_code,
	tbl_stockTakeDetail.tbl_item_description, tbl_stockTakeDetail.uom,
	tbl_stockTakeDetail.quantity, 
	tbl_stockTakeDetail.tbl_group_Id,
	tbl_stockTakeDetail.tbl_bin_Id
 	FROM tbl_stockTakeDetail
	WHERE tbl_stockTakeDetail.tbl_stockTake_Id = @keySTId
	AND tbl_stockTakeDetail.id = @keySTDetailId
END