﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllPendingAXTOwAllPendingAXTODetail]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [id]
      ,[axDATAAREAID]
      ,[axMODIFIEDDATETIME]
      ,[axPICKINGROUTEID]
      ,[axTRANSFERID]
      ,[axDocumentType]
      ,[axINVENTLOCATIONIDFROM]
      ,[axINVENTLOCATIONIDTO]
      ,[axTRANSFERSTATUS]
      ,[referenceNo]
      ,[creationDatetime]
      ,[completionDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
 	FROM tbl_ax_to
	WHERE tbl_status_Id = 1
	AND tbl_ax_to.id NOT IN(
	SELECT DISTINCT tbl_ax_toDetail.tbl_ax_to_Id
	FROM tbl_ax_toDetail
	WHERE tbl_ax_toDetail.tbl_status_Id <> 1)
	 
END