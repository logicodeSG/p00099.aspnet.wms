﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllGoodsReceiveInfo]
	-- Add the parameters for the stored procedure here
	--@keyUserId AS varchar(50),
	 @keySortHeader AS varchar(100),
	 @keySortContent AS varchar(50),
	 @keySearchType AS varchar(50),
	 @keySearchHeader AS varchar(50),
	 @keySearchContent AS varchar(50),
	 @keySearchContent2 AS varchar(50),
	 @keySearchLocationId AS varchar(50)
	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery NVARCHAR(MAX)
	DECLARE @SQLQuery3 NVARCHAR(MAX)

    -- Insert statements for procedure here
	-- Insert statements for procedure here

	SET @SQLQuery3 =
	CASE 
		WHEN @keySearchType IN ('DATE') 
				THEN 'WHERE '+@keySearchHeader+' BETWEEN '''+ @keySearchContent +''' AND ''' + @keySearchContent2 +''' '
		WHEN @keySearchType IN ('DROPDOWN') 
				THEN CASE 
					WHEN @keySearchContent IN ('UNDO')
						THEN 'WHERE '+@keySearchHeader+' = 3 '
					ELSE 'WHERE '+@keySearchHeader+' = 1 '
				END
		ELSE 'WHERE '+@keySearchHeader+' LIKE ''%'+ @keySearchContent +'%'' '
	END 

	SET @SQLQuery3 = @SQLQuery3 +
					'AND tbl_ax_po.tbl_location_Id ='+@keySearchLocationId+'  '

	SET @SQLQuery = ' 
		SELECT tbl_goodsReceive.*,
		tbl_ax_po.referenceNo AS ''poReferenceNo'',
		tbl_user.username AS ''createdBy''
		FROM tbl_goodsReceive
		LEFT JOIN tbl_ax_po ON tbl_ax_po.id = tbl_goodsReceive.tbl_ax_po_Id
		LEFT JOIN tbl_user ON tbl_goodsReceive.tbl_user_Id = tbl_user.id
        '+ @SQLQuery3 +'
		GROUP BY tbl_goodsReceive.id, tbl_goodsReceive.referenceNo, tbl_goodsReceive.doReferenceNo, 
		tbl_goodsReceive.tbl_ax_po_Id, tbl_goodsReceive.creationDatetime, 
		tbl_goodsReceive.receiveDatetime, tbl_goodsReceive.tbl_user_Id, tbl_goodsReceive.deviceSN, 
		tbl_goodsReceive.tbl_status_Id, tbl_goodsReceive.lastEdit,
		tbl_goodsReceive.tbl_location_Id, tbl_goodsReceive.referenceVersion,
		tbl_ax_po.referenceNo, tbl_user.username
		ORDER BY '+@keySortHeader+' '+@keySortContent+''

	EXECUTE(@SQLQuery)

END