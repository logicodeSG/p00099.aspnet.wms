﻿CREATE PROCEDURE [dbo].[insertNewPurchaseReturnPickingDetail]
	-- Add the parameters for the stored procedure here
	@keyPRPId bigint,
	@keyPRPDetailId smallint,
	@keyItemId bigint,
	@keyItemCode varchar(50),
	@keyItemDescription varchar(255),
	@keyItemUOM varchar(50),
	@keyItemQuantity int,
	@keyItemGroupId smallint,
	@keyItemBinId varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_purchaseReturnPickingDetail(id, tbl_purchaseReturnPicking_Id, tbl_item_Id, tbl_item_code,
	tbl_item_description, uom, quantity, tbl_group_Id, tbl_bin_Id)
	values(@keyPRPDetailId,@keyPRPId,@keyItemId,@keyItemCode,@keyItemDescription,
	@keyItemUOM,@keyItemQuantity,@keyItemGroupId,@keyItemBinId)
END