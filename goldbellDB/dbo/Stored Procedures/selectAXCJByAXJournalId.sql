﻿
CREATE PROCEDURE [dbo].[selectAXCJByAXJournalId]
	-- Add the parameters for the stored procedure here
	@keyAXJOURNALId nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [id]
      ,[axDATAAREAID]
      ,[axPOSTEDDATETIME]
      ,[axJOURNALID]
      ,[axDESCRIPTION]
      ,[referenceNo]
      ,[creationDatetime]
      ,[completionDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
 	FROM tbl_ax_cj
    WHERE axJOURNALID = @keyAXJOURNALId
END