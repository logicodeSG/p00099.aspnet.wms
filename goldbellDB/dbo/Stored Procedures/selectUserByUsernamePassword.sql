﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectUserByUsernamePassword]
	-- Add the parameters for the stored procedure here
	@keyUsername varchar(50),
	@keyUserPass nvarchar(64)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery AS NVARCHAR(1000)

    -- Insert statements for procedure here
	
		SELECT *
		FROM tbl_user
		INNER JOIN tbl_userLevel
		ON tbl_userLevel.Id = tbl_user.tbl_userLevel_Id
		WHERE tbl_user.username = @keyUsername
		AND tbl_user.[password] = @keyUserPass

END