﻿CREATE PROCEDURE [dbo].[selectAXTRDetailByAXTransferId]
	-- Add the parameters for the stored procedure here
	@keyAXTRANSFERID nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	    SELECT [id]
      ,[tbl_ax_tr_Id]
      ,[axDATAAREAID]
      ,[axLINENUM]
      ,[axTRANSFERID]
      ,[axTRANSFERSTATUS]
      ,[axINVENTTRANSID]
      ,[axITEMID]
      ,[axDOT_PARTCODE]
      ,[axNAMEALIAS]
      ,[axUNITID]
      ,[axINVENTLOCATIONID]
      ,[axINVENTSITEID]
      ,[axQTYSHIPPED]
      ,[axDocType]
      ,[scanQuantity]
      ,[scanDatetime]
      ,[remarks]
      ,[tbl_status_Id]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
      ,[axWMSLOCATIONID]
  FROM [tbl_ax_trDetail]
    WHERE [tbl_ax_trDetail].[axTRANSFERID] = @keyAXTRANSFERID
END