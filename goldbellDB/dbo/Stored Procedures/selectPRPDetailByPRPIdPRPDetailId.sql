﻿CREATE PROCEDURE [dbo].[selectPRPDetailByPRPIdPRPDetailId]
	-- Add the parameters for the stored procedure here
	@keyPRPId bigInt,
	@keyPRPDetailId smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_purchaseReturnPickingDetail.id, tbl_purchaseReturnPickingDetail.tbl_purchaseReturnPicking_Id,
	tbl_purchaseReturnPickingDetail.tbl_item_Id, tbl_purchaseReturnPickingDetail.tbl_item_code,
	tbl_purchaseReturnPickingDetail.tbl_item_description, tbl_purchaseReturnPickingDetail.uom,
	tbl_purchaseReturnPickingDetail.quantity, 
	tbl_purchaseReturnPickingDetail.tbl_group_Id, tbl_purchaseReturnPickingDetail.tbl_bin_Id
 	FROM tbl_purchaseReturnPickingDetail
	WHERE tbl_purchaseReturnPickingDetail.tbl_purchaseReturnPicking_Id = @keyPRPId
	AND tbl_purchaseReturnPickingDetail.id = @keyPRPDetailId
END