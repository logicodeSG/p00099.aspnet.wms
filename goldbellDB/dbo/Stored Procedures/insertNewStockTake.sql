﻿CREATE PROCEDURE [dbo].[insertNewStockTake]
	-- Add the parameters for the stored procedure here
	@keySTId bigint,
	@keySTReferenceNumber varchar(50),
	@keySTCJId bigint,
	@keyCreationDateTime varchar(50),
	@keySTDateTime varchar(50),
	@keyCreatedBy bigint,
	@keySTDeviceSN varchar(50),
	@keySTLocationId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_stockTake(id, referenceNo, tbl_ax_cj_Id, creationDatetime,
	stDatetime, tbl_user_Id, deviceSN, tbl_status_Id, lastEdit, tbl_location_Id, referenceVersion)
	values(
	@keySTId,@keySTReferenceNumber,@keySTCJId,@keyCreationDateTime,
	@keySTDateTime,@keyCreatedBy,@keySTDeviceSN,1,@keyCreatedBy, @keySTLocationId, 1)
END