﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXTO]
	-- Add the parameters for the stored procedure here
	@keyAXTO_Id bigint,
	@keyAXTO_DATAAREAID nvarchar(4),
	@keyAXTO_MODIFIEDDATETIME datetime,
	@keyAXTO_ROUTEID nvarchar(10),
	@keyAXTO_TRANSFERID nvarchar(20),
	@keyAXTO_DOCUMENTTYPE varchar(2),
	@keyAXTO_INVENTLOCATIONIDFROM nvarchar(20),
	@keyAXTO_INVENTLOCATIONIDTO nvarchar(20),
	@keyAXTO_TRANSFERSTATUS int,
	@keyReferenceNo varchar(50),
	@keyCreationDatetime datetime,
	@keyScanDatetime datetime,
	@keyStatusId bigint,
	@keyDocType varchar(50),
	@keyReferenceVersion bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_to(
		 [id]
      ,[axDATAAREAID]
      ,[axMODIFIEDDATETIME]
      ,[axPICKINGROUTEID]
      ,[axTRANSFERID]
      ,[axDocumentType]
      ,[axINVENTLOCATIONIDFROM]
      ,[axINVENTLOCATIONIDTO]
      ,[axTRANSFERSTATUS]
      ,[referenceNo]
      ,[creationDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
	)
	values(
	@keyAXTO_Id ,
	@keyAXTO_DATAAREAID ,
	@keyAXTO_MODIFIEDDATETIME,
	@keyAXTO_ROUTEID ,
	@keyAXTO_TRANSFERID ,
	@keyAXTO_DOCUMENTTYPE ,
	@keyAXTO_INVENTLOCATIONIDFROM ,
	@keyAXTO_INVENTLOCATIONIDTO ,
	@keyAXTO_TRANSFERSTATUS ,
	@keyReferenceNo ,
	@keyCreationDatetime ,
	@keyScanDatetime ,
	@keyStatusId ,
	@keyDocType ,
	@keyReferenceVersion ,
	@keyUserId ,
	@keyDeviceSN )
END