﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateGoodsReceiveDetail]
	-- Add the parameters for the stored procedure here
	
	@keyGRId bigint,
	@keyGRDetailId smallInt,
	@keyGRExistingQuantity smallInt,
	@keyGRNewQuantity smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_goodsReceiveDetail
	SET quantity = @keyGRNewQuantity
	WHERE id = @keyGRDetailId
	AND tbl_goodsReceive_Id = @keyGRId
	AND quantity = @keyGRExistingQuantity
END