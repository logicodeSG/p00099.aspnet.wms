﻿CREATE PROCEDURE [dbo].[selectNextPurchaseReturnPickingDetailId]
	-- Add the parameters for the stored procedure here
	@keyPRPId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CASE WHEN MAX(tbl_purchaseReturnPickingDetail.id) IS NULL THEN 1 ELSE MAX(tbl_purchaseReturnPickingDetail.id)+1 END 
	FROM tbl_purchaseReturnPickingDetail WHERE tbl_purchaseReturnPicking_Id=@keyPRPId
END