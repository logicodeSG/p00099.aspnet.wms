﻿CREATE PROCEDURE [dbo].[selectTPDetailByTOIdItemIdQtyGroupId]
	-- Add the parameters for the stored procedure here
	@keyTOId bigInt,
	@keyItemId smallInt,
	@keyTPQuantity Int,
	@keyGroupId smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_transferPickingDetail.id, tbl_transferPickingDetail.tbl_transferPicking_Id, 
	tbl_transferPickingDetail.tbl_item_Id, tbl_transferPickingDetail.tbl_item_code,
	tbl_transferPickingDetail.tbl_item_description, tbl_transferPickingDetail.uom,
	tbl_transferPickingDetail.quantity, tbl_transferPickingDetail.receive_quantity
 	FROM tbl_transferPickingDetail
	WHERE tbl_transferPickingDetail.tbl_transferPicking_Id = 
	(SELECT tbl_transferPicking.id FROM tbl_transferPicking 
	WHERE tbl_transferPicking.tbl_ax_to_Id=@keyTOId)
	AND tbl_transferPickingDetail.tbl_item_Id = @keyItemId
	AND tbl_transferPickingDetail.quantity = @keyTPQuantity
	AND tbl_transferPickingDetail.tbl_group_Id = @keyGroupId
END