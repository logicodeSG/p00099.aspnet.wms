﻿CREATE PROCEDURE [dbo].[selectPRPByPRPId]
	-- Add the parameters for the stored procedure here
	@keyPRPId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_purchaseReturnPicking.id, tbl_purchaseReturnPicking.referenceNo, tbl_ax_pr.referenceNo AS [PRReferenceNo],
	tbl_purchaseReturnPicking.pickingDatetime, tbl_user.username, tbl_purchaseReturnPicking.tbl_status_Id,
	tbl_purchaseReturnPicking.tbl_location_Id, tbl_purchaseReturnPicking.referenceVersion
 	FROM tbl_purchaseReturnPicking
	JOIN tbl_user ON tbl_purchaseReturnPicking.tbl_user_Id = tbl_user.id
	JOIN tbl_ax_pr ON tbl_ax_pr.id = tbl_purchaseReturnPicking.tbl_ax_pr_Id
	WHERE tbl_purchaseReturnPicking.id = @keyPRPId
END