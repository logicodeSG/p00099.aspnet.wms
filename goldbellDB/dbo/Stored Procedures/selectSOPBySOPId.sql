﻿CREATE PROCEDURE [dbo].[selectSOPBySOPId]
	-- Add the parameters for the stored procedure here
	@keySOPId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_salesOrderPicking.id, tbl_salesOrderPicking.referenceNo, tbl_ax_so.referenceNo AS [SOReferenceNo],
	tbl_salesOrderPicking.pickingDatetime, tbl_user.username, tbl_salesOrderPicking.tbl_status_Id,
	tbl_salesOrderPicking.tbl_location_Id, tbl_salesOrderPicking.referenceVersion
 	FROM tbl_salesOrderPicking
	JOIN tbl_user ON tbl_salesOrderPicking.tbl_user_Id = tbl_user.id
	JOIN tbl_ax_so ON tbl_ax_so.id = tbl_salesOrderPicking.tbl_ax_so_Id
	WHERE tbl_salesOrderPicking.id = @keySOPId
END