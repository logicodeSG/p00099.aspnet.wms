﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXPODetail]
	-- Add the parameters for the stored procedure here
	@keyAXPOD_Id bigint,
	@keyAXPO_Id bigint,
	@keyAXPOD_DATAAREAID nvarchar(4),
	@keyAXPOD_PURCHID nvarchar(20),
	@keyAXPOD_LINENUMBER bigint,
	@keyAXPOD_ITEMID nvarchar(50),
	@keyAXPOD_NAME nvarchar(1000),
	@keyAXPOD_DOT_PARTCODE nvarchar(80),
	@keyAXPOD_ORDERACCOUNT nvarchar(20),
	@keyAXPOD_INVOICEACCOUNT nvarchar(20),
	@keyAXPOD_PURCHUNIT nvarchar(10),
	@keyAXPOD_INVENTLOCATIONID nvarchar(20),
	@keyAXPOD_INVENTSITEID nvarchar(10),
	@keyAXPOD_WMSLOCATIONID nvarchar(25),
	@keyAXPOD_QTYORDERED numeric(32,16),
	@keyAXPOD_PURCHRECEIVEDNOW numeric(32,16),
	@keyScanQuantity numeric(32,16),
	@keyScanDatetime datetime,
	@keyRemarks nvarchar(255),
	@keyStatusId bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_poDetail(
	id,
	tbl_ax_po_Id,
	axDATAAREAID,
	axPURCHID,
	axLINENUMBER,
	axITEMID,
	axNAME,
	axDOT_PARTCODE,
	axORDERACCOUNT,
	axINVOICEACCOUNT,
	axPURCHUNIT,
	axINVENTLOCATIONID,
	axINVENTSITEID,
	axWMSLOCATIONID,
	axQTYORDERED,
	axPURCHRECEIVEDNOW,
	scanQuantity,
	scanDatetime,
	remarks,
	tbl_status_Id,
	tbl_user_Id,
	deviceSN
	)
	values(
	@keyAXPOD_Id,
	@keyAXPO_Id,
	@keyAXPOD_DATAAREAID,
	@keyAXPOD_PURCHID,
	@keyAXPOD_LINENUMBER,
	@keyAXPOD_ITEMID,
	@keyAXPOD_NAME,
	@keyAXPOD_DOT_PARTCODE ,
	@keyAXPOD_ORDERACCOUNT ,
	@keyAXPOD_INVOICEACCOUNT ,
	@keyAXPOD_PURCHUNIT ,
	@keyAXPOD_INVENTLOCATIONID ,
	@keyAXPOD_INVENTSITEID ,
	@keyAXPOD_WMSLOCATIONID ,
	@keyAXPOD_QTYORDERED ,
	@keyAXPOD_PURCHRECEIVEDNOW ,
	@keyScanQuantity ,
	@keyScanDatetime ,
	@keyRemarks ,
	@keyStatusId ,
	@keyUserId ,
	@keyDeviceSN)
END