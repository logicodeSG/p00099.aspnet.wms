﻿CREATE PROCEDURE [dbo].[insertNewPurchaseReturnPicking]
	-- Add the parameters for the stored procedure here
	@keyPRPId bigint,
	@keyPRPReferenceNumber varchar(50),
	@keyPRPPRId bigint,
	@keyCreationDateTime varchar(50),
	@keyPickingDateTime varchar(50),
	@keyCreatedBy bigint,
	@keyPRPDeviceSN varchar(50),
	@keyPRPLocationId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_purchaseReturnPicking(id, referenceNo, tbl_ax_pr_Id, creationDatetime,
	pickingDatetime, tbl_user_Id, deviceSN, tbl_status_Id, lastEdit, tbl_location_Id, referenceVersion)
	values(
	@keyPRPId,@keyPRPReferenceNumber,@keyPRPPRId,@keyCreationDateTime,
	@keyPickingDateTime,@keyCreatedBy,@keyPRPDeviceSN,1,@keyCreatedBy, @keyPRPLocationId, 1)
END