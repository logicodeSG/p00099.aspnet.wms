﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateDocNumber]
	-- Add the parameters for the stored procedure here
	@keyDocNumberId smallint,
	@keyDocNumberPrefix varchar(50),
	@keyDocNumberLength smallint,
	@keyDocNumberNextNumber bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_doc_reference_no
	SET 
		prefix = @keyDocNumberPrefix,
		numberLength = @keyDocNumberLength,
		nextNumber = @keyDocNumberNextNumber
	WHERE id = @keyDocNumberId
END