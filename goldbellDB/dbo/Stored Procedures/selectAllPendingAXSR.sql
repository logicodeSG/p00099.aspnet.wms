﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllPendingAXSR]
	-- Add the parameters for the stored procedure here
	@keyLocationId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for srocedure here
	SELECT tbl_ax_sr.id, tbl_ax_sr.referenceNo, tbl_ax_sr.srDatetime,
	tbl_ax_sr.creationDatetime, tbl_ax_sr.tbl_status_Id, tbl_ax_sr.docType,
	tbl_ax_sr.tbl_location_Id, tbl_ax_sr.referenceVersion
 	FROM tbl_ax_sr
	WHERE tbl_status_Id IN (1,4)
	AND tbl_ax_sr.tbl_location_Id = @keyLocationId
END