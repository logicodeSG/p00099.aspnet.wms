﻿CREATE PROCEDURE [dbo].[updateStockTakeDetail]
	-- Add the parameters for the stored procedure here
	
	@keySTId bigint,
	@keySTDetailId smallInt,
	@keySTExistingQuantity smallInt,
	@keySTNewQuantity smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_stockTakeDetail
	SET quantity = @keySTNewQuantity
	WHERE id = @keySTDetailId
	AND tbl_stockTake_Id = @keySTId
	AND quantity = @keySTExistingQuantity
END