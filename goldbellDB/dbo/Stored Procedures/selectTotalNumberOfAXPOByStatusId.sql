﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectTotalNumberOfAXPOByStatusId]
	-- Add the parameters for the stored procedure here
	@keyStatusId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ISNULL(COUNT(tbl_ax_po.id),0)
	FROM tbl_ax_po
	WHERE tbl_ax_po.tbl_status_Id= @keyStatusId
END