﻿CREATE PROCEDURE [dbo].[checkExistenceAXSODetailWRemainingQuantityBySOId]
	-- Add the parameters for the ssored procedure here
	@keyAXSOId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added so prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_ax_soDetail.id, tbl_ax_soDetail.tbl_ax_so_Id, tbl_ax_soDetail.tbl_item_Id,
	tbl_ax_soDetail.tbl_item_code, tbl_ax_soDetail.tbl_item_description,
	tbl_ax_soDetail.uom, tbl_ax_soDetail.quantity, 
	SUM(ISNULL(tbl_salesOrderPickingDetail.quantity,0)) AS [pickedQuantity],
	tbl_ax_soDetail.tbl_group_Id
 	FROM tbl_ax_soDetail
	LEFT JOIN tbl_salesOrderPicking ON tbl_salesOrderPicking.tbl_ax_so_Id = tbl_ax_soDetail.tbl_ax_so_Id
	AND tbl_salesOrderPicking.tbl_status_Id NOT IN (3)
	LEFT JOIN tbl_salesOrderPickingDetail ON tbl_salesOrderPickingDetail.tbl_salesOrderPicking_Id = tbl_salesOrderPicking.id
	AND tbl_salesOrderPickingDetail.tbl_item_Id = tbl_ax_soDetail.tbl_item_Id
	AND tbl_salesOrderPickingDetail.tbl_group_Id = tbl_ax_soDetail.tbl_group_Id
	WHERE tbl_ax_soDetail.tbl_ax_so_Id = @keyAXSOId
	GROUP BY tbl_ax_soDetail.id, tbl_ax_soDetail.tbl_ax_so_Id, tbl_ax_soDetail.tbl_item_Id,
	tbl_ax_soDetail.tbl_item_code, tbl_ax_soDetail.tbl_item_description,
	tbl_ax_soDetail.uom, tbl_ax_soDetail.quantity,tbl_ax_soDetail.tbl_group_Id
	HAVING tbl_ax_soDetail.quantity-SUM(ISNULL(tbl_salesOrderPickingDetail.quantity,0)) > 0
END