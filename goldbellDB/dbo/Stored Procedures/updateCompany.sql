﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateCompany]
	-- Add the parameters for the stored procedure here
	@keyCompanyName varchar(50),
	@keyCompanyAddress1 varchar(50),
	@keyCompanyAddress2 varchar(50),
	@keyCompanyAddress3 varchar(50),
	@keyCompanyPhoneNumber varchar(20),
	@keyCompanyFaxNumber varchar(20),
	@keyCompanyEmail varchar(50),
	@keyCompanyWebsite varchar(50),
	@keyCompanyRegNumber varchar(20),
	@keyCompanyGSTRegNumber varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_company
	SET 
		name = @keyCompanyName,
		address1 = @keyCompanyAddress1,
		address2 = @keyCompanyAddress2,
		address3 = @keyCompanyAddress3,
		phone = @keyCompanyPhoneNumber,
		fax = @keyCompanyFaxNumber,
		email = @keyCompanyEmail,
		website = @keyCompanyWebsite,
		regNo = @keyCompanyRegNumber,
		GSTRegNo = @keyCompanyGSTRegNumber
	WHERE id = 1
END