﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewMovementHistory]
	-- Add the parameters for the stored procedure here
	@axDATAREADID nvarchar(4),
    @axDOCUMENTID nvarchar(20),
    @axSubDOCUMENTID nvarchar(20),
    @axITEMID nvarchar(50),
    @axPART_CODE nvarchar(80),
    @axINVENTLOCATION_FROMID nvarchar(20),
    @axINVENTSITE_FROMID nvarchar(10),
    @axWMSLOCATION_FROMID nvarchar(25),
    @axINVENTLOCATION_TOID nvarchar(20),
    @axINVENTSITE_TOID nvarchar(10),
    @axWMSLOCATION_TOID nvarchar(25),
    @qty varchar(50),
    @movementDatetime varchar(50),
    @tbl_user_Id varchar(50),
    @deviceSN varchar(50),
    @movementType varchar(50),
    @axLINENUM numeric(36,18)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery AS NVARCHAR(MAX)
	DECLARE @SQLCond AS NVARCHAR(500)
	DECLARE @SQLCond2 AS NVARCHAR(500)

    -- Insert statements for procedure here
	SET @SQLCond =
	CASE 
	   WHEN @tbl_user_Id IN ('-1') 
			THEN 'SYSTEM'	
	   ELSE (SELECT username FROM tbl_user WHERE id=@tbl_user_Id)
	END

	SET @SQLCond2 =
	CASE 
	   WHEN @qty IN ('0') 
			THEN NULL	
	   ELSE @qty
	END

	INSERT INTO dbo.tbl_movementHistory(
		 [id]
      ,[axDATAREADID]
      ,[axDOCUMENTID]
      ,[axSubDOCUMENTID]
      ,[axITEMID]
      ,[axPART_CODE]
      ,[axINVENTLOCATION_FROMID]
      ,[axINVENTSITE_FROMID]
      ,[axWMSLOCATION_FROMID]
      ,[axINVENTLOCATION_TOID]
      ,[axINVENTSITE_TOID]
      ,[axWMSLOCATION_TOID]
      ,[qty]
      ,[movementDatetime]
      ,[tbl_user_Id]
      ,[tbl_username]
      ,[deviceSN]
      ,[movementType]
	  ,[axLINENUM])
	values(
	(SELECT CASE WHEN MAX(tbl_movementHistory.id) IS NULL THEN 1 ELSE MAX(tbl_movementHistory.id)+1 END FROM tbl_movementHistory),
	 @axDATAREADID,
	 @axDOCUMENTID,
	@axSubDOCUMENTID,
	@axITEMID,
	@axPART_CODE,
	@axINVENTLOCATION_FROMID,
	@axINVENTSITE_FROMID,
	@axWMSLOCATION_FROMID,
	@axINVENTLOCATION_TOID,
	@axINVENTSITE_TOID,
	@axWMSLOCATION_TOID,
	@SQLCond2,
	@movementDatetime,
	@tbl_user_Id,
	@SQLCond,
	@deviceSN,
	@movementType,
	@axLINENUM)

    -- Insert statements for procedure here
	--SET @SQLQuery = ' 
	--INSERT INTO dbo.tbl_movementHistory(
	--	 [id]
 --     ,[axDATAREADID]
 --     ,[axDOCUMENTID]
 --     ,[axSubDOCUMENTID]
 --     ,[axITEMID]
 --     ,[axPART_CODE]
 --     ,[axINVENTLOCATION_FROMID]
 --     ,[axINVENTSITE_FROMID]
 --     ,[axWMSLOCATION_FROMID]
 --     ,[axINVENTLOCATION_TOID]
 --     ,[axINVENTSITE_TOID]
 --     ,[axWMSLOCATION_TOID]
 --     ,[qty]
 --     ,[movementDatetime]
 --     ,[tbl_user_Id]
 --     ,[tbl_username]
 --     ,[deviceSN]
 --     ,[movementType])
	--values(
	--(SELECT CASE WHEN MAX(tbl_movementHistory.id) IS NULL THEN 1 ELSE MAX(tbl_movementHistory.id)+1 END FROM tbl_movementHistory),' +
	--'' + @axDATAREADID +
	--''','''+@axDOCUMENTID +
	--''','''+@axSubDOCUMENTID +
	--''','''+@axITEMID +
	--''','''+@axPART_CODE+
	--''','''+@axINVENTLOCATION_FROMID+
	--''','''+@axINVENTSITE_FROMID+
	--''','''+@axWMSLOCATION_FROMID+
	--''','''+@axINVENTLOCATION_TOID+
	--''','''+@axINVENTSITE_TOID+
	--''','''+@axWMSLOCATION_TOID+
	--''','+@SQLCond2+
	--','''+@movementDatetime +
	--''','+@tbl_user_Id +
	--','''+@SQLCond+
	--''','''+@deviceSN +
	--''','''+@movementType + ''')'

END