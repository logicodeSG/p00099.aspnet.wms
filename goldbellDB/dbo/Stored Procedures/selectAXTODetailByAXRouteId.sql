﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAXTODetailByAXRouteId]
	-- Add the parameters for the stored procedure here
	@keyAXROUTEID nvarchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT [tbl_ax_toDetail].[id]
          ,[tbl_ax_toDetail].[tbl_ax_to_Id]
          ,[tbl_ax_toDetail].[axDATAAREAID]
          ,[tbl_ax_toDetail].[axROUTEID]
          ,[tbl_ax_toDetail].[axTRANSFERID]
          ,[tbl_ax_toDetail].[axINVENTTRANSID]
          ,[tbl_ax_toDetail].[axITEMID]
          ,[tbl_ax_toDetail].[axDOT_PARTCODE]
          ,[tbl_ax_toDetail].[axNAME]
          ,[tbl_ax_toDetail].[axUNITID]
          ,[tbl_ax_toDetail].[axINVENTLOCATIONID]
          ,[tbl_ax_toDetail].[axINVENTSITEID]
          ,[tbl_ax_toDetail].[axQTY]
          ,[tbl_ax_toDetail].[axDocType]
          ,[tbl_ax_toDetail].[scanQuantity]
          ,[tbl_ax_toDetail].[scanDatetime]
          ,[tbl_ax_toDetail].[remarks]
          ,[tbl_ax_toDetail].[tbl_status_Id]
          ,[tbl_ax_toDetail].[tbl_user_Id]
          ,[tbl_ax_toDetail].[deviceSN]
          ,[tbl_ax_toDetail].[tbl_user_Username]
          ,[tbl_ax_toDetail].[axWMSLOCATIONID]
          ,[tbl_ax_toDetail].[axNAMEALIAS]
      FROM [tbl_ax_toDetail]
	WHERE axROUTEID = @keyAXROUTEID
END