﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXSODetail]
	-- Add the parameters for the stored procedure here
	@keyAXSOD_Id bigint,
	@keyAXSO_Id bigint,
	@keyAXSOD_DATAAREAID nvarchar(4),
	@keyAXSOD_LINENUMBER numeric(32,16),
	@keyAXSOD_ROUTEID nvarchar(10),
	@keyAXSOD_INVENTTRANSID nvarchar(20),
	@keyAXSOD_ITEMID nvarchar(50),
	@keyAXSOD_DOT_PARTCODE nvarchar(80),
	@keyAXSOD_NAME nvarchar(60),
	@keyAXSOD_SALESUNIT nvarchar(10),
	@keyAXSOD_INVENTLOCATIONID nvarchar(20),
	@keyAXSOD_INVENTSITEID nvarchar(10),
	@keyAXSOD_WMSLOCATIONID nvarchar(25),
	@keyAXSOD_QTY numeric(32,16),
	@keyScanQuantity numeric(32,16),
	@keyScanDatetime datetime,
	@keyRemarks nvarchar(255),
	@keyStatusId bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_soDetail(
	id,
	tbl_ax_so_Id,
	axDATAAREAID,
	axLINENUM,
	axROUTEID,
	axINVENTTRANSID,
	axITEMID,
	axDOT_PARTCODE,
	axNAME,
	axSALESUNIT,
	axINVENTLOCATIONID,
	axINVENTSITEID,
	axWMSLOCATIONID,
	axQTY,
	scanQuantity,
	scanDatetime,
	remarks,
	tbl_status_Id,
	tbl_user_Id,
	deviceSN
	)
	values(
	@keyAXSOD_Id,
	@keyAXSO_Id,
	@keyAXSOD_DATAAREAID,
	@keyAXSOD_LINENUMBER,
	@keyAXSOD_ROUTEID,
	@keyAXSOD_INVENTTRANSID,
	@keyAXSOD_ITEMID,
	@keyAXSOD_DOT_PARTCODE ,
	@keyAXSOD_NAME,
	@keyAXSOD_SALESUNIT ,
	@keyAXSOD_INVENTLOCATIONID ,
	@keyAXSOD_INVENTSITEID ,
	@keyAXSOD_WMSLOCATIONID ,
	@keyAXSOD_QTY ,
	@keyScanQuantity ,
	@keyScanDatetime ,
	@keyRemarks ,
	@keyStatusId ,
	@keyUserId ,
	@keyDeviceSN)
END