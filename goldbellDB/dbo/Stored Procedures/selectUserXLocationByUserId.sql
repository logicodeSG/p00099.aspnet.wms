﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectUserXLocationByUserId]
	-- Add the parameters for the stored procedure here
	@keyUserId AS varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @SQLQuery NVARCHAR(MAX)

    -- Insert statements for procedure here

	SET @SQLQuery =
    CASE 
		WHEN @keyUserId = 1 
			THEN 'SELECT 1,tbl_location.id,tbl_location.code,tbl_location.[description]
			FROM tbl_location
			INNER JOIN tbl_locationLevel
			ON tbl_location.tbl_locationLevel_Id = tbl_locationLevel.id
			WHERE tbl_locationLevel.[order] = 2
			AND tbl_location.active = 1'
		
		ELSE 
			'SELECT tbl_user_Id, tbl_location_Id, tbl_location.code, tbl_location.[description]
			FROM x_tbl_userXtbl_location
			JOIN tbl_location ON x_tbl_userXtbl_location.tbl_location_Id = tbl_location.id
			WHERE tbl_user_Id =' + @keyUserId
	END 

	EXECUTE(@SQLQuery)
	
END