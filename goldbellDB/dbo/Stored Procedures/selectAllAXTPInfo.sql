﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllAXTPInfo]
	-- Add the parameters for the stored procedure here
	--@keyUserId AS varchar(50),
	 @keySortHeader AS varchar(100),
	 @keySortContent AS varchar(50),
	 @keySearchType AS varchar(50),
	 @keySearchHeader AS varchar(50),
	 @keySearchContent AS varchar(50),
	 @keySearchContent2 AS varchar(50),
	 @keySearchWarehouseCode AS varchar(50)
	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery NVARCHAR(MAX)
	DECLARE @SQLQuery3 NVARCHAR(MAX)

    -- Insert statements for procedure here
	-- Insert statements for procedure here

	SET @SQLQuery3 =
	CASE 
		WHEN @keySearchType IN ('DATE') 
				THEN 'WHERE '+@keySearchHeader+' BETWEEN '''+ @keySearchContent +''' AND ''' + @keySearchContent2 +''' '
		WHEN @keySearchType IN ('DROPDOWN') 
				THEN CASE 
					WHEN @keySearchContent IN ('COMPLETED')
						THEN 'WHERE '+@keySearchHeader+' = 9 '
					ELSE 'WHERE '+@keySearchHeader+' = 1 '
				END
		ELSE 'WHERE '+@keySearchHeader+' LIKE ''%'+ @keySearchContent +'%'' '
	END 


	SET @SQLQuery = ' 
		SELECT DISTINCT TOP(SELECT CAST(dataValue AS INT) FROM tbl_systemData WHERE name=''DISPLAY ROWS'') 
			tbl_ax_to.[id]
		  ,tbl_ax_to.[axDATAAREAID]
		  ,tbl_ax_to.[axMODIFIEDDATETIME]
		  ,tbl_ax_to.[axPICKINGROUTEID]
		  ,tbl_ax_to.[axTRANSFERID]
		  ,tbl_ax_to.[axDocumentType]
		  ,tbl_ax_to.[axINVENTLOCATIONIDFROM]
		  ,tbl_ax_to.[axINVENTLOCATIONIDTO]
		  ,tbl_ax_to.[axTRANSFERSTATUS]
		  ,tbl_ax_to.[referenceNo]
		  ,tbl_ax_to.[creationDatetime]
		  ,tbl_ax_to.[completionDatetime]
		  ,tbl_ax_to.[scanDatetime]
		  ,tbl_ax_to.[tbl_status_Id]
		  ,tbl_ax_to.[docType]
		  ,tbl_ax_to.[referenceVersion]
		  ,tbl_ax_to.[tbl_user_Id]
		  ,tbl_ax_to.[deviceSN]
		  ,tbl_ax_to.[tbl_user_Username]
			FROM [tbl_ax_to] ' +
		  @SQLQuery3 +
		  ' AND tbl_ax_to.[axDATAAREAID] = ''' + @keySearchWarehouseCode + ''' 
		   ORDER BY '+@keySortHeader+' '+@keySortContent+''

	EXECUTE(@SQLQuery)

END