﻿CREATE PROCEDURE [dbo].[selectNextTransferPickingDetailId]
	-- Add the parameters for the stored procedure here
	@keyTPId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CASE WHEN MAX(tbl_transferPickingDetail.id) IS NULL THEN 1 ELSE MAX(tbl_transferPickingDetail.id)+1 END 
	FROM tbl_transferPickingDetail WHERE tbl_transferPicking_Id=@keyTPId
END