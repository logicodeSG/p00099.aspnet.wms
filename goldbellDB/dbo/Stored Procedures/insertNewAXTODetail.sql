﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXTODetail]
	-- Add the parameters for the stored procedure here
	@keyAXTOD_Id bigint,
	@keyAXTO_Id bigint,
	@keyAXTOD_DATAAREAID nvarchar(4),
	@keyAXTOD_ROUTEID nvarchar(10),
	@keyAXTOD_TRANSFERID nvarchar(20),
	@keyAXTOD_INVENTTRANSID nvarchar(20),
	@keyAXTOD_ITEMID nvarchar(50),
	@keyAXTOD_DOT_PARTCODE nvarchar(80),
	@keyAXTOD_NAME nvarchar(60),
	@keyAXTOD_UNITID nvarchar(10),
	@keyAXTOD_INVENTLOCATIONID nvarchar(20),
	@keyAXTOD_INVENTSITEID nvarchar(10),
	@keyAXTOD_QTY numeric(32,16),
	@keyAXTOD_DOCTYPE varchar(6),
	@keyScanQuantity numeric(32,16),
	@keyScanDatetime datetime,
	@keyRemarks nvarchar(255),
	@keyStatusId bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50),
	@keyAXTOD_WMSLOCATIONID nvarchar(25),
	@keyAXTOD_NAMEALIAS nvarchar(20)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_toDetail(
	[id]
      ,[tbl_ax_to_Id]
      ,[axDATAAREAID]
      ,[axROUTEID]
      ,[axTRANSFERID]
      ,[axINVENTTRANSID]
      ,[axITEMID]
      ,[axDOT_PARTCODE]
      ,[axNAME]
      ,[axUNITID]
      ,[axINVENTLOCATIONID]
      ,[axINVENTSITEID]
      ,[axQTY]
      ,[axDocType]
      ,[scanQuantity]
      ,[scanDatetime]
      ,[remarks]
      ,[tbl_status_Id]
      ,[tbl_user_Id]
      ,[deviceSN]
	  ,[axWMSLOCATIONID]
	  ,[axNAMEALIAS]
	)
	values(
	@keyAXTOD_Id ,
	@keyAXTO_Id ,
	@keyAXTOD_DATAAREAID ,
	@keyAXTOD_ROUTEID ,
	@keyAXTOD_TRANSFERID ,
	@keyAXTOD_INVENTTRANSID ,
	@keyAXTOD_ITEMID ,
	@keyAXTOD_DOT_PARTCODE ,
	@keyAXTOD_NAME ,
	@keyAXTOD_UNITID ,
	@keyAXTOD_INVENTLOCATIONID ,
	@keyAXTOD_INVENTSITEID ,
	@keyAXTOD_QTY ,
	@keyAXTOD_DOCTYPE ,
	@keyScanQuantity ,
	@keyScanDatetime ,
	@keyRemarks ,
	@keyStatusId ,
	@keyUserId ,
	@keyDeviceSN,
	@keyAXTOD_WMSLOCATIONID,
	@keyAXTOD_NAMEALIAS)
END