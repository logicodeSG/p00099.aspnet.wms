﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXPO]
	-- Add the parameters for the stored procedure here
	@keyAXPO_Id bigint,
	@keyAXPO_DATAAREAID nvarchar(4),
	@keyAXPO_PURCHID nvarchar(20),
	@keyAXPO_PURCHNAME nvarchar(60),
	@keyAXPO_ORDERACCOUNT nvarchar(20),
	@keyAXPO_INVOIRCEACCOUNT nvarchar(20),
	@keyAXPO_LastConfirmationDate datetime,
	@keyAXPO_DELIVERYDATE datetime,
	@keyAXPO_CREATEDDATETIME datetime,
	@keyReferenceNo varchar(50),
	@keyCreationDatetime datetime,
	@keyScanDatetime datetime,
	@keyStatusId bigint,
	@keyDocType varchar(50),
	@keyReferenceVersion bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_po(
	id, 
	axDATAAREAID, 
	axPURCHID, 
	axPURCHNAME,
	axORDERACCOUNT,
	axINVOICEACCOUNT,
	axLastConfirmationDate,
	axDELIVERYDATE,
	axCREATEDDATETIME,
	referenceNo,
	creationDatetime,
	scanDatetime,
	tbl_status_Id,
	docType,
	referenceVersion,
	tbl_user_Id,
	deviceSN
	)
	values(
	@keyAXPO_Id,
	@keyAXPO_DATAAREAID,
	@keyAXPO_PURCHID,
	@keyAXPO_PURCHNAME,
	@keyAXPO_ORDERACCOUNT,
	@keyAXPO_INVOIRCEACCOUNT,
	@keyAXPO_LastConfirmationDate,
	@keyAXPO_DELIVERYDATE ,
	@keyAXPO_CREATEDDATETIME ,
	@keyReferenceNo ,
	@keyCreationDatetime ,
	@keyScanDatetime ,
	@keyStatusId ,
	@keyDocType,
	@keyReferenceVersion ,
	@keyUserId,
	@keyDeviceSN)
END