﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllPendingAXPODetail]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_ax_poDetail.[id]
      ,tbl_ax_poDetail.[tbl_ax_po_Id]
      ,tbl_ax_poDetail.[axDATAAREAID]
      ,tbl_ax_poDetail.[axPURCHID]
      ,tbl_ax_poDetail.[axLINENUMBER]
      ,tbl_ax_poDetail.[axITEMID]
      ,tbl_ax_poDetail.[axNAME]
      ,tbl_ax_poDetail.[axDOT_PARTCODE]
      ,tbl_ax_poDetail.[axORDERACCOUNT]
      ,tbl_ax_poDetail.[axINVOICEACCOUNT]
      ,tbl_ax_poDetail.[axPURCHUNIT]
      ,tbl_ax_poDetail.[axINVENTLOCATIONID]
      ,tbl_ax_poDetail.[axINVENTSITEID]
      ,tbl_ax_poDetail.[axWMSLOCATIONID]
      ,tbl_ax_poDetail.[axQTYORDERED]
      ,tbl_ax_poDetail.[axPURCHRECEIVEDNOW]
      ,tbl_ax_poDetail.[scanQuantity]
      ,tbl_ax_poDetail.[scanDatetime]
      ,tbl_ax_poDetail.[remarks]
      ,tbl_ax_poDetail.[tbl_status_Id]
      ,tbl_ax_poDetail.[tbl_user_Id]
      ,tbl_ax_poDetail.[deviceSN]
      ,tbl_ax_poDetail.[tbl_user_Username]
      ,tbl_ax_poDetail.[grnDocNo]
	FROM tbl_ax_poDetail
	JOIN tbl_ax_po ON tbl_ax_po.axPURCHID = tbl_ax_poDetail.axPURCHID
	AND tbl_ax_po.tbl_status_Id = 1
END