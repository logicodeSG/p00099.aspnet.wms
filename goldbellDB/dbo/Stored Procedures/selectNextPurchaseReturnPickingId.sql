﻿CREATE PROCEDURE [dbo].[selectNextPurchaseReturnPickingId]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CASE WHEN MAX(tbl_purchaseReturnPicking.id) IS NULL THEN 1 ELSE MAX(tbl_purchaseReturnPicking.id)+1 END FROM tbl_purchaseReturnPicking
END