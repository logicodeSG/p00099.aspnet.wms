﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAXPRByAXPRId]
	-- Add the parameters for the stored procedure here
	@keyAXPRId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_ax_pr.id, tbl_ax_pr.referenceNo, tbl_ax_pr.prDatetime,
	tbl_ax_pr.creationDatetime, tbl_ax_pr.tbl_status_Id, tbl_ax_pr.docType,
	tbl_ax_pr.tbl_location_Id, tbl_ax_pr.referenceVersion
 	FROM tbl_ax_pr
	WHERE tbl_ax_pr.id = @keyAXPRId
END