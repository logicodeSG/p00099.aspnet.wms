﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXTRDetailScanInputByAXLineNum_TransferId_InventTransId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
	@keyAXLINENUM numeric(32,16),
	@keyAXTRANSFERID nvarchar(20),
	@keyAXINVENTTRANSID nvarchar(20),
	@keyAXITEMID nvarchar(50),
	@keyAXDOT_PARTCODE nvarchar(80),
	@keyScanQuantity numeric(32,16),
	@keyScanDatetime datetime,
	@keyRemarks nvarchar(255),
	@keyUserId bigint,
	@keyDeviceSN varchar(50),
	@keyStatusId bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_trDetail
	SET 
	scanQuantity = @keyScanQuantity,
	scanDatetime = @keyScanDatetime,
	remarks = @keyRemarks,
	tbl_user_Id = @keyUserId,
	deviceSN = @keyDeviceSN,
	tbl_user_Username = (SELECT username FROM tbl_user WHERE tbl_user.id = @keyUserId),
	tbl_status_Id = @keyStatusId
	WHERE axLINENUM = @keyAXLINENUM
	AND axTRANSFERID = @keyAXTRANSFERID
	AND axINVENTTRANSID = @keyAXINVENTTRANSID
	AND axITEMID = @keyAXITEMID
	AND axDOT_PARTCODE = @keyAXDOT_PARTCODE
END