﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateTableSingleField]
	-- Add the parameters for the stored procedure here
	@keyTableName varchar(50),
	@keyFieldName varchar(50),
	@keyFieldValue varchar(50),
	@keyIDName varchar(50),
	@keyIDValue varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery AS NVARCHAR(2000)

    -- Insert statements for procedure here
	-- Insert statements for procedure here
	
	SET @SQLQuery = ' 
		UPDATE ' + @keyTableName +
		' SET ' +  @keyFieldName + ' = ' + @keyFieldValue +
		' WHERE ' +  @keyIDName + ' = ' + @keyIDValue
		
	EXECUTE(@SQLQuery)

END