﻿CREATE PROCEDURE [dbo].[selectSRRDetailBySRIdItemIdQtyGroupId]
	-- Add the parameters for the stored procedure here
	@keySRId bigInt,
	@keyItemId smallInt,
	@keySRRQuantity Int,
	@keyGroupId smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_salesReturnReceiveDetail.id, tbl_salesReturnReceiveDetail.tbl_salesReturnReceive_Id, 
	tbl_salesReturnReceiveDetail.tbl_item_Id, tbl_salesReturnReceiveDetail.tbl_item_code,
	tbl_salesReturnReceiveDetail.tbl_item_description, tbl_salesReturnReceiveDetail.uom,
	tbl_salesReturnReceiveDetail.quantity
 	FROM tbl_salesReturnReceiveDetail
	WHERE tbl_salesReturnReceiveDetail.tbl_salesReturnReceive_Id = 
	(SELECT tbl_salesReturnReceive.id FROM tbl_salesReturnReceive 
	WHERE tbl_salesReturnReceive.tbl_ax_sr_Id=@keySRId)
	AND tbl_salesReturnReceiveDetail.tbl_item_Id = @keyItemId
	AND tbl_salesReturnReceiveDetail.quantity = @keySRRQuantity
	AND tbl_salesReturnReceiveDetail.tbl_group_Id = @keyGroupId
END