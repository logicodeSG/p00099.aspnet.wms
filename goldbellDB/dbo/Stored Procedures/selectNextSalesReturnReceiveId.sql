﻿CREATE PROCEDURE [dbo].[selectNextSalesReturnReceiveId]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CASE WHEN MAX(tbl_salesReturnReceive.id) IS NULL THEN 1 ELSE MAX(tbl_salesReturnReceive.id)+1 END FROM tbl_salesReturnReceive
END