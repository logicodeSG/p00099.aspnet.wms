﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllAXInventory]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [id]
      ,[DATAAREAID]
      ,[ITEMID]
      ,[DOT_PARTCODE]
      ,[NAME]
      ,[DESCRIPTION]
      ,[ITEMTYPE]
      ,[InventUnitID]
      ,[PurhUnitID]
      ,[SalesUnitID]
      ,[isSerialized]
      ,[active]
      ,[StartDate]
      ,[PRODUCT]
      ,[CREATEDDATETIME]
      ,[MODIFIEDDATETIME]
      ,[ITEMGROUPID]
 	FROM ax_inventory

	
END