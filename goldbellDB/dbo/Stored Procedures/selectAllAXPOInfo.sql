﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllAXPOInfo]
	-- Add the parameters for the stored procedure here
	--@keyUserId AS varchar(50),
	 @keySortHeader AS varchar(100),
	 @keySortContent AS varchar(50),
	 @keySearchType AS varchar(50),
	 @keySearchHeader AS varchar(50),
	 @keySearchContent AS varchar(50),
	 @keySearchContent2 AS varchar(50),
	 @keySearchLocationId AS varchar(50)
	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery1 AS NVARCHAR(MAX)
	DECLARE @SQLQuery2 AS NVARCHAR(MAX)
	DECLARE @SQLQuery3 AS NVARCHAR(MAX)
	DECLARE @SQLQuery4 AS NVARCHAR(MAX)
	DECLARE @SQLQuery NVARCHAR(MAX)

    -- Insert statements for procedure here
	-- Insert statements for procedure here

	SET @SQLQuery3 =
	CASE 
		WHEN @keySearchType IN ('DATE') 
			THEN 'WHERE '+@keySearchHeader+' BETWEEN '''+ @keySearchContent +''' AND ''' + @keySearchContent2 +''' '
		WHEN @keySearchType IN ('DROPDOWN') 
			THEN CASE 
				WHEN @keySearchContent IN ('CANCELED')
					THEN 'WHERE '+@keySearchHeader+' = 3 '
				WHEN @keySearchContent IN ('COMPLETED')
					THEN 'WHERE '+@keySearchHeader+' = 4 '
				WHEN @keySearchContent IN ('SYNCED')
					THEN 'WHERE '+@keySearchHeader+' = 5 '
				ELSE 'WHERE '+@keySearchHeader+' = 1 '
			END
		ELSE 'WHERE '+@keySearchHeader+' LIKE ''%'+ @keySearchContent +'%'' '
	END 

	SET @SQLQuery3 = @SQLQuery3 +
					'AND tbl_ax_po.tbl_location_Id ='+@keySearchLocationId+'  '

	SET @SQLQuery4 =
	CASE 
		WHEN @keySearchType IN ('DROPDOWN') 
			THEN CASE 
				WHEN @keySearchContent IN ('NEW')
					THEN 'HAVING SUM(ISNULL(gr.quantity,0)) = 0 '
				--WHEN @keySearchContent IN ('COMPLETED')
				--	THEN 'HAVING SUM(tbl_ax_poDetail.quantity) - SUM(ISNULL(gr.quantity,0)) = 0 '
				WHEN @keySearchContent IN ('PARTIAL')
					THEN 'HAVING SUM(tbl_ax_poDetail.quantity) - SUM(ISNULL(gr.quantity,0)) > 0 
					AND SUM(ISNULL(gr.quantity,0)) > 0 '
				ELSE ''
			END
		ELSE ''
	END 

	SET @SQLQuery1 = ' 
		SELECT DISTINCT TOP(SELECT CAST(dataValue AS INT) FROM tbl_systemData WHERE name=''DISPLAY ROWS'')
		tbl_ax_po.*,
			CONCAT(tbl_ax_po.tbl_status_Id,''|'',
				SUM(tbl_ax_poDetail.quantity),''|'',
				SUM(ISNULL(gr.quantity,0))) AS ''customizeStatus''
		FROM tbl_ax_po
		JOIN tbl_ax_poDetail ON tbl_ax_po.id = tbl_ax_poDetail.tbl_ax_po_Id
        LEFT JOIN (SELECT tbl_goodsReceive.tbl_ax_po_Id,tbl_goodsReceiveDetail.tbl_item_Id, 
			SUM(tbl_goodsReceiveDetail.quantity) AS ''quantity'' FROM tbl_goodsReceive 
			JOIN tbl_goodsReceiveDetail ON tbl_goodsReceive.id = tbl_goodsReceiveDetail.tbl_goodsReceive_Id 
			WHERE tbl_goodsReceive.tbl_status_Id NOT IN (3)
            GROUP BY tbl_goodsReceive.tbl_ax_po_Id, tbl_goodsReceiveDetail.tbl_item_Id)gr 
		ON gr.tbl_item_Id = tbl_ax_poDetail.tbl_item_Id 
		AND gr.tbl_ax_po_Id = tbl_ax_po.Id '
        
	
	SET @SQLQuery2 = @SQLQuery3 +
		'GROUP BY tbl_ax_po.id, tbl_ax_po.referenceNo, tbl_ax_po.creationDatetime, 
		tbl_ax_po.poDatetime, tbl_ax_po.tbl_status_Id, tbl_ax_po.docType,
		tbl_ax_po.tbl_location_Id, tbl_ax_po.referenceVersion ' + @SQLQuery4 +
		'ORDER BY '+@keySortHeader+' '+@keySortContent+''
		
	SET @SQLQuery = CAST(@SQLQuery1 AS NVARCHAR(MAX)) + CAST(@SQLQuery2 AS NVARCHAR(MAX))

	EXECUTE(@SQLQuery)

END