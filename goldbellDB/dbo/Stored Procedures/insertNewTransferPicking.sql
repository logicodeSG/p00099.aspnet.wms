﻿CREATE PROCEDURE [dbo].[insertNewTransferPicking]
	-- Add the parameters for the stored procedure here
	@keyTPId bigint,
	@keyTPReferenceNumber varchar(50),
	@keyTOId bigint,
	@keyCreationDateTime varchar(50),
	@keyPickingDateTime varchar(50),
	@keyCreatedBy bigint,
	@keyTPDeviceSN varchar(50),
	@keyTPLocationId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_transferPicking(id, referenceNo, tbl_ax_to_Id, creationDatetime,
	pickingDatetime, tbl_user_Id, deviceSN, tbl_status_Id, lastEdit, tbl_location_Id, referenceVersion)
	values(
	@keyTPId,@keyTPReferenceNumber,@keyTOId,@keyCreationDateTime,
	@keyPickingDateTime,@keyCreatedBy,@keyTPDeviceSN,1,@keyCreatedBy, @keyTPLocationId, 1)
END