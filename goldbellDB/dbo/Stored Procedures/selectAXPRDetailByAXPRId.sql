﻿CREATE PROCEDURE [dbo].[selectAXPRDetailByAXPRId]
	-- Add the parameters for the stored procedure here
	@keyAXPRId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_ax_prDetail.id, tbl_ax_prDetail.tbl_ax_pr_Id, tbl_ax_prDetail.tbl_item_Id,
	tbl_ax_prDetail.tbl_item_code, tbl_ax_prDetail.tbl_item_description,
	tbl_ax_prDetail.uom, tbl_ax_prDetail.quantity, 
	SUM(ISNULL(tbl_purchaseReturnPickingDetail.quantity,0)) AS [pickedQuantity],
	tbl_ax_prDetail.tbl_group_Id, tbl_ax_prDetail.tbl_bin_Id
 	FROM tbl_ax_prDetail
	LEFT JOIN tbl_purchaseReturnPicking ON tbl_purchaseReturnPicking.tbl_ax_pr_Id = tbl_ax_prDetail.tbl_ax_pr_Id
	AND tbl_purchaseReturnPicking.tbl_status_Id NOT IN (3)
	LEFT JOIN tbl_purchaseReturnPickingDetail ON tbl_purchaseReturnPickingDetail.tbl_purchaseReturnPicking_Id = tbl_purchaseReturnPicking.id
	AND tbl_purchaseReturnPickingDetail.tbl_item_Id = tbl_ax_prDetail.tbl_item_Id
	WHERE tbl_ax_prDetail.tbl_ax_pr_Id = @keyAXPRId
	GROUP BY tbl_ax_prDetail.id, tbl_ax_prDetail.tbl_ax_pr_Id, tbl_ax_prDetail.tbl_item_Id,
	tbl_ax_prDetail.tbl_item_code, tbl_ax_prDetail.tbl_item_description,
	tbl_ax_prDetail.uom, tbl_ax_prDetail.quantity,tbl_ax_prDetail.tbl_group_Id, 
	tbl_ax_prDetail.tbl_bin_Id
END