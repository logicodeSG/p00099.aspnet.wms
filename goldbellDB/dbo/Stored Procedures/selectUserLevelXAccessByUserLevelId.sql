﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectUserLevelXAccessByUserLevelId]
	-- Add the parameters for the stored procedure here
	@keyUserLevelId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_userLevel_Id, tbl_access_Id, canRead, canAdd, canEdit, canDelete, canPrint
	FROM x_tbl_userLevelXtbl_access
	WHERE tbl_userLevel_Id = @keyUserLevelId
END