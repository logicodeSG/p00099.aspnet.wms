﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[deleteAllPendingAXTRDetailByAXTransferId]
	-- Add the parameters for the stored procedure here
	@keyAXTRANSFERID nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM tbl_ax_trDetail
	WHERE tbl_ax_trDetail.tbl_status_Id = 1
	AND tbl_ax_trDetail.axTRANSFERID = @keyAXTRANSFERID
END