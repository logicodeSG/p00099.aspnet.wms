﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXCJDetail]
	-- Add the parameters for the stored procedure here
	@keyAXCJD_Id bigint,
	@keyAXCJ_Id bigint,
	@keyAXCJD_JOURNALID nvarchar(20),
	@keyAXCJD_LINENUMBER numeric(32,16),
	@keyAXCJD_ITEMID nvarchar(50),
	@keyAXCJD_DOT_PARTCODE nvarchar(80),
	@keyAXCJD_QTY numeric(32,16),
	@keyAXCJD_INVENTUNITID nvarchar(10),
	@keyAXCJD_INVENTLOCATIONID nvarchar(20),
	@keyAXCJD_INVENTSITEID nvarchar(10),
	@keyAXCJD_WMSLOCATIONID nvarchar(25),
	@keyScanQuantity numeric(32,16),
	@keyScanDatetime datetime,
	@keyRemarks nvarchar(255),
	@keyStatusId bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50),
	@keyAXCJD_INVENTTRANSID nvarchar(20),
	@keyAXCJD_PRODUCT_NAME nvarchar(60)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_cjDetail(
	[id]
      ,[tbl_ax_cj_Id]
      ,[axJOURNALID]
      ,[axLINENUM]
      ,[axITEMID]
      ,[axDOT_PARTCODE]
      ,[axQTY]
      ,[axINVENTUNITID]
      ,[axINVENTLOCATIONID]
      ,[axINVENTSITEID]
      ,[axWMSLOCATIONID]
      ,[scanQuantity]
      ,[scanDatetime]
      ,[remarks]
      ,[tbl_status_Id]
      ,[tbl_user_Id]
      ,[deviceSN]
	  ,[axINVENTTRANSID]
	  ,[axPRODUCT_NAME]
	)
	values(
	@keyAXCJD_Id ,
	@keyAXCJ_Id ,
	@keyAXCJD_JOURNALID ,
	@keyAXCJD_LINENUMBER ,
	@keyAXCJD_ITEMID ,
	@keyAXCJD_DOT_PARTCODE ,
	@keyAXCJD_QTY ,
	@keyAXCJD_INVENTUNITID ,
	@keyAXCJD_INVENTLOCATIONID ,
	@keyAXCJD_INVENTSITEID ,
	@keyAXCJD_WMSLOCATIONID ,
	@keyScanQuantity ,
	@keyScanDatetime ,
	@keyRemarks ,
	@keyStatusId ,
	@keyUserId ,
	@keyDeviceSN,
	@keyAXCJD_INVENTTRANSID,
	@keyAXCJD_PRODUCT_NAME )
END