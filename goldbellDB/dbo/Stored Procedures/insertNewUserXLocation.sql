﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewUserXLocation]
	-- Add the parameters for the stored procedure here
	@keyUserId Bigint,
	@keyLocationId Bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO x_tbl_userXtbl_location(tbl_user_Id, tbl_location_Id)
	VALUES(@keyUserId, @keyLocationId)
END