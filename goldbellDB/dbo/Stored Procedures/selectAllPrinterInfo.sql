﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllPrinterInfo]
	-- Add the parameters for the stored procedure here
	 @keySortHeader AS varchar(50),
	 @keySortContent AS varchar(50),
	 @keyShowAll AS varchar(50),
	 @keySearchHeader AS varchar(50),
	 @keySearchContent AS varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @SQLQuery AS NVARCHAR(1000)
	DECLARE @SQLCond AS NVARCHAR(50)

    -- Insert statements for procedure here
	IF @keyShowAll = 'false'
	SET @SQLCond = 'AND tbl_printer.active = 1 '
	ELSE
	SET @SQLCond = ''

	SET @SQLQuery = ' 
		SELECT tbl_printer.*
		FROM tbl_printer
		WHERE '+@keySearchHeader+' LIKE ''%'+ @keySearchContent +'%'' '
		+ @SQLCond +
		'ORDER BY '+@keySortHeader+' '+@keySortContent+'
		'
	EXECUTE(@SQLQuery)
END