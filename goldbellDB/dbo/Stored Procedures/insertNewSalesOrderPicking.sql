﻿CREATE PROCEDURE [dbo].[insertNewSalesOrderPicking]
	-- Add the parameters for the stored procedure here
	@keySOPId bigint,
	@keySOPReferenceNumber varchar(50),
	@keySOPSOId bigint,
	@keyCreationDateTime varchar(50),
	@keyPickingDateTime varchar(50),
	@keyCreatedBy bigint,
	@keySOPDeviceSN varchar(50),
	@keySOPLocationId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_salesOrderPicking(id, referenceNo, tbl_ax_so_Id, creationDatetime,
	pickingDatetime, tbl_user_Id, deviceSN, tbl_status_Id, lastEdit, tbl_location_Id, referenceVersion)
	values(
	@keySOPId,@keySOPReferenceNumber,@keySOPSOId,@keyCreationDateTime,
	@keyPickingDateTime,@keyCreatedBy,@keySOPDeviceSN,1,@keyCreatedBy, @keySOPLocationId, 1)
END