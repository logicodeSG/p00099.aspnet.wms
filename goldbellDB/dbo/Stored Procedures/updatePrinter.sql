﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updatePrinter]
	-- Add the parameters for the stored procedure here
	@keyPrinterId bigint,
	@keyPrinterName varchar(50),
	@keyPrinterActive bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_printer
	SET 
		name = @keyPrinterName,
		active = @keyPrinterActive
	WHERE id = @keyPrinterId
END