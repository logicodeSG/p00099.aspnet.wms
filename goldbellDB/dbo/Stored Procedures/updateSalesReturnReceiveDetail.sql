﻿CREATE PROCEDURE [dbo].[updateSalesReturnReceiveDetail]
	-- Add the parameters for the stored procedure here
	
	@keySRRId bigint,
	@keySRRDetailId smallInt,
	@keySRRExistingQuantity smallInt,
	@keySRRNewQuantity smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_salesReturnReceiveDetail
	SET quantity = @keySRRNewQuantity
	WHERE id = @keySRRDetailId
	AND tbl_salesReturnReceive_Id = @keySRRId
	AND quantity = @keySRRExistingQuantity
END