﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateUserPassword]
	-- Add the parameters for the stored procedure here
	@keyUserId bigint,
	@keyUserOldPass nvarchar(64),
	@keyUserNewPass nvarchar(64)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_user
	SET [password] = @keyUserNewPass
	WHERE Id = @keyUserId
	AND [password] = @keyUserOldPass
END