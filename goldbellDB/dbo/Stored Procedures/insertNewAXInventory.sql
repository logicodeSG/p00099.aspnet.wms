﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXInventory]
	-- Add the parameters for the stored procedure here
	@keyId bigint,
	@keyDATAAREAID nvarchar(4),
	@keyITEMID nvarchar(50),
	@keyDOT_PARCODE nvarchar(80),
	@keyNAME nvarchar(60),
	@keyDESCRIPTION nvarchar(1000),
	@keyITEMTYPE bigint,
	@keyInventUnitID nvarchar(10),
	@keyPurhUnitID  nvarchar(10),
	@keySalesUnitID  nvarchar(10),
	@keyIsSerialized int,
	@keyActive int,
	@keyStartDate datetime,
	@keyPRODUCT bigint,
	@keyCREATEDDATETIME datetime,
	@keyMODIFIEDDATETIME datetime,
	@keyITEMGROUPID  nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.ax_inventory(
	[id]
      ,[DATAAREAID]
      ,[ITEMID]
      ,[DOT_PARTCODE]
      ,[NAME]
      ,[DESCRIPTION]
      ,[ITEMTYPE]
      ,[InventUnitID]
      ,[PurhUnitID]
      ,[SalesUnitID]
      ,[isSerialized]
      ,[active]
      ,[StartDate]
      ,[PRODUCT]
      ,[CREATEDDATETIME]
      ,[MODIFIEDDATETIME]
      ,[ITEMGROUPID]
	)
	values(
	@keyId ,
	@keyDATAAREAID ,
	@keyITEMID ,
	@keyDOT_PARCODE ,
	@keyNAME ,
	@keyDESCRIPTION ,
	@keyITEMTYPE ,
	@keyInventUnitID ,
	@keyPurhUnitID  ,
	@keySalesUnitID  ,
	@keyIsSerialized ,
	@keyActive ,
	@keyStartDate ,
	@keyPRODUCT ,
	@keyCREATEDDATETIME ,
	@keyMODIFIEDDATETIME ,
	@keyITEMGROUPID  )
END