﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[deleteAXTRByAXTransferId]
	-- Add the parameters for the stored procedure here
	@keyAXTRANSFERID nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE tbl_ax_tr FROM tbl_ax_tr
	WHERE axTRANSFERID = @keyAXTRANSFERID
END