﻿CREATE PROCEDURE [dbo].[selectTPByTPId]
	-- Add the parameters for the stored procedure here
	@keyTPId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_transferPicking.id, tbl_transferPicking.referenceNo, tbl_ax_to.referenceNo AS [TOReferenceNo],
	tbl_transferPicking.pickingDatetime, tbl_user.username, tbl_transferPicking.tbl_status_Id,
	tbl_transferPicking.tbl_location_Id, tbl_transferPicking.referenceVersion
 	FROM tbl_transferPicking
	JOIN tbl_user ON tbl_transferPicking.tbl_user_Id = tbl_user.id
	JOIN tbl_ax_to ON tbl_ax_to.id = tbl_transferPicking.tbl_ax_to_Id
	WHERE tbl_transferPicking.id = @keyTPId
END