﻿CREATE PROCEDURE [dbo].[selectAllMovementHistoryInfo]
	-- Add the parameters for the stored procedure here
	 @keySortHeader AS varchar(100),
	 @keySortContent AS varchar(50),
	 @keySearchTypeList AS nvarchar(1000),
	 @keySearchHeaderList AS nvarchar(1000),
	 @keySearchContentList AS nvarchar(1000),
	 @keySearchContent2List AS nvarchar(1000),
     @keySearchWarehouseCode AS varchar(50),
     @keySearchShowEntries AS varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @SQLQuery AS NVARCHAR(MAX)
	DECLARE @SQLQuery2 NVARCHAR(50)
	DECLARE @SQLQuery3 NVARCHAR(MAX)

    -- Declare your array table variable  
    DECLARE @SearchTypeArray table (TEMPCOL nvarchar(50), ARRAYINDEX int identity(1,1) ) 
    DECLARE @SearchHeaderArray table (TEMPCOL nvarchar(50), ARRAYINDEX int identity(1,1) )  
    DECLARE @SearchContentArray table (TEMPCOL nvarchar(100), ARRAYINDEX int identity(1,1) )  
    DECLARE @SearchContentArray2 table (TEMPCOL nvarchar(50), ARRAYINDEX int identity(1,1) )  
	
    INSERT INTO @SearchTypeArray (TEMPCOL)  
    SELECT TEMPCOL = Item FROM dbo.SplitNvachars(@keySearchTypeList, ',')

    INSERT INTO @SearchHeaderArray (TEMPCOL)  
    SELECT TEMPCOL = Item FROM dbo.SplitNvachars(@keySearchHeaderList, ',')
    
    INSERT INTO @SearchContentArray (TEMPCOL)  
    SELECT TEMPCOL = Item FROM dbo.SplitNvachars(@keySearchContentList, ',')
    
    INSERT INTO @SearchContentArray2 (TEMPCOL)  
    SELECT TEMPCOL = Item FROM dbo.SplitNvachars(@keySearchContent2List, ',')

	DECLARE @INDEXVAR int  
	DECLARE @TOTALCOUNT int  
	DECLARE @CURINDEXED_SEARCH_TYPE nvarchar (50)  
	DECLARE @CURINDEXED_SEARCH_HEADER nvarchar (50)  
	DECLARE @CURINDEXED_SEARCH_CONTENT1 nvarchar (100)  
	DECLARE @CURINDEXED_SEARCH_CONTENT2 nvarchar (50)  
	SET @INDEXVAR = 0
    SET @SQLQuery2 = ''
	SET @SQLQuery3 = ''  

    IF @keySearchShowEntries <> ''   
    BEGIN
        SET @SQLQuery2 = 'TOP ' + @keySearchShowEntries + ' '
    END

	SELECT @TOTALCOUNT= COUNT(*) FROM @SearchHeaderArray  
	WHILE @INDEXVAR < @TOTALCOUNT  
    BEGIN  
        SELECT @INDEXVAR = @INDEXVAR + 1  
        -- Get value of current indexed product ID from array table  
        SELECT @CURINDEXED_SEARCH_TYPE = TEMPCOL from @SearchTypeArray where ARRAYINDEX = @INDEXVAR  
        SELECT @CURINDEXED_SEARCH_HEADER = TEMPCOL from @SearchHeaderArray where ARRAYINDEX = @INDEXVAR 
        SELECT @CURINDEXED_SEARCH_CONTENT1 = TEMPCOL from @SearchContentArray where ARRAYINDEX = @INDEXVAR 
        SELECT @CURINDEXED_SEARCH_CONTENT2 = TEMPCOL from @SearchContentArray2 where ARRAYINDEX = @INDEXVAR 
  
        BEGIN  
            SET @SQLQuery3 +=
            CASE 
	           WHEN @INDEXVAR = 1
			        THEN 'WHERE '
	           ELSE 'AND '
	        END
            
            SET @SQLQuery3 +=
	        CASE 
	           WHEN @CURINDEXED_SEARCH_TYPE IN ('DATE') 
			        THEN @CURINDEXED_SEARCH_HEADER+' BETWEEN '''+ @CURINDEXED_SEARCH_CONTENT1 +''' AND ''' + @CURINDEXED_SEARCH_CONTENT2 +''' '
	           ELSE @CURINDEXED_SEARCH_HEADER+' LIKE ''%'+ @CURINDEXED_SEARCH_CONTENT1 +'%'' '
	        END
        END  
    END  

    SET @SQLQuery3 += 'AND [axDATAREADID] LIKE ''%'+ @keySearchWarehouseCode +'%'' '

	SET @SQLQuery = ' 
		SELECT ' + @SQLQuery2 +
        '[id]
      ,[axDATAREADID]
      ,[axDOCUMENTID]
      ,[axSubDOCUMENTID]
      ,[axITEMID]
      ,[axPART_CODE]
      ,[axINVENTLOCATION_FROMID]
      ,[axINVENTSITE_FROMID]
      ,[axWMSLOCATION_FROMID]
      ,[axINVENTLOCATION_TOID]
      ,[axINVENTSITE_TOID]
      ,[axWMSLOCATION_TOID]
      ,[qty]
      ,[movementDatetime]
      ,[tbl_user_Id]
      ,[tbl_username]
      ,[deviceSN]
      ,[movementType]
      ,[axLINENUM]
		FROM [tbl_movementHistory]
		'+ @SQLQuery3 + '
		ORDER BY '+@keySortHeader+' '+@keySortContent+''

	EXECUTE(@SQLQuery)
END