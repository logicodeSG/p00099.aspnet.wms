﻿CREATE PROCEDURE [dbo].[updateUserSelectedLocationByUserId]
	-- Add the parameters for the stored procedure here
	@keySelectedLocationId bigint,
	@keyUserId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_user
	SET lastSelected_location_Id = @keySelectedLocationId
	WHERE id = @keyUserId
END