﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewPrinter]
	-- Add the parameters for the stored procedure here
	@keyPrinterName varchar(50),
	@keyPrinterActive bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tbl_printer(id,name,active,isDefault)
	VALUES((SELECT CASE WHEN MAX(tbl_printer.id) IS NULL THEN 1 ELSE MAX(tbl_printer.id)+1 END FROM tbl_printer),
	@keyPrinterName,@keyPrinterActive,0)
END