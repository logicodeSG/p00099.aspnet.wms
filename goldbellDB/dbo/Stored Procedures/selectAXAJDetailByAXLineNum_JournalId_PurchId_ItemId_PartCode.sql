﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAXAJDetailByAXLineNum_JournalId_PurchId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
    @keyAXLINENUM  numeric(32,16),
	@keyAXJOURNALID nvarchar(20),
	@keyAXPURCHID nvarchar(20),
	@keyAXITEMID nvarchar(50),
	@keyAXGOD_PARTCODE nvarchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT
        [id]
      ,[tbl_ax_aj_Id]
      ,[axDATAAREAID]
      ,[axJOURNALID]
      ,[axVENDACCOUNT]
      ,[axPurchID]
      ,[axLINENUM]
      ,[axITEMID]
      ,[axGOD_PARTCODE]
      ,[axQTY]
      ,[axTRANSDATE]
      ,[axINVENTTRANSID]
      ,[axINVENTDIMID]
      ,[axINVENTLOCATIONID]
      ,[axINVENTSITEID]
      ,[axWMSLOCATIONID]
      ,[axCREATEDDATETIME]
      ,[axCREATEDBY]
      ,[scanQuantity]
      ,[scanDatetime]
      ,[remarks]
      ,[tbl_status_Id]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
	FROM tbl_ax_ajDetail
	WHERE axJOURNALID = @keyAXJOURNALID
    AND axPURCHID = @keyAXPURCHID
    AND axITEMID= @keyAXITEMID
    AND axGOD_PARTCODE = @keyAXGOD_PARTCODE
    AND axLINENUM = @keyAXLINENUM
END