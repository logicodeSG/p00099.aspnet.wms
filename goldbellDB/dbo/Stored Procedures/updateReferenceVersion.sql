﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateReferenceVersion]
	-- Add the parameters for the stored procedure here
	@keyType varchar(50),
	@keyId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @SQLQuery NVARCHAR(MAX)
	DECLARE @SQLTableName NVARCHAR(MAX)

	SET @SQLTableName =
	CASE 
		WHEN @keyType IN ('PO') 
				THEN 'tbl_ax_po'
	END 

    -- Insert statements for procedure here
	SET @SQLQuery = ' UPDATE ' + @SQLTableName +
	' SET ' + @SQLTableName +'.referenceVersion = (SELECT CASE WHEN MAX(t2.referenceVersion) IS NULL 
							THEN 1 ELSE MAX(t2.referenceVersion)+1 END 
							FROM ' + @SQLTableName +' t2
							WHERE t2.id = ' + @keyId + ')
	WHERE ' + @SQLTableName +'.id = ' + @keyId

	EXECUTE(@SQLQuery)
END