﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAXInventoryByIdRange]
	-- Add the parameters for the stored procedure here
	@keyIdFrom bigInt,
	@keyIdTo bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [id]
      ,[DATAAREAID]
      ,[ITEMID]
      ,[DOT_PARTCODE]
 	FROM ax_inventory
	WHERE id BETWEEN @keyIdFrom AND @keyIdTo

END