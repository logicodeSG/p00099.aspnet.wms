﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[deleteAllPendingAXPODetailByAXPurchId]
	-- Add the parameters for the stored procedure here
	@keyAXPURCHID nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM tbl_ax_poDetail
	WHERE tbl_ax_poDetail.tbl_status_Id = 1
	AND tbl_ax_poDetail.axPURCHID = @keyAXPURCHID
END