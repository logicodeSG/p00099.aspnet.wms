﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllPendingAXAJDetail]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_ax_ajDetail.[id]
      ,tbl_ax_ajDetail.[tbl_ax_aj_Id]
      ,tbl_ax_ajDetail.[axDATAAREAID]
      ,tbl_ax_ajDetail.[axJOURNALID]
      ,tbl_ax_ajDetail.[axVENDACCOUNT]
      ,tbl_ax_ajDetail.[axPurchID]
      ,tbl_ax_ajDetail.[axLINENUM]
      ,tbl_ax_ajDetail.[axITEMID]
      ,tbl_ax_ajDetail.[axGOD_PARTCODE]
      ,tbl_ax_ajDetail.[axQTY]
      ,tbl_ax_ajDetail.[axTRANSDATE]
      ,tbl_ax_ajDetail.[axINVENTTRANSID]
      ,tbl_ax_ajDetail.[axINVENTDIMID]
      ,tbl_ax_ajDetail.[axINVENTLOCATIONID]
      ,tbl_ax_ajDetail.[axINVENTSITEID]
      ,tbl_ax_ajDetail.[axWMSLOCATIONID]
      ,tbl_ax_ajDetail.[axCREATEDDATETIME]
      ,tbl_ax_ajDetail.[axCREATEDBY]
      ,tbl_ax_ajDetail.[scanQuantity]
      ,tbl_ax_ajDetail.[scanDatetime]
      ,tbl_ax_ajDetail.[remarks]
      ,tbl_ax_ajDetail.[tbl_status_Id]
      ,tbl_ax_ajDetail.[tbl_user_Id]
      ,tbl_ax_ajDetail.[deviceSN]
      ,tbl_ax_ajDetail.[tbl_user_Username]
      ,tbl_ax_ajDetail.[axBOX_NO]
      ,tbl_ax_ajDetail.[axCASE]
      ,tbl_ax_ajDetail.[axPRODUCT_NAME]
      ,tbl_ax_ajDetail.[axSO_NUMBER]
      ,tbl_ax_ajDetail.[axCUSTOMER_NAME]
	FROM tbl_ax_ajDetail
	JOIN tbl_ax_aj ON tbl_ax_aj.axJOURNALID = tbl_ax_ajDetail.axJOURNALID
	AND tbl_ax_aj.tbl_status_Id = 1
END