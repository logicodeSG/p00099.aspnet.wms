﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXPODetailScanInputByAXLineNum_PurchId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
	@keyAXLINENUM bigint,
	@keyAXPURCHID nvarchar(20),
	@keyAXITEMID nvarchar(50),
	@keyAXDOT_PARTCODE nvarchar(80),
	@keyScanQuantity numeric(32,16),
	@keyScanDatetime datetime,
	@keyRemarks nvarchar(255),
	@keyUserId bigint,
	@keyGRNDocNo nvarchar(50),
	@keyDeviceSN varchar(50),
	@keyStatusId bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_poDetail
	SET 
	scanQuantity = @keyScanQuantity,
	scanDatetime = @keyScanDatetime,
	remarks = @keyRemarks,
	tbl_user_Id = @keyUserId,
	deviceSN = @keyDeviceSN,
	tbl_user_Username = (SELECT username FROM tbl_user WHERE tbl_user.id = @keyUserId),
	grnDocNo = @keyGRNDocNo,
	tbl_status_Id = @keyStatusId
	WHERE axPURCHID = @keyAXPURCHID
	AND axITEMID = @keyAXITEMID
	AND axDOT_PARTCODE = @keyAXDOT_PARTCODE
	AND axLINENUMBER = @keyAXLINENUM
END