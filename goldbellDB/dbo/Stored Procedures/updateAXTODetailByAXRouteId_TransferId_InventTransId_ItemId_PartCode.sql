﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXTODetailByAXRouteId_TransferId_InventTransId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
	@keyAXTOD_DATAAREAID nvarchar(4),
	@keyAXTOD_ROUTEID nvarchar(10),
	@keyAXTOD_TRANSFERID nvarchar(20),
	@keyAXTOD_INVENTTRANSID nvarchar(20),
	@keyAXTOD_ITEMID nvarchar(50),
	@keyAXTOD_DOT_PARTCODE nvarchar(80),
	@keyAXTOD_NAME nvarchar(60),
	@keyAXTOD_UNITID nvarchar(10),
	@keyAXTOD_INVENTLOCATIONID nvarchar(20),
	@keyAXTOD_INVENTSITEID nvarchar(10),
	@keyAXTOD_QTY numeric(32,16),
	@keyAXTOD_DOCTYPE varchar(6)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_toDetail
	SET 
	axDATAAREAID = @keyAXTOD_DATAAREAID,
	axNAME = @keyAXTOD_NAME,
	axUNITID = @keyAXTOD_UNITID,
	axINVENTLOCATIONID = @keyAXTOD_INVENTLOCATIONID,
	axINVENTSITEID = @keyAXTOD_INVENTSITEID,
	axQTY = @keyAXTOD_QTY,
	axDocType = @keyAXTOD_DOCTYPE
	WHERE axROUTEID = @keyAXTOD_ROUTEID
	AND axTRANSFERID = @keyAXTOD_TRANSFERID
	AND axINVENTTRANSID = @keyAXTOD_INVENTTRANSID
	AND axITEMID = @keyAXTOD_ITEMID
	AND axDOT_PARTCODE = @keyAXTOD_DOT_PARTCODE
END