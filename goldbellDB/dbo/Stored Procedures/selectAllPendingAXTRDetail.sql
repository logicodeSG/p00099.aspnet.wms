﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllPendingAXTRDetail]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	    SELECT [tbl_ax_trDetail].[id]
		  ,[tbl_ax_trDetail].[tbl_ax_tr_Id]
		  ,[tbl_ax_trDetail].[axDATAAREAID]
		  ,[tbl_ax_trDetail].[axLINENUM]
		  ,[tbl_ax_trDetail].[axTRANSFERID]
		  ,[tbl_ax_trDetail].[axTRANSFERSTATUS]
		  ,[tbl_ax_trDetail].[axINVENTTRANSID]
		  ,[tbl_ax_trDetail].[axITEMID]
		  ,[tbl_ax_trDetail].[axDOT_PARTCODE]
		  ,[tbl_ax_trDetail].[axNAMEALIAS]
		  ,[tbl_ax_trDetail].[axUNITID]
		  ,[tbl_ax_trDetail].[axINVENTLOCATIONID]
		  ,[tbl_ax_trDetail].[axINVENTSITEID]
		  ,[tbl_ax_trDetail].[axQTYSHIPPED]
		  ,[tbl_ax_trDetail].[axDocType]
		  ,[tbl_ax_trDetail].[scanQuantity]
		  ,[tbl_ax_trDetail].[scanDatetime]
		  ,[tbl_ax_trDetail].[remarks]
		  ,[tbl_ax_trDetail].[tbl_status_Id]
		  ,[tbl_ax_trDetail].[tbl_user_Id]
		  ,[tbl_ax_trDetail].[deviceSN]
		  ,[tbl_ax_trDetail].[tbl_user_Username]
		  ,[tbl_ax_trDetail].[axWMSLOCATIONID]
	  FROM [tbl_ax_trDetail]
      JOIN tbl_ax_tr ON tbl_ax_tr.axTRANSFERID = [tbl_ax_trDetail].axTRANSFERID
	  AND tbl_ax_tr.tbl_status_Id = 1
END