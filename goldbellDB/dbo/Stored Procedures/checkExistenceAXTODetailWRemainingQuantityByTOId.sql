﻿CREATE PROCEDURE [dbo].[checkExistenceAXTODetailWRemainingQuantityByTOId]
	-- Add the parameters for the stored procedure here
	@keyAXTOId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_ax_toDetail.id, tbl_ax_toDetail.tbl_ax_to_Id, tbl_ax_toDetail.tbl_item_Id,
	tbl_ax_toDetail.tbl_item_code, tbl_ax_toDetail.tbl_item_description,
	tbl_ax_toDetail.uom, tbl_ax_toDetail.quantity, 
	SUM(ISNULL(tbl_transferPickingDetail.quantity,0)) AS [pickedQuantity],
	tbl_ax_toDetail.tbl_group_Id
 	FROM tbl_ax_toDetail
	LEFT JOIN tbl_transferPicking ON tbl_transferPicking.tbl_ax_to_Id = tbl_ax_toDetail.tbl_ax_to_Id
	AND tbl_transferPicking.tbl_status_Id NOT IN (3)
	LEFT JOIN tbl_transferPickingDetail ON tbl_transferPickingDetail.tbl_transferPicking_Id = tbl_transferPicking.id
	AND tbl_transferPickingDetail.tbl_item_Id = tbl_ax_toDetail.tbl_item_Id
	AND tbl_transferPickingDetail.tbl_group_Id = tbl_ax_toDetail.tbl_group_Id
	WHERE tbl_ax_toDetail.tbl_ax_to_Id = @keyAXTOId
	GROUP BY tbl_ax_toDetail.id, tbl_ax_toDetail.tbl_ax_to_Id, tbl_ax_toDetail.tbl_item_Id,
	tbl_ax_toDetail.tbl_item_code, tbl_ax_toDetail.tbl_item_description,
	tbl_ax_toDetail.uom, tbl_ax_toDetail.quantity,tbl_ax_toDetail.tbl_group_Id
	HAVING tbl_ax_toDetail.quantity-SUM(ISNULL(tbl_transferPickingDetail.quantity,0)) > 0
END