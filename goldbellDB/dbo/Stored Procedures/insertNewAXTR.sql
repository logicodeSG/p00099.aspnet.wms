﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXTR]
	-- Add the parameters for the stored procedure here
	@keyAXTR_Id bigint,
	@keyAXTR_DATAAREAID nvarchar(4),
	@keyAXTR_MODIFIEDDATETIME datetime,
	@keyAXTR_VOURCHERID nvarchar(20),
	@keyAXTR_TRANSFERID nvarchar(20),
	@keyAXTR_DOCUMENTTYPE varchar(2),
	@keyAXTR_INVENTLOCATIONIDFROM nvarchar(20),
	@keyAXTR_INVENTLOCATIONIDTR nvarchar(20),
	@keyAXTR_TRANSFERSTATUS int,
	@keyReferenceNo varchar(50),
	@keyCreationDatetime datetime,
	@keyScanDatetime datetime,
	@keyStatusId bigint,
	@keyDocType varchar(50),
	@keyReferenceVersion bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_tr(
		 [id]
      ,[axDATAAREAID]
      ,[axMODIFIEDDATETIME]
      ,[axVOUCHERID]
      ,[axTRANSFERID]
      ,[axDocumentType]
      ,[axINVENTLOCATIONIDFROM]
      ,[axINVENTLOCATIONIDTO]
      ,[axTRANSFERSTATUS]
      ,[referenceNo]
      ,[creationDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
	)
	values(
	@keyAXTR_Id ,
	@keyAXTR_DATAAREAID ,
	@keyAXTR_MODIFIEDDATETIME ,
	@keyAXTR_VOURCHERID ,
	@keyAXTR_TRANSFERID ,
	@keyAXTR_DOCUMENTTYPE ,
	@keyAXTR_INVENTLOCATIONIDFROM ,
	@keyAXTR_INVENTLOCATIONIDTR ,
	@keyAXTR_TRANSFERSTATUS ,
	@keyReferenceNo ,
	@keyCreationDatetime ,
	@keyScanDatetime ,
	@keyStatusId ,
	@keyDocType ,
	@keyReferenceVersion ,
	@keyUserId ,
	@keyDeviceSN )
END