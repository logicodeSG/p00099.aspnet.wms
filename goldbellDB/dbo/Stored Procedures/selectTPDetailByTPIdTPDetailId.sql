﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectTPDetailByTPIdTPDetailId]
	-- Add the parameters for the stored procedure here
	@keyTPId bigInt,
	@keyTPDetailId smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_transferPickingDetail.id, tbl_transferPickingDetail.tbl_transferPicking_Id,
	tbl_transferPickingDetail.tbl_item_Id, tbl_transferPickingDetail.tbl_item_code,
	tbl_transferPickingDetail.tbl_item_description, tbl_transferPickingDetail.uom,
	tbl_transferPickingDetail.quantity, tbl_transferPickingDetail.receive_quantity, 
	tbl_transferPickingDetail.tbl_group_Id, tbl_transferPickingDetail.tbl_bin_Id
 	FROM tbl_transferPickingDetail
	WHERE tbl_transferPickingDetail.tbl_transferPicking_Id = @keyTPId
	AND tbl_transferPickingDetail.id = @keyTPDetailId
END