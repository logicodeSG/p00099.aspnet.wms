﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAXSOByAXRouteId]
	-- Add the parameters for the stored procedure here
	@keyAXROUTEId nvarchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  [id]
      ,[axDATAAREAID]
      ,[axPICKINGROUTEID]
      ,[axSALESId]
      ,[axConfirmationDate]
      ,[axCREATEDDATETIME]
      ,[axSALESTYPE]
      ,[referenceNo]
      ,[creationDatetime]
      ,[completionDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
      ,[axCustomerAccount]
      ,[axCustomerName]
	FROM
	tbl_ax_so
	WHERE [axPICKINGROUTEID] = @keyAXROUTEId
END