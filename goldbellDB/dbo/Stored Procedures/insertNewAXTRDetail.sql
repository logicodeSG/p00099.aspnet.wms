﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXTRDetail]
	-- Add the parameters for the stored procedure here
	@keyAXTRD_Id bigint,
	@keyAXTO_Id bigint,
	@keyAXTRD_DATAAREAID nvarchar(4),
	@keyAXTRD_LINENUMBER numeric(32,16),
	@keyAXTRD_TRANSFERID nvarchar(20),
	@keyAXTRD_TRANSFERSTATUS int,
	@keyAXTRD_INVENTTRANSID nvarchar(20),
	@keyAXTRD_ITEMID nvarchar(50),
	@keyAXTRD_DOT_PARTCODE nvarchar(80),
	@keyAXTRD_NAMEALIAS nvarchar(20),
	@keyAXTRD_UNITID nvarchar(10),
	@keyAXTRD_INVENTLOCATIONID nvarchar(20),
	@keyAXTRD_INVENTSITEID nvarchar(10),
	@keyAXTRD_WMSLOCATIONID nvarchar(25),
	@keyAXTRD_QTYSHIPPED numeric(32,16),
	@keyAXTRD_docType varchar(6),
	@keyScanQuantity numeric(32,16),
	@keyScanDatetime datetime,
	@keyRemarks nvarchar(255),
	@keyStatusId bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_trDetail(
		[id]
      ,[tbl_ax_tr_Id]
      ,[axDATAAREAID]
      ,[axLINENUM]
      ,[axTRANSFERID]
      ,[axTRANSFERSTATUS]
      ,[axINVENTTRANSID]
      ,[axITEMID]
      ,[axDOT_PARTCODE]
      ,[axNAMEALIAS]
      ,[axUNITID]
      ,[axINVENTLOCATIONID]
      ,[axINVENTSITEID]
	  ,[axWMSLOCATIONID]
      ,[axQTYSHIPPED]
      ,[axDocType]
      ,[scanQuantity]
      ,[scanDatetime]
      ,[remarks]
      ,[tbl_status_Id]
      ,[tbl_user_Id]
      ,[deviceSN]
	)
	values(
	@keyAXTRD_Id ,
	@keyAXTO_Id ,
	@keyAXTRD_DATAAREAID ,
	@keyAXTRD_LINENUMBER ,
	@keyAXTRD_TRANSFERID ,
	@keyAXTRD_TRANSFERSTATUS ,
	@keyAXTRD_INVENTTRANSID ,
	@keyAXTRD_ITEMID ,
	@keyAXTRD_DOT_PARTCODE ,
	@keyAXTRD_NAMEALIAS ,
	@keyAXTRD_UNITID ,
	@keyAXTRD_INVENTLOCATIONID ,
	@keyAXTRD_INVENTSITEID ,
	@keyAXTRD_WMSLOCATIONID,
	@keyAXTRD_QTYSHIPPED ,
	@keyAXTRD_docType ,
	@keyScanQuantity ,
	@keyScanDatetime ,
	@keyRemarks ,
	@keyStatusId ,
	@keyUserId ,
	@keyDeviceSN)
END