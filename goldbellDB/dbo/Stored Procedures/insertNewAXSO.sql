﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXSO]
	-- Add the parameters for the stored procedure here
	@keyAXSO_Id bigint,
	@keyAXSO_DATAAREAID nvarchar(4),
	@keyAXSO_ROUTEID nvarchar(10),
	@keyAXSO_SALESId nvarchar(20),
	@keyAXSO_ConfirmationDate datetime,
	@keyAXSO_CREATEDDATETIME datetime,
	@keyAXSO_SALESTYPE int,
	@keyReferenceNo varchar(50),
	@keyCreationDatetime datetime,
	@keyScanDatetime datetime,
	@keyStatusId bigint,
	@keyDocType varchar(50),
	@keyReferenceVersion bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50),
	@keyAXSO_CustomerAccount nvarchar(20),
	@keyAXSO_CustomerName nvarchar(60)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_so(
		[id]
      ,[axDATAAREAID]
      ,[axPICKINGROUTEID]
      ,[axSALESId]
      ,[axConfirmationDate]
      ,[axCREATEDDATETIME]
      ,[axSALESTYPE]
      ,[referenceNo]
      ,[creationDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
	  ,[axCustomerAccount]
	  ,[axCustomerName]
	)
	values(
	@keyAXSO_Id ,
	@keyAXSO_DATAAREAID ,
	@keyAXSO_ROUTEID ,
	@keyAXSO_SALESId ,
	@keyAXSO_ConfirmationDate ,
	@keyAXSO_CREATEDDATETIME ,
	@keyAXSO_SALESTYPE ,
	@keyReferenceNo ,
	@keyCreationDatetime ,
	@keyScanDatetime ,
	@keyStatusId ,
	@keyDocType ,
	@keyReferenceVersion ,
	@keyUserId ,
	@keyDeviceSN,
	@keyAXSO_CustomerAccount,
	@keyAXSO_CustomerName)
END