﻿CREATE PROCEDURE [dbo].[selectSRRDetailBySRRIdSRRDetailId]
	-- Add the parameters for the stored procedure here
	@keySRRId bigInt,
	@keySRRDetailId smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_salesReturnReceiveDetail.id, tbl_salesReturnReceiveDetail.tbl_salesReturnReceive_Id,
	tbl_salesReturnReceiveDetail.tbl_item_Id, tbl_salesReturnReceiveDetail.tbl_item_code,
	tbl_salesReturnReceiveDetail.tbl_item_description, tbl_salesReturnReceiveDetail.uom,
	tbl_salesReturnReceiveDetail.quantity, 
	tbl_salesReturnReceiveDetail.tbl_group_Id
 	FROM tbl_salesReturnReceiveDetail
	WHERE tbl_salesReturnReceiveDetail.tbl_salesReturnReceive_Id = @keySRRId
	AND tbl_salesReturnReceiveDetail.id = @keySRRDetailId
END