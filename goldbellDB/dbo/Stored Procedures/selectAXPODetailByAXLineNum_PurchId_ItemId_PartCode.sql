﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAXPODetailByAXLineNum_PurchId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
    @keyAXLINENUM  bigint,
	@keyAXPURCHID nvarchar(20),
	@keyAXITEMID nvarchar(50),
	@keyAXDOT_PARTCODE nvarchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT
        [id]
      ,[tbl_ax_po_Id]
      ,[axDATAAREAID]
      ,[axPURCHID]
      ,[axLINENUMBER]
      ,[axITEMID]
      ,[axNAME]
      ,[axDOT_PARTCODE]
      ,[axORDERACCOUNT]
      ,[axINVOICEACCOUNT]
      ,[axPURCHUNIT]
      ,[axINVENTLOCATIONID]
      ,[axINVENTSITEID]
      ,[axWMSLOCATIONID]
      ,[axQTYORDERED]
      ,[axPURCHRECEIVEDNOW]
      ,[scanQuantity]
      ,[scanDatetime]
      ,[remarks]
      ,[tbl_status_Id]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
	FROM tbl_ax_poDetail
	WHERE axPURCHID = @keyAXPURCHID
    AND axITEMID = @keyAXITEMID
    AND axDOT_PARTCODE = @keyAXDOT_PARTCODE
    AND axLINENUMBER = @keyAXLINENUM
END