﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectGRDetailByPOIdItemIdQty]
	-- Add the parameters for the stored procedure here
	@keyPOId bigInt,
	@keyItemId smallInt,
	@keyGRQuantity Int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_goodsReceiveDetail.id, tbl_goodsReceiveDetail.tbl_goodsReceive_Id, 
	tbl_goodsReceiveDetail.tbl_item_Id, tbl_goodsReceiveDetail.tbl_item_code,
	tbl_goodsReceiveDetail.tbl_item_description, tbl_goodsReceiveDetail.uom,
	tbl_goodsReceiveDetail.quantity, ISNULL(tbl_location.[description],'') AS [locationName], 
	tbl_goodsReceiveDetail.tbl_location_Id
 	FROM tbl_goodsReceiveDetail
	LEFT JOIN tbl_location ON tbl_location.id = tbl_goodsReceiveDetail.tbl_location_Id
	WHERE tbl_goodsReceiveDetail.tbl_goodsReceive_Id = 
	(SELECT tbl_goodsReceive.id FROM tbl_goodsReceive WHERE tbl_goodsReceive.tbl_ax_po_Id=@keyPOId)
	AND tbl_goodsReceiveDetail.tbl_item_Id = @keyItemId
	AND tbl_goodsReceiveDetail.quantity = @keyGRQuantity
END