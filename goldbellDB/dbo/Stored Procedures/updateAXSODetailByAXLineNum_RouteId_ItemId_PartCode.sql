﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXSODetailByAXLineNum_RouteId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
	@keyAXSOD_DATAAREAID nvarchar(4),
	@keyAXSOD_LINENUMBER numeric(32,16),
	@keyAXSOD_ROUTEID nvarchar(10),
	@keyAXSOD_INVENTTRANSID nvarchar(20),
	@keyAXSOD_ITEMID nvarchar(50),
	@keyAXSOD_DOT_PARTCODE nvarchar(80),
	@keyAXSOD_NAME nvarchar(60),
	@keyAXSOD_SALESUNIT nvarchar(10),
	@keyAXSOD_INVENTLOCATIONID nvarchar(20),
	@keyAXSOD_INVENTSITEID nvarchar(10),
	@keyAXSOD_WMSLOCATIONID nvarchar(25),
	@keyAXSOD_QTY numeric(32,16)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_soDetail
	SET 
	axDATAAREAID = @keyAXSOD_DATAAREAID,
	axINVENTTRANSID = @keyAXSOD_INVENTTRANSID,
	axNAME = @keyAXSOD_NAME,
	axSALESUNIT = @keyAXSOD_SALESUNIT,
	axINVENTLOCATIONID = @keyAXSOD_INVENTLOCATIONID,
	axINVENTSITEID = @keyAXSOD_INVENTSITEID,
	axWMSLOCATIONID = @keyAXSOD_WMSLOCATIONID,
	axQTY = @keyAXSOD_QTY
	WHERE axROUTEID = @keyAXSOD_ROUTEID
	AND axITEMID = @keyAXSOD_ITEMID
	AND axDOT_PARTCODE = @keyAXSOD_DOT_PARTCODE
	AND axLINENUM = @keyAXSOD_LINENUMBER
END