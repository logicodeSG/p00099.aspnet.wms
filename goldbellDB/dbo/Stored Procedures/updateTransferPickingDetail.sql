﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateTransferPickingDetail]
	-- Add the parameters for the stored procedure here
	
	@keyTPId bigint,
	@keyTPDetailId smallInt,
	@keyTPExistingQuantity smallInt,
	@keyTPNewQuantity smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_transferPickingDetail
	SET quantity = @keyTPNewQuantity
	WHERE id = @keyTPDetailId
	AND tbl_transferPicking_Id = @keyTPId
	AND quantity = @keyTPExistingQuantity
END