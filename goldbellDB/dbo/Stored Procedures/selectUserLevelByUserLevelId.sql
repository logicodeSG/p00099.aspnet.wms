﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectUserLevelByUserLevelId]
	-- Add the parameters for the stored procedure here
	@keyUserLevelId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_userLevel.id, tbl_userLevel.[order], tbl_userLevel.[description], tbl_userLevel.active 
	FROM tbl_userLevel
	WHERE tbl_userLevel.id = @keyUserLevelId
END