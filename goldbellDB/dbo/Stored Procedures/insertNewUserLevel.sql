﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewUserLevel]
	-- Add the parameters for the stored procedure here
	@keyUserLevelId bigint,
	@keyUserLevelDescription varchar(50),
	@keyUserLevelActive bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tbl_userLevel(id,[order],[description],active)
	VALUES(@keyUserLevelId,2
			,@keyUserLevelDescription
			,@keyUserLevelActive)
END