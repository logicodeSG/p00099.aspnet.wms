﻿Create PROCEDURE [dbo].[insertNewSalesReturnReceive]
	-- Add the parameters for the stored procedure here
	@keySRRId bigint,
	@keySRRReferenceNumber varchar(50),
	@keyTOId bigint,
	@keyCreationDateTime varchar(50),
	@keyReceiveDateTime varchar(50),
	@keyCreatedBy bigint,
	@keySRRDeviceSN varchar(50),
	@keySRRLocationId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_salesReturnReceive(id, referenceNo, tbl_ax_sr_Id, creationDatetime,
	receiveDatetime, tbl_user_Id, deviceSN, tbl_status_Id, lastEdit, tbl_location_Id, referenceVersion)
	values(
	@keySRRId,@keySRRReferenceNumber,@keyTOId,@keyCreationDateTime,
	@keyReceiveDateTime,@keyCreatedBy,@keySRRDeviceSN,1,@keyCreatedBy, @keySRRLocationId, 1)
END