﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXCJDetailByAXLineNum_JournalId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
	@keyAXCJD_JOURNALID nvarchar(20),
	@keyAXCJD_LINENUMBER numeric(32,16),
	@keyAXCJD_ITEMID nvarchar(50),
	@keyAXCJD_DOT_PARTCODE nvarchar(80),
	@keyAXCJD_QTY numeric(32,16),
	@keyAXCJD_INVENTUNITID nvarchar(10),
	@keyAXCJD_INVENTLOCATIONID nvarchar(20),
	@keyAXCJD_INVENTSITEID nvarchar(10),
	@keyAXCJD_WMSLOCATIONID nvarchar(25)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_cjDetail
	SET 
	axQTY = @keyAXCJD_QTY,
	axInventUnitID = @keyAXCJD_INVENTUNITID,
	axINVENTLOCATIONID = @keyAXCJD_INVENTLOCATIONID,
	axINVENTSITEID = @keyAXCJD_INVENTSITEID,
	axWMSLOCATIONID = @keyAXCJD_WMSLOCATIONID
	WHERE axJOURNALID = @keyAXCJD_JOURNALID
	AND axITEMID = @keyAXCJD_ITEMID
	AND axDOT_PARTCODE = @keyAXCJD_DOT_PARTCODE
	AND axLINENUM = @keyAXCJD_LINENUMBER
END