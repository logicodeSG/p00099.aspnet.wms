﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewLocation]
	-- Add the parameters for the stored procedure here
	@keyLocCode varchar(50),
	@keyLocDesc varchar(50),
	@keyLocLevel bigint,
	@keyLocParentId bigint,
	@keyLocActive bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tbl_location(id, code, [description], tbl_locationLevel_Id, tbl_locationParent_Id, active)
	values((SELECT CASE WHEN MAX(tbl_location.id) IS NULL THEN 1 ELSE MAX(tbl_location.id)+1 END FROM tbl_location),
	@keyLocCode,@keyLocDesc,@keyLocLevel,@keyLocParentId,@keyLocActive)
END