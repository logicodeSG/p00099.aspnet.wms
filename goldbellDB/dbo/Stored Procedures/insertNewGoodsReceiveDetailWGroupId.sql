﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewGoodsReceiveDetailWGroupId]
	-- Add the parameters for the stored procedure here
	@keyGRId bigint,
	@keyGRDetailId smallint,
	@keyItemId bigint,
	@keyItemCode varchar(50),
	@keyItemDescription varchar(255),
	@keyItemUOM varchar(50),
	@keyItemQuantity int,
	@keyItemLocationId bigint,
	@keyItemGroupId smallint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_goodsReceiveDetail(id, tbl_goodsReceive_Id, tbl_item_Id, tbl_item_code,
	tbl_item_description, uom, quantity, tbl_location_Id, tbl_group_Id)
	values(@keyGRDetailId,@keyGRId,@keyItemId,@keyItemCode,@keyItemDescription,
	@keyItemUOM,@keyItemQuantity,@keyItemLocationId,@keyItemGroupId)
END