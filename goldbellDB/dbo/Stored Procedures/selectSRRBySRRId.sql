﻿CREATE PROCEDURE [dbo].[selectSRRBySRRId]
	-- Add the parameters for the stored srocedure here
	@keySRRId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to srevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for srocedure here
	SELECT tbl_salesReturnReceive.id, tbl_salesReturnReceive.referenceNo, tbl_ax_sr.referenceNo AS [SRReferenceNo],
	tbl_salesReturnReceive.receiveDatetime, tbl_user.username, tbl_salesReturnReceive.tbl_status_Id,
	tbl_salesReturnReceive.tbl_location_Id, tbl_salesReturnReceive.referenceVersion
 	FROM tbl_salesReturnReceive
	JOIN tbl_user ON tbl_salesReturnReceive.tbl_user_Id = tbl_user.id
	JOIN tbl_ax_sr ON tbl_ax_sr.id = tbl_salesReturnReceive.tbl_ax_sr_Id
	WHERE tbl_salesReturnReceive.id = @keySRRId
END