﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllUserLevel]
	-- Add the parameters for the stored procedure here
	 --@keyUserId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_userLevel.id, tbl_userLevel.[order] 
		,tbl_userLevel.[description], tbl_userLevel.active
	FROM  tbl_userLevel
	WHERE tbl_userLevel.[order] > 1
	AND tbl_userLevel.active = 1

END