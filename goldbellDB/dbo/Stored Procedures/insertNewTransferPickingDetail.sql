﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewTransferPickingDetail]
	-- Add the parameters for the stored procedure here
	@keyTPId bigint,
	@keyTPDetailId smallint,
	@keyItemId bigint,
	@keyItemCode varchar(50),
	@keyItemDescription varchar(255),
	@keyItemUOM varchar(50),
	@keyItemQuantity int,
	@keyItemReceiveQuantity int,
	@keyItemGroupId smallint,
	@keyItemBinId varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_transferPickingDetail(id, tbl_transferPicking_Id, tbl_item_Id, tbl_item_code,
	tbl_item_description, uom, quantity, receive_quantity, tbl_group_Id, tbl_bin_Id)
	values(@keyTPDetailId,@keyTPId,@keyItemId,@keyItemCode,@keyItemDescription,
	@keyItemUOM,@keyItemQuantity,@keyItemReceiveQuantity,@keyItemGroupId,@keyItemBinId)
END