﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewExceptionMsg]
	-- Add the parameters for the stored procedure here
    @axDOCUMENTID nvarchar(20),
    @creationDatetime varchar(50),
    @tbl_user_Id varchar(50),
    @deviceSN varchar(50),
    @docType varchar(50),
    @exceptionMsg nvarchar(255),
    @messageId nvarchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery AS NVARCHAR(MAX)
	DECLARE @SQLCond AS NVARCHAR(500)

    -- Insert statements for procedure here
	SET @SQLCond =
	CASE 
	   WHEN @tbl_user_Id IN ('-1') 
			THEN 'SYSTEM'	
	   ELSE (SELECT username FROM tbl_user WHERE id=@tbl_user_Id)
	END

	INSERT INTO dbo.tbl_exceptionMsg(
		 [id]
      ,[axDOCUMENTID]
      ,[creationDatetime]
      ,[tbl_user_Id]
      ,[tbl_username]
      ,[deviceSN]
      ,[docType]
	  ,[exceptionMsg]
	  ,[messageId])
	values(
	(SELECT CASE WHEN MAX(tbl_exceptionMsg.id) IS NULL THEN 1 ELSE MAX(tbl_exceptionMsg.id)+1 END FROM tbl_exceptionMsg),
	 @axDOCUMENTID,
	@creationDatetime,
	@tbl_user_Id,
	@SQLCond,
	@deviceSN,
	@docType,
	@exceptionMsg,
	@messageId)

END