﻿CREATE PROCEDURE [dbo].[selectSOPDetailBySOIdItemIdQtyGroupId]
	-- Add the parameters for the stored procedure here
	@keySOId bigInt,
	@keyItemId smallInt,
	@keySOPQuantity Int,
	@keyGroupId smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_salesOrderPickingDetail.id, tbl_salesOrderPickingDetail.tbl_salesOrderPicking_Id, 
	tbl_salesOrderPickingDetail.tbl_item_Id, tbl_salesOrderPickingDetail.tbl_item_code,
	tbl_salesOrderPickingDetail.tbl_item_description, tbl_salesOrderPickingDetail.uom,
	tbl_salesOrderPickingDetail.quantity, 
	tbl_salesOrderPickingDetail.tbl_group_Id, tbl_salesOrderPickingDetail.tbl_bin_Id
 	FROM tbl_salesOrderPickingDetail
	WHERE tbl_salesOrderPickingDetail.tbl_salesOrderPicking_Id = 
	(SELECT tbl_salesOrderPicking.id FROM tbl_salesOrderPicking 
	WHERE tbl_salesOrderPicking.tbl_ax_so_Id=@keySOId)
	AND tbl_salesOrderPickingDetail.tbl_item_Id = @keyItemId
	AND tbl_salesOrderPickingDetail.quantity = @keySOPQuantity
	AND tbl_salesOrderPickingDetail.tbl_group_Id = @keyGroupId
END