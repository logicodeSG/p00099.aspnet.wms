﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[deleteAXSOByAXRouteId]
	-- Add the parameters for the stored procedure here
	@keyAXROUTEID nvarchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE tbl_ax_so FROM tbl_ax_so
	WHERE axPICKINGROUTEID = @keyAXROUTEID
END