﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXAJDetail]
	-- Add the parameters for the stored procedure here
	@keyAXAJD_Id bigint,
	@keyAXAJ_Id bigint,
	@keyAXAJD_DATAAREAID nvarchar(4),
	@keyAXAJD_JOURNALID nvarchar(20),
	@keyAXAJD_VENDACCOUNT nvarchar(20),
	@keyAXAJD_PURCHID nvarchar(20),
	@keyAXAJD_LINENUMBER numeric(32,16),
	@keyAXAJD_ITEMID nvarchar(50),
	@keyAXAJD_GOD_PARTCODE nvarchar(80),
	@keyAXAJD_QTY numeric(32,16),
	@keyAXAJD_TRANSDATE datetime,
	@keyAXAJD_INVENTTRANSID nvarchar(20),
	@keyAXAJD_INVENTDIMID nvarchar(20),
	@keyAXAJD_INVENTLOCATIONID nvarchar(20),
	@keyAXAJD_INVENTSITEID nvarchar(10),
	@keyAXAJD_WMSLOCATIONID nvarchar(25),
	@keyAXAJD_CREATEDDATETIME datetime,
	@keyAXAJD_CREATEDBY nvarchar(8),
	@keyScanQuantity numeric(32,16),
	@keyScanDatetime datetime,
	@keyRemarks nvarchar(255),
	@keyStatusId bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50),
	@keyAXAJD_BOX_NO nvarchar(20),
	@keyAXAJD_CASE nvarchar(20),
	@keyAXAJD_PRODUCT_NAME nvarchar(60),
	@keyAXAJD_SO_NUMBER nvarchar(20),
	@keyAXAJD_CUSTOMER_NAME nvarchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_ajDetail(
	[id]
      ,[tbl_ax_aj_Id]
      ,[axDATAAREAID]
      ,[axJOURNALID]
      ,[axVENDACCOUNT]
      ,[axPurchID]
      ,[axLINENUM]
      ,[axITEMID]
      ,[axGOD_PARTCODE]
      ,[axQTY]
      ,[axTRANSDATE]
      ,[axINVENTTRANSID]
      ,[axINVENTDIMID]
      ,[axINVENTLOCATIONID]
      ,[axINVENTSITEID]
      ,[axWMSLOCATIONID]
      ,[axCREATEDDATETIME]
      ,[axCREATEDBY]
      ,[scanQuantity]
      ,[scanDatetime]
      ,[remarks]
      ,[tbl_status_Id]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[axBOX_NO]
      ,[axCASE]
	  ,[axPRODUCT_NAME]
	  ,[axSO_NUMBER]
	  ,[axCUSTOMER_NAME]
	)
	values(
	@keyAXAJD_Id ,
	@keyAXAJ_Id ,
	@keyAXAJD_DATAAREAID ,
	@keyAXAJD_JOURNALID ,
	@keyAXAJD_VENDACCOUNT ,
	@keyAXAJD_PURCHID ,
	@keyAXAJD_LINENUMBER ,
	@keyAXAJD_ITEMID ,
	@keyAXAJD_GOD_PARTCODE ,
	@keyAXAJD_QTY ,
	@keyAXAJD_TRANSDATE ,
	@keyAXAJD_INVENTTRANSID ,
	@keyAXAJD_INVENTDIMID ,
	@keyAXAJD_INVENTLOCATIONID ,
	@keyAXAJD_INVENTSITEID ,
	@keyAXAJD_WMSLOCATIONID ,
	@keyAXAJD_CREATEDDATETIME ,
	@keyAXAJD_CREATEDBY ,
	@keyScanQuantity ,
	@keyScanDatetime ,
	@keyRemarks ,
	@keyStatusId ,
	@keyUserId ,
	@keyDeviceSN,
	@keyAXAJD_BOX_NO,
	@keyAXAJD_CASE,
	@keyAXAJD_PRODUCT_NAME,
	@keyAXAJD_SO_NUMBER,
	@keyAXAJD_CUSTOMER_NAME)
END