﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllPendingAXCJDetail]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	    SELECT [tbl_ax_cjDetail].[id]
          ,[tbl_ax_cjDetail].[tbl_ax_cj_Id]
          ,[tbl_ax_cjDetail].[axJOURNALID]
          ,[tbl_ax_cjDetail].[axLINENUM]
          ,[tbl_ax_cjDetail].[axITEMID]
          ,[tbl_ax_cjDetail].[axDOT_PARTCODE]
          ,[tbl_ax_cjDetail].[axQTY]
          ,[tbl_ax_cjDetail].[axInventUnitID]
          ,[tbl_ax_cjDetail].[axINVENTLOCATIONID]
          ,[tbl_ax_cjDetail].[axINVENTSITEID]
          ,[tbl_ax_cjDetail].[axWMSLOCATIONID]
          ,[tbl_ax_cjDetail].[scanQuantity]
          ,[tbl_ax_cjDetail].[scanDatetime]
          ,[tbl_ax_cjDetail].[remarks]
          ,[tbl_ax_cjDetail].[tbl_status_Id]
          ,[tbl_ax_cjDetail].[tbl_user_Id]
          ,[tbl_ax_cjDetail].[deviceSN]
          ,[tbl_ax_cjDetail].[tbl_user_Username]
          ,[tbl_ax_cjDetail].[axPRODUCT_NAME]
      FROM [tbl_ax_cjDetail]
      JOIN tbl_ax_cj ON tbl_ax_cj.axJOURNALID = tbl_ax_cjDetail.axJOURNALID
	    AND tbl_ax_cj.tbl_status_Id = 1
END