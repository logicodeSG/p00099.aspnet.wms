﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewUserLevelXAccess]
	-- Add the parameters for the stored procedure here
	@keyUserLevelId Bigint,
	@keyAccessId Tinyint,
	@keyCanRead bit,
	@keyCanAdd bit,
	@keyCanEdit bit,
	@keyCanDelete bit,
	@keyCanPrint bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO x_tbl_userLevelXtbl_access(tbl_userLevel_Id, tbl_access_Id, canRead, canAdd, canEdit, canDelete, canPrint)
	VALUES(@keyUserLevelId, @keyAccessId, @keyCanRead, @keyCanAdd, @keyCanEdit, @keyCanDelete, @keyCanPrint)
END