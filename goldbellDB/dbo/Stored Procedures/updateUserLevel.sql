﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateUserLevel]
	-- Add the parameters for the stored procedure here
	
	@keyUserLevelId bigint,
	@keyUserLevelDescription varchar(50),
	@keyUserLevelActive bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_userLevel
	SET [description] = @keyUserLevelDescription,
		active = @keyUserLevelActive
	WHERE id = @keyUserLevelId
END