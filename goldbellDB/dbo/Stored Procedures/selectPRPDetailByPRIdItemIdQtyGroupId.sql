﻿CREATE PROCEDURE [dbo].[selectPRPDetailByPRIdItemIdQtyGroupId]
	-- Add the parameters for the stored procedure here
	@keyPRId bigInt,
	@keyItemId smallInt,
	@keyPRPQuantity Int,
	@keyGroupId smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_purchaseReturnPickingDetail.id, tbl_purchaseReturnPickingDetail.tbl_purchaseReturnPicking_Id, 
	tbl_purchaseReturnPickingDetail.tbl_item_Id, tbl_purchaseReturnPickingDetail.tbl_item_code,
	tbl_purchaseReturnPickingDetail.tbl_item_description, tbl_purchaseReturnPickingDetail.uom,
	tbl_purchaseReturnPickingDetail.quantity
 	FROM tbl_purchaseReturnPickingDetail
	WHERE tbl_purchaseReturnPickingDetail.tbl_purchaseReturnPicking_Id = 
	(SELECT tbl_purchaseReturnPicking.id FROM tbl_purchaseReturnPicking 
	WHERE tbl_purchaseReturnPicking.tbl_ax_pr_Id=@keyPRId)
	AND tbl_purchaseReturnPickingDetail.tbl_item_Id = @keyItemId
	AND tbl_purchaseReturnPickingDetail.quantity = @keyPRPQuantity
	AND tbl_purchaseReturnPickingDetail.tbl_group_Id = @keyGroupId
END