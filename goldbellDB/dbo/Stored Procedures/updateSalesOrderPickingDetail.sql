﻿CREATE PROCEDURE [dbo].[updateSalesOrderPickingDetail]
	-- Add the parameters for the stored procedure here
	
	@keySOPId bigint,
	@keySOPDetailId smallInt,
	@keySOPExistingQuantity smallInt,
	@keySOPNewQuantity smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_salesOrderPickingDetail
	SET quantity = @keySOPNewQuantity
	WHERE id = @keySOPDetailId
	AND tbl_salesOrderPicking_Id = @keySOPId
	AND quantity = @keySOPExistingQuantity
END