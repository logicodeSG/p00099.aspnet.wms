﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAXAJByAXJournalId]
	-- Add the parameters for the stored procedure here
	@keyAXJOURNALID nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  [id]
      ,[axJOURNALID]
      ,[axDESCRIPTION]
      ,[axPOSTED]
      ,[axJOURNALNAMEID]
      ,[axINVENTDIMID]
      ,[axPACKINGSLIP]
      ,[axCREATEDDATETIME]
      ,[axDATAAREAID]
      ,[referenceNo]
      ,[creationDatetime]
      ,[completionDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
	FROM
	tbl_ax_aj
	WHERE [axJOURNALID] = @keyAXJOURNALID
END