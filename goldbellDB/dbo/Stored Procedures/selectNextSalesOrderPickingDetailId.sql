﻿CREATE PROCEDURE [dbo].[selectNextSalesOrderPickingDetailId]
	-- Add the parameters for the stored procedure here
	@keySOPId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CASE WHEN MAX(tbl_salesOrderPickingDetail.id) IS NULL THEN 1 ELSE MAX(tbl_salesOrderPickingDetail.id)+1 END 
	FROM tbl_salesOrderPickingDetail WHERE tbl_salesOrderPicking_Id=@keySOPId
END