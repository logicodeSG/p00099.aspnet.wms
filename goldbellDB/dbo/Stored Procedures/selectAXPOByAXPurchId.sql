﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAXPOByAXPurchId]
	-- Add the parameters for the stored procedure here
	@keyAXPURCHID nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  [id]
      ,[axDATAAREAID]
      ,[axPURCHID]
      ,[axPURCHNAME]
      ,[axORDERACCOUNT]
      ,[axINVOICEACCOUNT]
      ,[axLastConfirmationDate]
      ,[axDELIVERYDATE]
      ,[axCREATEDDATETIME]
      ,[referenceNo]
      ,[creationDatetime]
      ,[completionDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
	FROM
	tbl_ax_po
	WHERE [axPURCHID] = @keyAXPURCHID
END