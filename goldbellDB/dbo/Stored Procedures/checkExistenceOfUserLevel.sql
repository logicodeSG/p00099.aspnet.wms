﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[checkExistenceOfUserLevel]
	-- Add the parameters for the stored procedure here
	@keyUserLevelDescription varchar(50),
	@keyExcludedUserLevelId bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM tbl_userLevel
	WHERE  tbl_userLevel.[description] = @keyUserLevelDescription
	AND id <> @keyExcludedUserLevelId
END