﻿
CREATE PROCEDURE [dbo].[updateTransferPickingDetailWReceiveQty]
	-- Add the parameters for the stored procedure here
	
	@keyTPId bigint,
	@keyTPDetailId smallInt,
	@keyTPExistingReceiveQuantity smallInt,
	@keyTPNewReceiveQuantity smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_transferPickingDetail
	SET receive_quantity = @keyTPNewReceiveQuantity
	WHERE id = @keyTPDetailId
	AND tbl_transferPicking_Id = @keyTPId
	AND receive_quantity = @keyTPExistingReceiveQuantity
END