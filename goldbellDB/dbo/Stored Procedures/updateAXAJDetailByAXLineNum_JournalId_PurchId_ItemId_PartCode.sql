﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXAJDetailByAXLineNum_JournalId_PurchId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
	@keyAXAJD_DATAAREAID nvarchar(4),
	@keyAXAJD_JOURNALID nvarchar(20),
	@keyAXAJD_VENDACCOUNT nvarchar(20),
	@keyAXAJD_PURCHID nvarchar(20),
	@keyAXAJD_LINENUMBER numeric(32,16),
	@keyAXAJD_ITEMID nvarchar(50),
	@keyAXAJD_GOD_PARTCODE nvarchar(80),
	@keyAXAJD_QTY numeric(32,16),
	@keyAXAJD_TRANSDATE datetime,
	@keyAXAJD_INVENTTRANSID nvarchar(20),
	@keyAXAJD_INVENTDIMID nvarchar(20),
	@keyAXAJD_INVENTLOCATIONID nvarchar(20),
	@keyAXAJD_INVENTSITEID nvarchar(10),
	@keyAXAJD_WMSLOCATIONID nvarchar(25),
	@keyAXAJD_CREATEDDATETIME datetime,
	@keyAXAJD_CREATEDBY nvarchar(8)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_ajDetail
	SET 
	[axDATAAREAID] = @keyAXAJD_DATAAREAID
      ,[axVENDACCOUNT] = @keyAXAJD_VENDACCOUNT
      ,[axGOD_PARTCODE] = @keyAXAJD_GOD_PARTCODE
      ,[axQTY] = @keyAXAJD_QTY
      ,[axTRANSDATE] = @keyAXAJD_TRANSDATE
      ,[axINVENTTRANSID] = @keyAXAJD_INVENTTRANSID
      ,[axINVENTDIMID] = @keyAXAJD_INVENTDIMID
      ,[axINVENTLOCATIONID] = @keyAXAJD_INVENTLOCATIONID
      ,[axINVENTSITEID] = @keyAXAJD_INVENTSITEID
      ,[axWMSLOCATIONID] = @keyAXAJD_WMSLOCATIONID
      ,[axCREATEDDATETIME] = @keyAXAJD_CREATEDDATETIME
      ,[axCREATEDBY] = @keyAXAJD_CREATEDBY
	WHERE axJOURNALID = @keyAXAJD_JOURNALID
	AND axPURCHID = @keyAXAJD_PURCHID
	AND axITEMID = @keyAXAJD_ITEMID
	AND axGOD_PARTCODE = @keyAXAJD_GOD_PARTCODE
	AND [axLINENUM] = @keyAXAJD_LINENUMBER
END