﻿CREATE PROCEDURE [dbo].[insertNewSalesReturnReceiveDetail]
	-- Add the parameters for the stored procedure here
	@keySRRId bigint,
	@keySRRDetailId smallint,
	@keyItemId bigint,
	@keyItemCode varchar(50),
	@keyItemDescription varchar(255),
	@keyItemUOM varchar(50),
	@keyItemQuantity int,
	@keyItemGroupId smallint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_salesReturnReceiveDetail(id, tbl_salesReturnReceive_Id, tbl_item_Id, tbl_item_code,
	tbl_item_description, uom, quantity, tbl_group_Id)
	values(@keySRRDetailId,@keySRRId,@keyItemId,@keyItemCode,@keyItemDescription,
	@keyItemUOM,@keyItemQuantity,@keyItemGroupId)
END