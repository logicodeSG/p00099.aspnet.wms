﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXAJDetailScanInputByAXLineNum_JournalId_PurchId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
	@keyAXLINENUM numeric(32,16),
	@keyAXJOURNALID nvarchar(20),
	@keyAXPURCHID nvarchar(20),
	@keyAXITEMID nvarchar(50),
	@keyAXGOD_PARTCODE nvarchar(80),
	@keyScanQuantity numeric(32,16),
	@keyScanDatetime datetime,
	@keyRemarks nvarchar(255),
	@keyUserId bigint,
	@keyDeviceSN varchar(50),
	@keyStatusId bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_ajDetail
	SET 
	scanQuantity = @keyScanQuantity,
	scanDatetime = @keyScanDatetime,
	remarks = @keyRemarks,
	tbl_user_Id = @keyUserId,
	deviceSN = @keyDeviceSN,
	tbl_user_Username = (SELECT username FROM tbl_user WHERE tbl_user.id = @keyUserId),
	tbl_status_Id = @keyStatusId
	WHERE axJOURNALID = @keyAXJOURNALID
	AND axPURCHID = @keyAXPURCHID
	AND axITEMID = @keyAXITEMID
	AND axGOD_PARTCODE = @keyAXGOD_PARTCODE
	AND axLINENUM = @keyAXLINENUM
END