﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectGRByGRId]
	-- Add the parameters for the stored procedure here
	@keyGRId bigInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_goodsReceive.id, tbl_goodsReceive.referenceNo, tbl_goodsReceive.doReferenceNo, tbl_ax_po.referenceNo AS [POReferenceNo],
	tbl_goodsReceive.receiveDatetime, tbl_user.username, tbl_goodsReceive.tbl_status_Id,
	tbl_goodsReceive.tbl_location_Id, tbl_goodsReceive.referenceVersion
 	FROM tbl_goodsReceive
	JOIN tbl_user ON tbl_goodsReceive.tbl_user_Id = tbl_user.id
	JOIN tbl_ax_po ON tbl_ax_po.id = tbl_goodsReceive.tbl_ax_po_Id
	WHERE tbl_goodsReceive.id = @keyGRId
END