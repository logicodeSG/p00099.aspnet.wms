﻿CREATE PROCEDURE [dbo].[updatePurchaseReturnPickingDetail]
	-- Add the parameters for the stored procedure here
	
	@keyPRPId bigint,
	@keyPRPDetailId smallInt,
	@keyPRPExistingQuantity smallInt,
	@keyPRPNewQuantity smallInt
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_purchaseReturnPickingDetail
	SET quantity = @keyPRPNewQuantity
	WHERE id = @keyPRPDetailId
	AND tbl_purchaseReturnPicking_Id = @keyPRPId
	AND quantity = @keyPRPExistingQuantity
END