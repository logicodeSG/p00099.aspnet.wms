﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXPODetailByAXLineNum_PurchId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
	@keyAXPOD_DATAAREAID nvarchar(4),
	@keyAXPOD_PURCHID nvarchar(20),
	@keyAXPOD_LINENUMBER bigint,
	@keyAXPOD_ITEMID nvarchar(50),
	@keyAXPOD_NAME nvarchar(1000),
	@keyAXPOD_DOT_PARTCODE nvarchar(80),
	@keyAXPOD_ORDERACCOUNT nvarchar(20),
	@keyAXPOD_INVOICEACCOUNT nvarchar(20),
	@keyAXPOD_PURCHUNIT nvarchar(10),
	@keyAXPOD_INVENTLOCATIONID nvarchar(20),
	@keyAXPOD_INVENTSITEID nvarchar(10),
	@keyAXPOD_WMSLOCATIONID nvarchar(25),
	@keyAXPOD_QTYORDERED numeric(32,16),
	@keyAXPOD_PURCHRECEIVEDNOW numeric(32,16)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_poDetail
	SET 
	axDATAAREAID = @keyAXPOD_DATAAREAID,
	axNAME = @keyAXPOD_NAME,
	axORDERACCOUNT = @keyAXPOD_ORDERACCOUNT,
	axINVOICEACCOUNT = @keyAXPOD_INVOICEACCOUNT,
	axPURCHUNIT = @keyAXPOD_PURCHUNIT,
	axINVENTLOCATIONID = @keyAXPOD_INVENTLOCATIONID,
	axINVENTSITEID = @keyAXPOD_INVENTSITEID,
	axWMSLOCATIONID = @keyAXPOD_WMSLOCATIONID,
	axQTYORDERED = @keyAXPOD_QTYORDERED,
	axPURCHRECEIVEDNOW = @keyAXPOD_PURCHRECEIVEDNOW
	WHERE axPURCHID = @keyAXPOD_PURCHID
	AND axITEMID = @keyAXPOD_ITEMID
	AND axDOT_PARTCODE = @keyAXPOD_DOT_PARTCODE
	AND axLINENUMBER = @keyAXPOD_LINENUMBER
END