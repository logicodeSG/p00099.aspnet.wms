﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAXSODetailByAXLineNum_RouteId_ItemId_PartCode]
	-- Add the parameters for the stored procedure here
    @keyAXLINENUM  numeric(32,16),
	@keyAXROUTEID nvarchar(10),
	@keyAXITEMID nvarchar(50),
	@keyAXDOT_PARTCODE nvarchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT
         tbl_ax_soDetail.[id]
      ,tbl_ax_soDetail.[tbl_ax_so_Id]
      ,tbl_ax_soDetail.[axDATAAREAID]
      ,tbl_ax_soDetail.[axLINENUM]
      ,tbl_ax_soDetail.[axROUTEID]
      ,tbl_ax_soDetail.[axINVENTTRANSID]
      ,tbl_ax_soDetail.[axITEMID]
      ,tbl_ax_soDetail.[axDOT_PARTCODE]
      ,tbl_ax_soDetail.[axNAME]
      ,tbl_ax_soDetail.[axSALESUNIT]
      ,tbl_ax_soDetail.[axINVENTLOCATIONID]
      ,tbl_ax_soDetail.[axINVENTSITEID]
      ,tbl_ax_soDetail.[axWMSLOCATIONID]
      ,tbl_ax_soDetail.[axQTY]
      ,tbl_ax_soDetail.[scanQuantity]
      ,tbl_ax_soDetail.[scanDatetime]
      ,tbl_ax_soDetail.[remarks]
      ,tbl_ax_soDetail.[tbl_status_Id]
      ,tbl_ax_soDetail.[tbl_user_Id]
      ,tbl_ax_soDetail.[deviceSN]
      ,tbl_ax_soDetail.[tbl_user_Username]
	FROM tbl_ax_soDetail
	WHERE axROUTEID = @keyAXROUTEID
    AND axITEMID = @keyAXITEMID
    AND axDOT_PARTCODE = @keyAXDOT_PARTCODE
    AND axLINENUM = @keyAXLINENUM
END