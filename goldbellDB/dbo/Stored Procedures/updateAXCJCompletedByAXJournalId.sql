﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateAXCJCompletedByAXJournalId]
	-- Add the parameters for the stored procedure here
	@keyAXJOURNALID nvarchar(20),
	@keyCompletionDatetime datetime,
	@keyStatusId bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE tbl_ax_cj
	SET 
	completionDatetime = @keyCompletionDatetime,
	tbl_status_Id = @keyStatusId
	WHERE axJOURNALID = @keyAXJOURNALID
END