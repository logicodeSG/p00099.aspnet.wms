﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateUser]
	-- Add the parameters for the stored procedure here
	@keyUserId bigint,
	@keyUserCode varchar(50),
	@keyUserPass nvarchar(64),
	@keyUserName varchar(50),
	@keyUserLevelId bigint,
	@keyUserActive bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_user
	SET code = @keyUserCode,
		username = @keyUserName,
		[password] = @keyUserPass,
		tbl_userlevel_Id = @keyUserLevelId,
		active = @keyUserActive
	WHERE id = @keyUserId
END