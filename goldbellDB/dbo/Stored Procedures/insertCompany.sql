﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertCompany]
	-- Add the parameters for the stored procedure here
	@keyCompanyName varchar(50),
	@keyCompanyAddress1 varchar(50),
	@keyCompanyAddress2 varchar(50),
	@keyCompanyAddress3 varchar(50),
	@keyCompanyPhoneNumber varchar(20),
	@keyCompanyFaxNumber varchar(20),
	@keyCompanyEmail varchar(50),
	@keyCompanyWebsite varchar(50),
	@keyCompanyRegNumber varchar(20),
	@keyCompanyGSTRegNumber varchar(20)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tbl_company(id,name,address1,address2,address3,phone,fax,email,website,regNo,GSTRegNo)
	VALUES(1,
	@keyCompanyName,@keyCompanyAddress1,@keyCompanyAddress2,@keyCompanyAddress3,
	@keyCompanyPhoneNumber,@keyCompanyFaxNumber,@keyCompanyEmail,
	@keyCompanyWebsite,@keyCompanyRegNumber,@keyCompanyGSTRegNumber)
END