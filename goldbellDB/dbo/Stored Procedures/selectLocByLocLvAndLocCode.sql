﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectLocByLocLvAndLocCode]
	-- Add the parameters for the stored procedure here
	@keyLocLv varchar(50),
	@keyParentLocCode varchar(50),
	@keySortHeader AS varchar(50),
	@keySortContent AS varchar(50),
	@keyShowAll AS varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery AS NVARCHAR(1500)
	DECLARE @SQLCond AS NVARCHAR(50)

    -- Insert statements for procedure here
	IF @keyShowAll = 'false'
	SET @SQLCond = 'AND active = 1 '
	ELSE
	SET @SQLCond = ''

    -- Insert statements for procedure here
	SET @SQLQuery = ' 
		SELECT tbl_location.id
			,tbl_location.code
			,tbl_location.[description] 
			,tbl_location.active
		FROM tbl_location
		WHERE tbl_location.tbl_locationLevel_Id = ' + @keyLocLv + '
		AND tbl_location.tbl_locationParent_Id = ( SELECT tbl_location.id
											FROM tbl_location
											WHERE tbl_location.code = ''' + @keyParentLocCode + ''') ' + @SQLCond +
		'ORDER BY '+@keySortHeader+' '+@keySortContent+'
		'
	EXECUTE(@SQLQuery)
	
END