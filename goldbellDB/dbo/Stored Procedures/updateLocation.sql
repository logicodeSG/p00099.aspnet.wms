﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateLocation]
	-- Add the parameters for the stored procedure here
	@keyLocationId bigint,
	@keyLocationCode varchar(50),
	@keyLocationDesc varchar(50),
	@keyLocationActive bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tbl_location
	SET code = @keyLocationCode,
		[description] = @keyLocationDesc,
		active = @keyLocationActive
	WHERE id = @keyLocationId
END

