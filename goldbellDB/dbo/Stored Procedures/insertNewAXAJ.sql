﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewAXAJ]
	-- Add the parameters for the stored procedure here
	@keyAXAJ_Id bigint,
	@keyAXAJ_JOURNALID nvarchar(20),
	@keyAXAJ_DESCRIPTION nvarchar(60),
	@keyAXAJ_POSTED int,
	@keyAXAJ_JOURNALNAMEID nvarchar(20),
	@keyAXAJ_INVENTDIMID nvarchar(20),
	@keyAXAJ_PACKINGSLIP nvarchar(20),
	@keyAXAJ_CREATEDDATETIME datetime,
	@keyAXAJ_DATAAREAID nvarchar(4),
	@keyReferenceNo varchar(50),
	@keyCreationDatetime datetime,
	@keyScanDatetime datetime,
	@keyStatusId bigint,
	@keyDocType varchar(50),
	@keyReferenceVersion bigint,
	@keyUserId bigint,
	@keyDeviceSN varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.tbl_ax_aj(
	[id]
      ,[axJOURNALID]
      ,[axDESCRIPTION]
      ,[axPOSTED]
      ,[axJOURNALNAMEID]
      ,[axINVENTDIMID]
      ,[axPACKINGSLIP]
      ,[axCREATEDDATETIME]
      ,[axDATAAREAID]
      ,[referenceNo]
      ,[creationDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
	)
	values(
	@keyAXAJ_Id ,
	@keyAXAJ_JOURNALID ,
	@keyAXAJ_DESCRIPTION ,
	@keyAXAJ_POSTED ,
	@keyAXAJ_JOURNALNAMEID ,
	@keyAXAJ_INVENTDIMID ,
	@keyAXAJ_PACKINGSLIP ,
	@keyAXAJ_CREATEDDATETIME ,
	@keyAXAJ_DATAAREAID ,
	@keyReferenceNo ,
	@keyCreationDatetime ,
	@keyScanDatetime ,
	@keyStatusId ,
	@keyDocType,
	@keyReferenceVersion ,
	@keyUserId,
	@keyDeviceSN)
END