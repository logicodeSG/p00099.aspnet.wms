﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insertNewUser]
	-- Add the parameters for the stored procedure here
	@keyUserId Bigint,
	@keyUserCode varchar(50),
	@keyUserName varchar(50),
	@keyUserPass nvarchar(64),
	@keyUserLevelId Bigint,
	@keyUserActive bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tbl_user(id,code,username,[password],tbl_userLevel_Id,active)
	VALUES(@keyUserId
			,@keyUserCode
			,@keyUserName
			,@keyUserPass
			,@keyUserLevelId
			,@keyUserActive)
END