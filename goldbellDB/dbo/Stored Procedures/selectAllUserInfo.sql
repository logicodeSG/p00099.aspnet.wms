﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllUserInfo]
	-- Add the parameters for the stored procedure here
	--@keyUserId AS varchar(50),
	 @keySortHeader AS varchar(50),
	 @keySortContent AS varchar(50),
	 @keyShowAll AS varchar(50),
	 @keySearchHeader AS varchar(50),
	 @keySearchContent AS varchar(50)
	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQLQuery AS NVARCHAR(2000)
	DECLARE @SQLCond AS NVARCHAR(50)

    -- Insert statements for procedure here
	-- Insert statements for procedure here
	IF @keyShowAll = 'false'
	SET @SQLCond = 'AND tbl_user.active = 1 '
	ELSE
	SET @SQLCond = ''

	SET @SQLQuery = ' 
		SELECT tbl_user.id, tbl_user.code, tbl_user.username, tbl_user.[password], tbl_user.active, tbl_userLevel.[description]
		FROM tbl_user
		INNER JOIN tbl_userLevel 
		ON tbl_user.tbl_userLevel_Id = tbl_userLevel.id 
		WHERE tbl_userLevel.[order] > 1       
		AND '+@keySearchHeader+' LIKE ''%'+ @keySearchContent +'%'' ' 
		+ @SQLCond +
		'ORDER BY '+@keySortHeader+' '+@keySortContent
		
	EXECUTE(@SQLQuery)

END