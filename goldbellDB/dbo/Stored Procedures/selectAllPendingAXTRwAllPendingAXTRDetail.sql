﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectAllPendingAXTRwAllPendingAXTRDetail]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [id]
      ,[axDATAAREAID]
      ,[axMODIFIEDDATETIME]
      ,[axVOUCHERID]
      ,[axTRANSFERID]
      ,[axDocumentType]
      ,[axINVENTLOCATIONIDFROM]
      ,[axINVENTLOCATIONIDTO]
      ,[axTRANSFERSTATUS]
      ,[referenceNo]
      ,[creationDatetime]
      ,[completionDatetime]
      ,[scanDatetime]
      ,[tbl_status_Id]
      ,[docType]
      ,[referenceVersion]
      ,[tbl_user_Id]
      ,[deviceSN]
      ,[tbl_user_Username]
	FROM [tbl_ax_tr]
	WHERE tbl_status_Id = 1
	AND tbl_ax_tr.id NOT IN(
	SELECT DISTINCT tbl_ax_trDetail.tbl_ax_tr_Id
	FROM tbl_ax_trDetail
	WHERE tbl_ax_trDetail.tbl_status_Id <> 1)
	 
END