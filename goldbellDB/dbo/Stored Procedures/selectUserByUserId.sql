﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectUserByUserId]
	-- Add the parameters for the stored procedure here
	@keyUserId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tbl_user.id, tbl_user.code, tbl_user.username, tbl_user.[password], 
	tbl_user.tbl_userLevel_Id, tbl_user.active, tbl_user.lastSelected_location_Id,
	tbl_userLevel.id, tbl_userLevel.[order], tbl_userLevel.[description]
	FROM tbl_user
	INNER JOIN tbl_userLevel
	ON tbl_user.tbl_userLevel_Id = tbl_userLevel.id
	WHERE tbl_user.id = @keyUserId
END